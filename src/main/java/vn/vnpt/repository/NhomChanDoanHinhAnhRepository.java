package vn.vnpt.repository;

import vn.vnpt.domain.NhomChanDoanHinhAnh;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the NhomChanDoanHinhAnh entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NhomChanDoanHinhAnhRepository extends JpaRepository<NhomChanDoanHinhAnh, Long>, JpaSpecificationExecutor<NhomChanDoanHinhAnh> {
}
