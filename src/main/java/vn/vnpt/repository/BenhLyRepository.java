package vn.vnpt.repository;

import vn.vnpt.domain.BenhLy;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the BenhLy entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BenhLyRepository extends JpaRepository<BenhLy, Long>, JpaSpecificationExecutor<BenhLy> {
}
