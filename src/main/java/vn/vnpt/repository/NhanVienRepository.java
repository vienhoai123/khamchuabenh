package vn.vnpt.repository;

import vn.vnpt.domain.NhanVien;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the NhanVien entity.
 */
@Repository
public interface NhanVienRepository extends JpaRepository<NhanVien, Long>, JpaSpecificationExecutor<NhanVien> {

    @Query(value = "select distinct nhanVien from NhanVien nhanVien left join fetch nhanVien.chucVus",
        countQuery = "select count(distinct nhanVien) from NhanVien nhanVien")
    Page<NhanVien> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct nhanVien from NhanVien nhanVien left join fetch nhanVien.chucVus")
    List<NhanVien> findAllWithEagerRelationships();

    @Query("select nhanVien from NhanVien nhanVien left join fetch nhanVien.chucVus where nhanVien.id =:id")
    Optional<NhanVien> findOneWithEagerRelationships(@Param("id") Long id);
}
