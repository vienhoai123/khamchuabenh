package vn.vnpt.repository;

import vn.vnpt.domain.ThuThuatPhauThuatApDung;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ThuThuatPhauThuatApDung entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ThuThuatPhauThuatApDungRepository extends JpaRepository<ThuThuatPhauThuatApDung, Long>, JpaSpecificationExecutor<ThuThuatPhauThuatApDung> {
}
