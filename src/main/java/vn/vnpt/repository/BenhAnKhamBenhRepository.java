package vn.vnpt.repository;

import vn.vnpt.domain.BenhAnKhamBenh;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import vn.vnpt.domain.BenhAnKhamBenhId;

/**
 * Spring Data  repository for the BenhAnKhamBenh entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BenhAnKhamBenhRepository extends JpaRepository<BenhAnKhamBenh, BenhAnKhamBenhId>, JpaSpecificationExecutor<BenhAnKhamBenh> {
}
