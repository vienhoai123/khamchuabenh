package vn.vnpt.repository;

import vn.vnpt.domain.TBenhLyKhamBenh;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the TBenhLyKhamBenh entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TBenhLyKhamBenhRepository extends JpaRepository<TBenhLyKhamBenh, Long>, JpaSpecificationExecutor<TBenhLyKhamBenh> {
}
