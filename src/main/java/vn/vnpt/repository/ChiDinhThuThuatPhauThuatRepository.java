package vn.vnpt.repository;

import vn.vnpt.domain.ChiDinhTTPTId;
import vn.vnpt.domain.ChiDinhThuThuatPhauThuat;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ChiDinhThuThuatPhauThuat entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChiDinhThuThuatPhauThuatRepository extends JpaRepository<ChiDinhThuThuatPhauThuat, ChiDinhTTPTId>, JpaSpecificationExecutor<ChiDinhThuThuatPhauThuat> {
}
