package vn.vnpt.repository;

import vn.vnpt.domain.Menu;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Menu entity.
 */
@Repository
public interface MenuRepository extends JpaRepository<Menu, Long>, JpaSpecificationExecutor<Menu> {

    @Query(value = "select distinct menu from Menu menu left join fetch menu.users",
        countQuery = "select count(distinct menu) from Menu menu")
    Page<Menu> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct menu from Menu menu left join fetch menu.users")
    List<Menu> findAllWithEagerRelationships();

    @Query("select menu from Menu menu left join fetch menu.users where menu.id =:id")
    Optional<Menu> findOneWithEagerRelationships(@Param("id") Long id);
}
