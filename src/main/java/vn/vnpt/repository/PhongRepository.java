package vn.vnpt.repository;

import vn.vnpt.domain.Phong;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import vn.vnpt.service.dto.PhongDTO;

/**
 * Spring Data  repository for the Phong entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PhongRepository extends JpaRepository<Phong, Long>, JpaSpecificationExecutor<Phong> {
    PhongDTO findByKhoaId(Long khoaId);
}
