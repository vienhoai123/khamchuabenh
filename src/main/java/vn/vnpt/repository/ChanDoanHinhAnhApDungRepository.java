package vn.vnpt.repository;

import vn.vnpt.domain.ChanDoanHinhAnhApDung;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ChanDoanHinhAnhApDung entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChanDoanHinhAnhApDungRepository extends JpaRepository<ChanDoanHinhAnhApDung, Long>, JpaSpecificationExecutor<ChanDoanHinhAnhApDung> {
}
