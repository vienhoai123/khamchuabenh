package vn.vnpt.repository;

import vn.vnpt.domain.DichVuKham;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the DichVuKham entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DichVuKhamRepository extends JpaRepository<DichVuKham, Long>, JpaSpecificationExecutor<DichVuKham> {
}
