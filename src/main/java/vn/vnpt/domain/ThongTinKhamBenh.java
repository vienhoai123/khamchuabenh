package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.persistence.criteria.Join;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * A ThongTinKhamBenh.
 */
@Entity
@Table(name = "thong_tin_kham_benh")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@IdClass(ThongTinKhamBenhId.class)
public class ThongTinKhamBenh implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Chỉ số BMI
     */
    @Column(name = "bmi", precision = 21, scale = 2)
    private BigDecimal bmi;

    /**
     * Cân nặng
     */
    @Column(name = "can_nang", precision = 21, scale = 2)
    private BigDecimal canNang;

    /**
     * Chiều cao (tính bằng cm)
     */
    @Column(name = "chieu_cao")
    private Integer chieuCao;

    /**
     * Creatinin
     */
    @Column(name = "creatinin", precision = 21, scale = 2)
    private BigDecimal creatinin;

    /**
     * Dấu hiệu lâm sàng
     */
    @Size(max = 500)
    @Column(name = "dau_hieu_lam_sang", length = 500)
    private String dauHieuLamSang;

    /**
     * Độ thành thải Creatinin
     */
    @Column(name = "do_thanh_thai", precision = 21, scale = 2)
    private BigDecimal doThanhThai;

    /**
     * Huyết áp cao
     */
    @Column(name = "huyet_ap_cao")
    private Integer huyetApCao;

    /**
     * Huyết áp thấp
     */
    @Column(name = "huyet_ap_thap")
    private Integer huyetApThap;

    /**
     * ICD
     */
    @Size(max = 50)
    @Column(name = "icd", length = 50)
    private String icd;

    /**
     * Lần hẹn
     */
    @Column(name = "lan_hen")
    private Integer lanHen;

    /**
     * Lời dặn
     */
    @Size(max = 500)
    @Column(name = "loi_dan", length = 500)
    private String loiDan;

    /**
     * Lý do chuyển
     */
    @Size(max = 2000)
    @Column(name = "ly_do_chuyen", length = 2000)
    private String lyDoChuyen;

    /**
     * Mã bệnh y học cổ truyền
     */
    @Size(max = 100)
    @Column(name = "ma_benh_yhct", length = 100)
    private String maBenhYhct;

    /**
     * Mạch
     */
    @Column(name = "mach")
    private Integer mach;

    /**
     * Ngày ra toa tối đa
     */
    @Column(name = "max_ngay_ra_toa")
    private Integer maxNgayRaToa;

    /**
     * Ngày hẹn
     */
    @Column(name = "ngay_hen")
    private LocalDate ngayHen;

    /**
     * Nhận định BMI
     */
    @Size(max = 500)
    @Column(name = "nhan_dinh_bmi", length = 500)
    private String nhanDinhBmi;

    /**
     * Nhận định độ thanh thải
     */
    @Size(max = 500)
    @Column(name = "nhan_dinh_do_thanh_thai", length = 500)
    private String nhanDinhDoThanhThai;

    /**
     * Trận thái nhập viện. 0: Không nhập viện. 1: Nhập viện
     */
    @NotNull
    @Column(name = "nhap_vien", nullable = false)
    private Boolean nhapVien;

    /**
     * Nhiệt độ
     */
    @Column(name = "nhiet_do", precision = 21, scale = 2)
    private BigDecimal nhietDo;

    /**
     * Nhiệp thở
     */
    @Column(name = "nhip_tho")
    private Integer nhipTho;

    /**
     * Phương pháp điều trị y học cổ truyền
     */
    @Size(max = 2000)
    @Column(name = "pp_dieu_tri_ytct", length = 2000)
    private String ppDieuTriYtct;

    /**
     * Số lần in bảng kê
     */
    @Column(name = "so_lan_in_bang_ke")
    private Integer soLanInBangKe;

    /**
     * Số lần in toa thuốc
     */
    @Column(name = "so_lan_in_toa_thuoc")
    private Integer soLanInToaThuoc;

    /**
     * Tên bệnh theo icd
     */
    @Size(max = 500)
    @Column(name = "ten_benh_icd", length = 500)
    private String tenBenhIcd;

    /**
     * Tên bệnh theo bác sĩ
     */
    @Size(max = 500)
    @Column(name = "ten_benh_theo_bac_si", length = 500)
    private String tenBenhTheoBacSi;

    /**
     * Tên bệnh y học cổ truyền
     */
    @Size(max = 500)
    @Column(name = "ten_benh_yhct", length = 500)
    private String tenBenhYhct;

    /**
     * Trạng thái đã chẩn đoán hình ảnh: 0: Chưa thực hiện đủ: 1: Đã thực hiện đủ
     */
    @NotNull
    @Column(name = "trang_thai_cdha", nullable = false)
    private Boolean trangThaiCdha;

    /**
     * Trạng thái đã thực hiện ttpt: 0: Chưa thực hiện đủ: 1: Đã thực hiện đủ
     */
    @NotNull
    @Column(name = "trang_thai_ttpt", nullable = false)
    private Boolean trangThaiTtpt;

    /**
     * Trạng thái đã xét nghiệm: 0: Chưa thực hiện đủ: 1: Đã thực hiện đủ
     */
    @NotNull
    @Column(name = "trang_thai_xet_nghiem", nullable = false)
    private Boolean trangThaiXetNghiem;

    /**
     * Triệu chứng lâm sàng
     */
    @Size(max = 255)
    @Column(name = "trieu_chung_lam_sang", length = 255)
    private String trieuChungLamSang;

    /**
     * Vòng bụng
     */
    @Column(name = "vong_bung")
    private Integer vongBung;

    /**
     * Trạng thái lần khám bệnh; 1 chờ khám; 2 đang khám, 3 đã khám, 4 kết thúc bắt buộc, 5 chuyển phòng khám, 6 chuyển viện, 7 nhập viện nội trú từ ngoại trú
     */
    @Column(name = "trang_thai")
    private Integer trangThai;

    /**
     * Thời gian kết thúc khám
     */
    @Column(name = "thoi_gian_ket_thuc_kham")
    private LocalDate thoiGianKetThucKham;

    /**
     * Mã phòng chuyển từ
     */
    @Column(name = "phong_id_chuyen_tu")
    private Long phongIdChuyenTu;

    /**
     * Thời gian khám bệnh
     */
    @Column(name = "thoi_gian_kham_benh")
    private LocalDate thoiGianKhamBenh;

    /**
     * Nội dung y lệnh
     */
    @Size(max = 255)
    @Column(name = "y_lenh", length = 255)
    private String yLenh;

    /**
     * Loại y lệnh: 0: Bình thường. 1: Tây y: 2 Đông Y
     */
    @Column(name = "loai_y_lenh")
    private Integer loaiYLenh;

    /**
     * Chẩn đoán
     */
    @Size(max = 255)
    @Column(name = "chan_doan", length = 255)
    private String chanDoan;

    /**
     * Loại chẩn đoán: 0: Bình thường. 1: Tây y: 2 Đông Y
     */
    @Column(name = "loai_chan_doan")
    private Integer loaiChanDoan;

    @NotNull
    @Column(name = "nam", nullable = false)
    private Integer nam;

//    @ManyToOne(optional = false)
//    @NotNull
//    @JsonIgnoreProperties(value = "thongTinKhamBenhs", allowSetters = true)
//    private ThongTinKhoa bakb;
//
//    @ManyToOne(optional = false)
//    @NotNull
//    @JsonIgnoreProperties(value = "thongTinKhamBenhs", allowSetters = true)
//    private ThongTinKhoa donVi;
//
//    @ManyToOne(optional = false)
//    @NotNull
//    @JsonIgnoreProperties(value = "thongTinKhamBenhs", allowSetters = true)
//    private ThongTinKhoa benhNhan;
//
//    @ManyToOne(optional = false)
//    @NotNull
//    @JsonIgnoreProperties(value = "thongTinKhamBenhs", allowSetters = true)
//    private ThongTinKhoa dotDieuTri;
    @Id
    @Column(name = "bakb_id")
    private Long bakbId;

    @Id
    @Column(name = "benh_nhan_id")
    private Long benhNhanId;

    @Id
    @Column(name = "don_vi_id")
    private Long donViId;

    @Id
    @Column(name = "dot_dieu_tri_id")
    private Long dotDieuTriId;

    @Id
    @Column(name = "thong_tin_khoa_id")
    private Long thongTinKhoaId;

    @ManyToOne(optional = false)
    @JsonIgnoreProperties(value = "thongTinKhamBenhs", allowSetters = true)
    @JoinColumns({
        @JoinColumn(name = "bakb_id", referencedColumnName = "bakb_id",insertable = false, updatable = false),
        @JoinColumn(name = "benh_nhan_id", referencedColumnName = "benh_nhan_id", insertable = false, updatable = false),
        @JoinColumn(name = "don_vi_id", referencedColumnName = "don_vi_id", insertable = false,updatable = false),
        @JoinColumn(name = "dot_dieu_tri_id", referencedColumnName = "dot_dieu_tri_id", insertable = false,updatable = false),
        @JoinColumn(name = "thong_tin_khoa_id", referencedColumnName = "id", insertable = false,updatable = false),
    })
    private ThongTinKhoa thongTinKhoa;

    /**
     * Mã nhân viên
     */
    @ManyToOne
    @JsonIgnoreProperties(value = "thongTinKhamBenhs", allowSetters = true)
    private NhanVien nhanVien;

    /**
     * Mã hướng điều trị
     */
    @ManyToOne
    @JsonIgnoreProperties(value = "thongTinKhamBenhs", allowSetters = true)
    private HuongDieuTri huongDieuTri;

    /**
     * Mã phòng
     */
    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "thongTinKhamBenhs", allowSetters = true)
    private Phong phong;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getBmi() {
        return bmi;
    }

    public ThongTinKhamBenh bmi(BigDecimal bmi) {
        this.bmi = bmi;
        return this;
    }

    public void setBmi(BigDecimal bmi) {
        this.bmi = bmi;
    }

    public BigDecimal getCanNang() {
        return canNang;
    }

    public ThongTinKhamBenh canNang(BigDecimal canNang) {
        this.canNang = canNang;
        return this;
    }

    public void setCanNang(BigDecimal canNang) {
        this.canNang = canNang;
    }

    public Integer getChieuCao() {
        return chieuCao;
    }

    public ThongTinKhamBenh chieuCao(Integer chieuCao) {
        this.chieuCao = chieuCao;
        return this;
    }

    public void setChieuCao(Integer chieuCao) {
        this.chieuCao = chieuCao;
    }

    public BigDecimal getCreatinin() {
        return creatinin;
    }

    public ThongTinKhamBenh creatinin(BigDecimal creatinin) {
        this.creatinin = creatinin;
        return this;
    }

    public void setCreatinin(BigDecimal creatinin) {
        this.creatinin = creatinin;
    }

    public String getDauHieuLamSang() {
        return dauHieuLamSang;
    }

    public ThongTinKhamBenh dauHieuLamSang(String dauHieuLamSang) {
        this.dauHieuLamSang = dauHieuLamSang;
        return this;
    }

    public void setDauHieuLamSang(String dauHieuLamSang) {
        this.dauHieuLamSang = dauHieuLamSang;
    }

    public BigDecimal getDoThanhThai() {
        return doThanhThai;
    }

    public ThongTinKhamBenh doThanhThai(BigDecimal doThanhThai) {
        this.doThanhThai = doThanhThai;
        return this;
    }

    public void setDoThanhThai(BigDecimal doThanhThai) {
        this.doThanhThai = doThanhThai;
    }

    public Integer getHuyetApCao() {
        return huyetApCao;
    }

    public ThongTinKhamBenh huyetApCao(Integer huyetApCao) {
        this.huyetApCao = huyetApCao;
        return this;
    }

    public void setHuyetApCao(Integer huyetApCao) {
        this.huyetApCao = huyetApCao;
    }

    public Integer getHuyetApThap() {
        return huyetApThap;
    }

    public ThongTinKhamBenh huyetApThap(Integer huyetApThap) {
        this.huyetApThap = huyetApThap;
        return this;
    }

    public void setHuyetApThap(Integer huyetApThap) {
        this.huyetApThap = huyetApThap;
    }

    public String getIcd() {
        return icd;
    }

    public ThongTinKhamBenh icd(String icd) {
        this.icd = icd;
        return this;
    }

    public void setIcd(String icd) {
        this.icd = icd;
    }

    public Integer getLanHen() {
        return lanHen;
    }

    public ThongTinKhamBenh lanHen(Integer lanHen) {
        this.lanHen = lanHen;
        return this;
    }

    public void setLanHen(Integer lanHen) {
        this.lanHen = lanHen;
    }

    public String getLoiDan() {
        return loiDan;
    }

    public ThongTinKhamBenh loiDan(String loiDan) {
        this.loiDan = loiDan;
        return this;
    }

    public void setLoiDan(String loiDan) {
        this.loiDan = loiDan;
    }

    public String getLyDoChuyen() {
        return lyDoChuyen;
    }

    public ThongTinKhamBenh lyDoChuyen(String lyDoChuyen) {
        this.lyDoChuyen = lyDoChuyen;
        return this;
    }

    public void setLyDoChuyen(String lyDoChuyen) {
        this.lyDoChuyen = lyDoChuyen;
    }

    public String getMaBenhYhct() {
        return maBenhYhct;
    }

    public ThongTinKhamBenh maBenhYhct(String maBenhYhct) {
        this.maBenhYhct = maBenhYhct;
        return this;
    }

    public void setMaBenhYhct(String maBenhYhct) {
        this.maBenhYhct = maBenhYhct;
    }

    public Integer getMach() {
        return mach;
    }

    public ThongTinKhamBenh mach(Integer mach) {
        this.mach = mach;
        return this;
    }

    public void setMach(Integer mach) {
        this.mach = mach;
    }

    public Integer getMaxNgayRaToa() {
        return maxNgayRaToa;
    }

    public ThongTinKhamBenh maxNgayRaToa(Integer maxNgayRaToa) {
        this.maxNgayRaToa = maxNgayRaToa;
        return this;
    }

    public void setMaxNgayRaToa(Integer maxNgayRaToa) {
        this.maxNgayRaToa = maxNgayRaToa;
    }

    public LocalDate getNgayHen() {
        return ngayHen;
    }

    public ThongTinKhamBenh ngayHen(LocalDate ngayHen) {
        this.ngayHen = ngayHen;
        return this;
    }

    public void setNgayHen(LocalDate ngayHen) {
        this.ngayHen = ngayHen;
    }

    public String getNhanDinhBmi() {
        return nhanDinhBmi;
    }

    public ThongTinKhamBenh nhanDinhBmi(String nhanDinhBmi) {
        this.nhanDinhBmi = nhanDinhBmi;
        return this;
    }

    public void setNhanDinhBmi(String nhanDinhBmi) {
        this.nhanDinhBmi = nhanDinhBmi;
    }

    public String getNhanDinhDoThanhThai() {
        return nhanDinhDoThanhThai;
    }

    public ThongTinKhamBenh nhanDinhDoThanhThai(String nhanDinhDoThanhThai) {
        this.nhanDinhDoThanhThai = nhanDinhDoThanhThai;
        return this;
    }

    public void setNhanDinhDoThanhThai(String nhanDinhDoThanhThai) {
        this.nhanDinhDoThanhThai = nhanDinhDoThanhThai;
    }

    public Boolean isNhapVien() {
        return nhapVien;
    }

    public ThongTinKhamBenh nhapVien(Boolean nhapVien) {
        this.nhapVien = nhapVien;
        return this;
    }

    public void setNhapVien(Boolean nhapVien) {
        this.nhapVien = nhapVien;
    }

    public BigDecimal getNhietDo() {
        return nhietDo;
    }

    public ThongTinKhamBenh nhietDo(BigDecimal nhietDo) {
        this.nhietDo = nhietDo;
        return this;
    }

    public void setNhietDo(BigDecimal nhietDo) {
        this.nhietDo = nhietDo;
    }

    public Integer getNhipTho() {
        return nhipTho;
    }

    public ThongTinKhamBenh nhipTho(Integer nhipTho) {
        this.nhipTho = nhipTho;
        return this;
    }

    public void setNhipTho(Integer nhipTho) {
        this.nhipTho = nhipTho;
    }

    public String getPpDieuTriYtct() {
        return ppDieuTriYtct;
    }

    public ThongTinKhamBenh ppDieuTriYtct(String ppDieuTriYtct) {
        this.ppDieuTriYtct = ppDieuTriYtct;
        return this;
    }

    public void setPpDieuTriYtct(String ppDieuTriYtct) {
        this.ppDieuTriYtct = ppDieuTriYtct;
    }

    public Integer getSoLanInBangKe() {
        return soLanInBangKe;
    }

    public ThongTinKhamBenh soLanInBangKe(Integer soLanInBangKe) {
        this.soLanInBangKe = soLanInBangKe;
        return this;
    }

    public void setSoLanInBangKe(Integer soLanInBangKe) {
        this.soLanInBangKe = soLanInBangKe;
    }

    public Integer getSoLanInToaThuoc() {
        return soLanInToaThuoc;
    }

    public ThongTinKhamBenh soLanInToaThuoc(Integer soLanInToaThuoc) {
        this.soLanInToaThuoc = soLanInToaThuoc;
        return this;
    }

    public void setSoLanInToaThuoc(Integer soLanInToaThuoc) {
        this.soLanInToaThuoc = soLanInToaThuoc;
    }

    public String getTenBenhIcd() {
        return tenBenhIcd;
    }

    public ThongTinKhamBenh tenBenhIcd(String tenBenhIcd) {
        this.tenBenhIcd = tenBenhIcd;
        return this;
    }

    public void setTenBenhIcd(String tenBenhIcd) {
        this.tenBenhIcd = tenBenhIcd;
    }

    public String getTenBenhTheoBacSi() {
        return tenBenhTheoBacSi;
    }

    public ThongTinKhamBenh tenBenhTheoBacSi(String tenBenhTheoBacSi) {
        this.tenBenhTheoBacSi = tenBenhTheoBacSi;
        return this;
    }

    public void setTenBenhTheoBacSi(String tenBenhTheoBacSi) {
        this.tenBenhTheoBacSi = tenBenhTheoBacSi;
    }

    public String getTenBenhYhct() {
        return tenBenhYhct;
    }

    public ThongTinKhamBenh tenBenhYhct(String tenBenhYhct) {
        this.tenBenhYhct = tenBenhYhct;
        return this;
    }

    public void setTenBenhYhct(String tenBenhYhct) {
        this.tenBenhYhct = tenBenhYhct;
    }

    public Boolean isTrangThaiCdha() {
        return trangThaiCdha;
    }

    public ThongTinKhamBenh trangThaiCdha(Boolean trangThaiCdha) {
        this.trangThaiCdha = trangThaiCdha;
        return this;
    }

    public void setTrangThaiCdha(Boolean trangThaiCdha) {
        this.trangThaiCdha = trangThaiCdha;
    }

    public Boolean isTrangThaiTtpt() {
        return trangThaiTtpt;
    }

    public ThongTinKhamBenh trangThaiTtpt(Boolean trangThaiTtpt) {
        this.trangThaiTtpt = trangThaiTtpt;
        return this;
    }

    public void setTrangThaiTtpt(Boolean trangThaiTtpt) {
        this.trangThaiTtpt = trangThaiTtpt;
    }

    public Boolean isTrangThaiXetNghiem() {
        return trangThaiXetNghiem;
    }

    public ThongTinKhamBenh trangThaiXetNghiem(Boolean trangThaiXetNghiem) {
        this.trangThaiXetNghiem = trangThaiXetNghiem;
        return this;
    }

    public void setTrangThaiXetNghiem(Boolean trangThaiXetNghiem) {
        this.trangThaiXetNghiem = trangThaiXetNghiem;
    }

    public String getTrieuChungLamSang() {
        return trieuChungLamSang;
    }

    public ThongTinKhamBenh trieuChungLamSang(String trieuChungLamSang) {
        this.trieuChungLamSang = trieuChungLamSang;
        return this;
    }

    public void setTrieuChungLamSang(String trieuChungLamSang) {
        this.trieuChungLamSang = trieuChungLamSang;
    }

    public Integer getVongBung() {
        return vongBung;
    }

    public ThongTinKhamBenh vongBung(Integer vongBung) {
        this.vongBung = vongBung;
        return this;
    }

    public void setVongBung(Integer vongBung) {
        this.vongBung = vongBung;
    }

    public Integer getTrangThai() {
        return trangThai;
    }

    public ThongTinKhamBenh trangThai(Integer trangThai) {
        this.trangThai = trangThai;
        return this;
    }

    public void setTrangThai(Integer trangThai) {
        this.trangThai = trangThai;
    }

    public LocalDate getThoiGianKetThucKham() {
        return thoiGianKetThucKham;
    }

    public ThongTinKhamBenh thoiGianKetThucKham(LocalDate thoiGianKetThucKham) {
        this.thoiGianKetThucKham = thoiGianKetThucKham;
        return this;
    }

    public void setThoiGianKetThucKham(LocalDate thoiGianKetThucKham) {
        this.thoiGianKetThucKham = thoiGianKetThucKham;
    }

    public Long getPhongIdChuyenTu() {
        return phongIdChuyenTu;
    }

    public ThongTinKhamBenh phongIdChuyenTu(Long phongIdChuyenTu) {
        this.phongIdChuyenTu = phongIdChuyenTu;
        return this;
    }

    public void setPhongIdChuyenTu(Long phongIdChuyenTu) {
        this.phongIdChuyenTu = phongIdChuyenTu;
    }

    public LocalDate getThoiGianKhamBenh() {
        return thoiGianKhamBenh;
    }

    public ThongTinKhamBenh thoiGianKhamBenh(LocalDate thoiGianKhamBenh) {
        this.thoiGianKhamBenh = thoiGianKhamBenh;
        return this;
    }

    public void setThoiGianKhamBenh(LocalDate thoiGianKhamBenh) {
        this.thoiGianKhamBenh = thoiGianKhamBenh;
    }

    public String getyLenh() {
        return yLenh;
    }

    public ThongTinKhamBenh yLenh(String yLenh) {
        this.yLenh = yLenh;
        return this;
    }

    public void setyLenh(String yLenh) {
        this.yLenh = yLenh;
    }

    public Integer getLoaiYLenh() {
        return loaiYLenh;
    }

    public ThongTinKhamBenh loaiYLenh(Integer loaiYLenh) {
        this.loaiYLenh = loaiYLenh;
        return this;
    }

    public void setLoaiYLenh(Integer loaiYLenh) {
        this.loaiYLenh = loaiYLenh;
    }

    public String getChanDoan() {
        return chanDoan;
    }

    public ThongTinKhamBenh chanDoan(String chanDoan) {
        this.chanDoan = chanDoan;
        return this;
    }

    public void setChanDoan(String chanDoan) {
        this.chanDoan = chanDoan;
    }

    public Integer getLoaiChanDoan() {
        return loaiChanDoan;
    }

    public ThongTinKhamBenh loaiChanDoan(Integer loaiChanDoan) {
        this.loaiChanDoan = loaiChanDoan;
        return this;
    }

    public void setLoaiChanDoan(Integer loaiChanDoan) {
        this.loaiChanDoan = loaiChanDoan;
    }

    public Integer getNam() {
        return nam;
    }

    public ThongTinKhamBenh nam(Integer nam) {
        this.nam = nam;
        return this;
    }

    public void setNam(Integer nam) {
        this.nam = nam;
    }

//    public ThongTinKhoa getBakb() {
//        return bakb;
//    }
//
//    public ThongTinKhamBenh bakb(ThongTinKhoa thongTinKhoa) {
//        this.bakb = thongTinKhoa;
//        return this;
//    }
//
//    public void setBakb(ThongTinKhoa thongTinKhoa) {
//        this.bakb = thongTinKhoa;
//    }
//
//    public ThongTinKhoa getDonVi() {
//        return donVi;
//    }
//
//    public ThongTinKhamBenh donVi(ThongTinKhoa thongTinKhoa) {
//        this.donVi = thongTinKhoa;
//        return this;
//    }
//
//    public void setDonVi(ThongTinKhoa thongTinKhoa) {
//        this.donVi = thongTinKhoa;
//    }
//
//    public ThongTinKhoa getBenhNhan() {
//        return benhNhan;
//    }
//
//    public ThongTinKhamBenh benhNhan(ThongTinKhoa thongTinKhoa) {
//        this.benhNhan = thongTinKhoa;
//        return this;
//    }
//
//    public void setBenhNhan(ThongTinKhoa thongTinKhoa) {
//        this.benhNhan = thongTinKhoa;
//    }
//
//    public ThongTinKhoa getDotDieuTri() {
//        return dotDieuTri;
//    }
//
//    public ThongTinKhamBenh dotDieuTri(ThongTinKhoa thongTinKhoa) {
//        this.dotDieuTri = thongTinKhoa;
//        return this;
//    }
//
//    public void setDotDieuTri(ThongTinKhoa thongTinKhoa) {
//        this.dotDieuTri = thongTinKhoa;
//    }

    public ThongTinKhoa getThongTinKhoa() {
        return thongTinKhoa;
    }

    public ThongTinKhamBenh thongTinKhoa(ThongTinKhoa thongTinKhoa) {
        this.thongTinKhoa = thongTinKhoa;
        return this;
    }

    public void setThongTinKhoa(ThongTinKhoa thongTinKhoa) {
        this.thongTinKhoa = thongTinKhoa;
    }

    public NhanVien getNhanVien() {
        return nhanVien;
    }

    public ThongTinKhamBenh nhanVien(NhanVien nhanVien) {
        this.nhanVien = nhanVien;
        return this;
    }

    public void setNhanVien(NhanVien nhanVien) {
        this.nhanVien = nhanVien;
    }

    public HuongDieuTri getHuongDieuTri() {
        return huongDieuTri;
    }

    public ThongTinKhamBenh huongDieuTri(HuongDieuTri huongDieuTri) {
        this.huongDieuTri = huongDieuTri;
        return this;
    }

    public void setHuongDieuTri(HuongDieuTri huongDieuTri) {
        this.huongDieuTri = huongDieuTri;
    }

    public Phong getPhong() {
        return phong;
    }

    public ThongTinKhamBenh phong(Phong phong) {
        this.phong = phong;
        return this;
    }

    public void setPhong(Phong phong) {
        this.phong = phong;
    }

    public Boolean getNhapVien() {
        return nhapVien;
    }

    public Boolean getTrangThaiCdha() {
        return trangThaiCdha;
    }

    public Boolean getTrangThaiTtpt() {
        return trangThaiTtpt;
    }

    public Boolean getTrangThaiXetNghiem() {
        return trangThaiXetNghiem;
    }

    public Long getBakbId() {
        return bakbId;
    }

    public void setBakbId(Long bakbId) {
        this.bakbId = bakbId;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    public Long getDotDieuTriId() {
        return dotDieuTriId;
    }

    public void setDotDieuTriId(Long dotDieuTriId) {
        this.dotDieuTriId = dotDieuTriId;
    }

    public Long getThongTinKhoaId() {
        return thongTinKhoaId;
    }

    public void setThongTinKhoaId(Long thongTinKhoaId) {
        this.thongTinKhoaId = thongTinKhoaId;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ThongTinKhamBenh)) {
            return false;
        }
        return id != null && id.equals(((ThongTinKhamBenh) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ThongTinKhamBenh{" +
            "id=" + getId() +
            ", bmi=" + getBmi() +
            ", canNang=" + getCanNang() +
            ", chieuCao=" + getChieuCao() +
            ", creatinin=" + getCreatinin() +
            ", dauHieuLamSang='" + getDauHieuLamSang() + "'" +
            ", doThanhThai=" + getDoThanhThai() +
            ", huyetApCao=" + getHuyetApCao() +
            ", huyetApThap=" + getHuyetApThap() +
            ", icd='" + getIcd() + "'" +
            ", lanHen=" + getLanHen() +
            ", loiDan='" + getLoiDan() + "'" +
            ", lyDoChuyen='" + getLyDoChuyen() + "'" +
            ", maBenhYhct='" + getMaBenhYhct() + "'" +
            ", mach=" + getMach() +
            ", maxNgayRaToa=" + getMaxNgayRaToa() +
            ", ngayHen='" + getNgayHen() + "'" +
            ", nhanDinhBmi='" + getNhanDinhBmi() + "'" +
            ", nhanDinhDoThanhThai='" + getNhanDinhDoThanhThai() + "'" +
            ", nhapVien='" + isNhapVien() + "'" +
            ", nhietDo=" + getNhietDo() +
            ", nhipTho=" + getNhipTho() +
            ", ppDieuTriYtct='" + getPpDieuTriYtct() + "'" +
            ", soLanInBangKe=" + getSoLanInBangKe() +
            ", soLanInToaThuoc=" + getSoLanInToaThuoc() +
            ", tenBenhIcd='" + getTenBenhIcd() + "'" +
            ", tenBenhTheoBacSi='" + getTenBenhTheoBacSi() + "'" +
            ", tenBenhYhct='" + getTenBenhYhct() + "'" +
            ", trangThaiCdha='" + isTrangThaiCdha() + "'" +
            ", trangThaiTtpt='" + isTrangThaiTtpt() + "'" +
            ", trangThaiXetNghiem='" + isTrangThaiXetNghiem() + "'" +
            ", trieuChungLamSang='" + getTrieuChungLamSang() + "'" +
            ", vongBung=" + getVongBung() +
            ", trangThai=" + getTrangThai() +
            ", thoiGianKetThucKham='" + getThoiGianKetThucKham() + "'" +
            ", phongIdChuyenTu=" + getPhongIdChuyenTu() +
            ", thoiGianKhamBenh='" + getThoiGianKhamBenh() + "'" +
            ", yLenh='" + getyLenh() + "'" +
            ", loaiYLenh=" + getLoaiYLenh() +
            ", chanDoan='" + getChanDoan() + "'" +
            ", loaiChanDoan=" + getLoaiChanDoan() +
            ", nam=" + getNam() +
            "}";
    }
}
