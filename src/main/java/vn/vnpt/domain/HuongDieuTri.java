package vn.vnpt.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A HuongDieuTri.
 */
@Entity
@Table(name = "huong_dieu_tri")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class HuongDieuTri implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Trạng thái hướng điều trị: 0: Cả nội và ngoại trú. 1: Ngoại trú, 2: Nội trú
     */
    @NotNull
    @Column(name = "noi_tru", nullable = false)
    private Integer noiTru;

    /**
     * Tên hướng điều trị
     */
    @Size(max = 200)
    @Column(name = "ten", length = 200)
    private String ten;

    /**
     * Mã hướng điều trị
     */
    @NotNull
    @Column(name = "ma", nullable = false)
    private Integer ma;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNoiTru() {
        return noiTru;
    }

    public HuongDieuTri noiTru(Integer noiTru) {
        this.noiTru = noiTru;
        return this;
    }

    public void setNoiTru(Integer noiTru) {
        this.noiTru = noiTru;
    }

    public String getTen() {
        return ten;
    }

    public HuongDieuTri ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Integer getMa() {
        return ma;
    }

    public HuongDieuTri ma(Integer ma) {
        this.ma = ma;
        return this;
    }

    public void setMa(Integer ma) {
        this.ma = ma;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HuongDieuTri)) {
            return false;
        }
        return id != null && id.equals(((HuongDieuTri) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HuongDieuTri{" +
            "id=" + getId() +
            ", noiTru=" + getNoiTru() +
            ", ten='" + getTen() + "'" +
            ", ma=" + getMa() +
            "}";
    }
}
