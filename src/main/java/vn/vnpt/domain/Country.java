package vn.vnpt.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * NDB_TABLE=READ_BACKUP=1 Thông tin các nước trên thế giới
 */
@Entity
@Table(name = "country")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Country implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Mã 2 ký tự của quốc gia theo ISO
     */
    @Size(max = 255)
    @Column(name = "iso_code", length = 255)
    private String isoCode;

    /**
     * Mã theo số của quốc gia theo ISO
     */
    @Size(max = 255)
    @Column(name = "iso_numeric", length = 255)
    private String isoNumeric;

    /**
     * Tên quốc gia
     */
    @Size(max = 255)
    @Column(name = "name", length = 255)
    private String name;

    /**
     * Mã cục thống kê
     */
    @Column(name = "ma_ctk")
    private Integer maCtk;

    /**
     * Tên cục thống kê
     */
    @Size(max = 255)
    @Column(name = "ten_ctk", length = 255)
    private String tenCtk;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public Country isoCode(String isoCode) {
        this.isoCode = isoCode;
        return this;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public String getIsoNumeric() {
        return isoNumeric;
    }

    public Country isoNumeric(String isoNumeric) {
        this.isoNumeric = isoNumeric;
        return this;
    }

    public void setIsoNumeric(String isoNumeric) {
        this.isoNumeric = isoNumeric;
    }

    public String getName() {
        return name;
    }

    public Country name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMaCtk() {
        return maCtk;
    }

    public Country maCtk(Integer maCtk) {
        this.maCtk = maCtk;
        return this;
    }

    public void setMaCtk(Integer maCtk) {
        this.maCtk = maCtk;
    }

    public String getTenCtk() {
        return tenCtk;
    }

    public Country tenCtk(String tenCtk) {
        this.tenCtk = tenCtk;
        return this;
    }

    public void setTenCtk(String tenCtk) {
        this.tenCtk = tenCtk;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Country)) {
            return false;
        }
        return id != null && id.equals(((Country) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Country{" +
            "id=" + getId() +
            ", isoCode='" + getIsoCode() + "'" +
            ", isoNumeric='" + getIsoNumeric() + "'" +
            ", name='" + getName() + "'" +
            ", maCtk=" + getMaCtk() +
            ", tenCtk='" + getTenCtk() + "'" +
            "}";
    }
}
