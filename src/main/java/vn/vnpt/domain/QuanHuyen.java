package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A QuanHuyen.
 */
@Entity
@Table(name = "quan_huyen")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class QuanHuyen implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Cấp
     */
    @NotNull
    @Size(max = 255)
    @Column(name = "cap", length = 255, nullable = false)
    private String cap;

    /**
     * Đoạn cụm từ gợi ý
     */
    @Size(max = 255)
    @Column(name = "guess_phrase", length = 255)
    private String guessPhrase;

    /**
     * Tên quận huyện
     */
    @NotNull
    @Size(max = 255)
    @Column(name = "ten", length = 255, nullable = false)
    private String ten;

    /**
     * Tên không dấu
     */
    @NotNull
    @Size(max = 255)
    @Column(name = "ten_khong_dau", length = 255, nullable = false)
    private String tenKhongDau;

    /**
     * Mã tỉnh thành phố
     */
    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("quanHuyens")
    private TinhThanhPho tinhThanhPho;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCap() {
        return cap;
    }

    public QuanHuyen cap(String cap) {
        this.cap = cap;
        return this;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    public String getGuessPhrase() {
        return guessPhrase;
    }

    public QuanHuyen guessPhrase(String guessPhrase) {
        this.guessPhrase = guessPhrase;
        return this;
    }

    public void setGuessPhrase(String guessPhrase) {
        this.guessPhrase = guessPhrase;
    }

    public String getTen() {
        return ten;
    }

    public QuanHuyen ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getTenKhongDau() {
        return tenKhongDau;
    }

    public QuanHuyen tenKhongDau(String tenKhongDau) {
        this.tenKhongDau = tenKhongDau;
        return this;
    }

    public void setTenKhongDau(String tenKhongDau) {
        this.tenKhongDau = tenKhongDau;
    }

    public TinhThanhPho getTinhThanhPho() {
        return tinhThanhPho;
    }

    public QuanHuyen tinhThanhPho(TinhThanhPho tinhThanhPho) {
        this.tinhThanhPho = tinhThanhPho;
        return this;
    }

    public void setTinhThanhPho(TinhThanhPho tinhThanhPho) {
        this.tinhThanhPho = tinhThanhPho;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof QuanHuyen)) {
            return false;
        }
        return id != null && id.equals(((QuanHuyen) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "QuanHuyen{" +
            "id=" + getId() +
            ", cap='" + getCap() + "'" +
            ", guessPhrase='" + getGuessPhrase() + "'" +
            ", ten='" + getTen() + "'" +
            ", tenKhongDau='" + getTenKhongDau() + "'" +
            "}";
    }
}
