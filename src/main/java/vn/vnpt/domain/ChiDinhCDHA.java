package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * A ChiDinhCDHA.
 */
@Entity
@Table(name = "chi_dinh_chan_doan_anh")
@IdClass(ChiDinhCDHAId.class)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ChiDinhCDHA implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Id
    @Column(name = "bakb_id")
    private Long bakbId;

    @Id
    @Column(name = "benh_nhan_id")
    private Long benhNhanId;

    @Id
    @Column(name = "don_vi_id")
    private Long donViId;

    @Id
    @NotNull
    @Column(name = "phieu_cd_id")
    private Long phieuCDId;

    /**
     * Trạng thái được thanh toán bảo hiểm của chỉ định: \r\n1: Có bảo hiểm. \r\n0: Không bảo hiểm. \r\n
     */
    @NotNull
    @Column(name = "co_bao_hiem", nullable = false)
    private Boolean coBaoHiem;

    /**
     * Trạng thái có kết quả của chỉ định: \r\n1: Có kết quả. \r\n0: Chưa có kết quả. \r\n
     */
    @NotNull
    @Column(name = "co_ket_qua", nullable = false)
    private Boolean coKetQua;

    /**
     * Trạng thái đã thanh toán của chỉ định: \r\n0: Chưa thanh toán. \r\n1: Đã thanh toán. \r\n-1: Đã hủy thanh toán
     */
    @NotNull
    @Column(name = "da_thanh_toan", nullable = false)
    private Integer daThanhToan;

    /**
     * Trạng thái đã thanh toán chênh lệch của chỉ định: \r\n0: Chưa thanh toán. \r\n1: Đã thanh toán. \r\n-1: Đã hủy thanh toán
     */
    @NotNull
    @Column(name = "da_thanh_toan_chenh_lech", nullable = false)
    private Integer daThanhToanChenhLech;

    /**
     * Trạng thái đã thực hiện của chỉ định: \r\n0: Chưa thực hiện. \r\n1: Đã thực hiện. \r\n-1: Đã hủy thực hiện
     */
    @NotNull
    @Column(name = "da_thuc_hien", nullable = false)
    private Integer daThucHien;

    /**
     * Đơn giá
     */
    @NotNull
    @Column(name = "don_gia", precision = 21, scale = 2, nullable = false)
    private BigDecimal donGia;

    /**
     * Đơn giá Bảo hiểm y tế
     */
    @NotNull
    @Column(name = "don_gia_bhyt", precision = 21, scale = 2, nullable = false)
    private BigDecimal donGiaBhyt;

    /**
     * Đơn giá không Bảo hiểm y tế
     */
    @Column(name = "don_gia_khong_bhyt", precision = 21, scale = 2)
    private BigDecimal donGiaKhongBhyt;

    /**
     * Ghi chú chỉ định
     */
    @Size(max = 2000)
    @Column(name = "ghi_chu_chi_dinh", length = 2000)
    private String ghiChuChiDinh;

    /**
     * Mô tả chỉ định
     */
    @Size(max = 2000)
    @Column(name = "mo_ta", length = 2000)
    private String moTa;

    /**
     * Mô tả chỉ định theo xm15
     */
    @Column(name = "mo_ta_xm_15")
    private String moTaXm15;

    /**
     * Mã người chỉ định
     */
    @Column(name = "nguoi_chi_dinh_id")
    private Long nguoiChiDinhId;

    /**
     * Tên người chỉ định
     */
    @Size(max = 255)
    @Column(name = "nguoi_chi_dinh", length = 255)
    private String nguoiChiDinh;

    /**
     * Số lần chụp
     */
    @Column(name = "so_lan_chup")
    private Integer soLanChup;

    /**
     * Số lượng
     */
    @Column(name = "so_luong", precision = 21, scale = 2)
    private BigDecimal soLuong;

    /**
     * Số lượng có film
     */
    @Column(name = "so_luong_co_film", precision = 21, scale = 2)
    private BigDecimal soLuongCoFilm;

    /**
     * Số lượng có film 1318
     */
    @Column(name = "so_luong_co_film_1318", precision = 21, scale = 2)
    private BigDecimal soLuongCoFilm1318;

    /**
     * Số lượng có film 1820
     */
    @Column(name = "so_luong_co_film_1820", precision = 21, scale = 2)
    private BigDecimal soLuongCoFilm1820;

    /**
     * Số lượng có film 2025
     */
    @Column(name = "so_luong_co_film_2025", precision = 21, scale = 2)
    private BigDecimal soLuongCoFilm2025;

    /**
     * Số lượng có film 2430
     */
    @Column(name = "so_luong_co_film_2430", precision = 21, scale = 2)
    private BigDecimal soLuongCoFilm2430;

    /**
     * Số lượng có film 2530
     */
    @Column(name = "so_luong_co_film_2530", precision = 21, scale = 2)
    private BigDecimal soLuongCoFilm2530;

    /**
     * Số lượng có film 3040
     */
    @Column(name = "so_luong_co_film_3040", precision = 21, scale = 2)
    private BigDecimal soLuongCoFilm3040;

    /**
     * Số lượng film
     */
    @Column(name = "so_luong_film", precision = 21, scale = 2)
    private BigDecimal soLuongFilm;

    /**
     * Tên chỉ định
     */
    @Size(max = 500)
    @Column(name = "ten", length = 500)
    private String ten;

    /**
     * Thành tiền
     */
    @NotNull
    @Column(name = "thanh_tien", precision = 21, scale = 2, nullable = false)
    private BigDecimal thanhTien;

    /**
     * Thành tiền BHYT
     */
    @Column(name = "thanh_tien_bhyt", precision = 21, scale = 2)
    private BigDecimal thanhTienBhyt;

    /**
     * Thành tiền không BHYT
     */
    @Column(name = "thanh_tien_khong_bhyt", precision = 21, scale = 2)
    private BigDecimal thanhTienKhongBHYT;

    /**
     * Thời gian chỉ định
     */
    @Column(name = "thoi_gian_chi_dinh")
    private LocalDate thoiGianChiDinh;

    /**
     * Thời gian tạo
     */
    @NotNull
    @Column(name = "thoi_gian_tao", nullable = false)
    private LocalDate thoiGianTao;

    /**
     * Tiền ngoài BHYT
     */
    @NotNull
    @Column(name = "tien_ngoai_bhyt", precision = 21, scale = 2, nullable = false)
    private BigDecimal tienNgoaiBHYT;

    /**
     * Trạng thái thanh toán chênh lệch: \r\n0: Không có thanh toán chênh lệch. \r\n1: Có thanh toán chênh lệch
     */
    @NotNull
    @Column(name = "thanh_toan_chenh_lech", nullable = false)
    private Boolean thanhToanChenhLech;

    /**
     * Tỷ lệ thanh toán
     */
    @Column(name = "ty_le_thanh_toan")
    private Integer tyLeThanhToan;

    /**
     * Mã dùng chung cho dịch vụ
     */
    @Size(max = 255)
    @Column(name = "ma_dung_chung", length = 255)
    private String maDungChung;

    /**
     * Trạng thái theo yêu cầu của dịch vụ: 0: Bình thường. 1: Theo yêu cầu
     */
    @Column(name = "dich_vu_yeu_cau")
    private Boolean dichVuYeuCau;

    @NotNull
    @Column(name = "nam", nullable = false)
    private Integer nam;

//    @ManyToOne(optional = false)
//    @NotNull
//    @JsonIgnoreProperties("chiDinhCDHAS")
//    private PhieuChiDinhCDHA donVi;
//
//    @ManyToOne(optional = false)
//    @NotNull
//    @JsonIgnoreProperties("chiDinhCDHAS")
//    private PhieuChiDinhCDHA benhNhan;
//
//    @ManyToOne(optional = false)
//    @NotNull
//    @JsonIgnoreProperties("chiDinhCDHAS")
//    private PhieuChiDinhCDHA bakb;
//
    @ManyToOne(optional = false)
    @NotNull
    @JoinColumns(
        {
            @JoinColumn(name = "phieu_cd_id", referencedColumnName = "id",insertable = false, updatable = false),
            @JoinColumn(name = "bakb_id", referencedColumnName = "bakb_id",insertable = false, updatable = false),
            @JoinColumn(name = "benh_nhan_id", referencedColumnName = "benh_nhan_id", insertable = false, updatable = false),
            @JoinColumn(name = "don_vi_id", referencedColumnName = "don_vi_id", insertable = false,updatable = false)
        })
    @JsonIgnoreProperties("chiDinhCDHAS")
    private PhieuChiDinhCDHA phieuCD;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("chiDinhCDHAS")
    private Phong phong;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("chiDinhCDHAS")
    private ChanDoanHinhAnh cdha;


    public Long getBakbId() {
        return bakbId;
    }

    public void setBakbId(Long bakbId) {
        this.bakbId = bakbId;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    public Long getPhieuCDId() {
        return phieuCDId;
    }

    public void setPhieuCDId(Long phieuCDId) {
        this.phieuCDId = phieuCDId;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isCoBaoHiem() {
        return coBaoHiem;
    }

    public ChiDinhCDHA coBaoHiem(Boolean coBaoHiem) {
        this.coBaoHiem = coBaoHiem;
        return this;
    }

    public void setCoBaoHiem(Boolean coBaoHiem) {
        this.coBaoHiem = coBaoHiem;
    }

    public Boolean isCoKetQua() {
        return coKetQua;
    }

    public ChiDinhCDHA coKetQua(Boolean coKetQua) {
        this.coKetQua = coKetQua;
        return this;
    }

    public void setCoKetQua(Boolean coKetQua) {
        this.coKetQua = coKetQua;
    }

    public Integer getDaThanhToan() {
        return daThanhToan;
    }

    public ChiDinhCDHA daThanhToan(Integer daThanhToan) {
        this.daThanhToan = daThanhToan;
        return this;
    }

    public void setDaThanhToan(Integer daThanhToan) {
        this.daThanhToan = daThanhToan;
    }

    public Integer getDaThanhToanChenhLech() {
        return daThanhToanChenhLech;
    }

    public ChiDinhCDHA daThanhToanChenhLech(Integer daThanhToanChenhLech) {
        this.daThanhToanChenhLech = daThanhToanChenhLech;
        return this;
    }

    public void setDaThanhToanChenhLech(Integer daThanhToanChenhLech) {
        this.daThanhToanChenhLech = daThanhToanChenhLech;
    }

    public Integer getDaThucHien() {
        return daThucHien;
    }

    public ChiDinhCDHA daThucHien(Integer daThucHien) {
        this.daThucHien = daThucHien;
        return this;
    }

    public void setDaThucHien(Integer daThucHien) {
        this.daThucHien = daThucHien;
    }

    public BigDecimal getDonGia() {
        return donGia;
    }

    public ChiDinhCDHA donGia(BigDecimal donGia) {
        this.donGia = donGia;
        return this;
    }

    public void setDonGia(BigDecimal donGia) {
        this.donGia = donGia;
    }

    public BigDecimal getDonGiaBhyt() {
        return donGiaBhyt;
    }

    public ChiDinhCDHA donGiaBhyt(BigDecimal donGiaBhyt) {
        this.donGiaBhyt = donGiaBhyt;
        return this;
    }

    public void setDonGiaBhyt(BigDecimal donGiaBhyt) {
        this.donGiaBhyt = donGiaBhyt;
    }

    public BigDecimal getDonGiaKhongBhyt() {
        return donGiaKhongBhyt;
    }

    public ChiDinhCDHA donGiaKhongBhyt(BigDecimal donGiaKhongBhyt) {
        this.donGiaKhongBhyt = donGiaKhongBhyt;
        return this;
    }

    public void setDonGiaKhongBhyt(BigDecimal donGiaKhongBhyt) {
        this.donGiaKhongBhyt = donGiaKhongBhyt;
    }

    public String getGhiChuChiDinh() {
        return ghiChuChiDinh;
    }

    public ChiDinhCDHA ghiChuChiDinh(String ghiChuChiDinh) {
        this.ghiChuChiDinh = ghiChuChiDinh;
        return this;
    }

    public void setGhiChuChiDinh(String ghiChuChiDinh) {
        this.ghiChuChiDinh = ghiChuChiDinh;
    }

    public String getMoTa() {
        return moTa;
    }

    public ChiDinhCDHA moTa(String moTa) {
        this.moTa = moTa;
        return this;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public String getMoTaXm15() {
        return moTaXm15;
    }

    public ChiDinhCDHA moTaXm15(String moTaXm15) {
        this.moTaXm15 = moTaXm15;
        return this;
    }

    public void setMoTaXm15(String moTaXm15) {
        this.moTaXm15 = moTaXm15;
    }

    public Long getNguoiChiDinhId() {
        return nguoiChiDinhId;
    }

    public ChiDinhCDHA nguoiChiDinhId(Long nguoiChiDinhId) {
        this.nguoiChiDinhId = nguoiChiDinhId;
        return this;
    }

    public void setNguoiChiDinhId(Long nguoiChiDinhId) {
        this.nguoiChiDinhId = nguoiChiDinhId;
    }

    public String getNguoiChiDinh() {
        return nguoiChiDinh;
    }

    public ChiDinhCDHA nguoiChiDinh(String nguoiChiDinh) {
        this.nguoiChiDinh = nguoiChiDinh;
        return this;
    }

    public void setNguoiChiDinh(String nguoiChiDinh) {
        this.nguoiChiDinh = nguoiChiDinh;
    }

    public Integer getSoLanChup() {
        return soLanChup;
    }

    public ChiDinhCDHA soLanChup(Integer soLanChup) {
        this.soLanChup = soLanChup;
        return this;
    }

    public void setSoLanChup(Integer soLanChup) {
        this.soLanChup = soLanChup;
    }

    public BigDecimal getSoLuong() {
        return soLuong;
    }

    public ChiDinhCDHA soLuong(BigDecimal soLuong) {
        this.soLuong = soLuong;
        return this;
    }

    public void setSoLuong(BigDecimal soLuong) {
        this.soLuong = soLuong;
    }

    public BigDecimal getSoLuongCoFilm() {
        return soLuongCoFilm;
    }

    public ChiDinhCDHA soLuongCoFilm(BigDecimal soLuongCoFilm) {
        this.soLuongCoFilm = soLuongCoFilm;
        return this;
    }

    public void setSoLuongCoFilm(BigDecimal soLuongCoFilm) {
        this.soLuongCoFilm = soLuongCoFilm;
    }

    public BigDecimal getSoLuongCoFilm1318() {
        return soLuongCoFilm1318;
    }

    public ChiDinhCDHA soLuongCoFilm1318(BigDecimal soLuongCoFilm1318) {
        this.soLuongCoFilm1318 = soLuongCoFilm1318;
        return this;
    }

    public void setSoLuongCoFilm1318(BigDecimal soLuongCoFilm1318) {
        this.soLuongCoFilm1318 = soLuongCoFilm1318;
    }

    public BigDecimal getSoLuongCoFilm1820() {
        return soLuongCoFilm1820;
    }

    public ChiDinhCDHA soLuongCoFilm1820(BigDecimal soLuongCoFilm1820) {
        this.soLuongCoFilm1820 = soLuongCoFilm1820;
        return this;
    }

    public void setSoLuongCoFilm1820(BigDecimal soLuongCoFilm1820) {
        this.soLuongCoFilm1820 = soLuongCoFilm1820;
    }

    public BigDecimal getSoLuongCoFilm2025() {
        return soLuongCoFilm2025;
    }

    public ChiDinhCDHA soLuongCoFilm2025(BigDecimal soLuongCoFilm2025) {
        this.soLuongCoFilm2025 = soLuongCoFilm2025;
        return this;
    }

    public void setSoLuongCoFilm2025(BigDecimal soLuongCoFilm2025) {
        this.soLuongCoFilm2025 = soLuongCoFilm2025;
    }

    public BigDecimal getSoLuongCoFilm2430() {
        return soLuongCoFilm2430;
    }

    public ChiDinhCDHA soLuongCoFilm2430(BigDecimal soLuongCoFilm2430) {
        this.soLuongCoFilm2430 = soLuongCoFilm2430;
        return this;
    }

    public void setSoLuongCoFilm2430(BigDecimal soLuongCoFilm2430) {
        this.soLuongCoFilm2430 = soLuongCoFilm2430;
    }

    public BigDecimal getSoLuongCoFilm2530() {
        return soLuongCoFilm2530;
    }

    public ChiDinhCDHA soLuongCoFilm2530(BigDecimal soLuongCoFilm2530) {
        this.soLuongCoFilm2530 = soLuongCoFilm2530;
        return this;
    }

    public void setSoLuongCoFilm2530(BigDecimal soLuongCoFilm2530) {
        this.soLuongCoFilm2530 = soLuongCoFilm2530;
    }

    public BigDecimal getSoLuongCoFilm3040() {
        return soLuongCoFilm3040;
    }

    public ChiDinhCDHA soLuongCoFilm3040(BigDecimal soLuongCoFilm3040) {
        this.soLuongCoFilm3040 = soLuongCoFilm3040;
        return this;
    }

    public void setSoLuongCoFilm3040(BigDecimal soLuongCoFilm3040) {
        this.soLuongCoFilm3040 = soLuongCoFilm3040;
    }

    public BigDecimal getSoLuongFilm() {
        return soLuongFilm;
    }

    public ChiDinhCDHA soLuongFilm(BigDecimal soLuongFilm) {
        this.soLuongFilm = soLuongFilm;
        return this;
    }

    public void setSoLuongFilm(BigDecimal soLuongFilm) {
        this.soLuongFilm = soLuongFilm;
    }

    public String getTen() {
        return ten;
    }

    public ChiDinhCDHA ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public BigDecimal getThanhTien() {
        return thanhTien;
    }

    public ChiDinhCDHA thanhTien(BigDecimal thanhTien) {
        this.thanhTien = thanhTien;
        return this;
    }

    public void setThanhTien(BigDecimal thanhTien) {
        this.thanhTien = thanhTien;
    }

    public BigDecimal getThanhTienBhyt() {
        return thanhTienBhyt;
    }

    public ChiDinhCDHA thanhTienBhyt(BigDecimal thanhTienBhyt) {
        this.thanhTienBhyt = thanhTienBhyt;
        return this;
    }

    public void setThanhTienBhyt(BigDecimal thanhTienBhyt) {
        this.thanhTienBhyt = thanhTienBhyt;
    }

    public BigDecimal getThanhTienKhongBHYT() {
        return thanhTienKhongBHYT;
    }

    public ChiDinhCDHA thanhTienKhongBHYT(BigDecimal thanhTienKhongBHYT) {
        this.thanhTienKhongBHYT = thanhTienKhongBHYT;
        return this;
    }

    public void setThanhTienKhongBHYT(BigDecimal thanhTienKhongBHYT) {
        this.thanhTienKhongBHYT = thanhTienKhongBHYT;
    }

    public LocalDate getThoiGianChiDinh() {
        return thoiGianChiDinh;
    }

    public ChiDinhCDHA thoiGianChiDinh(LocalDate thoiGianChiDinh) {
        this.thoiGianChiDinh = thoiGianChiDinh;
        return this;
    }

    public void setThoiGianChiDinh(LocalDate thoiGianChiDinh) {
        this.thoiGianChiDinh = thoiGianChiDinh;
    }

    public LocalDate getThoiGianTao() {
        return thoiGianTao;
    }

    public ChiDinhCDHA thoiGianTao(LocalDate thoiGianTao) {
        this.thoiGianTao = thoiGianTao;
        return this;
    }

    public void setThoiGianTao(LocalDate thoiGianTao) {
        this.thoiGianTao = thoiGianTao;
    }

    public BigDecimal getTienNgoaiBHYT() {
        return tienNgoaiBHYT;
    }

    public ChiDinhCDHA tienNgoaiBHYT(BigDecimal tienNgoaiBHYT) {
        this.tienNgoaiBHYT = tienNgoaiBHYT;
        return this;
    }

    public void setTienNgoaiBHYT(BigDecimal tienNgoaiBHYT) {
        this.tienNgoaiBHYT = tienNgoaiBHYT;
    }

    public Boolean isThanhToanChenhLech() {
        return thanhToanChenhLech;
    }

    public ChiDinhCDHA thanhToanChenhLech(Boolean thanhToanChenhLech) {
        this.thanhToanChenhLech = thanhToanChenhLech;
        return this;
    }

    public void setThanhToanChenhLech(Boolean thanhToanChenhLech) {
        this.thanhToanChenhLech = thanhToanChenhLech;
    }

    public Integer getTyLeThanhToan() {
        return tyLeThanhToan;
    }

    public ChiDinhCDHA tyLeThanhToan(Integer tyLeThanhToan) {
        this.tyLeThanhToan = tyLeThanhToan;
        return this;
    }

    public void setTyLeThanhToan(Integer tyLeThanhToan) {
        this.tyLeThanhToan = tyLeThanhToan;
    }

    public String getMaDungChung() {
        return maDungChung;
    }

    public ChiDinhCDHA maDungChung(String maDungChung) {
        this.maDungChung = maDungChung;
        return this;
    }

    public void setMaDungChung(String maDungChung) {
        this.maDungChung = maDungChung;
    }

    public Boolean isDichVuYeuCau() {
        return dichVuYeuCau;
    }

    public ChiDinhCDHA dichVuYeuCau(Boolean dichVuYeuCau) {
        this.dichVuYeuCau = dichVuYeuCau;
        return this;
    }

    public void setDichVuYeuCau(Boolean dichVuYeuCau) {
        this.dichVuYeuCau = dichVuYeuCau;
    }

    public Integer getNam() {
        return nam;
    }

    public ChiDinhCDHA nam(Integer nam) {
        this.nam = nam;
        return this;
    }

    public void setNam(Integer nam) {
        this.nam = nam;
    }

//    public PhieuChiDinhCDHA getDonVi() {
//        return donVi;
//    }
//
//    public ChiDinhCDHA donVi(PhieuChiDinhCDHA phieuChiDinhCDHA) {
//        this.donVi = phieuChiDinhCDHA;
//        return this;
//    }
//
//    public void setDonVi(PhieuChiDinhCDHA phieuChiDinhCDHA) {
//        this.donVi = phieuChiDinhCDHA;
//    }
//
//    public PhieuChiDinhCDHA getBenhNhan() {
//        return benhNhan;
//    }
//
//    public ChiDinhCDHA benhNhan(PhieuChiDinhCDHA phieuChiDinhCDHA) {
//        this.benhNhan = phieuChiDinhCDHA;
//        return this;
//    }
//
//    public void setBenhNhan(PhieuChiDinhCDHA phieuChiDinhCDHA) {
//        this.benhNhan = phieuChiDinhCDHA;
//    }
//
//    public PhieuChiDinhCDHA getBakb() {
//        return bakb;
//    }
//
//    public ChiDinhCDHA bakb(PhieuChiDinhCDHA phieuChiDinhCDHA) {
//        this.bakb = phieuChiDinhCDHA;
//        return this;
//    }
//
//    public void setBakb(PhieuChiDinhCDHA phieuChiDinhCDHA) {
//        this.bakb = phieuChiDinhCDHA;
//    }
//
    public PhieuChiDinhCDHA getPhieuCD() {
        return phieuCD;
    }

    public ChiDinhCDHA phieuCD(PhieuChiDinhCDHA phieuChiDinhCDHA) {
        this.phieuCD = phieuChiDinhCDHA;
        return this;
    }
//
//    public void setPhieuCD(PhieuChiDinhCDHA phieuChiDinhCDHA) {
//        this.phieuCD = phieuChiDinhCDHA;
//    }

    public Phong getPhong() {
        return phong;
    }

    public ChiDinhCDHA phong(Phong phong) {
        this.phong = phong;
        return this;
    }

    public void setPhong(Phong phong) {
        this.phong = phong;
    }

    public ChanDoanHinhAnh getCdha() {
        return cdha;
    }

    public ChiDinhCDHA cdha(ChanDoanHinhAnh chanDoanHinhAnh) {
        this.cdha = chanDoanHinhAnh;
        return this;
    }

    public void setCdha(ChanDoanHinhAnh chanDoanHinhAnh) {
        this.cdha = chanDoanHinhAnh;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ChiDinhCDHA)) return false;
        ChiDinhCDHA that = (ChiDinhCDHA) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getBakbId(), that.getBakbId()) &&
            Objects.equals(getBenhNhanId(), that.getBenhNhanId()) &&
            Objects.equals(getDonViId(), that.getDonViId()) &&
            Objects.equals(getPhieuCDId(), that.getPhieuCDId()) &&
            Objects.equals(coBaoHiem, that.coBaoHiem) &&
            Objects.equals(coKetQua, that.coKetQua) &&
            Objects.equals(getDaThanhToan(), that.getDaThanhToan()) &&
            Objects.equals(getDaThanhToanChenhLech(), that.getDaThanhToanChenhLech()) &&
            Objects.equals(getDaThucHien(), that.getDaThucHien()) &&
            Objects.equals(getDonGia(), that.getDonGia()) &&
            Objects.equals(getDonGiaBhyt(), that.getDonGiaBhyt()) &&
            Objects.equals(getDonGiaKhongBhyt(), that.getDonGiaKhongBhyt()) &&
            Objects.equals(getGhiChuChiDinh(), that.getGhiChuChiDinh()) &&
            Objects.equals(getMoTa(), that.getMoTa()) &&
            Objects.equals(getMoTaXm15(), that.getMoTaXm15()) &&
            Objects.equals(getNguoiChiDinhId(), that.getNguoiChiDinhId()) &&
            Objects.equals(getNguoiChiDinh(), that.getNguoiChiDinh()) &&
            Objects.equals(getSoLanChup(), that.getSoLanChup()) &&
            Objects.equals(getSoLuong(), that.getSoLuong()) &&
            Objects.equals(getSoLuongCoFilm(), that.getSoLuongCoFilm()) &&
            Objects.equals(getSoLuongCoFilm1318(), that.getSoLuongCoFilm1318()) &&
            Objects.equals(getSoLuongCoFilm1820(), that.getSoLuongCoFilm1820()) &&
            Objects.equals(getSoLuongCoFilm2025(), that.getSoLuongCoFilm2025()) &&
            Objects.equals(getSoLuongCoFilm2430(), that.getSoLuongCoFilm2430()) &&
            Objects.equals(getSoLuongCoFilm2530(), that.getSoLuongCoFilm2530()) &&
            Objects.equals(getSoLuongCoFilm3040(), that.getSoLuongCoFilm3040()) &&
            Objects.equals(getSoLuongFilm(), that.getSoLuongFilm()) &&
            Objects.equals(getTen(), that.getTen()) &&
            Objects.equals(getThanhTien(), that.getThanhTien()) &&
            Objects.equals(getThanhTienBhyt(), that.getThanhTienBhyt()) &&
            Objects.equals(getThanhTienKhongBHYT(), that.getThanhTienKhongBHYT()) &&
            Objects.equals(getThoiGianChiDinh(), that.getThoiGianChiDinh()) &&
            Objects.equals(getThoiGianTao(), that.getThoiGianTao()) &&
            Objects.equals(getTienNgoaiBHYT(), that.getTienNgoaiBHYT()) &&
            Objects.equals(thanhToanChenhLech, that.thanhToanChenhLech) &&
            Objects.equals(getTyLeThanhToan(), that.getTyLeThanhToan()) &&
            Objects.equals(getMaDungChung(), that.getMaDungChung()) &&
            Objects.equals(dichVuYeuCau, that.dichVuYeuCau) &&
            Objects.equals(getNam(), that.getNam()) &&
            Objects.equals(getPhong(), that.getPhong()) &&
            Objects.equals(getCdha(), that.getCdha());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBakbId(), getBenhNhanId(), getDonViId(), getPhieuCDId(), coBaoHiem, coKetQua, getDaThanhToan(), getDaThanhToanChenhLech(), getDaThucHien(), getDonGia(), getDonGiaBhyt(), getDonGiaKhongBhyt(), getGhiChuChiDinh(), getMoTa(), getMoTaXm15(), getNguoiChiDinhId(), getNguoiChiDinh(), getSoLanChup(), getSoLuong(), getSoLuongCoFilm(), getSoLuongCoFilm1318(), getSoLuongCoFilm1820(), getSoLuongCoFilm2025(), getSoLuongCoFilm2430(), getSoLuongCoFilm2530(), getSoLuongCoFilm3040(), getSoLuongFilm(), getTen(), getThanhTien(), getThanhTienBhyt(), getThanhTienKhongBHYT(), getThoiGianChiDinh(), getThoiGianTao(), getTienNgoaiBHYT(), thanhToanChenhLech, getTyLeThanhToan(), getMaDungChung(), dichVuYeuCau, getNam(), getPhong(), getCdha());
    }

    @Override
    public String toString() {
        return "ChiDinhCDHA{" +
            "id=" + id +
            ", bakbId=" + bakbId +
            ", benhNhanId=" + benhNhanId +
            ", donViId=" + donViId +
            ", phieuCDId=" + phieuCDId +
            ", coBaoHiem=" + coBaoHiem +
            ", coKetQua=" + coKetQua +
            ", daThanhToan=" + daThanhToan +
            ", daThanhToanChenhLech=" + daThanhToanChenhLech +
            ", daThucHien=" + daThucHien +
            ", donGia=" + donGia +
            ", donGiaBhyt=" + donGiaBhyt +
            ", donGiaKhongBhyt=" + donGiaKhongBhyt +
            ", ghiChuChiDinh='" + ghiChuChiDinh + '\'' +
            ", moTa='" + moTa + '\'' +
            ", moTaXm15='" + moTaXm15 + '\'' +
            ", nguoiChiDinhId=" + nguoiChiDinhId +
            ", nguoiChiDinh='" + nguoiChiDinh + '\'' +
            ", soLanChup=" + soLanChup +
            ", soLuong=" + soLuong +
            ", soLuongCoFilm=" + soLuongCoFilm +
            ", soLuongCoFilm1318=" + soLuongCoFilm1318 +
            ", soLuongCoFilm1820=" + soLuongCoFilm1820 +
            ", soLuongCoFilm2025=" + soLuongCoFilm2025 +
            ", soLuongCoFilm2430=" + soLuongCoFilm2430 +
            ", soLuongCoFilm2530=" + soLuongCoFilm2530 +
            ", soLuongCoFilm3040=" + soLuongCoFilm3040 +
            ", soLuongFilm=" + soLuongFilm +
            ", ten='" + ten + '\'' +
            ", thanhTien=" + thanhTien +
            ", thanhTienBhyt=" + thanhTienBhyt +
            ", thanhTienKhongBHYT=" + thanhTienKhongBHYT +
            ", thoiGianChiDinh=" + thoiGianChiDinh +
            ", thoiGianTao=" + thoiGianTao +
            ", tienNgoaiBHYT=" + tienNgoaiBHYT +
            ", thanhToanChenhLech=" + thanhToanChenhLech +
            ", tyLeThanhToan=" + tyLeThanhToan +
            ", maDungChung='" + maDungChung + '\'' +
            ", dichVuYeuCau=" + dichVuYeuCau +
            ", nam=" + nam +
            ", phong=" + phong +
            ", cdha=" + cdha +
            '}';
    }
}
