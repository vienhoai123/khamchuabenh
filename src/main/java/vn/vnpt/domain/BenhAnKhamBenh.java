package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import vn.vnpt.domain.custom.keys.BenhAnKhamBenhPrimaryKey;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.LocalDate;

/**
 * A BenhAnKhamBenh.
 */
@Entity
@Table(name = "benh_an_kham_benh")
@IdClass(BenhAnKhamBenhId.class)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BenhAnKhamBenh implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Id
    @NotNull
    @Column(name = "benh_nhan_id")
    private Long benhNhanId;

    @Id
    @NotNull
    @Column(name = "don_vi_id")
    private Long donViId;
    /**
     * Trạng thái bệnh nhân đã tiếp nhận\n1: Bệnh nhân cũ\n0: Bệnh nhân mới
     */
    @NotNull
    @Column(name = "benh_nhan_cu", nullable = false)
    private Boolean benhNhanCu;

    /**
     * Cảnh báo còn thuộc khi bệnh nhân khám BHYT
     */
    @NotNull
    @Column(name = "canh_bao", nullable = false)
    private Boolean canhBao;

    /**
     * Trạng thái cấp cứu của bệnh nhân\n1: Cấp cứu\n0: Bình thường
     */
    @NotNull
    @Column(name = "cap_cuu", nullable = false)
    private Boolean capCuu;

    /**
     * Trạng thái chỉ có năm sinh của bệnh nhân\n1: Chỉ có năm sinh\n0: Đủ thông tin
     */
    @NotNull
    @Column(name = "chi_co_nam_sinh", nullable = false)
    private Boolean chiCoNamSinh;

    /**
     * Thông tin chứng minh nhân dân của bệnh nhân
     */
    @Size(max = 20)
    @Column(name = "cmnd_benh_nhan", length = 20)
    private String cmndBenhNhan;

    /**
     * Thông tin chứng minh nhân dân của người nhà bệnh nhân
     */
    @Size(max = 20)
    @Column(name = "cmnd_nguoi_nha", length = 20)
    private String cmndNguoiNha;

    /**
     * Trạng thái có bảo hiểm của
     */
    @NotNull
    @Column(name = "co_bao_hiem", nullable = false)
    private Boolean coBaoHiem;

    /**
     * Thông tin địa chỉ của bệnh nhân
     */
    @NotNull
    @Size(max = 500)
    @Column(name = "dia_chi", length = 500, nullable = false)
    private String diaChi;

    /**
     * Thông tin email của bệnh nhân
     */
    @Size(max = 255)
    @Column(name = "email", length = 255)
    private String email;

    /**
     * Trạng thái giới tính của bệnh nhân:\n\n1: Nam\n.\n0: Nữ.
     */
    @NotNull
    @Column(name = "gioi_tinh", nullable = false)
    private Integer gioiTinh;

    /**
     * Thông tin kháng thể của bệnh nhân
     */
    @Size(max = 5)
    @Column(name = "khang_the", length = 5)
    private String khangThe;

    /**
     * Lần khám trong ngày của bệnh nhân
     */
    @Column(name = "lan_kham_trong_ngay")
    private Integer lanKhamTrongNgay;

    /**
     * Mã loại giấy tờ của trẻ em như giấy khai sinh, giấy chứng sinh
     */
    @Column(name = "loai_giay_to_tre_em")
    private Integer loaiGiayToTreEm;

    /**
     * Thông tin ngày cấp CMND của bệnh nhân
     */
    @Column(name = "ngay_cap_cmnd")
    private LocalDate ngayCapCmnd;

    /**
     * Thời gian miễn cùng chi trả của thẻ BHYT
     */
    @Column(name = "ngay_mien_cung_chi_tra")
    private LocalDate ngayMienCungChiTra;

    /**
     * Thôi tin ngày sinh của bệnh nhân
     */
    @NotNull
    @Column(name = "ngay_sinh", nullable = false)
    private LocalDate ngaySinh;

    /**
     * Thông tin nhóm máu cảu bệnh nhân
     */
    @Size(max = 5)
    @Column(name = "nhom_mau", length = 5)
    private String nhomMau;

    /**
     * Thông tin nơi cấp của bệnh nhân
     */
    @Size(max = 500)
    @Column(name = "noi_cap_cmnd", length = 500)
    private String noiCapCmnd;

    /**
     * Thông tin nơi làm việc của bệnh nhân
     */
    @Size(max = 500)
    @Column(name = "noi_lam_viec", length = 500)
    private String noiLamViec;

    /**
     * Thông tin số điện thoại của bệnh nhân
     */
    @Size(max = 15)
    @Column(name = "phone", length = 15)
    private String phone;

    /**
     * Thông tin số điện thoại người nhà
     */
    @Size(max = 15)
    @Column(name = "phone_nguoi_nha", length = 15)
    private String phoneNguoiNha;

    /**
     * Thông tin quốc tịch của bệnh nhân
     */
    @Size(max = 200)
    @Column(name = "quoc_tich", length = 200)
    private String quocTich;

    /**
     * Tên bệnh nhân
     */
    @NotNull
    @Size(max = 200)
    @Column(name = "ten_benh_nhan", length = 200, nullable = false)
    private String tenBenhNhan;

    /**
     * Tên người nhà bệnh nhân
     */
    @Size(max = 200)
    @Column(name = "ten_nguoi_nha", length = 200)
    private String tenNguoiNha;

    /**
     * Thông tin tháng tuổi nếu là bệnh nhân nhi
     */
    @Column(name = "thang_tuoi")
    private Integer thangTuoi;

    /**
     * Thông tin thời gian tiếp nhận bệnh nhân
     */
    @NotNull
    @Column(name = "thoi_gian_tiep_nhan", nullable = false)
    private LocalDate thoiGianTiepNhan;

    /**
     * Thông tin buổi của bệnh nhân
     */
    @Column(name = "tuoi")
    private Integer tuoi;

    /**
     * Trạng thái ưu tiên của bệnh nhân:\n1: Ưu tiên\n0: Không ưu tiên
     */
    @Column(name = "uu_tien")
    private Integer uuTien;

    /**
     * Mã bệnh nhân theo công thức UUID
     */
    @Size(max = 255)
    @Column(name = "uuid", length = 255)
    private String uuid;

    /**
     * Trạng thái của Bệnh án khám bệnh:\n1: Chờ khám, mới vào khoa\n2: Đang khám, đang điều trị\n3: Đã khám, xuất viện\n4: Chuyển khoa phòng khám\n5: Chuyển viện, chuyển tuyến\n6: Nhập viện,\n7: Tử vong;\n8: trốn viện;
     */
    @Column(name = "trang_thai")
    private Integer trangThai;

    /**
     * Mã khoa hoàn tất khám
     */
    @Column(name = "ma_khoa_hoan_tat_kham")
    private Integer maKhoaHoanTatKham;

    /**
     * Mã phòng hoàn tất khám
     */
    @Column(name = "ma_phong_hoan_tat_kham")
    private Integer maPhongHoanTatKham;

    /**
     * Tên khoa hoàn tất khám
     */
    @Size(max = 200)
    @Column(name = "ten_khoa_hoan_tat_kham", length = 200)
    private String tenKhoaHoanTatKham;

    /**
     * Tên phòng hoàn tất khám
     */
    @Size(max = 200)
    @Column(name = "ten_phong_hoan_tat_kham", length = 200)
    private String tenPhongHoanTatKham;

    /**
     * Thời gian hoàn tất khám
     */
    @Column(name = "thoi_gian_hoan_tat_kham")
    private LocalDate thoiGianHoanTatKham;

    /**
     * Số bệnh án
     */
    @Column(name = "so_benh_an")
    private String soBenhAn;

    /**
     * Số bệnh án theo khoa
     */
    @Column(name = "so_benh_an_khoa")
    private String soBenhAnKhoa;

    @NotNull
    @Column(name = "nam", nullable = false)
    private Integer nam;

    @ManyToOne(optional = false)
    @JoinColumn(name = "benh_nhan_id",referencedColumnName = "id",insertable = false,updatable = false)
    @JsonIgnoreProperties("benhAnKhamBenhs")
    private BenhNhan benhNhan;

    @ManyToOne(optional = false)
    @JoinColumn(name = "don_vi_id",referencedColumnName = "id",insertable = false, updatable = false)
    @JsonIgnoreProperties("benhAnKhamBenhs")
    private DonVi donVi;

    /**
     * Mã nhân viên tiếp nhận
     */
    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("benhAnKhamBenhs")
    private NhanVien nhanVienTiepNhan;

    /**
     * Id phòng hoàn tất khám
     */
    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("benhAnKhamBenhs")
    private Phong phongTiepNhan;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    public Boolean isBenhNhanCu() {
        return benhNhanCu;
    }

    public BenhAnKhamBenh benhNhanCu(Boolean benhNhanCu) {
        this.benhNhanCu = benhNhanCu;
        return this;
    }

    public void setBenhNhanCu(Boolean benhNhanCu) {
        this.benhNhanCu = benhNhanCu;
    }

    public Boolean isCanhBao() {
        return canhBao;
    }

    public BenhAnKhamBenh canhBao(Boolean canhBao) {
        this.canhBao = canhBao;
        return this;
    }

    public void setCanhBao(Boolean canhBao) {
        this.canhBao = canhBao;
    }

    public Boolean isCapCuu() {
        return capCuu;
    }

    public BenhAnKhamBenh capCuu(Boolean capCuu) {
        this.capCuu = capCuu;
        return this;
    }

    public void setCapCuu(Boolean capCuu) {
        this.capCuu = capCuu;
    }

    public Boolean isChiCoNamSinh() {
        return chiCoNamSinh;
    }

    public BenhAnKhamBenh chiCoNamSinh(Boolean chiCoNamSinh) {
        this.chiCoNamSinh = chiCoNamSinh;
        return this;
    }

    public void setChiCoNamSinh(Boolean chiCoNamSinh) {
        this.chiCoNamSinh = chiCoNamSinh;
    }

    public String getCmndBenhNhan() {
        return cmndBenhNhan;
    }

    public BenhAnKhamBenh cmndBenhNhan(String cmndBenhNhan) {
        this.cmndBenhNhan = cmndBenhNhan;
        return this;
    }

    public void setCmndBenhNhan(String cmndBenhNhan) {
        this.cmndBenhNhan = cmndBenhNhan;
    }

    public String getCmndNguoiNha() {
        return cmndNguoiNha;
    }

    public BenhAnKhamBenh cmndNguoiNha(String cmndNguoiNha) {
        this.cmndNguoiNha = cmndNguoiNha;
        return this;
    }

    public void setCmndNguoiNha(String cmndNguoiNha) {
        this.cmndNguoiNha = cmndNguoiNha;
    }

    public Boolean isCoBaoHiem() {
        return coBaoHiem;
    }

    public BenhAnKhamBenh coBaoHiem(Boolean coBaoHiem) {
        this.coBaoHiem = coBaoHiem;
        return this;
    }

    public void setCoBaoHiem(Boolean coBaoHiem) {
        this.coBaoHiem = coBaoHiem;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public BenhAnKhamBenh diaChi(String diaChi) {
        this.diaChi = diaChi;
        return this;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getEmail() {
        return email;
    }

    public BenhAnKhamBenh email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getGioiTinh() {
        return gioiTinh;
    }

    public BenhAnKhamBenh gioiTinh(Integer gioiTinh) {
        this.gioiTinh = gioiTinh;
        return this;
    }

    public void setGioiTinh(Integer gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public String getKhangThe() {
        return khangThe;
    }

    public BenhAnKhamBenh khangThe(String khangThe) {
        this.khangThe = khangThe;
        return this;
    }

    public void setKhangThe(String khangThe) {
        this.khangThe = khangThe;
    }

    public Integer getLanKhamTrongNgay() {
        return lanKhamTrongNgay;
    }

    public BenhAnKhamBenh lanKhamTrongNgay(Integer lanKhamTrongNgay) {
        this.lanKhamTrongNgay = lanKhamTrongNgay;
        return this;
    }

    public void setLanKhamTrongNgay(Integer lanKhamTrongNgay) {
        this.lanKhamTrongNgay = lanKhamTrongNgay;
    }

    public Integer getLoaiGiayToTreEm() {
        return loaiGiayToTreEm;
    }

    public BenhAnKhamBenh loaiGiayToTreEm(Integer loaiGiayToTreEm) {
        this.loaiGiayToTreEm = loaiGiayToTreEm;
        return this;
    }

    public void setLoaiGiayToTreEm(Integer loaiGiayToTreEm) {
        this.loaiGiayToTreEm = loaiGiayToTreEm;
    }

    public LocalDate getNgayCapCmnd() {
        return ngayCapCmnd;
    }

    public BenhAnKhamBenh ngayCapCmnd(LocalDate ngayCapCmnd) {
        this.ngayCapCmnd = ngayCapCmnd;
        return this;
    }

    public void setNgayCapCmnd(LocalDate ngayCapCmnd) {
        this.ngayCapCmnd = ngayCapCmnd;
    }

    public LocalDate getNgayMienCungChiTra() {
        return ngayMienCungChiTra;
    }

    public BenhAnKhamBenh ngayMienCungChiTra(LocalDate ngayMienCungChiTra) {
        this.ngayMienCungChiTra = ngayMienCungChiTra;
        return this;
    }

    public void setNgayMienCungChiTra(LocalDate ngayMienCungChiTra) {
        this.ngayMienCungChiTra = ngayMienCungChiTra;
    }

    public LocalDate getNgaySinh() {
        return ngaySinh;
    }

    public BenhAnKhamBenh ngaySinh(LocalDate ngaySinh) {
        this.ngaySinh = ngaySinh;
        return this;
    }

    public void setNgaySinh(LocalDate ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getNhomMau() {
        return nhomMau;
    }

    public BenhAnKhamBenh nhomMau(String nhomMau) {
        this.nhomMau = nhomMau;
        return this;
    }

    public void setNhomMau(String nhomMau) {
        this.nhomMau = nhomMau;
    }

    public String getNoiCapCmnd() {
        return noiCapCmnd;
    }

    public BenhAnKhamBenh noiCapCmnd(String noiCapCmnd) {
        this.noiCapCmnd = noiCapCmnd;
        return this;
    }

    public void setNoiCapCmnd(String noiCapCmnd) {
        this.noiCapCmnd = noiCapCmnd;
    }

    public String getNoiLamViec() {
        return noiLamViec;
    }

    public BenhAnKhamBenh noiLamViec(String noiLamViec) {
        this.noiLamViec = noiLamViec;
        return this;
    }

    public void setNoiLamViec(String noiLamViec) {
        this.noiLamViec = noiLamViec;
    }

    public String getPhone() {
        return phone;
    }

    public BenhAnKhamBenh phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneNguoiNha() {
        return phoneNguoiNha;
    }

    public BenhAnKhamBenh phoneNguoiNha(String phoneNguoiNha) {
        this.phoneNguoiNha = phoneNguoiNha;
        return this;
    }

    public void setPhoneNguoiNha(String phoneNguoiNha) {
        this.phoneNguoiNha = phoneNguoiNha;
    }

    public String getQuocTich() {
        return quocTich;
    }

    public BenhAnKhamBenh quocTich(String quocTich) {
        this.quocTich = quocTich;
        return this;
    }

    public void setQuocTich(String quocTich) {
        this.quocTich = quocTich;
    }

    public String getTenBenhNhan() {
        return tenBenhNhan;
    }

    public BenhAnKhamBenh tenBenhNhan(String tenBenhNhan) {
        this.tenBenhNhan = tenBenhNhan;
        return this;
    }

    public void setTenBenhNhan(String tenBenhNhan) {
        this.tenBenhNhan = tenBenhNhan;
    }

    public String getTenNguoiNha() {
        return tenNguoiNha;
    }

    public BenhAnKhamBenh tenNguoiNha(String tenNguoiNha) {
        this.tenNguoiNha = tenNguoiNha;
        return this;
    }

    public void setTenNguoiNha(String tenNguoiNha) {
        this.tenNguoiNha = tenNguoiNha;
    }

    public Integer getThangTuoi() {
        return thangTuoi;
    }

    public BenhAnKhamBenh thangTuoi(Integer thangTuoi) {
        this.thangTuoi = thangTuoi;
        return this;
    }

    public void setThangTuoi(Integer thangTuoi) {
        this.thangTuoi = thangTuoi;
    }

    public LocalDate getThoiGianTiepNhan() {
        return thoiGianTiepNhan;
    }

    public BenhAnKhamBenh thoiGianTiepNhan(LocalDate thoiGianTiepNhan) {
        this.thoiGianTiepNhan = thoiGianTiepNhan;
        return this;
    }

    public void setThoiGianTiepNhan(LocalDate thoiGianTiepNhan) {
        this.thoiGianTiepNhan = thoiGianTiepNhan;
    }

    public Integer getTuoi() {
        return tuoi;
    }

    public BenhAnKhamBenh tuoi(Integer tuoi) {
        this.tuoi = tuoi;
        return this;
    }

    public void setTuoi(Integer tuoi) {
        this.tuoi = tuoi;
    }

    public Integer getUuTien() {
        return uuTien;
    }

    public BenhAnKhamBenh uuTien(Integer uuTien) {
        this.uuTien = uuTien;
        return this;
    }

    public void setUuTien(Integer uuTien) {
        this.uuTien = uuTien;
    }

    public String getUuid() {
        return uuid;
    }

    public BenhAnKhamBenh uuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getTrangThai() {
        return trangThai;
    }

    public BenhAnKhamBenh trangThai(Integer trangThai) {
        this.trangThai = trangThai;
        return this;
    }

    public void setTrangThai(Integer trangThai) {
        this.trangThai = trangThai;
    }

    public Integer getMaKhoaHoanTatKham() {
        return maKhoaHoanTatKham;
    }

    public BenhAnKhamBenh maKhoaHoanTatKham(Integer maKhoaHoanTatKham) {
        this.maKhoaHoanTatKham = maKhoaHoanTatKham;
        return this;
    }

    public void setMaKhoaHoanTatKham(Integer maKhoaHoanTatKham) {
        this.maKhoaHoanTatKham = maKhoaHoanTatKham;
    }

    public Integer getMaPhongHoanTatKham() {
        return maPhongHoanTatKham;
    }

    public BenhAnKhamBenh maPhongHoanTatKham(Integer maPhongHoanTatKham) {
        this.maPhongHoanTatKham = maPhongHoanTatKham;
        return this;
    }

    public void setMaPhongHoanTatKham(Integer maPhongHoanTatKham) {
        this.maPhongHoanTatKham = maPhongHoanTatKham;
    }

    public String getTenKhoaHoanTatKham() {
        return tenKhoaHoanTatKham;
    }

    public BenhAnKhamBenh tenKhoaHoanTatKham(String tenKhoaHoanTatKham) {
        this.tenKhoaHoanTatKham = tenKhoaHoanTatKham;
        return this;
    }

    public void setTenKhoaHoanTatKham(String tenKhoaHoanTatKham) {
        this.tenKhoaHoanTatKham = tenKhoaHoanTatKham;
    }

    public String getTenPhongHoanTatKham() {
        return tenPhongHoanTatKham;
    }

    public BenhAnKhamBenh tenPhongHoanTatKham(String tenPhongHoanTatKham) {
        this.tenPhongHoanTatKham = tenPhongHoanTatKham;
        return this;
    }

    public void setTenPhongHoanTatKham(String tenPhongHoanTatKham) {
        this.tenPhongHoanTatKham = tenPhongHoanTatKham;
    }

    public LocalDate getThoiGianHoanTatKham() {
        return thoiGianHoanTatKham;
    }

    public BenhAnKhamBenh thoiGianHoanTatKham(LocalDate thoiGianHoanTatKham) {
        this.thoiGianHoanTatKham = thoiGianHoanTatKham;
        return this;
    }

    public void setThoiGianHoanTatKham(LocalDate thoiGianHoanTatKham) {
        this.thoiGianHoanTatKham = thoiGianHoanTatKham;
    }

    public String getSoBenhAn() {
        return soBenhAn;
    }

    public BenhAnKhamBenh soBenhAn(String soBenhAn) {
        this.soBenhAn = soBenhAn;
        return this;
    }

    public void setSoBenhAn(String soBenhAn) {
        this.soBenhAn = soBenhAn;
    }

    public String getSoBenhAnKhoa() {
        return soBenhAnKhoa;
    }

    public BenhAnKhamBenh soBenhAnKhoa(String soBenhAnKhoa) {
        this.soBenhAnKhoa = soBenhAnKhoa;
        return this;
    }

    public void setSoBenhAnKhoa(String soBenhAnKhoa) {
        this.soBenhAnKhoa = soBenhAnKhoa;
    }

    public Integer getNam() {
        return nam;
    }

    public BenhAnKhamBenh nam(Integer nam) {
        this.nam = nam;
        return this;
    }

    public void setNam(Integer nam) {
        this.nam = nam;
    }

    public BenhNhan getBenhNhan() {
        return benhNhan;
    }

    public BenhAnKhamBenh benhNhan(BenhNhan benhNhan) {
        this.benhNhan = benhNhan;
        return this;
    }

    public void setBenhNhan(BenhNhan benhNhan) {
        this.benhNhan = benhNhan;
    }

    public DonVi getDonVi() {
        return donVi;
    }

    public BenhAnKhamBenh donVi(DonVi donVi) {
        this.donVi = donVi;
        return this;
    }

    public void setDonVi(DonVi donVi) {
        this.donVi = donVi;
    }

    public NhanVien getNhanVienTiepNhan() {
        return nhanVienTiepNhan;
    }

    public BenhAnKhamBenh nhanVienTiepNhan(NhanVien nhanVien) {
        this.nhanVienTiepNhan = nhanVien;
        return this;
    }

    public void setNhanVienTiepNhan(NhanVien nhanVien) {
        this.nhanVienTiepNhan = nhanVien;
    }

    public Phong getPhongTiepNhan() {
        return phongTiepNhan;
    }

    public BenhAnKhamBenh phongTiepNhan(Phong phong) {
        this.phongTiepNhan = phong;
        return this;
    }

    public void setPhongTiepNhan(Phong phong) {
        this.phongTiepNhan = phong;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BenhAnKhamBenh)) return false;
        BenhAnKhamBenh that = (BenhAnKhamBenh) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(benhNhanCu, that.benhNhanCu) &&
            Objects.equals(canhBao, that.canhBao) &&
            Objects.equals(capCuu, that.capCuu) &&
            Objects.equals(chiCoNamSinh, that.chiCoNamSinh) &&
            Objects.equals(getCmndBenhNhan(), that.getCmndBenhNhan()) &&
            Objects.equals(getCmndNguoiNha(), that.getCmndNguoiNha()) &&
            Objects.equals(coBaoHiem, that.coBaoHiem) &&
            Objects.equals(getDiaChi(), that.getDiaChi()) &&
            Objects.equals(getEmail(), that.getEmail()) &&
            Objects.equals(getGioiTinh(), that.getGioiTinh()) &&
            Objects.equals(getKhangThe(), that.getKhangThe()) &&
            Objects.equals(getLanKhamTrongNgay(), that.getLanKhamTrongNgay()) &&
            Objects.equals(getLoaiGiayToTreEm(), that.getLoaiGiayToTreEm()) &&
            Objects.equals(getNgayCapCmnd(), that.getNgayCapCmnd()) &&
            Objects.equals(getNgayMienCungChiTra(), that.getNgayMienCungChiTra()) &&
            Objects.equals(getNgaySinh(), that.getNgaySinh()) &&
            Objects.equals(getNhomMau(), that.getNhomMau()) &&
            Objects.equals(getNoiCapCmnd(), that.getNoiCapCmnd()) &&
            Objects.equals(getNoiLamViec(), that.getNoiLamViec()) &&
            Objects.equals(getPhone(), that.getPhone()) &&
            Objects.equals(getPhoneNguoiNha(), that.getPhoneNguoiNha()) &&
            Objects.equals(getQuocTich(), that.getQuocTich()) &&
            Objects.equals(getTenBenhNhan(), that.getTenBenhNhan()) &&
            Objects.equals(getTenNguoiNha(), that.getTenNguoiNha()) &&
            Objects.equals(getThangTuoi(), that.getThangTuoi()) &&
            Objects.equals(getThoiGianTiepNhan(), that.getThoiGianTiepNhan()) &&
            Objects.equals(getTuoi(), that.getTuoi()) &&
            Objects.equals(getUuTien(), that.getUuTien()) &&
            Objects.equals(getUuid(), that.getUuid()) &&
            Objects.equals(getTrangThai(), that.getTrangThai()) &&
            Objects.equals(getMaKhoaHoanTatKham(), that.getMaKhoaHoanTatKham()) &&
            Objects.equals(getMaPhongHoanTatKham(), that.getMaPhongHoanTatKham()) &&
            Objects.equals(getTenKhoaHoanTatKham(), that.getTenKhoaHoanTatKham()) &&
            Objects.equals(getTenPhongHoanTatKham(), that.getTenPhongHoanTatKham()) &&
            Objects.equals(getThoiGianHoanTatKham(), that.getThoiGianHoanTatKham()) &&
            Objects.equals(getSoBenhAn(), that.getSoBenhAn()) &&
            Objects.equals(getSoBenhAnKhoa(), that.getSoBenhAnKhoa()) &&
            Objects.equals(getNam(), that.getNam()) &&
            Objects.equals(getBenhNhan(), that.getBenhNhan()) &&
            Objects.equals(getDonVi(), that.getDonVi()) &&
            Objects.equals(getNhanVienTiepNhan(), that.getNhanVienTiepNhan()) &&
            Objects.equals(getPhongTiepNhan(), that.getPhongTiepNhan());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), benhNhanCu, canhBao, capCuu, chiCoNamSinh, getCmndBenhNhan(), getCmndNguoiNha(), coBaoHiem, getDiaChi(), getEmail(), getGioiTinh(), getKhangThe(), getLanKhamTrongNgay(), getLoaiGiayToTreEm(), getNgayCapCmnd(), getNgayMienCungChiTra(), getNgaySinh(), getNhomMau(), getNoiCapCmnd(), getNoiLamViec(), getPhone(), getPhoneNguoiNha(), getQuocTich(), getTenBenhNhan(), getTenNguoiNha(), getThangTuoi(), getThoiGianTiepNhan(), getTuoi(), getUuTien(), getUuid(), getTrangThai(), getMaKhoaHoanTatKham(), getMaPhongHoanTatKham(), getTenKhoaHoanTatKham(), getTenPhongHoanTatKham(), getThoiGianHoanTatKham(), getSoBenhAn(), getSoBenhAnKhoa(), getNam(), getBenhNhan(), getDonVi(), getNhanVienTiepNhan(), getPhongTiepNhan());
    }

    @Override
    public String toString() {
        return "BenhAnKhamBenh{" +
            "id=" + id +
            ", benhNhanCu=" + benhNhanCu +
            ", canhBao=" + canhBao +
            ", capCuu=" + capCuu +
            ", chiCoNamSinh=" + chiCoNamSinh +
            ", cmndBenhNhan='" + cmndBenhNhan + '\'' +
            ", cmndNguoiNha='" + cmndNguoiNha + '\'' +
            ", coBaoHiem=" + coBaoHiem +
            ", diaChi='" + diaChi + '\'' +
            ", email='" + email + '\'' +
            ", gioiTinh=" + gioiTinh +
            ", khangThe='" + khangThe + '\'' +
            ", lanKhamTrongNgay=" + lanKhamTrongNgay +
            ", loaiGiayToTreEm=" + loaiGiayToTreEm +
            ", ngayCapCmnd=" + ngayCapCmnd +
            ", ngayMienCungChiTra=" + ngayMienCungChiTra +
            ", ngaySinh=" + ngaySinh +
            ", nhomMau='" + nhomMau + '\'' +
            ", noiCapCmnd='" + noiCapCmnd + '\'' +
            ", noiLamViec='" + noiLamViec + '\'' +
            ", phone='" + phone + '\'' +
            ", phoneNguoiNha='" + phoneNguoiNha + '\'' +
            ", quocTich='" + quocTich + '\'' +
            ", tenBenhNhan='" + tenBenhNhan + '\'' +
            ", tenNguoiNha='" + tenNguoiNha + '\'' +
            ", thangTuoi=" + thangTuoi +
            ", thoiGianTiepNhan=" + thoiGianTiepNhan +
            ", tuoi=" + tuoi +
            ", uuTien=" + uuTien +
            ", uuid='" + uuid + '\'' +
            ", trangThai=" + trangThai +
            ", maKhoaHoanTatKham=" + maKhoaHoanTatKham +
            ", maPhongHoanTatKham=" + maPhongHoanTatKham +
            ", tenKhoaHoanTatKham='" + tenKhoaHoanTatKham + '\'' +
            ", tenPhongHoanTatKham='" + tenPhongHoanTatKham + '\'' +
            ", thoiGianHoanTatKham=" + thoiGianHoanTatKham +
            ", soBenhAn='" + soBenhAn + '\'' +
            ", soBenhAnKhoa='" + soBenhAnKhoa + '\'' +
            ", nam=" + nam +
            ", benhNhan=" + benhNhan +
            ", donVi=" + donVi +
            ", nhanVienTiepNhan=" + nhanVienTiepNhan +
            ", phongTiepNhan=" + phongTiepNhan +
            '}';
    }
}
