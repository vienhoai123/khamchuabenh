package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.math.BigDecimal;

/**
 * A XetNghiem.
 */
@Entity
@Table(name = "xet_nghiem")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class XetNghiem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Cận dưới nam
     */
    @NotNull
    @Column(name = "can_duoi_nam", precision = 21, scale = 2, nullable = false)
    private BigDecimal canDuoiNam;

    /**
     * Cận dưới nữ
     */
    @NotNull
    @Column(name = "can_duoi_nu", precision = 21, scale = 2, nullable = false)
    private BigDecimal canDuoiNu;

    /**
     * Cận trên nam
     */
    @NotNull
    @Column(name = "can_tren_nam", precision = 21, scale = 2, nullable = false)
    private BigDecimal canTrenNam;

    /**
     * Cận trên nữ
     */
    @NotNull
    @Column(name = "can_tren_nu", precision = 21, scale = 2, nullable = false)
    private BigDecimal canTrenNu;

    /**
     * Chỉ số bình thường nam
     */
    @Size(max = 100)
    @Column(name = "chi_so_binh_thuong_nam", length = 100)
    private String chiSoBinhThuongNam;

    /**
     * Chỉ số max
     */
    @NotNull
    @Column(name = "chi_so_max", precision = 21, scale = 2, nullable = false)
    private BigDecimal chiSoMax;

    /**
     * Chỉ số min
     */
    @NotNull
    @Column(name = "chi_so_min", precision = 21, scale = 2, nullable = false)
    private BigDecimal chiSoMin;

    /**
     * Công thức
     */
    @Size(max = 100)
    @Column(name = "cong_thuc", length = 100)
    private String congThuc;

    /**
     * Trạng thái xóa bỏ của dòng dữ liệu. \n1: Đã xóa. \n0: Còn hiệu lực.
     */
    @NotNull
    @Column(name = "deleted", nullable = false)
    private Boolean deleted;

    /**
     * Độ pha loãng
     */
    @Size(max = 10)
    @Column(name = "do_pha_loang", length = 10)
    private String doPhaLoang;

    /**
     * Đơn giá bệnh viện
     */
    @NotNull
    @Column(name = "don_gia_benh_vien", precision = 21, scale = 2, nullable = false)
    private BigDecimal donGiaBenhVien;

    /**
     * Đơn vị tính
     */
    @Size(max = 30)
    @Column(name = "don_vi_tinh", length = 30)
    private String donViTinh;

    /**
     * Trạng thái có hiệu lực của dịch vụ: \r\n1: Còn hiệu lực. \r\n0: Đã ẩn.
     */
    @NotNull
    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    /**
     * Số lượng giới hạn của 1 chỉ định
     */
    @Column(name = "gioi_han_chi_dinh", precision = 21, scale = 2)
    private BigDecimal gioiHanChiDinh;

    /**
     * Kết quả bất thường
     */
    @Size(max = 100)
    @Column(name = "ket_qua_bat_thuong", length = 100)
    private String ketQuaBatThuong;

    /**
     * Mã dùng chung
     */
    @Size(max = 45)
    @Column(name = "ma_dung_chung", length = 45)
    private String maDungChung;

    /**
     * Kết quả mặc định
     */
    @Size(max = 500)
    @Column(name = "ket_qua_mac_dinh", length = 500)
    private String ketQuaMacDinh;

    /**
     * Trạng thái phạm vi của chỉ định:\r\n0: Chỉ có thu phí. \r\n1: Có cả BHYT và thu phí
     */
    @NotNull
    @Column(name = "pham_vi_chi_dinh", nullable = false)
    private Boolean phamViChiDinh;

    /**
     * Trạng thái phân biệt theo giới tính của chỉ định
     */
    @NotNull
    @Column(name = "phan_theo_gioi_t_inh", nullable = false)
    private Boolean phanTheoGioiTInh;

    /**
     * Làm tròn kết quả đến bao nhiêu chữ số lẻ
     */
    @NotNull
    @Column(name = "so_le_lam_tron", nullable = false)
    private Integer soLeLamTron;

    /**
     * Số lần thực hiện trong ngày
     */
    @NotNull
    @Column(name = "so_luong_thuc_hien", precision = 21, scale = 2, nullable = false)
    private BigDecimal soLuongThucHien;

    /**
     * Tên của chỉ định
     */
    @NotNull
    @Size(max = 500)
    @Column(name = "ten", length = 500, nullable = false)
    private String ten;

    /**
     * Tên hiển thị của chỉ định
     */
    @NotNull
    @Size(max = 500)
    @Column(name = "ten_hien_thi", length = 500, nullable = false)
    private String tenHienThi;

    /**
     * Mã nội bộ
     */
    @NotNull
    @Size(max = 255)
    @Column(name = "ma_noi_bo", length = 255, nullable = false)
    private String maNoiBo;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("XNS")
    private DonVi donVi;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("xetNghiems")
    private DotThayDoiMaDichVu dotMa;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("xetNghiems")
    private LoaiXetNghiem loaiXN;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getCanDuoiNam() {
        return canDuoiNam;
    }

    public XetNghiem canDuoiNam(BigDecimal canDuoiNam) {
        this.canDuoiNam = canDuoiNam;
        return this;
    }

    public void setCanDuoiNam(BigDecimal canDuoiNam) {
        this.canDuoiNam = canDuoiNam;
    }

    public BigDecimal getCanDuoiNu() {
        return canDuoiNu;
    }

    public XetNghiem canDuoiNu(BigDecimal canDuoiNu) {
        this.canDuoiNu = canDuoiNu;
        return this;
    }

    public void setCanDuoiNu(BigDecimal canDuoiNu) {
        this.canDuoiNu = canDuoiNu;
    }

    public BigDecimal getCanTrenNam() {
        return canTrenNam;
    }

    public XetNghiem canTrenNam(BigDecimal canTrenNam) {
        this.canTrenNam = canTrenNam;
        return this;
    }

    public void setCanTrenNam(BigDecimal canTrenNam) {
        this.canTrenNam = canTrenNam;
    }

    public BigDecimal getCanTrenNu() {
        return canTrenNu;
    }

    public XetNghiem canTrenNu(BigDecimal canTrenNu) {
        this.canTrenNu = canTrenNu;
        return this;
    }

    public void setCanTrenNu(BigDecimal canTrenNu) {
        this.canTrenNu = canTrenNu;
    }

    public String getChiSoBinhThuongNam() {
        return chiSoBinhThuongNam;
    }

    public XetNghiem chiSoBinhThuongNam(String chiSoBinhThuongNam) {
        this.chiSoBinhThuongNam = chiSoBinhThuongNam;
        return this;
    }

    public void setChiSoBinhThuongNam(String chiSoBinhThuongNam) {
        this.chiSoBinhThuongNam = chiSoBinhThuongNam;
    }

    public BigDecimal getChiSoMax() {
        return chiSoMax;
    }

    public XetNghiem chiSoMax(BigDecimal chiSoMax) {
        this.chiSoMax = chiSoMax;
        return this;
    }

    public void setChiSoMax(BigDecimal chiSoMax) {
        this.chiSoMax = chiSoMax;
    }

    public BigDecimal getChiSoMin() {
        return chiSoMin;
    }

    public XetNghiem chiSoMin(BigDecimal chiSoMin) {
        this.chiSoMin = chiSoMin;
        return this;
    }

    public void setChiSoMin(BigDecimal chiSoMin) {
        this.chiSoMin = chiSoMin;
    }

    public String getCongThuc() {
        return congThuc;
    }

    public XetNghiem congThuc(String congThuc) {
        this.congThuc = congThuc;
        return this;
    }

    public void setCongThuc(String congThuc) {
        this.congThuc = congThuc;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public XetNghiem deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getDoPhaLoang() {
        return doPhaLoang;
    }

    public XetNghiem doPhaLoang(String doPhaLoang) {
        this.doPhaLoang = doPhaLoang;
        return this;
    }

    public void setDoPhaLoang(String doPhaLoang) {
        this.doPhaLoang = doPhaLoang;
    }

    public BigDecimal getDonGiaBenhVien() {
        return donGiaBenhVien;
    }

    public XetNghiem donGiaBenhVien(BigDecimal donGiaBenhVien) {
        this.donGiaBenhVien = donGiaBenhVien;
        return this;
    }

    public void setDonGiaBenhVien(BigDecimal donGiaBenhVien) {
        this.donGiaBenhVien = donGiaBenhVien;
    }

    public String getDonViTinh() {
        return donViTinh;
    }

    public XetNghiem donViTinh(String donViTinh) {
        this.donViTinh = donViTinh;
        return this;
    }

    public void setDonViTinh(String donViTinh) {
        this.donViTinh = donViTinh;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public XetNghiem enabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public BigDecimal getGioiHanChiDinh() {
        return gioiHanChiDinh;
    }

    public XetNghiem gioiHanChiDinh(BigDecimal gioiHanChiDinh) {
        this.gioiHanChiDinh = gioiHanChiDinh;
        return this;
    }

    public void setGioiHanChiDinh(BigDecimal gioiHanChiDinh) {
        this.gioiHanChiDinh = gioiHanChiDinh;
    }

    public String getKetQuaBatThuong() {
        return ketQuaBatThuong;
    }

    public XetNghiem ketQuaBatThuong(String ketQuaBatThuong) {
        this.ketQuaBatThuong = ketQuaBatThuong;
        return this;
    }

    public void setKetQuaBatThuong(String ketQuaBatThuong) {
        this.ketQuaBatThuong = ketQuaBatThuong;
    }

    public String getMaDungChung() {
        return maDungChung;
    }

    public XetNghiem maDungChung(String maDungChung) {
        this.maDungChung = maDungChung;
        return this;
    }

    public void setMaDungChung(String maDungChung) {
        this.maDungChung = maDungChung;
    }

    public String getKetQuaMacDinh() {
        return ketQuaMacDinh;
    }

    public XetNghiem ketQuaMacDinh(String ketQuaMacDinh) {
        this.ketQuaMacDinh = ketQuaMacDinh;
        return this;
    }

    public void setKetQuaMacDinh(String ketQuaMacDinh) {
        this.ketQuaMacDinh = ketQuaMacDinh;
    }

    public Boolean isPhamViChiDinh() {
        return phamViChiDinh;
    }

    public XetNghiem phamViChiDinh(Boolean phamViChiDinh) {
        this.phamViChiDinh = phamViChiDinh;
        return this;
    }

    public void setPhamViChiDinh(Boolean phamViChiDinh) {
        this.phamViChiDinh = phamViChiDinh;
    }

    public Boolean isPhanTheoGioiTInh() {
        return phanTheoGioiTInh;
    }

    public XetNghiem phanTheoGioiTInh(Boolean phanTheoGioiTInh) {
        this.phanTheoGioiTInh = phanTheoGioiTInh;
        return this;
    }

    public void setPhanTheoGioiTInh(Boolean phanTheoGioiTInh) {
        this.phanTheoGioiTInh = phanTheoGioiTInh;
    }

    public Integer getSoLeLamTron() {
        return soLeLamTron;
    }

    public XetNghiem soLeLamTron(Integer soLeLamTron) {
        this.soLeLamTron = soLeLamTron;
        return this;
    }

    public void setSoLeLamTron(Integer soLeLamTron) {
        this.soLeLamTron = soLeLamTron;
    }

    public BigDecimal getSoLuongThucHien() {
        return soLuongThucHien;
    }

    public XetNghiem soLuongThucHien(BigDecimal soLuongThucHien) {
        this.soLuongThucHien = soLuongThucHien;
        return this;
    }

    public void setSoLuongThucHien(BigDecimal soLuongThucHien) {
        this.soLuongThucHien = soLuongThucHien;
    }

    public String getTen() {
        return ten;
    }

    public XetNghiem ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getTenHienThi() {
        return tenHienThi;
    }

    public XetNghiem tenHienThi(String tenHienThi) {
        this.tenHienThi = tenHienThi;
        return this;
    }

    public void setTenHienThi(String tenHienThi) {
        this.tenHienThi = tenHienThi;
    }

    public String getMaNoiBo() {
        return maNoiBo;
    }

    public XetNghiem maNoiBo(String maNoiBo) {
        this.maNoiBo = maNoiBo;
        return this;
    }

    public void setMaNoiBo(String maNoiBo) {
        this.maNoiBo = maNoiBo;
    }

    public DonVi getDonVi() {
        return donVi;
    }

    public XetNghiem donVi(DonVi donVi) {
        this.donVi = donVi;
        return this;
    }

    public void setDonVi(DonVi donVi) {
        this.donVi = donVi;
    }

    public DotThayDoiMaDichVu getDotMa() {
        return dotMa;
    }

    public XetNghiem dotMa(DotThayDoiMaDichVu dotThayDoiMaDichVu) {
        this.dotMa = dotThayDoiMaDichVu;
        return this;
    }

    public void setDotMa(DotThayDoiMaDichVu dotThayDoiMaDichVu) {
        this.dotMa = dotThayDoiMaDichVu;
    }

    public LoaiXetNghiem getLoaiXN() {
        return loaiXN;
    }

    public XetNghiem loaiXN(LoaiXetNghiem loaiXetNghiem) {
        this.loaiXN = loaiXetNghiem;
        return this;
    }

    public void setLoaiXN(LoaiXetNghiem loaiXetNghiem) {
        this.loaiXN = loaiXetNghiem;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof XetNghiem)) {
            return false;
        }
        return id != null && id.equals(((XetNghiem) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "XetNghiem{" +
            "id=" + getId() +
            ", canDuoiNam=" + getCanDuoiNam() +
            ", canDuoiNu=" + getCanDuoiNu() +
            ", canTrenNam=" + getCanTrenNam() +
            ", canTrenNu=" + getCanTrenNu() +
            ", chiSoBinhThuongNam='" + getChiSoBinhThuongNam() + "'" +
            ", chiSoMax=" + getChiSoMax() +
            ", chiSoMin=" + getChiSoMin() +
            ", congThuc='" + getCongThuc() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", doPhaLoang='" + getDoPhaLoang() + "'" +
            ", donGiaBenhVien=" + getDonGiaBenhVien() +
            ", donViTinh='" + getDonViTinh() + "'" +
            ", enabled='" + isEnabled() + "'" +
            ", gioiHanChiDinh=" + getGioiHanChiDinh() +
            ", ketQuaBatThuong='" + getKetQuaBatThuong() + "'" +
            ", maDungChung='" + getMaDungChung() + "'" +
            ", ketQuaMacDinh='" + getKetQuaMacDinh() + "'" +
            ", phamViChiDinh='" + isPhamViChiDinh() + "'" +
            ", phanTheoGioiTInh='" + isPhanTheoGioiTInh() + "'" +
            ", soLeLamTron=" + getSoLeLamTron() +
            ", soLuongThucHien=" + getSoLuongThucHien() +
            ", ten='" + getTen() + "'" +
            ", tenHienThi='" + getTenHienThi() + "'" +
            ", maNoiBo='" + getMaNoiBo() + "'" +
            "}";
    }
}
