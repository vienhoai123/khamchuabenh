package vn.vnpt.domain;
import sun.jvm.hotspot.debugger.LongHashMap;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Objects;

@Embeddable
public class DotDieuTriId implements java.io.Serializable {

//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
//    @SequenceGenerator(name = "sequenceGenerator")
//    @Column(name = "id")
    private Long id;
    private Long bakbId;
    private Long benhNhanId;
    private Long donViId;

    public DotDieuTriId(){}

    public DotDieuTriId(Long id, BenhAnKhamBenhId bakbCId) {
        this.id = id;
        this.bakbId = bakbCId.getId();
        this.benhNhanId = bakbCId.getBenhNhanId();
        this.donViId = bakbCId.getDonViId();
    }

    public DotDieuTriId(Long id, Long bakbId, Long benhNhanId, Long donViId) {
        this.id = id;
        this.bakbId = bakbId;
        this.benhNhanId = benhNhanId;
        this.donViId = donViId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBakbId() {
        return bakbId;
    }

    public void setBakbId(Long bakbId) {
        this.bakbId = bakbId;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DotDieuTriId)) return false;
        DotDieuTriId that = (DotDieuTriId) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getBakbId(), that.getBakbId()) &&
            Objects.equals(getBenhNhanId(), that.getBenhNhanId()) &&
            Objects.equals(getDonViId(), that.getDonViId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBakbId(), getBenhNhanId(), getDonViId());
    }
}
