//package vn.vnpt.domain.custom.types;
//
//import org.hibernate.HibernateException;
//import org.hibernate.engine.spi.SharedSessionContractImplementor;
//import org.hibernate.usertype.UserType;
//import vn.vnpt.domain.custom.keys.BenhAnKhamBenhPrimaryKey;
//
//import java.io.Serializable;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Types;
//import java.util.Objects;
//
//public class BenhAnKhamBenhType implements UserType {
//    @Override
//    public int[] sqlTypes() {
//        return new int[] {Types.LONGNVARCHAR, Types.LONGNVARCHAR};
//    }
//
//    @Override
//    public Class returnedClass() {
//        return BenhAnKhamBenhPrimaryKey.class;
//    }
//
//    @Override
//    public boolean equals(Object x, Object y) throws HibernateException {
//        if (x == y)
//            return true;
//        if (Objects.isNull(x) || Objects.isNull(y))
//            return false;
//        return x.equals(y);
//    }
//
//    @Override
//    public int hashCode(Object x) throws HibernateException {
//        return x.hashCode();
//    }
//
//    @Override
//    public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner) throws HibernateException, SQLException {
//
//
//        if (rs.wasNull())
//            return null;
//
//        Long benhNhanId = rs.getLong(names[0]);
//        Long donViId = rs.getLong(names[1]);
//
//        return new BenhAnKhamBenhPrimaryKey( benhNhanId, donViId);
//    }
//
//    @Override
//    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session) throws HibernateException, SQLException {
//        if (Objects.isNull(value)) {
//            st.setNull(index, Types.INTEGER);
//        } else {
//            BenhAnKhamBenhForeignKey benhAnKhamBenhForeignKey= (BenhAnKhamBenhForeignKey) value;
//            st.setLong(index,benhAnKhamBenhForeignKey.getBenhNhanId());
//            st.setLong(index+1,benhAnKhamBenhForeignKey.getDonViId());
//        }
//    }
//
//    @Override
//    public Object deepCopy(Object value) throws HibernateException {
//        if (Objects.isNull(value))
//            return null;
//        BenhAnKhamBenhForeignKey benhAnKhamBenhKey = (BenhAnKhamBenhForeignKey) value;
//        return new BenhAnKhamBenhForeignKey(benhAnKhamBenhKey.getBenhNhanId(), benhAnKhamBenhKey.getDonViId());
//    }
//
//    @Override
//    public boolean isMutable() {
//        return false;
//    }
//
//    @Override
//    public Serializable disassemble(Object value) throws HibernateException {
//        return (Serializable) value;
//    }
//
//    @Override
//    public Object assemble(Serializable cached, Object owner) throws HibernateException {
//        return cached;
//    }
//
//    @Override
//    public Object replace(Object original, Object target, Object owner) throws HibernateException {
//        return original;
//    }
//}
