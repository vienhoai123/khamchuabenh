package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A BenhNhan.
 */
@Entity
@Table(name = "benh_nhan")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class BenhNhan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Trạng thái thông tin bệnh nhân chỉ có năm sinh:\n1: Chỉ có năm sinh.\n0: Đầy đủ ngày tháng năm sinh.
     */
    @NotNull
    @Column(name = "chi_co_nam_sinh", nullable = false)
    private Boolean chiCoNamSinh;

    /**
     * Thông tin chứng minh nhân dân của bệnh nhân
     */
    @Size(max = 20)
    @Column(name = "cmnd", length = 20)
    private String cmnd;

    /**
     * Thông tin địa chỉ email của bệnh nhân
     */
    @Size(max = 255)
    @Column(name = "email", length = 255)
    private String email;

    /**
     * Trạng thái của 1 dòng thông tin bệnh nhân\n\n1: Có hiệu lực.\n\n0: Không có hiệu lực.
     */
    @NotNull
    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    /**
     * Trạng thái giới tính của bệnh nhân:\n\n1: Nam\n.\n0: Nữ.
     */
    @NotNull
    @Column(name = "gioi_tinh", nullable = false)
    private Integer gioiTinh;

    /**
     * Thông tin kháng thể của bệnh nhân
     */
    @Size(max = 5)
    @Column(name = "khang_the", length = 5)
    private String khangThe;

    /**
     * Thông tin mã số thuế của bệnh nhân
     */
    @Size(max = 100)
    @Column(name = "ma_so_thue", length = 100)
    private String maSoThue;

    /**
     * Ngày cấp chứng minh nhân dân của bệnh nhân
     */
    @Column(name = "ngay_cap_cmnd")
    private LocalDate ngayCapCmnd;

    /**
     * Thông tin ngày sinh của bệnh nhân
     */
    @NotNull
    @Column(name = "ngay_sinh", nullable = false)
    private LocalDate ngaySinh;

    /**
     * Thông tin nhóm máu của bệnh nhân
     */
    @Size(max = 5)
    @Column(name = "nhom_mau", length = 5)
    private String nhomMau;

    /**
     * Thông tin nơi cấp chứng minh nhân dân
     */
    @Size(max = 500)
    @Column(name = "noi_cap_cmnd", length = 500)
    private String noiCapCmnd;

    /**
     * Thông tin nơi làm việc của bệnh nhân
     */
    @Size(max = 500)
    @Column(name = "noi_lam_viec", length = 500)
    private String noiLamViec;

    /**
     * Thông tin số điện thoại của bệnh nhân
     */
    @Size(max = 15)
    @Column(name = "phone", length = 15)
    private String phone;

    /**
     * Tên bệnh nhân
     */
    @NotNull
    @Size(max = 200)
    @Column(name = "ten", length = 200, nullable = false)
    private String ten;

    /**
     * Số nhà của bệnh nhân
     */
    @Size(max = 255)
    @Column(name = "so_nha_xom", length = 255)
    private String soNhaXom;

    /**
     * Thông tin ấp, thôn, xóm của bệnh nhân
     */
    @Size(max = 255)
    @Column(name = "ap_thon", length = 255)
    private String apThon;

    /**
     * Mã danh mục địa phương
     */
    @Column(name = "dia_phuong_id")
    private Long diaPhuongId;

    /**
     * Thông tin địa chỉ thường trú của bệnh nhân
     */
    @NotNull
    @Size(max = 500)
    @Column(name = "dia_chi_thuong_tru", length = 500, nullable = false)
    private String diaChiThuongTru;

    /**
     * Mã dân tộc
     */
    @ManyToOne
    @JsonIgnoreProperties(value = "benhNhans", allowSetters = true)
    private DanToc danToc;

    /**
     * Mã nghề nghiệp
     */
    @ManyToOne
    @JsonIgnoreProperties(value = "benhNhans", allowSetters = true)
    private NgheNghiep ngheNghiep;

    /**
     * Mã quốc tịch
     */
    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "benhNhans", allowSetters = true)
    private Country quocTich;

    /**
     * Mã phường xã
     */
    @ManyToOne
    @JsonIgnoreProperties(value = "benhNhans", allowSetters = true)
    private PhuongXa phuongXa;

    /**
     * Mã quận huyện
     */
    @ManyToOne
    @JsonIgnoreProperties(value = "benhNhans", allowSetters = true)
    private QuanHuyen quanHuyen;

    /**
     * Mã tỉnh thành phố
     */
    @ManyToOne
    @JsonIgnoreProperties(value = "benhNhans", allowSetters = true)
    private TinhThanhPho tinhThanhPho;

    @ManyToOne
    @JsonIgnoreProperties(value = "benhNhans", allowSetters = true)
    private UserExtra userExtra;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isChiCoNamSinh() {
        return chiCoNamSinh;
    }

    public BenhNhan chiCoNamSinh(Boolean chiCoNamSinh) {
        this.chiCoNamSinh = chiCoNamSinh;
        return this;
    }

    public void setChiCoNamSinh(Boolean chiCoNamSinh) {
        this.chiCoNamSinh = chiCoNamSinh;
    }

    public String getCmnd() {
        return cmnd;
    }

    public BenhNhan cmnd(String cmnd) {
        this.cmnd = cmnd;
        return this;
    }

    public void setCmnd(String cmnd) {
        this.cmnd = cmnd;
    }

    public String getEmail() {
        return email;
    }

    public BenhNhan email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public BenhNhan enabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getGioiTinh() {
        return gioiTinh;
    }

    public BenhNhan gioiTinh(Integer gioiTinh) {
        this.gioiTinh = gioiTinh;
        return this;
    }

    public void setGioiTinh(Integer gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public String getKhangThe() {
        return khangThe;
    }

    public BenhNhan khangThe(String khangThe) {
        this.khangThe = khangThe;
        return this;
    }

    public void setKhangThe(String khangThe) {
        this.khangThe = khangThe;
    }

    public String getMaSoThue() {
        return maSoThue;
    }

    public BenhNhan maSoThue(String maSoThue) {
        this.maSoThue = maSoThue;
        return this;
    }

    public void setMaSoThue(String maSoThue) {
        this.maSoThue = maSoThue;
    }

    public LocalDate getNgayCapCmnd() {
        return ngayCapCmnd;
    }

    public BenhNhan ngayCapCmnd(LocalDate ngayCapCmnd) {
        this.ngayCapCmnd = ngayCapCmnd;
        return this;
    }

    public void setNgayCapCmnd(LocalDate ngayCapCmnd) {
        this.ngayCapCmnd = ngayCapCmnd;
    }

    public LocalDate getNgaySinh() {
        return ngaySinh;
    }

    public BenhNhan ngaySinh(LocalDate ngaySinh) {
        this.ngaySinh = ngaySinh;
        return this;
    }

    public void setNgaySinh(LocalDate ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getNhomMau() {
        return nhomMau;
    }

    public BenhNhan nhomMau(String nhomMau) {
        this.nhomMau = nhomMau;
        return this;
    }

    public void setNhomMau(String nhomMau) {
        this.nhomMau = nhomMau;
    }

    public String getNoiCapCmnd() {
        return noiCapCmnd;
    }

    public BenhNhan noiCapCmnd(String noiCapCmnd) {
        this.noiCapCmnd = noiCapCmnd;
        return this;
    }

    public void setNoiCapCmnd(String noiCapCmnd) {
        this.noiCapCmnd = noiCapCmnd;
    }

    public String getNoiLamViec() {
        return noiLamViec;
    }

    public BenhNhan noiLamViec(String noiLamViec) {
        this.noiLamViec = noiLamViec;
        return this;
    }

    public void setNoiLamViec(String noiLamViec) {
        this.noiLamViec = noiLamViec;
    }

    public String getPhone() {
        return phone;
    }

    public BenhNhan phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTen() {
        return ten;
    }

    public BenhNhan ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getSoNhaXom() {
        return soNhaXom;
    }

    public BenhNhan soNhaXom(String soNhaXom) {
        this.soNhaXom = soNhaXom;
        return this;
    }

    public void setSoNhaXom(String soNhaXom) {
        this.soNhaXom = soNhaXom;
    }

    public String getApThon() {
        return apThon;
    }

    public BenhNhan apThon(String apThon) {
        this.apThon = apThon;
        return this;
    }

    public void setApThon(String apThon) {
        this.apThon = apThon;
    }

    public Long getDiaPhuongId() {
        return diaPhuongId;
    }

    public BenhNhan diaPhuongId(Long diaPhuongId) {
        this.diaPhuongId = diaPhuongId;
        return this;
    }

    public void setDiaPhuongId(Long diaPhuongId) {
        this.diaPhuongId = diaPhuongId;
    }

    public String getDiaChiThuongTru() {
        return diaChiThuongTru;
    }

    public BenhNhan diaChiThuongTru(String diaChiThuongTru) {
        this.diaChiThuongTru = diaChiThuongTru;
        return this;
    }

    public void setDiaChiThuongTru(String diaChiThuongTru) {
        this.diaChiThuongTru = diaChiThuongTru;
    }

    public DanToc getDanToc() {
        return danToc;
    }

    public BenhNhan danToc(DanToc danToc) {
        this.danToc = danToc;
        return this;
    }

    public void setDanToc(DanToc danToc) {
        this.danToc = danToc;
    }

    public NgheNghiep getNgheNghiep() {
        return ngheNghiep;
    }

    public BenhNhan ngheNghiep(NgheNghiep ngheNghiep) {
        this.ngheNghiep = ngheNghiep;
        return this;
    }

    public void setNgheNghiep(NgheNghiep ngheNghiep) {
        this.ngheNghiep = ngheNghiep;
    }

    public Country getQuocTich() {
        return quocTich;
    }

    public BenhNhan quocTich(Country country) {
        this.quocTich = country;
        return this;
    }

    public void setQuocTich(Country country) {
        this.quocTich = country;
    }

    public PhuongXa getPhuongXa() {
        return phuongXa;
    }

    public BenhNhan phuongXa(PhuongXa phuongXa) {
        this.phuongXa = phuongXa;
        return this;
    }

    public void setPhuongXa(PhuongXa phuongXa) {
        this.phuongXa = phuongXa;
    }

    public QuanHuyen getQuanHuyen() {
        return quanHuyen;
    }

    public BenhNhan quanHuyen(QuanHuyen quanHuyen) {
        this.quanHuyen = quanHuyen;
        return this;
    }

    public void setQuanHuyen(QuanHuyen quanHuyen) {
        this.quanHuyen = quanHuyen;
    }

    public TinhThanhPho getTinhThanhPho() {
        return tinhThanhPho;
    }

    public BenhNhan tinhThanhPho(TinhThanhPho tinhThanhPho) {
        this.tinhThanhPho = tinhThanhPho;
        return this;
    }

    public void setTinhThanhPho(TinhThanhPho tinhThanhPho) {
        this.tinhThanhPho = tinhThanhPho;
    }

    public UserExtra getUserExtra() {
        return userExtra;
    }

    public BenhNhan userExtra(UserExtra userExtra) {
        this.userExtra = userExtra;
        return this;
    }

    public void setUserExtra(UserExtra userExtra) {
        this.userExtra = userExtra;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BenhNhan)) {
            return false;
        }
        return id != null && id.equals(((BenhNhan) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BenhNhan{" +
            "id=" + getId() +
            ", chiCoNamSinh='" + isChiCoNamSinh() + "'" +
            ", cmnd='" + getCmnd() + "'" +
            ", email='" + getEmail() + "'" +
            ", enabled='" + isEnabled() + "'" +
            ", gioiTinh=" + getGioiTinh() +
            ", khangThe='" + getKhangThe() + "'" +
            ", maSoThue='" + getMaSoThue() + "'" +
            ", ngayCapCmnd='" + getNgayCapCmnd() + "'" +
            ", ngaySinh='" + getNgaySinh() + "'" +
            ", nhomMau='" + getNhomMau() + "'" +
            ", noiCapCmnd='" + getNoiCapCmnd() + "'" +
            ", noiLamViec='" + getNoiLamViec() + "'" +
            ", phone='" + getPhone() + "'" +
            ", ten='" + getTen() + "'" +
            ", soNhaXom='" + getSoNhaXom() + "'" +
            ", apThon='" + getApThon() + "'" +
            ", diaPhuongId=" + getDiaPhuongId() +
            ", diaChiThuongTru='" + getDiaChiThuongTru() + "'" +
            "}";
    }
}
