package vn.vnpt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.LocalDate;

/**
 * NDB_TABLE=READ_BACKUP=1 Đợt điều trị theo
 */
@Entity
@Table(name = "dot_dieu_tri")
@IdClass(DotDieuTriId.class)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DotDieuTri implements Serializable {

   // private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Id
    @Column(name = "bakb_id")
    private Long bakbId;

    @Id
    @Column(name = "benh_nhan_id")
    private Long benhNhanId;

    @Id
    @Column(name = "don_vi_id")
    private Long donViId;



    @Column(name = "so_thu_tu")
    private String soThuTu;

    /**
     * Giá tiền cận trên BHYT
     */
    @Column(name = "bhyt_can_tren")
    private Integer bhytCanTren;

    /**
     * Giá tiền cận trên BHYT quy định cho các Dịch vụ kỹ thuật cao
     */
    @Column(name = "bhyt_can_tren_ktc")
    private Integer bhytCanTrenKtc;

    /**
     * Thông tin mã khu vực của thẻ BHYT
     */
    @Column(name = "bhyt_ma_khu_vuc")
    private String bhytMaKhuVuc;

    /**
     * Thời gian có hiệu lực của BHYT
     */
    @Column(name = "bhyt_ngay_bat_dau")
    private LocalDate bhytNgayBatDau;

    /**
     * Thời gian được hưởng quyền lợi bảo hiểm 5 năm
     */
    @Column(name = "bhyt_ngay_du_nam_nam")
    private LocalDate bhytNgayDuNamNam;

    /**
     * Thời gian hết hạn của thẻ BHYT
     */
    @Column(name = "bhyt_ngay_het_han")
    private LocalDate bhytNgayHetHan;

    /**
     * Trạng thái nội tỉnh của thẻ BHYT:\n1: Nội tỉnh\n0: Ngoại tỉnh
     */
    @Column(name = "bhyt_noi_tinh")
    private Boolean bhytNoiTinh;

    /**
     * Mã thẻ BHYT
     */
    @Size(max = 30)
    @Column(name = "bhyt_so_the", length = 30)
    private String bhytSoThe;

    /**
     * Tỷ lệ miễn giảm của thẻ BHYT
     */
    @Column(name = "bhyt_ty_le_mien_giam")
    private Integer bhytTyLeMienGiam;

    /**
     * Tỷ lệ miễn giảm cho các dịch vụ kỹ thuật cao của thẻ BHYT
     */
    @Column(name = "bhyt_ty_le_mien_giam_ktc")
    private Integer bhytTyLeMienGiamKtc;

    /**
     * Trạng thái có bảo hiểm của
     */
    @NotNull
    @Column(name = "co_bao_hiem", nullable = false)
    private Boolean coBaoHiem;

    /**
     * Thông tin địa chỉ BHYT của bệnh nhân
     */
    @Size(max = 500)
    @Column(name = "bhyt_dia_chi", length = 500)
    private String bhytDiaChi;

    /**
     * Tên đối tượng của thẻ BHYT
     */
    @Size(max = 200)
    @Column(name = "doi_tuong_bhyt_ten", length = 200)
    private String doiTuongBhytTen;

    /**
     * Trạng thái đúng tuyến của thẻ BHYT\n1: Đúng tuyến\n0: Không đúng tuyến
     */
    @Column(name = "dung_tuyen")
    private Boolean dungTuyen;

    /**
     * Mã giấy chứng sinh, giấy khai sinh....
     */
    @Size(max = 200)
    @Column(name = "giay_to_tre_em", length = 200)
    private String giayToTreEm;

    /**
     * Mã loại giấy tờ của trẻ em như giấy khai sinh, giấy chứng sinh
     */
    @Column(name = "loai_giay_to_tre_em")
    private Integer loaiGiayToTreEm;

    /**
     * Thời gian miễn cùng chi trả của thẻ BHYT
     */
    @Column(name = "ngay_mien_cung_chi_tra")
    private LocalDate ngayMienCungChiTra;

    /**
     * Thông tin thẻ TEKT
     */
    @Column(name = "the_tre_em")
    private Boolean theTreEm;

    /**
     * Trạng thái thông tuyến theo bhxh xml 4210:\n1: Thông tuyến\n0: không thông tuyến
     */
    @Column(name = "thong_tuyen_bhxh_xml_4210")
    private Boolean thongTuyenBhxhXml4210;

    /**
     * Trạng thái của Bệnh án khám bệnh:\n0: mới vào khoa, chờ khám;\n1: Điều trị, khám có giường;\n2: Điều trị, khám không giường;\n3: Xuất viện, hoàn tất khám;\n4: chuyển tuyến, chuyển viện;\n5: Nhập viện;\n6: tử vong;\n7: trốn viện;\n8: Kết thúc đợt ĐT, thêm đợt ĐT mới;
     */
    @Column(name = "trang_thai")
    private Integer trangThai;

    /**
     * Loại của bệnh án 1: ngoại trú. 2: Nội trú, 3: điều trị bệnh án ngoại trú
     */
    @Column(name = "loai")
    private Integer loai;

    /**
     * Nơi đăng ký khám chữa bệnh ban đầu
     */
    @Size(max = 30)
    @Column(name = "bhyt_noi_dk_kcbbd", length = 30)
    private String bhytNoiDkKcbbd;

    @NotNull
    @Column(name = "nam", nullable = false)
    private Integer nam;

    @ManyToOne(optional = false)
    @JoinColumns(
        {
            @JoinColumn(name = "bakb_id", referencedColumnName = "id", insertable = false, updatable = false),
            @JoinColumn(name = "benh_nhan_id", referencedColumnName = "benh_nhan_id", insertable = false, updatable = false),
            @JoinColumn(name = "don_vi_id", referencedColumnName = "don_vi_id", insertable = false, updatable = false)
        })
    @JsonIgnoreProperties("dotDieuTris")
    private BenhAnKhamBenh bakb;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBakbId() {
        return bakbId;
    }

    public void setBakbId(Long bakbId) {
        this.bakbId = bakbId;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    public Boolean getBhytNoiTinh() {
        return bhytNoiTinh;
    }

    public Boolean getCoBaoHiem() {
        return coBaoHiem;
    }

    public Boolean getDungTuyen() {
        return dungTuyen;
    }

    public Boolean getTheTreEm() {
        return theTreEm;
    }

    public Boolean getThongTuyenBhxhXml4210() {
        return thongTuyenBhxhXml4210;
    }

    public String getSoThuTu() {
        return soThuTu;
    }

    public DotDieuTri soThuTu(String soThuTu) {
        this.soThuTu = soThuTu;
        return this;
    }

    public void setSoThuTu(String soThuTu) {
        this.soThuTu = soThuTu;
    }

    public Integer getBhytCanTren() {
        return bhytCanTren;
    }

    public DotDieuTri bhytCanTren(Integer bhytCanTren) {
        this.bhytCanTren = bhytCanTren;
        return this;
    }

    public void setBhytCanTren(Integer bhytCanTren) {
        this.bhytCanTren = bhytCanTren;
    }

    public Integer getBhytCanTrenKtc() {
        return bhytCanTrenKtc;
    }

    public DotDieuTri bhytCanTrenKtc(Integer bhytCanTrenKtc) {
        this.bhytCanTrenKtc = bhytCanTrenKtc;
        return this;
    }

    public void setBhytCanTrenKtc(Integer bhytCanTrenKtc) {
        this.bhytCanTrenKtc = bhytCanTrenKtc;
    }

    public String getBhytMaKhuVuc() {
        return bhytMaKhuVuc;
    }

    public DotDieuTri bhytMaKhuVuc(String bhytMaKhuVuc) {
        this.bhytMaKhuVuc = bhytMaKhuVuc;
        return this;
    }

    public void setBhytMaKhuVuc(String bhytMaKhuVuc) {
        this.bhytMaKhuVuc = bhytMaKhuVuc;
    }

    public LocalDate getBhytNgayBatDau() {
        return bhytNgayBatDau;
    }

    public DotDieuTri bhytNgayBatDau(LocalDate bhytNgayBatDau) {
        this.bhytNgayBatDau = bhytNgayBatDau;
        return this;
    }

    public void setBhytNgayBatDau(LocalDate bhytNgayBatDau) {
        this.bhytNgayBatDau = bhytNgayBatDau;
    }

    public LocalDate getBhytNgayDuNamNam() {
        return bhytNgayDuNamNam;
    }

    public DotDieuTri bhytNgayDuNamNam(LocalDate bhytNgayDuNamNam) {
        this.bhytNgayDuNamNam = bhytNgayDuNamNam;
        return this;
    }

    public void setBhytNgayDuNamNam(LocalDate bhytNgayDuNamNam) {
        this.bhytNgayDuNamNam = bhytNgayDuNamNam;
    }

    public LocalDate getBhytNgayHetHan() {
        return bhytNgayHetHan;
    }

    public DotDieuTri bhytNgayHetHan(LocalDate bhytNgayHetHan) {
        this.bhytNgayHetHan = bhytNgayHetHan;
        return this;
    }

    public void setBhytNgayHetHan(LocalDate bhytNgayHetHan) {
        this.bhytNgayHetHan = bhytNgayHetHan;
    }

    public Boolean isBhytNoiTinh() {
        return bhytNoiTinh;
    }

    public DotDieuTri bhytNoiTinh(Boolean bhytNoiTinh) {
        this.bhytNoiTinh = bhytNoiTinh;
        return this;
    }

    public void setBhytNoiTinh(Boolean bhytNoiTinh) {
        this.bhytNoiTinh = bhytNoiTinh;
    }

    public String getBhytSoThe() {
        return bhytSoThe;
    }

    public DotDieuTri bhytSoThe(String bhytSoThe) {
        this.bhytSoThe = bhytSoThe;
        return this;
    }

    public void setBhytSoThe(String bhytSoThe) {
        this.bhytSoThe = bhytSoThe;
    }

    public Integer getBhytTyLeMienGiam() {
        return bhytTyLeMienGiam;
    }

    public DotDieuTri bhytTyLeMienGiam(Integer bhytTyLeMienGiam) {
        this.bhytTyLeMienGiam = bhytTyLeMienGiam;
        return this;
    }

    public void setBhytTyLeMienGiam(Integer bhytTyLeMienGiam) {
        this.bhytTyLeMienGiam = bhytTyLeMienGiam;
    }

    public Integer getBhytTyLeMienGiamKtc() {
        return bhytTyLeMienGiamKtc;
    }

    public DotDieuTri bhytTyLeMienGiamKtc(Integer bhytTyLeMienGiamKtc) {
        this.bhytTyLeMienGiamKtc = bhytTyLeMienGiamKtc;
        return this;
    }

    public void setBhytTyLeMienGiamKtc(Integer bhytTyLeMienGiamKtc) {
        this.bhytTyLeMienGiamKtc = bhytTyLeMienGiamKtc;
    }

    public Boolean isCoBaoHiem() {
        return coBaoHiem;
    }

    public DotDieuTri coBaoHiem(Boolean coBaoHiem) {
        this.coBaoHiem = coBaoHiem;
        return this;
    }

    public void setCoBaoHiem(Boolean coBaoHiem) {
        this.coBaoHiem = coBaoHiem;
    }

    public String getBhytDiaChi() {
        return bhytDiaChi;
    }

    public DotDieuTri bhytDiaChi(String bhytDiaChi) {
        this.bhytDiaChi = bhytDiaChi;
        return this;
    }

    public void setBhytDiaChi(String bhytDiaChi) {
        this.bhytDiaChi = bhytDiaChi;
    }

    public String getDoiTuongBhytTen() {
        return doiTuongBhytTen;
    }

    public DotDieuTri doiTuongBhytTen(String doiTuongBhytTen) {
        this.doiTuongBhytTen = doiTuongBhytTen;
        return this;
    }

    public void setDoiTuongBhytTen(String doiTuongBhytTen) {
        this.doiTuongBhytTen = doiTuongBhytTen;
    }

    public Boolean isDungTuyen() {
        return dungTuyen;
    }

    public DotDieuTri dungTuyen(Boolean dungTuyen) {
        this.dungTuyen = dungTuyen;
        return this;
    }

    public void setDungTuyen(Boolean dungTuyen) {
        this.dungTuyen = dungTuyen;
    }

    public String getGiayToTreEm() {
        return giayToTreEm;
    }

    public DotDieuTri giayToTreEm(String giayToTreEm) {
        this.giayToTreEm = giayToTreEm;
        return this;
    }

    public void setGiayToTreEm(String giayToTreEm) {
        this.giayToTreEm = giayToTreEm;
    }

    public Integer getLoaiGiayToTreEm() {
        return loaiGiayToTreEm;
    }

    public DotDieuTri loaiGiayToTreEm(Integer loaiGiayToTreEm) {
        this.loaiGiayToTreEm = loaiGiayToTreEm;
        return this;
    }

    public void setLoaiGiayToTreEm(Integer loaiGiayToTreEm) {
        this.loaiGiayToTreEm = loaiGiayToTreEm;
    }

    public LocalDate getNgayMienCungChiTra() {
        return ngayMienCungChiTra;
    }

    public DotDieuTri ngayMienCungChiTra(LocalDate ngayMienCungChiTra) {
        this.ngayMienCungChiTra = ngayMienCungChiTra;
        return this;
    }

    public void setNgayMienCungChiTra(LocalDate ngayMienCungChiTra) {
        this.ngayMienCungChiTra = ngayMienCungChiTra;
    }

    public Boolean isTheTreEm() {
        return theTreEm;
    }

    public DotDieuTri theTreEm(Boolean theTreEm) {
        this.theTreEm = theTreEm;
        return this;
    }

    public void setTheTreEm(Boolean theTreEm) {
        this.theTreEm = theTreEm;
    }

    public Boolean isThongTuyenBhxhXml4210() {
        return thongTuyenBhxhXml4210;
    }

    public DotDieuTri thongTuyenBhxhXml4210(Boolean thongTuyenBhxhXml4210) {
        this.thongTuyenBhxhXml4210 = thongTuyenBhxhXml4210;
        return this;
    }

    public void setThongTuyenBhxhXml4210(Boolean thongTuyenBhxhXml4210) {
        this.thongTuyenBhxhXml4210 = thongTuyenBhxhXml4210;
    }

    public Integer getTrangThai() {
        return trangThai;
    }

    public DotDieuTri trangThai(Integer trangThai) {
        this.trangThai = trangThai;
        return this;
    }

    public void setTrangThai(Integer trangThai) {
        this.trangThai = trangThai;
    }

    public Integer getLoai() {
        return loai;
    }

    public DotDieuTri loai(Integer loai) {
        this.loai = loai;
        return this;
    }

    public void setLoai(Integer loai) {
        this.loai = loai;
    }

    public String getBhytNoiDkKcbbd() {
        return bhytNoiDkKcbbd;
    }

    public DotDieuTri bhytNoiDkKcbbd(String bhytNoiDkKcbbd) {
        this.bhytNoiDkKcbbd = bhytNoiDkKcbbd;
        return this;
    }

    public void setBhytNoiDkKcbbd(String bhytNoiDkKcbbd) {
        this.bhytNoiDkKcbbd = bhytNoiDkKcbbd;
    }

    public Integer getNam() {
        return nam;
    }

    public DotDieuTri nam(Integer nam) {
        this.nam = nam;
        return this;
    }

    public void setNam(Integer nam) {
        this.nam = nam;
    }

    public BenhAnKhamBenh getBakb() {
        return bakb;
    }

    public DotDieuTri bakb(BenhAnKhamBenh benhAnKhamBenh) {
        this.bakb = benhAnKhamBenh;
        return this;
    }

    public void setBakb(BenhAnKhamBenh benhAnKhamBenh) {
        this.bakb = benhAnKhamBenh;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DotDieuTri)) return false;
        DotDieuTri that = (DotDieuTri) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getSoThuTu(), that.getSoThuTu()) &&
            Objects.equals(getBhytCanTren(), that.getBhytCanTren()) &&
            Objects.equals(getBhytCanTrenKtc(), that.getBhytCanTrenKtc()) &&
            Objects.equals(getBhytMaKhuVuc(), that.getBhytMaKhuVuc()) &&
            Objects.equals(getBhytNgayBatDau(), that.getBhytNgayBatDau()) &&
            Objects.equals(getBhytNgayDuNamNam(), that.getBhytNgayDuNamNam()) &&
            Objects.equals(getBhytNgayHetHan(), that.getBhytNgayHetHan()) &&
            Objects.equals(getBhytNoiTinh(), that.getBhytNoiTinh()) &&
            Objects.equals(getBhytSoThe(), that.getBhytSoThe()) &&
            Objects.equals(getBhytTyLeMienGiam(), that.getBhytTyLeMienGiam()) &&
            Objects.equals(getBhytTyLeMienGiamKtc(), that.getBhytTyLeMienGiamKtc()) &&
            Objects.equals(getCoBaoHiem(), that.getCoBaoHiem()) &&
            Objects.equals(getBhytDiaChi(), that.getBhytDiaChi()) &&
            Objects.equals(getDoiTuongBhytTen(), that.getDoiTuongBhytTen()) &&
            Objects.equals(getDungTuyen(), that.getDungTuyen()) &&
            Objects.equals(getGiayToTreEm(), that.getGiayToTreEm()) &&
            Objects.equals(getLoaiGiayToTreEm(), that.getLoaiGiayToTreEm()) &&
            Objects.equals(getNgayMienCungChiTra(), that.getNgayMienCungChiTra()) &&
            Objects.equals(getTheTreEm(), that.getTheTreEm()) &&
            Objects.equals(getThongTuyenBhxhXml4210(), that.getThongTuyenBhxhXml4210()) &&
            Objects.equals(getTrangThai(), that.getTrangThai()) &&
            Objects.equals(getLoai(), that.getLoai()) &&
            Objects.equals(getBhytNoiDkKcbbd(), that.getBhytNoiDkKcbbd()) &&
            Objects.equals(getNam(), that.getNam()) &&
            Objects.equals(getBakb(), that.getBakb());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getSoThuTu(), getBhytCanTren(), getBhytCanTrenKtc(), getBhytMaKhuVuc(), getBhytNgayBatDau(), getBhytNgayDuNamNam(), getBhytNgayHetHan(), getBhytNoiTinh(), getBhytSoThe(), getBhytTyLeMienGiam(), getBhytTyLeMienGiamKtc(), getCoBaoHiem(), getBhytDiaChi(), getDoiTuongBhytTen(), getDungTuyen(), getGiayToTreEm(), getLoaiGiayToTreEm(), getNgayMienCungChiTra(), getTheTreEm(), getThongTuyenBhxhXml4210(), getTrangThai(), getLoai(), getBhytNoiDkKcbbd(), getNam(), getBakb());
    }

    @Override
    public String toString() {
        return "DotDieuTri{" +
            "id=" + id +
            ", soThuTu='" + soThuTu + '\'' +
            ", bhytCanTren=" + bhytCanTren +
            ", bhytCanTrenKtc=" + bhytCanTrenKtc +
            ", bhytMaKhuVuc='" + bhytMaKhuVuc + '\'' +
            ", bhytNgayBatDau=" + bhytNgayBatDau +
            ", bhytNgayDuNamNam=" + bhytNgayDuNamNam +
            ", bhytNgayHetHan=" + bhytNgayHetHan +
            ", bhytNoiTinh=" + bhytNoiTinh +
            ", bhytSoThe='" + bhytSoThe + '\'' +
            ", bhytTyLeMienGiam=" + bhytTyLeMienGiam +
            ", bhytTyLeMienGiamKtc=" + bhytTyLeMienGiamKtc +
            ", coBaoHiem=" + coBaoHiem +
            ", bhytDiaChi='" + bhytDiaChi + '\'' +
            ", doiTuongBhytTen='" + doiTuongBhytTen + '\'' +
            ", dungTuyen=" + dungTuyen +
            ", giayToTreEm='" + giayToTreEm + '\'' +
            ", loaiGiayToTreEm=" + loaiGiayToTreEm +
            ", ngayMienCungChiTra=" + ngayMienCungChiTra +
            ", theTreEm=" + theTreEm +
            ", thongTuyenBhxhXml4210=" + thongTuyenBhxhXml4210 +
            ", trangThai=" + trangThai +
            ", loai=" + loai +
            ", bhytNoiDkKcbbd='" + bhytNoiDkKcbbd + '\'' +
            ", nam=" + nam +
            ", bakb=" + bakb +
            '}';
    }
}
