package vn.vnpt.web.rest;

import vn.vnpt.service.TheBhytService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.TheBhytDTO;
import vn.vnpt.service.dto.TheBhytCriteria;
import vn.vnpt.service.TheBhytQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.TheBhyt}.
 */
@RestController
@RequestMapping("/api")
public class TheBhytResource {

    private final Logger log = LoggerFactory.getLogger(TheBhytResource.class);

    private static final String ENTITY_NAME = "khamchuabenhTheBhyt";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TheBhytService theBhytService;

    private final TheBhytQueryService theBhytQueryService;

    public TheBhytResource(TheBhytService theBhytService, TheBhytQueryService theBhytQueryService) {
        this.theBhytService = theBhytService;
        this.theBhytQueryService = theBhytQueryService;
    }

    /**
     * {@code POST  /the-bhyts} : Create a new theBhyt.
     *
     * @param theBhytDTO the theBhytDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new theBhytDTO, or with status {@code 400 (Bad Request)} if the theBhyt has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/the-bhyts")
    public ResponseEntity<TheBhytDTO> createTheBhyt(@Valid @RequestBody TheBhytDTO theBhytDTO) throws URISyntaxException {
        log.debug("REST request to save TheBhyt : {}", theBhytDTO);
        if (theBhytDTO.getId() != null) {
            throw new BadRequestAlertException("A new theBhyt cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TheBhytDTO result = theBhytService.save(theBhytDTO);
        return ResponseEntity.created(new URI("/api/the-bhyts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /the-bhyts} : Updates an existing theBhyt.
     *
     * @param theBhytDTO the theBhytDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated theBhytDTO,
     * or with status {@code 400 (Bad Request)} if the theBhytDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the theBhytDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/the-bhyts")
    public ResponseEntity<TheBhytDTO> updateTheBhyt(@Valid @RequestBody TheBhytDTO theBhytDTO) throws URISyntaxException {
        log.debug("REST request to update TheBhyt : {}", theBhytDTO);
        if (theBhytDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TheBhytDTO result = theBhytService.save(theBhytDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, theBhytDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /the-bhyts} : get all the theBhyts.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of theBhyts in body.
     */
    @GetMapping("/the-bhyts")
    public ResponseEntity<List<TheBhytDTO>> getAllTheBhyts(TheBhytCriteria criteria, Pageable pageable) {
        log.debug("REST request to get TheBhyts by criteria: {}", criteria);
        Page<TheBhytDTO> page = theBhytQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /the-bhyts/count} : count all the theBhyts.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/the-bhyts/count")
    public ResponseEntity<Long> countTheBhyts(TheBhytCriteria criteria) {
        log.debug("REST request to count TheBhyts by criteria: {}", criteria);
        return ResponseEntity.ok().body(theBhytQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /the-bhyts/:id} : get the "id" theBhyt.
     *
     * @param id the id of the theBhytDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the theBhytDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/the-bhyts/{id}")
    public ResponseEntity<TheBhytDTO> getTheBhyt(@PathVariable Long id) {
        log.debug("REST request to get TheBhyt : {}", id);
        Optional<TheBhytDTO> theBhytDTO = theBhytService.findOne(id);
        return ResponseUtil.wrapOrNotFound(theBhytDTO);
    }

    /**
     * {@code DELETE  /the-bhyts/:id} : delete the "id" theBhyt.
     *
     * @param id the id of the theBhytDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/the-bhyts/{id}")
    public ResponseEntity<Void> deleteTheBhyt(@PathVariable Long id) {
        log.debug("REST request to delete TheBhyt : {}", id);
        theBhytService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
