package vn.vnpt.web.rest;

import vn.vnpt.service.DichVuKhamApDungService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.DichVuKhamApDungDTO;
import vn.vnpt.service.dto.DichVuKhamApDungCriteria;
import vn.vnpt.service.DichVuKhamApDungQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.DichVuKhamApDung}.
 */
@RestController
@RequestMapping("/api")
public class DichVuKhamApDungResource {

    private final Logger log = LoggerFactory.getLogger(DichVuKhamApDungResource.class);

    private static final String ENTITY_NAME = "khamchuabenhDichVuKhamApDung";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DichVuKhamApDungService dichVuKhamApDungService;

    private final DichVuKhamApDungQueryService dichVuKhamApDungQueryService;

    public DichVuKhamApDungResource(DichVuKhamApDungService dichVuKhamApDungService, DichVuKhamApDungQueryService dichVuKhamApDungQueryService) {
        this.dichVuKhamApDungService = dichVuKhamApDungService;
        this.dichVuKhamApDungQueryService = dichVuKhamApDungQueryService;
    }

    /**
     * {@code POST  /dich-vu-kham-ap-dungs} : Create a new dichVuKhamApDung.
     *
     * @param dichVuKhamApDungDTO the dichVuKhamApDungDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dichVuKhamApDungDTO, or with status {@code 400 (Bad Request)} if the dichVuKhamApDung has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/dich-vu-kham-ap-dungs")
    public ResponseEntity<DichVuKhamApDungDTO> createDichVuKhamApDung(@Valid @RequestBody DichVuKhamApDungDTO dichVuKhamApDungDTO) throws URISyntaxException {
        log.debug("REST request to save DichVuKhamApDung : {}", dichVuKhamApDungDTO);
        if (dichVuKhamApDungDTO.getId() != null) {
            throw new BadRequestAlertException("A new dichVuKhamApDung cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DichVuKhamApDungDTO result = dichVuKhamApDungService.save(dichVuKhamApDungDTO);
        return ResponseEntity.created(new URI("/api/dich-vu-kham-ap-dungs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /dich-vu-kham-ap-dungs} : Updates an existing dichVuKhamApDung.
     *
     * @param dichVuKhamApDungDTO the dichVuKhamApDungDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dichVuKhamApDungDTO,
     * or with status {@code 400 (Bad Request)} if the dichVuKhamApDungDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dichVuKhamApDungDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/dich-vu-kham-ap-dungs")
    public ResponseEntity<DichVuKhamApDungDTO> updateDichVuKhamApDung(@Valid @RequestBody DichVuKhamApDungDTO dichVuKhamApDungDTO) throws URISyntaxException {
        log.debug("REST request to update DichVuKhamApDung : {}", dichVuKhamApDungDTO);
        if (dichVuKhamApDungDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DichVuKhamApDungDTO result = dichVuKhamApDungService.save(dichVuKhamApDungDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /dich-vu-kham-ap-dungs} : get all the dichVuKhamApDungs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dichVuKhamApDungs in body.
     */
    @GetMapping("/dich-vu-kham-ap-dungs")
    public ResponseEntity<List<DichVuKhamApDungDTO>> getAllDichVuKhamApDungs(DichVuKhamApDungCriteria criteria, Pageable pageable) {
        log.debug("REST request to get DichVuKhamApDungs by criteria: {}", criteria);
        Page<DichVuKhamApDungDTO> page = dichVuKhamApDungQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /dich-vu-kham-ap-dungs/count} : count all the dichVuKhamApDungs.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/dich-vu-kham-ap-dungs/count")
    public ResponseEntity<Long> countDichVuKhamApDungs(DichVuKhamApDungCriteria criteria) {
        log.debug("REST request to count DichVuKhamApDungs by criteria: {}", criteria);
        return ResponseEntity.ok().body(dichVuKhamApDungQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /dich-vu-kham-ap-dungs/:id} : get the "id" dichVuKhamApDung.
     *
     * @param id the id of the dichVuKhamApDungDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dichVuKhamApDungDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/dich-vu-kham-ap-dungs/{id}")
    public ResponseEntity<DichVuKhamApDungDTO> getDichVuKhamApDung(@PathVariable Long id) {
        log.debug("REST request to get DichVuKhamApDung : {}", id);
        Optional<DichVuKhamApDungDTO> dichVuKhamApDungDTO = dichVuKhamApDungService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dichVuKhamApDungDTO);
    }

    /**
     * {@code DELETE  /dich-vu-kham-ap-dungs/:id} : delete the "id" dichVuKhamApDung.
     *
     * @param id the id of the dichVuKhamApDungDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/dich-vu-kham-ap-dungs/{id}")
    public ResponseEntity<Void> deleteDichVuKhamApDung(@PathVariable Long id) {
        log.debug("REST request to delete DichVuKhamApDung : {}", id);
        dichVuKhamApDungService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
