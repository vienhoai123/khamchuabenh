package vn.vnpt.web.rest;

import vn.vnpt.service.UserTypeService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.UserTypeDTO;
import vn.vnpt.service.dto.UserTypeCriteria;
import vn.vnpt.service.UserTypeQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.UserType}.
 */
@RestController
@RequestMapping("/api")
public class UserTypeResource {

    private final Logger log = LoggerFactory.getLogger(UserTypeResource.class);

    private static final String ENTITY_NAME = "khamchuabenhUserType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserTypeService userTypeService;

    private final UserTypeQueryService userTypeQueryService;

    public UserTypeResource(UserTypeService userTypeService, UserTypeQueryService userTypeQueryService) {
        this.userTypeService = userTypeService;
        this.userTypeQueryService = userTypeQueryService;
    }

    /**
     * {@code POST  /user-types} : Create a new userType.
     *
     * @param userTypeDTO the userTypeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userTypeDTO, or with status {@code 400 (Bad Request)} if the userType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-types")
    public ResponseEntity<UserTypeDTO> createUserType(@Valid @RequestBody UserTypeDTO userTypeDTO) throws URISyntaxException {
        log.debug("REST request to save UserType : {}", userTypeDTO);
        if (userTypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new userType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserTypeDTO result = userTypeService.save(userTypeDTO);
        return ResponseEntity.created(new URI("/api/user-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-types} : Updates an existing userType.
     *
     * @param userTypeDTO the userTypeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userTypeDTO,
     * or with status {@code 400 (Bad Request)} if the userTypeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userTypeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-types")
    public ResponseEntity<UserTypeDTO> updateUserType(@Valid @RequestBody UserTypeDTO userTypeDTO) throws URISyntaxException {
        log.debug("REST request to update UserType : {}", userTypeDTO);
        if (userTypeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserTypeDTO result = userTypeService.save(userTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /user-types} : get all the userTypes.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userTypes in body.
     */
    @GetMapping("/user-types")
    public ResponseEntity<List<UserTypeDTO>> getAllUserTypes(UserTypeCriteria criteria, Pageable pageable) {
        log.debug("REST request to get UserTypes by criteria: {}", criteria);
        Page<UserTypeDTO> page = userTypeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /user-types/count} : count all the userTypes.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/user-types/count")
    public ResponseEntity<Long> countUserTypes(UserTypeCriteria criteria) {
        log.debug("REST request to count UserTypes by criteria: {}", criteria);
        return ResponseEntity.ok().body(userTypeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /user-types/:id} : get the "id" userType.
     *
     * @param id the id of the userTypeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userTypeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-types/{id}")
    public ResponseEntity<UserTypeDTO> getUserType(@PathVariable Long id) {
        log.debug("REST request to get UserType : {}", id);
        Optional<UserTypeDTO> userTypeDTO = userTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userTypeDTO);
    }

    /**
     * {@code DELETE  /user-types/:id} : delete the "id" userType.
     *
     * @param id the id of the userTypeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-types/{id}")
    public ResponseEntity<Void> deleteUserType(@PathVariable Long id) {
        log.debug("REST request to delete UserType : {}", id);
        userTypeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
