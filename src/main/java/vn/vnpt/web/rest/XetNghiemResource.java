package vn.vnpt.web.rest;

import vn.vnpt.service.XetNghiemService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.XetNghiemDTO;
import vn.vnpt.service.dto.XetNghiemCriteria;
import vn.vnpt.service.XetNghiemQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.XetNghiem}.
 */
@RestController
@RequestMapping("/api")
public class XetNghiemResource {

    private final Logger log = LoggerFactory.getLogger(XetNghiemResource.class);

    private static final String ENTITY_NAME = "khamchuabenhXetNghiem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final XetNghiemService xetNghiemService;

    private final XetNghiemQueryService xetNghiemQueryService;

    public XetNghiemResource(XetNghiemService xetNghiemService, XetNghiemQueryService xetNghiemQueryService) {
        this.xetNghiemService = xetNghiemService;
        this.xetNghiemQueryService = xetNghiemQueryService;
    }

    /**
     * {@code POST  /xet-nghiems} : Create a new xetNghiem.
     *
     * @param xetNghiemDTO the xetNghiemDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new xetNghiemDTO, or with status {@code 400 (Bad Request)} if the xetNghiem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/xet-nghiems")
    public ResponseEntity<XetNghiemDTO> createXetNghiem(@Valid @RequestBody XetNghiemDTO xetNghiemDTO) throws URISyntaxException {
        log.debug("REST request to save XetNghiem : {}", xetNghiemDTO);
        if (xetNghiemDTO.getId() != null) {
            throw new BadRequestAlertException("A new xetNghiem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        XetNghiemDTO result = xetNghiemService.save(xetNghiemDTO);
        return ResponseEntity.created(new URI("/api/xet-nghiems/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /xet-nghiems} : Updates an existing xetNghiem.
     *
     * @param xetNghiemDTO the xetNghiemDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated xetNghiemDTO,
     * or with status {@code 400 (Bad Request)} if the xetNghiemDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the xetNghiemDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/xet-nghiems")
    public ResponseEntity<XetNghiemDTO> updateXetNghiem(@Valid @RequestBody XetNghiemDTO xetNghiemDTO) throws URISyntaxException {
        log.debug("REST request to update XetNghiem : {}", xetNghiemDTO);
        if (xetNghiemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        XetNghiemDTO result = xetNghiemService.save(xetNghiemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, xetNghiemDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /xet-nghiems} : get all the xetNghiems.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of xetNghiems in body.
     */
    @GetMapping("/xet-nghiems")
    public ResponseEntity<List<XetNghiemDTO>> getAllXetNghiems(XetNghiemCriteria criteria, Pageable pageable) {
        log.debug("REST request to get XetNghiems by criteria: {}", criteria);
        Page<XetNghiemDTO> page = xetNghiemQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /xet-nghiems/count} : count all the xetNghiems.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/xet-nghiems/count")
    public ResponseEntity<Long> countXetNghiems(XetNghiemCriteria criteria) {
        log.debug("REST request to count XetNghiems by criteria: {}", criteria);
        return ResponseEntity.ok().body(xetNghiemQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /xet-nghiems/:id} : get the "id" xetNghiem.
     *
     * @param id the id of the xetNghiemDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the xetNghiemDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/xet-nghiems/{id}")
    public ResponseEntity<XetNghiemDTO> getXetNghiem(@PathVariable Long id) {
        log.debug("REST request to get XetNghiem : {}", id);
        Optional<XetNghiemDTO> xetNghiemDTO = xetNghiemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(xetNghiemDTO);
    }

    /**
     * {@code DELETE  /xet-nghiems/:id} : delete the "id" xetNghiem.
     *
     * @param id the id of the xetNghiemDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/xet-nghiems/{id}")
    public ResponseEntity<Void> deleteXetNghiem(@PathVariable Long id) {
        log.debug("REST request to delete XetNghiem : {}", id);
        xetNghiemService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
