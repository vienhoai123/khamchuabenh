package vn.vnpt.web.rest;

import vn.vnpt.service.NhomXetNghiemService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.NhomXetNghiemDTO;
import vn.vnpt.service.dto.NhomXetNghiemCriteria;
import vn.vnpt.service.NhomXetNghiemQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.NhomXetNghiem}.
 */
@RestController
@RequestMapping("/api")
public class NhomXetNghiemResource {

    private final Logger log = LoggerFactory.getLogger(NhomXetNghiemResource.class);

    private static final String ENTITY_NAME = "khamchuabenhNhomXetNghiem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NhomXetNghiemService nhomXetNghiemService;

    private final NhomXetNghiemQueryService nhomXetNghiemQueryService;

    public NhomXetNghiemResource(NhomXetNghiemService nhomXetNghiemService, NhomXetNghiemQueryService nhomXetNghiemQueryService) {
        this.nhomXetNghiemService = nhomXetNghiemService;
        this.nhomXetNghiemQueryService = nhomXetNghiemQueryService;
    }

    /**
     * {@code POST  /nhom-xet-nghiems} : Create a new nhomXetNghiem.
     *
     * @param nhomXetNghiemDTO the nhomXetNghiemDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new nhomXetNghiemDTO, or with status {@code 400 (Bad Request)} if the nhomXetNghiem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/nhom-xet-nghiems")
    public ResponseEntity<NhomXetNghiemDTO> createNhomXetNghiem(@Valid @RequestBody NhomXetNghiemDTO nhomXetNghiemDTO) throws URISyntaxException {
        log.debug("REST request to save NhomXetNghiem : {}", nhomXetNghiemDTO);
        if (nhomXetNghiemDTO.getId() != null) {
            throw new BadRequestAlertException("A new nhomXetNghiem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NhomXetNghiemDTO result = nhomXetNghiemService.save(nhomXetNghiemDTO);
        return ResponseEntity.created(new URI("/api/nhom-xet-nghiems/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /nhom-xet-nghiems} : Updates an existing nhomXetNghiem.
     *
     * @param nhomXetNghiemDTO the nhomXetNghiemDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated nhomXetNghiemDTO,
     * or with status {@code 400 (Bad Request)} if the nhomXetNghiemDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the nhomXetNghiemDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/nhom-xet-nghiems")
    public ResponseEntity<NhomXetNghiemDTO> updateNhomXetNghiem(@Valid @RequestBody NhomXetNghiemDTO nhomXetNghiemDTO) throws URISyntaxException {
        log.debug("REST request to update NhomXetNghiem : {}", nhomXetNghiemDTO);
        if (nhomXetNghiemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NhomXetNghiemDTO result = nhomXetNghiemService.save(nhomXetNghiemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, nhomXetNghiemDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /nhom-xet-nghiems} : get all the nhomXetNghiems.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of nhomXetNghiems in body.
     */
    @GetMapping("/nhom-xet-nghiems")
    public ResponseEntity<List<NhomXetNghiemDTO>> getAllNhomXetNghiems(NhomXetNghiemCriteria criteria, Pageable pageable) {
        log.debug("REST request to get NhomXetNghiems by criteria: {}", criteria);
        Page<NhomXetNghiemDTO> page = nhomXetNghiemQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /nhom-xet-nghiems/count} : count all the nhomXetNghiems.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/nhom-xet-nghiems/count")
    public ResponseEntity<Long> countNhomXetNghiems(NhomXetNghiemCriteria criteria) {
        log.debug("REST request to count NhomXetNghiems by criteria: {}", criteria);
        return ResponseEntity.ok().body(nhomXetNghiemQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /nhom-xet-nghiems/:id} : get the "id" nhomXetNghiem.
     *
     * @param id the id of the nhomXetNghiemDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the nhomXetNghiemDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/nhom-xet-nghiems/{id}")
    public ResponseEntity<NhomXetNghiemDTO> getNhomXetNghiem(@PathVariable Long id) {
        log.debug("REST request to get NhomXetNghiem : {}", id);
        Optional<NhomXetNghiemDTO> nhomXetNghiemDTO = nhomXetNghiemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(nhomXetNghiemDTO);
    }

    /**
     * {@code DELETE  /nhom-xet-nghiems/:id} : delete the "id" nhomXetNghiem.
     *
     * @param id the id of the nhomXetNghiemDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/nhom-xet-nghiems/{id}")
    public ResponseEntity<Void> deleteNhomXetNghiem(@PathVariable Long id) {
        log.debug("REST request to delete NhomXetNghiem : {}", id);
        nhomXetNghiemService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
