package vn.vnpt.web.rest;

import vn.vnpt.service.PhuongXaService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.PhuongXaDTO;
import vn.vnpt.service.dto.PhuongXaCriteria;
import vn.vnpt.service.PhuongXaQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.PhuongXa}.
 */
@RestController
@RequestMapping("/api")
public class PhuongXaResource {

    private final Logger log = LoggerFactory.getLogger(PhuongXaResource.class);

    private static final String ENTITY_NAME = "khamchuabenhPhuongXa";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PhuongXaService phuongXaService;

    private final PhuongXaQueryService phuongXaQueryService;

    public PhuongXaResource(PhuongXaService phuongXaService, PhuongXaQueryService phuongXaQueryService) {
        this.phuongXaService = phuongXaService;
        this.phuongXaQueryService = phuongXaQueryService;
    }

    /**
     * {@code POST  /phuong-xas} : Create a new phuongXa.
     *
     * @param phuongXaDTO the phuongXaDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new phuongXaDTO, or with status {@code 400 (Bad Request)} if the phuongXa has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/phuong-xas")
    public ResponseEntity<PhuongXaDTO> createPhuongXa(@Valid @RequestBody PhuongXaDTO phuongXaDTO) throws URISyntaxException {
        log.debug("REST request to save PhuongXa : {}", phuongXaDTO);
        if (phuongXaDTO.getId() != null) {
            throw new BadRequestAlertException("A new phuongXa cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PhuongXaDTO result = phuongXaService.save(phuongXaDTO);
        return ResponseEntity.created(new URI("/api/phuong-xas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /phuong-xas} : Updates an existing phuongXa.
     *
     * @param phuongXaDTO the phuongXaDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated phuongXaDTO,
     * or with status {@code 400 (Bad Request)} if the phuongXaDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the phuongXaDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/phuong-xas")
    public ResponseEntity<PhuongXaDTO> updatePhuongXa(@Valid @RequestBody PhuongXaDTO phuongXaDTO) throws URISyntaxException {
        log.debug("REST request to update PhuongXa : {}", phuongXaDTO);
        if (phuongXaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PhuongXaDTO result = phuongXaService.save(phuongXaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, phuongXaDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /phuong-xas} : get all the phuongXas.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of phuongXas in body.
     */
    @GetMapping("/phuong-xas")
    public ResponseEntity<List<PhuongXaDTO>> getAllPhuongXas(PhuongXaCriteria criteria) {
        log.debug("REST request to get PhuongXas by criteria: {}", criteria);
        List<PhuongXaDTO> entityList = phuongXaQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * {@code GET  /phuong-xas/count} : count all the phuongXas.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/phuong-xas/count")
    public ResponseEntity<Long> countPhuongXas(PhuongXaCriteria criteria) {
        log.debug("REST request to count PhuongXas by criteria: {}", criteria);
        return ResponseEntity.ok().body(phuongXaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /phuong-xas/:id} : get the "id" phuongXa.
     *
     * @param id the id of the phuongXaDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the phuongXaDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/phuong-xas/{id}")
    public ResponseEntity<PhuongXaDTO> getPhuongXa(@PathVariable Long id) {
        log.debug("REST request to get PhuongXa : {}", id);
        Optional<PhuongXaDTO> phuongXaDTO = phuongXaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(phuongXaDTO);
    }

    /**
     * {@code DELETE  /phuong-xas/:id} : delete the "id" phuongXa.
     *
     * @param id the id of the phuongXaDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/phuong-xas/{id}")
    public ResponseEntity<Void> deletePhuongXa(@PathVariable Long id) {
        log.debug("REST request to delete PhuongXa : {}", id);
        phuongXaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
