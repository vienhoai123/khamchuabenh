package vn.vnpt.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.keycloak.representations.idm.RoleRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import vn.vnpt.config.Constants;
import vn.vnpt.domain.Authority;
import vn.vnpt.repository.AuthorityRepository;
import vn.vnpt.security.AuthoritiesConstants;
import vn.vnpt.service.KeycloakService;
import vn.vnpt.service.UserService;
import vn.vnpt.service.dto.UserDTO;
import vn.vnpt.service.dto.UserNhanVienDTO;
import vn.vnpt.web.rest.errors.BadRequestAlertException;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * REST controller for managing users.
 * <p>
 * This class accesses the {@link vn.vnpt.domain.User} entity, and needs to fetch its collection of authorities.
 * <p>
 * For a normal use-case, it would be better to have an eager relationship between User and Authority,
 * and send everything to the client side: there would be no View Model and DTO, a lot less code, and an outer-join
 * which would be good for performance.
 * <p>
 * We use a View Model and a DTO for 3 reasons:
 * <ul>
 * <li>We want to keep a lazy association between the user and the authorities, because people will
 * quite often do relationships with the user, and we don't want them to get the authorities all
 * the time for nothing (for performance reasons). This is the #1 goal: we should not impact our users'
 * application because of this use-case.</li>
 * <li> Not having an outer join causes n+1 requests to the database. This is not a real issue as
 * we have by default a second-level cache. This means on the first HTTP call we do the n+1 requests,
 * but then all authorities come from the cache, so in fact it's much better than doing an outer join
 * (which will get lots of data from the database, for each HTTP call).</li>
 * <li> As this manages users, for security reasons, we'd rather have a DTO layer.</li>
 * </ul>
 * <p>
 * Another option would be to have a specific JPA entity graph to handle this case.
 */
@RestController
@RequestMapping("/api")
public class UserResource {

    private final Logger log = LoggerFactory.getLogger(UserResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserService userService;
    private final KeycloakService keycloakService;
    private final AuthorityRepository authorityRepository;

    private static final String ENTITY_NAME = "khamchuabenhUser";

    public UserResource(UserService userService, KeycloakService keycloakService, AuthorityRepository authorityRepository) {
        this.userService = userService;
        this.keycloakService = keycloakService;
        this.authorityRepository = authorityRepository;
    }

    /**
     * {@code GET /users} : get all users.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body all users.
     */
    @GetMapping("/users")
    public ResponseEntity<List<UserDTO>> getAllUsers(Pageable pageable) {
        final Page<UserDTO> page = userService.getAllManagedUsers(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * Gets a list of all roles.
     * @return a string list of all roles.
     */
    @GetMapping("/users/authorities")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public List<String> getAuthorities() {
        return userService.getAuthorities();
    }

    /**
     * {@code GET /users/:login} : get the "login" user.
     *
     * @param login the login of the user to find.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the "login" user, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/users/{login:" + Constants.LOGIN_REGEX + "}")
    public ResponseEntity<UserDTO> getUser(@PathVariable String login) {
        log.debug("REST request to get User : {}", login);
        return ResponseUtil.wrapOrNotFound(
            userService.getUserWithAuthoritiesByLogin(login)
                .map(UserDTO::new));
    }
    /**
     * {@code POST /authority/} : Create a new role.
     *
     * @param authority the role to create.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the "login" user, or with status {@code 404 (Not Found)}.
     */
    @PostMapping("/authority")
    public ResponseEntity<Authority> createAuthority(@RequestBody Authority authority) throws URISyntaxException {
        log.debug("REST request to create Authority : {}", authority);
        boolean result = keycloakService.createRole(authority.getName());
        if (result) {
            Authority authoritySaved = authorityRepository.save(authority);
            log.debug("saved Authority : {}", authoritySaved);
        }
        return ResponseEntity.created(new URI("/api/authority/" + authority.getName()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, Boolean.toString(result)))
            .body(authority);
    }

    @GetMapping("/keycloak-authorities")
    public ResponseEntity<List<Authority>> getKhamChuaBenhAuthorities() {
        log.debug("REST request to get all Authority :");
        List<RoleRepresentation> result = keycloakService.getRoles();
        List<Authority> authorities = result.stream()
            .map(a -> new Authority(a.getName())).collect(Collectors.toList());
        return ResponseEntity.ok().body(authorities);
    }

    @GetMapping("/authorities")
    @PreAuthorize(
        "hasRole('" + AuthoritiesConstants.ADMIN + "')"
            + " || " +
            "hasRole('" + AuthoritiesConstants.SUB_ADMIN + "')"
    )
    public ResponseEntity<List<Authority>> getKeycloakAuthorities() {
        log.debug("REST request to get all Authority :");
        List<String> result = userService.getAuthorities();
        List<Authority> authorities = result.stream()
            .map(Authority::new).collect(Collectors.toList());
        return ResponseEntity.ok().body(authorities);
    }
    @DeleteMapping("/authority/{name}")
    public ResponseEntity<Void> deleteKeycloakAuthority(@PathVariable String name) {
        log.debug("REST request to delete Authority : {}", name);
        boolean status = keycloakService.deleteRole(name);

        if (status) {
            authorityRepository.delete(new Authority(name));
            log.debug("deleted Authority : {}", name);
        }
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, name)).build();
    }

    @PostMapping("/user/nhanvien")
    public ResponseEntity<UserNhanVienDTO> createUser(@RequestBody UserNhanVienDTO userNhanVienDTO) throws URISyntaxException {
        log.debug("REST request to save UserExtra : {}", userNhanVienDTO);
        if (userNhanVienDTO.getId() != null) {
            throw new BadRequestAlertException("A new UserNhanVienDTO cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserNhanVienDTO result = userService.save(userNhanVienDTO);
        return ResponseEntity.created(new URI("/api/user/nhanvien" + userNhanVienDTO.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, userNhanVienDTO.toString()))
            .body(result);
    }

    @PostMapping("/user/nhanvien/excel")
    public ResponseEntity<Void> createUserFromExcel(@RequestParam("file") MultipartFile file) {
        log.debug("File name: {}", file.getName());
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, file.getName())).build();
    }

    @GetMapping("/user/nhanvien/excel-template")
    public ResponseEntity<InputStreamSource> getTemplateExcel(@RequestParam("file") MultipartFile file) throws URISyntaxException, IOException {
        log.debug("File name: {}", file.getName());
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, file.getName())).build();
    }
}
