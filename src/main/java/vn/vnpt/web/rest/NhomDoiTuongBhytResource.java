package vn.vnpt.web.rest;

import vn.vnpt.service.NhomDoiTuongBhytService;
import vn.vnpt.web.rest.errors.BadRequestAlertException;
import vn.vnpt.service.dto.NhomDoiTuongBhytDTO;
import vn.vnpt.service.dto.NhomDoiTuongBhytCriteria;
import vn.vnpt.service.NhomDoiTuongBhytQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link vn.vnpt.domain.NhomDoiTuongBhyt}.
 */
@RestController
@RequestMapping("/api")
public class NhomDoiTuongBhytResource {

    private final Logger log = LoggerFactory.getLogger(NhomDoiTuongBhytResource.class);

    private static final String ENTITY_NAME = "khamchuabenhNhomDoiTuongBhyt";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NhomDoiTuongBhytService nhomDoiTuongBhytService;

    private final NhomDoiTuongBhytQueryService nhomDoiTuongBhytQueryService;

    public NhomDoiTuongBhytResource(NhomDoiTuongBhytService nhomDoiTuongBhytService, NhomDoiTuongBhytQueryService nhomDoiTuongBhytQueryService) {
        this.nhomDoiTuongBhytService = nhomDoiTuongBhytService;
        this.nhomDoiTuongBhytQueryService = nhomDoiTuongBhytQueryService;
    }

    /**
     * {@code POST  /nhom-doi-tuong-bhyts} : Create a new nhomDoiTuongBhyt.
     *
     * @param nhomDoiTuongBhytDTO the nhomDoiTuongBhytDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new nhomDoiTuongBhytDTO, or with status {@code 400 (Bad Request)} if the nhomDoiTuongBhyt has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/nhom-doi-tuong-bhyts")
    public ResponseEntity<NhomDoiTuongBhytDTO> createNhomDoiTuongBhyt(@Valid @RequestBody NhomDoiTuongBhytDTO nhomDoiTuongBhytDTO) throws URISyntaxException {
        log.debug("REST request to save NhomDoiTuongBhyt : {}", nhomDoiTuongBhytDTO);
        if (nhomDoiTuongBhytDTO.getId() != null) {
            throw new BadRequestAlertException("A new nhomDoiTuongBhyt cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NhomDoiTuongBhytDTO result = nhomDoiTuongBhytService.save(nhomDoiTuongBhytDTO);
        return ResponseEntity.created(new URI("/api/nhom-doi-tuong-bhyts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /nhom-doi-tuong-bhyts} : Updates an existing nhomDoiTuongBhyt.
     *
     * @param nhomDoiTuongBhytDTO the nhomDoiTuongBhytDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated nhomDoiTuongBhytDTO,
     * or with status {@code 400 (Bad Request)} if the nhomDoiTuongBhytDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the nhomDoiTuongBhytDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/nhom-doi-tuong-bhyts")
    public ResponseEntity<NhomDoiTuongBhytDTO> updateNhomDoiTuongBhyt(@Valid @RequestBody NhomDoiTuongBhytDTO nhomDoiTuongBhytDTO) throws URISyntaxException {
        log.debug("REST request to update NhomDoiTuongBhyt : {}", nhomDoiTuongBhytDTO);
        if (nhomDoiTuongBhytDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NhomDoiTuongBhytDTO result = nhomDoiTuongBhytService.save(nhomDoiTuongBhytDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, nhomDoiTuongBhytDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /nhom-doi-tuong-bhyts} : get all the nhomDoiTuongBhyts.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of nhomDoiTuongBhyts in body.
     */
    @GetMapping("/nhom-doi-tuong-bhyts")
    public ResponseEntity<List<NhomDoiTuongBhytDTO>> getAllNhomDoiTuongBhyts(NhomDoiTuongBhytCriteria criteria) {
        log.debug("REST request to get NhomDoiTuongBhyts by criteria: {}", criteria);
        List<NhomDoiTuongBhytDTO> entityList = nhomDoiTuongBhytQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * {@code GET  /nhom-doi-tuong-bhyts/count} : count all the nhomDoiTuongBhyts.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/nhom-doi-tuong-bhyts/count")
    public ResponseEntity<Long> countNhomDoiTuongBhyts(NhomDoiTuongBhytCriteria criteria) {
        log.debug("REST request to count NhomDoiTuongBhyts by criteria: {}", criteria);
        return ResponseEntity.ok().body(nhomDoiTuongBhytQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /nhom-doi-tuong-bhyts/:id} : get the "id" nhomDoiTuongBhyt.
     *
     * @param id the id of the nhomDoiTuongBhytDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the nhomDoiTuongBhytDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/nhom-doi-tuong-bhyts/{id}")
    public ResponseEntity<NhomDoiTuongBhytDTO> getNhomDoiTuongBhyt(@PathVariable Long id) {
        log.debug("REST request to get NhomDoiTuongBhyt : {}", id);
        Optional<NhomDoiTuongBhytDTO> nhomDoiTuongBhytDTO = nhomDoiTuongBhytService.findOne(id);
        return ResponseUtil.wrapOrNotFound(nhomDoiTuongBhytDTO);
    }

    /**
     * {@code DELETE  /nhom-doi-tuong-bhyts/:id} : delete the "id" nhomDoiTuongBhyt.
     *
     * @param id the id of the nhomDoiTuongBhytDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/nhom-doi-tuong-bhyts/{id}")
    public ResponseEntity<Void> deleteNhomDoiTuongBhyt(@PathVariable Long id) {
        log.debug("REST request to delete NhomDoiTuongBhyt : {}", id);
        nhomDoiTuongBhytService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
