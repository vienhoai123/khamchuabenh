package vn.vnpt.web.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import vn.vnpt.config.KafkaProperties;
import vn.vnpt.service.dto.TestDTO;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class KafkaTestResource {

    private final Logger log = LoggerFactory.getLogger(NgheNghiepResource.class);
    private final ObjectMapper objectMapper = new ObjectMapper();
    private KafkaConsumer<String,Object> kafkaConsumer;
    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;
    private KafkaProducer<String, String> producer;
    private ExecutorService sseExecutorService = Executors.newCachedThreadPool();
    public TestDTO messagenhan;
    private final KafkaProperties kafkaProperties;

    private static final String TOPIC="khamchuabenh";
    public KafkaTestResource(KafkaProperties kafkaProperties) {
        this.kafkaProperties = kafkaProperties;
        this.producer = new KafkaProducer(kafkaProperties.getProducerProps());
    }


    //  Publish theo KafkaTemplate với topic muốn publish là khamchuabenh
    @PostMapping("/publish1")
    public String postsomething(@RequestBody TestDTO message) throws JsonProcessingException {
        kafkaTemplate.send(TOPIC,message);
        return "Published Successfully!";
    }
//  Publish theo Kafka Producer
    @PostMapping("/publish2/{topic}")
    public PublishResult publish(@PathVariable String topic, @RequestParam String message, @RequestParam(required = false) String key) throws ExecutionException, InterruptedException {
        log.debug("REST request to send to Kafka topic {} with key {} the message : {}", topic, key, message);
        RecordMetadata metadata = producer.send(new ProducerRecord<>(topic, key, message)).get();
        return new PublishResult(metadata.topic(), metadata.partition(), metadata.offset(), Instant.ofEpochMilli(metadata.timestamp()));
    }
    private static class PublishResult {
        public final String topic;
        public final int partition;
        public final long offset;
        public final Instant timestamp;

        private PublishResult(String topic, int partition, long offset, Instant timestamp) {
            this.topic = topic;
            this.partition = partition;
            this.offset = offset;
            this.timestamp = timestamp;
        }
    }

    // Đây là phần Consumer theo KafkaListener
    // KafkaListener sẽ luôn luôn listen topic trên Kafka để khi Producer publish 1 thông tin gì mới thì nó sẽ tự động nhận
    // groupId="kafkatest" sẽ tương ứng với Vien-Phi
    // Do sử dụng JsonDeserializer nên sẽ bỏ vào Object lại còn map qua sẽ lấy value (thì e map đang bị lỗi :v)
    @KafkaListener(topics="khamchuabenh",groupId = "kafkatest")
    public Object nhandulieu(Object message){
//        ConsumerRecords<String,Object> record=kafkaConsumer.poll(Duration.ofSeconds(3));
//        ObjectMapper objectMapper=new ObjectMapper();
////        TestDTO result=objectMapper.readValue(message,TestDTO.class);
        return message;
    }
    // Tạm thời khi listener nhận dữ liệu sẽ lưu vào 1 biến tạm sau đó thì muốn Get lại hay xử lý gì đó cũng đc
    @GetMapping("/consumekq")
    public ResponseEntity<TestDTO> nhandulieu2(){
        System.out.println("Received Messasge in group khamchuabenh: " + messagenhan);
        Optional<TestDTO> result= Optional.ofNullable(messagenhan);
        return ResponseUtil.wrapOrNotFound(result);
    }
    // Consume mặc định do Jhipster sinh (chưa xài)
    @GetMapping("/consume")
    public SseEmitter consume(@RequestParam("topic") List<String> topics, @RequestParam Map<String, String> consumerParams) {
        log.debug("REST request to consume records from Kafka topics {}", topics);
        Map<String, Object> consumerProps = kafkaProperties.getConsumerProps();
        consumerProps.putAll(consumerParams);
        consumerProps.remove("topic");

        SseEmitter emitter = new SseEmitter(0L);
        sseExecutorService.execute(() -> {
            KafkaConsumer<String, String> consumer = new KafkaConsumer<>(consumerProps);
            emitter.onCompletion(consumer::close);
            consumer.subscribe(topics);
            boolean exitLoop = false;
            while(!exitLoop) {
                try {
                    ConsumerRecords<String, String> records = consumer.poll(Duration.ofSeconds(5));
                    for (ConsumerRecord<String, String> record : records) {
                        emitter.send(record.value());
                    }
                    emitter.send(SseEmitter.event().comment(""));
                } catch (Exception ex) {
                    log.trace("Complete with error {}", ex.getMessage(), ex);
                    emitter.completeWithError(ex);
                    exitLoop = true;
                }
            }
            consumer.close();
            emitter.complete();
        });
        return emitter;
    }

}
