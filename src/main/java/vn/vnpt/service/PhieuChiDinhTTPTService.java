package vn.vnpt.service;

import vn.vnpt.domain.PhieuChiDinhTTPTId;
import vn.vnpt.service.dto.PhieuChiDinhTTPTDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.PhieuChiDinhTTPT}.
 */
public interface PhieuChiDinhTTPTService {

    /**
     * Save a phieuChiDinhTTPT.
     *
     * @param phieuChiDinhTTPTDTO the entity to save.
     * @return the persisted entity.
     */
    PhieuChiDinhTTPTDTO save(PhieuChiDinhTTPTDTO phieuChiDinhTTPTDTO);

    /**
     * Get all the phieuChiDinhTTPTS.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PhieuChiDinhTTPTDTO> findAll(Pageable pageable);

    /**
     * Get the "id" phieuChiDinhTTPT.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PhieuChiDinhTTPTDTO> findOne(PhieuChiDinhTTPTId id);

    /**
     * Delete the "id" phieuChiDinhTTPT.
     *
     * @param id the id of the entity.
     */
    void delete(PhieuChiDinhTTPTId id);
}
