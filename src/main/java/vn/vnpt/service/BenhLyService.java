package vn.vnpt.service;

import vn.vnpt.service.dto.BenhLyDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.BenhLy}.
 */
public interface BenhLyService {

    /**
     * Save a benhLy.
     *
     * @param benhLyDTO the entity to save.
     * @return the persisted entity.
     */
    BenhLyDTO save(BenhLyDTO benhLyDTO);

    /**
     * Get all the benhLies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BenhLyDTO> findAll(Pageable pageable);

    /**
     * Get the "id" benhLy.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BenhLyDTO> findOne(Long id);

    /**
     * Delete the "id" benhLy.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
