package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.ChucDanh;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.ChucDanhRepository;
import vn.vnpt.service.dto.ChucDanhCriteria;
import vn.vnpt.service.dto.ChucDanhDTO;
import vn.vnpt.service.mapper.ChucDanhMapper;

/**
 * Service for executing complex queries for {@link ChucDanh} entities in the database.
 * The main input is a {@link ChucDanhCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ChucDanhDTO} or a {@link Page} of {@link ChucDanhDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ChucDanhQueryService extends QueryService<ChucDanh> {

    private final Logger log = LoggerFactory.getLogger(ChucDanhQueryService.class);

    private final ChucDanhRepository chucDanhRepository;

    private final ChucDanhMapper chucDanhMapper;

    public ChucDanhQueryService(ChucDanhRepository chucDanhRepository, ChucDanhMapper chucDanhMapper) {
        this.chucDanhRepository = chucDanhRepository;
        this.chucDanhMapper = chucDanhMapper;
    }

    /**
     * Return a {@link List} of {@link ChucDanhDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ChucDanhDTO> findByCriteria(ChucDanhCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ChucDanh> specification = createSpecification(criteria);
        return chucDanhMapper.toDto(chucDanhRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ChucDanhDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ChucDanhDTO> findByCriteria(ChucDanhCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ChucDanh> specification = createSpecification(criteria);
        return chucDanhRepository.findAll(specification, page)
            .map(chucDanhMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ChucDanhCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ChucDanh> specification = createSpecification(criteria);
        return chucDanhRepository.count(specification);
    }

    /**
     * Function to convert {@link ChucDanhCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ChucDanh> createSpecification(ChucDanhCriteria criteria) {
        Specification<ChucDanh> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ChucDanh_.id));
            }
            if (criteria.getMoTa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMoTa(), ChucDanh_.moTa));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), ChucDanh_.ten));
            }
        }
        return specification;
    }
}
