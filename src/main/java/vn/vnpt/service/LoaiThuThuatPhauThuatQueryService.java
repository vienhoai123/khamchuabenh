package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.LoaiThuThuatPhauThuat;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.LoaiThuThuatPhauThuatRepository;
import vn.vnpt.service.dto.LoaiThuThuatPhauThuatCriteria;
import vn.vnpt.service.dto.LoaiThuThuatPhauThuatDTO;
import vn.vnpt.service.mapper.LoaiThuThuatPhauThuatMapper;

/**
 * Service for executing complex queries for {@link LoaiThuThuatPhauThuat} entities in the database.
 * The main input is a {@link LoaiThuThuatPhauThuatCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link LoaiThuThuatPhauThuatDTO} or a {@link Page} of {@link LoaiThuThuatPhauThuatDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class LoaiThuThuatPhauThuatQueryService extends QueryService<LoaiThuThuatPhauThuat> {

    private final Logger log = LoggerFactory.getLogger(LoaiThuThuatPhauThuatQueryService.class);

    private final LoaiThuThuatPhauThuatRepository loaiThuThuatPhauThuatRepository;

    private final LoaiThuThuatPhauThuatMapper loaiThuThuatPhauThuatMapper;

    public LoaiThuThuatPhauThuatQueryService(LoaiThuThuatPhauThuatRepository loaiThuThuatPhauThuatRepository, LoaiThuThuatPhauThuatMapper loaiThuThuatPhauThuatMapper) {
        this.loaiThuThuatPhauThuatRepository = loaiThuThuatPhauThuatRepository;
        this.loaiThuThuatPhauThuatMapper = loaiThuThuatPhauThuatMapper;
    }

    /**
     * Return a {@link List} of {@link LoaiThuThuatPhauThuatDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<LoaiThuThuatPhauThuatDTO> findByCriteria(LoaiThuThuatPhauThuatCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<LoaiThuThuatPhauThuat> specification = createSpecification(criteria);
        return loaiThuThuatPhauThuatMapper.toDto(loaiThuThuatPhauThuatRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link LoaiThuThuatPhauThuatDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<LoaiThuThuatPhauThuatDTO> findByCriteria(LoaiThuThuatPhauThuatCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<LoaiThuThuatPhauThuat> specification = createSpecification(criteria);
        return loaiThuThuatPhauThuatRepository.findAll(specification, page)
            .map(loaiThuThuatPhauThuatMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(LoaiThuThuatPhauThuatCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<LoaiThuThuatPhauThuat> specification = createSpecification(criteria);
        return loaiThuThuatPhauThuatRepository.count(specification);
    }

    /**
     * Function to convert {@link LoaiThuThuatPhauThuatCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<LoaiThuThuatPhauThuat> createSpecification(LoaiThuThuatPhauThuatCriteria criteria) {
        Specification<LoaiThuThuatPhauThuat> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), LoaiThuThuatPhauThuat_.id));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), LoaiThuThuatPhauThuat_.ten));
            }
            if (criteria.getMoTa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMoTa(), LoaiThuThuatPhauThuat_.moTa));
            }
            if (criteria.getEnable() != null) {
                specification = specification.and(buildSpecification(criteria.getEnable(), LoaiThuThuatPhauThuat_.enable));
            }
            if (criteria.getUuTien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUuTien(), LoaiThuThuatPhauThuat_.uuTien));
            }
            if (criteria.getMaPhanLoai() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaPhanLoai(), LoaiThuThuatPhauThuat_.maPhanLoai));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(),
                    root -> root.join(LoaiThuThuatPhauThuat_.donVi, JoinType.LEFT).get(DonVi_.id)));
            }
        }
        return specification;
    }
}
