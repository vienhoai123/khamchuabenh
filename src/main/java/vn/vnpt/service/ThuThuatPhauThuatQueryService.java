package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.ThuThuatPhauThuat;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.ThuThuatPhauThuatRepository;
import vn.vnpt.service.dto.ThuThuatPhauThuatCriteria;
import vn.vnpt.service.dto.ThuThuatPhauThuatDTO;
import vn.vnpt.service.mapper.ThuThuatPhauThuatMapper;

/**
 * Service for executing complex queries for {@link ThuThuatPhauThuat} entities in the database.
 * The main input is a {@link ThuThuatPhauThuatCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ThuThuatPhauThuatDTO} or a {@link Page} of {@link ThuThuatPhauThuatDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ThuThuatPhauThuatQueryService extends QueryService<ThuThuatPhauThuat> {

    private final Logger log = LoggerFactory.getLogger(ThuThuatPhauThuatQueryService.class);

    private final ThuThuatPhauThuatRepository thuThuatPhauThuatRepository;

    private final ThuThuatPhauThuatMapper thuThuatPhauThuatMapper;

    public ThuThuatPhauThuatQueryService(ThuThuatPhauThuatRepository thuThuatPhauThuatRepository, ThuThuatPhauThuatMapper thuThuatPhauThuatMapper) {
        this.thuThuatPhauThuatRepository = thuThuatPhauThuatRepository;
        this.thuThuatPhauThuatMapper = thuThuatPhauThuatMapper;
    }

    /**
     * Return a {@link List} of {@link ThuThuatPhauThuatDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ThuThuatPhauThuatDTO> findByCriteria(ThuThuatPhauThuatCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ThuThuatPhauThuat> specification = createSpecification(criteria);
        return thuThuatPhauThuatMapper.toDto(thuThuatPhauThuatRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ThuThuatPhauThuatDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ThuThuatPhauThuatDTO> findByCriteria(ThuThuatPhauThuatCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ThuThuatPhauThuat> specification = createSpecification(criteria);
        return thuThuatPhauThuatRepository.findAll(specification, page)
            .map(thuThuatPhauThuatMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ThuThuatPhauThuatCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ThuThuatPhauThuat> specification = createSpecification(criteria);
        return thuThuatPhauThuatRepository.count(specification);
    }

    /**
     * Function to convert {@link ThuThuatPhauThuatCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ThuThuatPhauThuat> createSpecification(ThuThuatPhauThuatCriteria criteria) {
        Specification<ThuThuatPhauThuat> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ThuThuatPhauThuat_.id));
            }
            if (criteria.getDeleted() != null) {
                specification = specification.and(buildSpecification(criteria.getDeleted(), ThuThuatPhauThuat_.deleted));
            }
            if (criteria.getDichVuYeuCau() != null) {
                specification = specification.and(buildSpecification(criteria.getDichVuYeuCau(), ThuThuatPhauThuat_.dichVuYeuCau));
            }
            if (criteria.getDonGiaBenhVien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDonGiaBenhVien(), ThuThuatPhauThuat_.donGiaBenhVien));
            }
            if (criteria.getEnabled() != null) {
                specification = specification.and(buildSpecification(criteria.getEnabled(), ThuThuatPhauThuat_.enabled));
            }
            if (criteria.getGoiHanChiDinh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGoiHanChiDinh(), ThuThuatPhauThuat_.goiHanChiDinh));
            }
            if (criteria.getPhamViChiDinh() != null) {
                specification = specification.and(buildSpecification(criteria.getPhamViChiDinh(), ThuThuatPhauThuat_.phamViChiDinh));
            }
            if (criteria.getPhanTheoGioiTinh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPhanTheoGioiTinh(), ThuThuatPhauThuat_.phanTheoGioiTinh));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), ThuThuatPhauThuat_.ten));
            }
            if (criteria.getTenHienThi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenHienThi(), ThuThuatPhauThuat_.tenHienThi));
            }
            if (criteria.getMaNoiBo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaNoiBo(), ThuThuatPhauThuat_.maNoiBo));
            }
            if (criteria.getMaDungChung() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaDungChung(), ThuThuatPhauThuat_.maDungChung));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(),
                    root -> root.join(ThuThuatPhauThuat_.donVi, JoinType.LEFT).get(DonVi_.id)));
            }
            if (criteria.getDotMaId() != null) {
                specification = specification.and(buildSpecification(criteria.getDotMaId(),
                    root -> root.join(ThuThuatPhauThuat_.dotMa, JoinType.LEFT).get(DotThayDoiMaDichVu_.id)));
            }
            if (criteria.getLoaittptId() != null) {
                specification = specification.and(buildSpecification(criteria.getLoaittptId(),
                    root -> root.join(ThuThuatPhauThuat_.loaittpt, JoinType.LEFT).get(LoaiThuThuatPhauThuat_.id)));
            }
        }
        return specification;
    }
}
