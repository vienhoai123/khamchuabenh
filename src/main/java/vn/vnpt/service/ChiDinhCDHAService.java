package vn.vnpt.service;

import vn.vnpt.domain.ChiDinhCDHA;
import vn.vnpt.domain.ChiDinhCDHAId;
import vn.vnpt.service.dto.ChiDinhCDHADTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.ChiDinhCDHA}.
 */
public interface ChiDinhCDHAService {

    /**
     * Save a chiDinhCDHA.
     *
     * @param chiDinhCDHADTO the entity to save.
     * @return the persisted entity.
     */
    ChiDinhCDHADTO save(ChiDinhCDHADTO chiDinhCDHADTO);

    /**
     * Get all the chiDinhCDHAS.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ChiDinhCDHADTO> findAll(Pageable pageable);

    /**
     * Get the "id" chiDinhCDHA.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ChiDinhCDHADTO> findOne(ChiDinhCDHAId id);

    /**
     * Delete the "id" chiDinhCDHA.
     *
//     * @param id the id of the entity.
     */
    void delete(ChiDinhCDHAId id);
}
