package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.LoaiBenhLy;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.LoaiBenhLyRepository;
import vn.vnpt.service.dto.LoaiBenhLyCriteria;
import vn.vnpt.service.dto.LoaiBenhLyDTO;
import vn.vnpt.service.mapper.LoaiBenhLyMapper;

/**
 * Service for executing complex queries for {@link LoaiBenhLy} entities in the database.
 * The main input is a {@link LoaiBenhLyCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link LoaiBenhLyDTO} or a {@link Page} of {@link LoaiBenhLyDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class LoaiBenhLyQueryService extends QueryService<LoaiBenhLy> {

    private final Logger log = LoggerFactory.getLogger(LoaiBenhLyQueryService.class);

    private final LoaiBenhLyRepository loaiBenhLyRepository;

    private final LoaiBenhLyMapper loaiBenhLyMapper;

    public LoaiBenhLyQueryService(LoaiBenhLyRepository loaiBenhLyRepository, LoaiBenhLyMapper loaiBenhLyMapper) {
        this.loaiBenhLyRepository = loaiBenhLyRepository;
        this.loaiBenhLyMapper = loaiBenhLyMapper;
    }

    /**
     * Return a {@link List} of {@link LoaiBenhLyDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<LoaiBenhLyDTO> findByCriteria(LoaiBenhLyCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<LoaiBenhLy> specification = createSpecification(criteria);
        return loaiBenhLyMapper.toDto(loaiBenhLyRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link LoaiBenhLyDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<LoaiBenhLyDTO> findByCriteria(LoaiBenhLyCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<LoaiBenhLy> specification = createSpecification(criteria);
        return loaiBenhLyRepository.findAll(specification, page)
            .map(loaiBenhLyMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(LoaiBenhLyCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<LoaiBenhLy> specification = createSpecification(criteria);
        return loaiBenhLyRepository.count(specification);
    }

    /**
     * Function to convert {@link LoaiBenhLyCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<LoaiBenhLy> createSpecification(LoaiBenhLyCriteria criteria) {
        Specification<LoaiBenhLy> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), LoaiBenhLy_.id));
            }
            if (criteria.getKyHieu() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKyHieu(), LoaiBenhLy_.kyHieu));
            }
            if (criteria.getMoTa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMoTa(), LoaiBenhLy_.moTa));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), LoaiBenhLy_.ten));
            }
            if (criteria.getTenTiengAnh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenTiengAnh(), LoaiBenhLy_.tenTiengAnh));
            }
        }
        return specification;
    }
}
