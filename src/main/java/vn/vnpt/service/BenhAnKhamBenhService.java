package vn.vnpt.service;

import vn.vnpt.service.dto.BenhAnKhamBenhDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import vn.vnpt.domain.BenhAnKhamBenhId;
import vn.vnpt.service.dto.BenhNhanDTO;

/**
 * Service Interface for managing {@link vn.vnpt.domain.BenhAnKhamBenh}.
 */
public interface BenhAnKhamBenhService {

    /**
     * Save a benhAnKhamBenh.
     *
     * @param benhAnKhamBenhDTO the entity to save.
     * @return the persisted entity.
     */
    BenhAnKhamBenhDTO save(BenhAnKhamBenhDTO benhAnKhamBenhDTO);

    /**
     * Get all the benhAnKhamBenhs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BenhAnKhamBenhDTO> findAll(Pageable pageable);


    /**
     * Get the "id" benhAnKhamBenh.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BenhAnKhamBenhDTO> findOne(BenhAnKhamBenhId id);

    /**
     * Delete the "id" benhAnKhamBenh.
     *
     * @param id the id of the entity.
     */
    void delete(BenhAnKhamBenhId id);

    Optional<BenhAnKhamBenhDTO> makeBakbDefault(BenhNhanDTO benhNhanDTO, Boolean coBaoHiem);

//    Optional<BenhAnKhamBenhDTO> makeBakbDefault(BenhNhanDTO benhNhanDTO, Boolean coBaoHiem);
}
