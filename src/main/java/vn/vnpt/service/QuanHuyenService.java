package vn.vnpt.service;

import vn.vnpt.service.dto.QuanHuyenDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.QuanHuyen}.
 */
public interface QuanHuyenService {

    /**
     * Save a quanHuyen.
     *
     * @param quanHuyenDTO the entity to save.
     * @return the persisted entity.
     */
    QuanHuyenDTO save(QuanHuyenDTO quanHuyenDTO);

    /**
     * Get all the quanHuyens.
     *
     * @return the list of entities.
     */
    List<QuanHuyenDTO> findAll();

    /**
     * Get the "id" quanHuyen.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<QuanHuyenDTO> findOne(Long id);

    /**
     * Delete the "id" quanHuyen.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
