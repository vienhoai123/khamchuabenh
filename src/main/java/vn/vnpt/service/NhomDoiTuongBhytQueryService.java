package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.NhomDoiTuongBhyt;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.NhomDoiTuongBhytRepository;
import vn.vnpt.service.dto.NhomDoiTuongBhytCriteria;
import vn.vnpt.service.dto.NhomDoiTuongBhytDTO;
import vn.vnpt.service.mapper.NhomDoiTuongBhytMapper;

/**
 * Service for executing complex queries for {@link NhomDoiTuongBhyt} entities in the database.
 * The main input is a {@link NhomDoiTuongBhytCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link NhomDoiTuongBhytDTO} or a {@link Page} of {@link NhomDoiTuongBhytDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class NhomDoiTuongBhytQueryService extends QueryService<NhomDoiTuongBhyt> {

    private final Logger log = LoggerFactory.getLogger(NhomDoiTuongBhytQueryService.class);

    private final NhomDoiTuongBhytRepository nhomDoiTuongBhytRepository;

    private final NhomDoiTuongBhytMapper nhomDoiTuongBhytMapper;

    public NhomDoiTuongBhytQueryService(NhomDoiTuongBhytRepository nhomDoiTuongBhytRepository, NhomDoiTuongBhytMapper nhomDoiTuongBhytMapper) {
        this.nhomDoiTuongBhytRepository = nhomDoiTuongBhytRepository;
        this.nhomDoiTuongBhytMapper = nhomDoiTuongBhytMapper;
    }

    /**
     * Return a {@link List} of {@link NhomDoiTuongBhytDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NhomDoiTuongBhytDTO> findByCriteria(NhomDoiTuongBhytCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<NhomDoiTuongBhyt> specification = createSpecification(criteria);
        return nhomDoiTuongBhytMapper.toDto(nhomDoiTuongBhytRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link NhomDoiTuongBhytDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NhomDoiTuongBhytDTO> findByCriteria(NhomDoiTuongBhytCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<NhomDoiTuongBhyt> specification = createSpecification(criteria);
        return nhomDoiTuongBhytRepository.findAll(specification, page)
            .map(nhomDoiTuongBhytMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(NhomDoiTuongBhytCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<NhomDoiTuongBhyt> specification = createSpecification(criteria);
        return nhomDoiTuongBhytRepository.count(specification);
    }

    /**
     * Function to convert {@link NhomDoiTuongBhytCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<NhomDoiTuongBhyt> createSpecification(NhomDoiTuongBhytCriteria criteria) {
        Specification<NhomDoiTuongBhyt> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), NhomDoiTuongBhyt_.id));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), NhomDoiTuongBhyt_.ten));
            }
        }
        return specification;
    }
}
