package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.TPhanNhomTTPT;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.TPhanNhomTTPTRepository;
import vn.vnpt.service.dto.TPhanNhomTTPTCriteria;
import vn.vnpt.service.dto.TPhanNhomTTPTDTO;
import vn.vnpt.service.mapper.TPhanNhomTTPTMapper;

/**
 * Service for executing complex queries for {@link TPhanNhomTTPT} entities in the database.
 * The main input is a {@link TPhanNhomTTPTCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TPhanNhomTTPTDTO} or a {@link Page} of {@link TPhanNhomTTPTDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TPhanNhomTTPTQueryService extends QueryService<TPhanNhomTTPT> {

    private final Logger log = LoggerFactory.getLogger(TPhanNhomTTPTQueryService.class);

    private final TPhanNhomTTPTRepository tPhanNhomTTPTRepository;

    private final TPhanNhomTTPTMapper tPhanNhomTTPTMapper;

    public TPhanNhomTTPTQueryService(TPhanNhomTTPTRepository tPhanNhomTTPTRepository, TPhanNhomTTPTMapper tPhanNhomTTPTMapper) {
        this.tPhanNhomTTPTRepository = tPhanNhomTTPTRepository;
        this.tPhanNhomTTPTMapper = tPhanNhomTTPTMapper;
    }

    /**
     * Return a {@link List} of {@link TPhanNhomTTPTDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TPhanNhomTTPTDTO> findByCriteria(TPhanNhomTTPTCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TPhanNhomTTPT> specification = createSpecification(criteria);
        return tPhanNhomTTPTMapper.toDto(tPhanNhomTTPTRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TPhanNhomTTPTDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TPhanNhomTTPTDTO> findByCriteria(TPhanNhomTTPTCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TPhanNhomTTPT> specification = createSpecification(criteria);
        return tPhanNhomTTPTRepository.findAll(specification, page)
            .map(tPhanNhomTTPTMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TPhanNhomTTPTCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TPhanNhomTTPT> specification = createSpecification(criteria);
        return tPhanNhomTTPTRepository.count(specification);
    }

    /**
     * Function to convert {@link TPhanNhomTTPTCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TPhanNhomTTPT> createSpecification(TPhanNhomTTPTCriteria criteria) {
        Specification<TPhanNhomTTPT> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TPhanNhomTTPT_.id));
            }
            if (criteria.getNhomttptId() != null) {
                specification = specification.and(buildSpecification(criteria.getNhomttptId(),
                    root -> root.join(TPhanNhomTTPT_.nhomttpt, JoinType.LEFT).get(NhomThuThuatPhauThuat_.id)));
            }
            if (criteria.getTtptId() != null) {
                specification = specification.and(buildSpecification(criteria.getTtptId(),
                    root -> root.join(TPhanNhomTTPT_.ttpt, JoinType.LEFT).get(ThuThuatPhauThuat_.id)));
            }
        }
        return specification;
    }
}
