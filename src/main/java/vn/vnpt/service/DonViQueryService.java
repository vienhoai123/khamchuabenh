package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.DonVi;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.DonViRepository;
import vn.vnpt.service.dto.DonViCriteria;
import vn.vnpt.service.dto.DonViDTO;
import vn.vnpt.service.mapper.DonViMapper;

/**
 * Service for executing complex queries for {@link DonVi} entities in the database.
 * The main input is a {@link DonViCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DonViDTO} or a {@link Page} of {@link DonViDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DonViQueryService extends QueryService<DonVi> {

    private final Logger log = LoggerFactory.getLogger(DonViQueryService.class);

    private final DonViRepository donViRepository;

    private final DonViMapper donViMapper;

    public DonViQueryService(DonViRepository donViRepository, DonViMapper donViMapper) {
        this.donViRepository = donViRepository;
        this.donViMapper = donViMapper;
    }

    /**
     * Return a {@link List} of {@link DonViDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DonViDTO> findByCriteria(DonViCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DonVi> specification = createSpecification(criteria);
        return donViMapper.toDto(donViRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DonViDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DonViDTO> findByCriteria(DonViCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DonVi> specification = createSpecification(criteria);
        return donViRepository.findAll(specification, page)
            .map(donViMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DonViCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DonVi> specification = createSpecification(criteria);
        return donViRepository.count(specification);
    }

    /**
     * Function to convert {@link DonViCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DonVi> createSpecification(DonViCriteria criteria) {
        Specification<DonVi> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), DonVi_.id));
            }
            if (criteria.getCap() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCap(), DonVi_.cap));
            }
            if (criteria.getChiNhanhNganHang() != null) {
                specification = specification.and(buildStringSpecification(criteria.getChiNhanhNganHang(), DonVi_.chiNhanhNganHang));
            }
            if (criteria.getDiaChi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDiaChi(), DonVi_.diaChi));
            }
            if (criteria.getDonViQuanLyId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDonViQuanLyId(), DonVi_.donViQuanLyId));
            }
            if (criteria.getDvtt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDvtt(), DonVi_.dvtt));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), DonVi_.email));
            }
            if (criteria.getEnabled() != null) {
                specification = specification.and(buildSpecification(criteria.getEnabled(), DonVi_.enabled));
            }
            if (criteria.getKyHieu() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKyHieu(), DonVi_.kyHieu));
            }
            if (criteria.getLoai() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLoai(), DonVi_.loai));
            }
            if (criteria.getMaTinh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaTinh(), DonVi_.maTinh));
            }
            if (criteria.getPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhone(), DonVi_.phone));
            }
            if (criteria.getSoTaiKhoan() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSoTaiKhoan(), DonVi_.soTaiKhoan));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), DonVi_.ten));
            }
            if (criteria.getTenNganHang() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenNganHang(), DonVi_.tenNganHang));
            }
        }
        return specification;
    }
}
