package vn.vnpt.service.customservice;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.vnpt.domain.BenhAnKhamBenhId;
import vn.vnpt.service.dto.BenhAnKhamBenhCriteria;
import vn.vnpt.service.dto.customdto.*;

import java.util.List;
import java.util.Optional;


public interface TiepNhanService  {
    TiepNhanDTO save(TiepNhanDTO tiepNhanDTO);
    Optional<TiepNhanDTO> findOne(Long BakbId);
    Page<TiepNhanDTO> findByCriteria(BenhAnKhamBenhCriteria criteria, Pageable page);
    void delete(Long bakbId);
    List<SoLuongBenhNhanDTO> countingBN(BenhAnKhamBenhCriteria criteria);
    List<DanhSachTiepNhanDTO> getDanhSachTiepNhan(BenhAnKhamBenhCriteria criteria,Pageable page);


    /**
     * Custom cho tiếp nhận ngoại trú
     */
    TiepNhanDTO saveNgoaiTru(TiepNhanDTO tiepNhanDTO);
    Optional<TiepNhanDTO> findOneNgoaiTru(BenhAnKhamBenhId id);
    List<TiepNhanDTO> findNgoaiTruByCriteria(BenhAnKhamBenhCriteria criteria, Pageable pageable);
    TiepNhanResponseDTO tiepNhanNgoaiTru(TiepNhanRequestDTO tiepNhanRequestDTO);
    TiepNhanResponseDTO capNhatTiepNhanNgoaiTru(TiepNhanRequestDTO tiepNhanRequestDTO);
    List<TiepNhanResponseDTO> getDanhSachTiepNhanNgoaiTru(BenhAnKhamBenhCriteria criteria, Pageable pageable);
    Optional<TiepNhanResponseDTO> getOneTiepNhanNgoaiTru(BenhAnKhamBenhId id);
    void deleteNgoaiTru(BenhAnKhamBenhId id);
}
