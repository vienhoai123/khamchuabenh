package vn.vnpt.service;

import vn.vnpt.service.dto.TinhThanhPhoDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.TinhThanhPho}.
 */
public interface TinhThanhPhoService {

    /**
     * Save a tinhThanhPho.
     *
     * @param tinhThanhPhoDTO the entity to save.
     * @return the persisted entity.
     */
    TinhThanhPhoDTO save(TinhThanhPhoDTO tinhThanhPhoDTO);

    /**
     * Get all the tinhThanhPhos.
     *
     * @return the list of entities.
     */
    List<TinhThanhPhoDTO> findAll();

    /**
     * Get the "id" tinhThanhPho.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TinhThanhPhoDTO> findOne(Long id);

    /**
     * Delete the "id" tinhThanhPho.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
