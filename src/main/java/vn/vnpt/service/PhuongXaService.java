package vn.vnpt.service;

import vn.vnpt.service.dto.PhuongXaDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.PhuongXa}.
 */
public interface PhuongXaService {

    /**
     * Save a phuongXa.
     *
     * @param phuongXaDTO the entity to save.
     * @return the persisted entity.
     */
    PhuongXaDTO save(PhuongXaDTO phuongXaDTO);

    /**
     * Get all the phuongXas.
     *
     * @return the list of entities.
     */
    List<PhuongXaDTO> findAll();

    /**
     * Get the "id" phuongXa.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PhuongXaDTO> findOne(Long id);

    /**
     * Delete the "id" phuongXa.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
