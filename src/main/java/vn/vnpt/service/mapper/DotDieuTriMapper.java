package vn.vnpt.service.mapper;

import vn.vnpt.domain.*;
import vn.vnpt.service.dto.DotDieuTriDTO;

import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Mapper for the entity {@link DotDieuTri} and its DTO {@link DotDieuTriDTO}.
 */
@Mapper(componentModel = "spring", uses = {BenhAnKhamBenhMapper.class})
public abstract class DotDieuTriMapper implements EntityMapper<DotDieuTriDTO, DotDieuTri> {

    @Autowired
    BenhAnKhamBenhMapper benhAnKhamBenhMapper;

//    @Mapping(source = "id.id", target = "id")
//    @Mapping(source = "id.bakbId", target = "bakbId")
//    @Mapping(source = "id.benhNhanId", target = "benhNhanId")
//    @Mapping(source = "id.donViId", target = "donViId")
    @Mapping(target = "bakbId", source = "bakbId")
    @Mapping(target = "benhNhanId", source = "benhNhanId")
    @Mapping(target = "donViId", source = "donViId")
    public abstract DotDieuTriDTO toDto(DotDieuTri dotDieuTri);

//    @Mapping(source = "id", target = "id.id")
//    @Mapping(source = "bakbId", target = "id.bakbId")
//    @Mapping(source = "benhNhanId", target = "id.benhNhanId")
//    @Mapping(source = "donViId", target = "id.donViId")
    @Mapping(expression = "java(" +
            "this.benhAnKhamBenhMapper.fromId(" +
            "new vn.vnpt.domain.BenhAnKhamBenhId(" +
                "dotDieuTriDTO.getBakbId(), " +
                "dotDieuTriDTO.getBenhNhanId(), " +
                "dotDieuTriDTO.getDonViId()" +
                ")" +
            ")" +
        ")", target = "bakb")
//    @Mapping(target = "bakb.id", source = "bakbId")
    @Mapping(target = "bakbId", source = "bakbId")
//    @Mapping(target = "bakb.benhNhan.id", source = "benhNhanId")
    @Mapping(target = "benhNhanId", source = "benhNhanId")
//    @Mapping(target = "bakb.donVi.id", source = "donViId")
    @Mapping(target = "donViId", source = "donViId")
    public abstract DotDieuTri toEntity(DotDieuTriDTO dotDieuTriDTO);

    public DotDieuTri fromId(DotDieuTriId id) {
        if (id == null) {
            return null;
        }
        DotDieuTri dotDieuTri = new DotDieuTri();
        dotDieuTri.setId(id.getId());
        dotDieuTri.setBakbId(id.getBakbId());
        dotDieuTri.setDonViId(id.getDonViId());
        dotDieuTri.setBenhNhanId(id.getBenhNhanId());
        return dotDieuTri;
    }
}
