package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.HuongDieuTriDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link HuongDieuTri} and its DTO {@link HuongDieuTriDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface HuongDieuTriMapper extends EntityMapper<HuongDieuTriDTO, HuongDieuTri> {



    default HuongDieuTri fromId(Long id) {
        if (id == null) {
            return null;
        }
        HuongDieuTri huongDieuTri = new HuongDieuTri();
        huongDieuTri.setId(id);
        return huongDieuTri;
    }
}
