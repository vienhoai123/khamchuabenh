package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.NgheNghiepDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link NgheNghiep} and its DTO {@link NgheNghiepDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface NgheNghiepMapper extends EntityMapper<NgheNghiepDTO, NgheNghiep> {



    default NgheNghiep fromId(Long id) {
        if (id == null) {
            return null;
        }
        NgheNghiep ngheNghiep = new NgheNghiep();
        ngheNghiep.setId(id);
        return ngheNghiep;
    }
}
