package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.TheBhytDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TheBhyt} and its DTO {@link TheBhytDTO}.
 */
@Mapper(componentModel = "spring", uses = {BenhNhanMapper.class, DoiTuongBhytMapper.class, ThongTinBhxhMapper.class})
public interface TheBhytMapper extends EntityMapper<TheBhytDTO, TheBhyt> {

    @Mapping(source = "benhNhan.id", target = "benhNhanId")
    @Mapping(source = "doiTuongBhyt.id", target = "doiTuongBhytId")
    @Mapping(source = "thongTinBhxh.id", target = "thongTinBhxhId")
    TheBhytDTO toDto(TheBhyt theBhyt);

    @Mapping(source = "benhNhanId", target = "benhNhan")
    @Mapping(source = "doiTuongBhytId", target = "doiTuongBhyt")
    @Mapping(source = "thongTinBhxhId", target = "thongTinBhxh")
    TheBhyt toEntity(TheBhytDTO theBhytDTO);

    default TheBhyt fromId(Long id) {
        if (id == null) {
            return null;
        }
        TheBhyt theBhyt = new TheBhyt();
        theBhyt.setId(id);
        return theBhyt;
    }
}
