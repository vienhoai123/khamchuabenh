package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.DoiTuongBhytDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link DoiTuongBhyt} and its DTO {@link DoiTuongBhytDTO}.
 */
@Mapper(componentModel = "spring", uses = {NhomDoiTuongBhytMapper.class})
public interface DoiTuongBhytMapper extends EntityMapper<DoiTuongBhytDTO, DoiTuongBhyt> {

    @Mapping(source = "nhomDoiTuongBhyt.id", target = "nhomDoiTuongBhytId")
    DoiTuongBhytDTO toDto(DoiTuongBhyt doiTuongBhyt);

    @Mapping(source = "nhomDoiTuongBhytId", target = "nhomDoiTuongBhyt")
    DoiTuongBhyt toEntity(DoiTuongBhytDTO doiTuongBhytDTO);

    default DoiTuongBhyt fromId(Long id) {
        if (id == null) {
            return null;
        }
        DoiTuongBhyt doiTuongBhyt = new DoiTuongBhyt();
        doiTuongBhyt.setId(id);
        return doiTuongBhyt;
    }
}
