package vn.vnpt.service.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import vn.vnpt.domain.UserExtra;
import vn.vnpt.service.dto.UserExtraDTO;

/**
 * Mapper for the entity {@link UserExtra} and its DTO {@link UserExtraDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, UserTypeMapper.class})
public interface UserExtraMapper extends EntityMapper<UserExtraDTO, UserExtra> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "userType.id", target = "userTypeId")
    UserExtraDTO toDto(UserExtra userExtra);

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "userTypeId", target = "userType")
    @Mapping(target = "menus", ignore = true)
    @Mapping(target = "removeMenu", ignore = true)
    UserExtra toEntity(UserExtraDTO userExtraDTO);

    default UserExtra fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserExtra userExtra = new UserExtra();
        userExtra.setId(id);
        return userExtra;
    }
}
