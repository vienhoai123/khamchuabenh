package vn.vnpt.service.mapper;


import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.domain.*;
import vn.vnpt.service.dto.ChiDinhDichVuKhamDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ChiDinhDichVuKham} and its DTO {@link ChiDinhDichVuKhamDTO}.
 */
@Mapper(componentModel = "spring", uses = {ThongTinKhamBenhMapper.class, DichVuKhamMapper.class})
public abstract class ChiDinhDichVuKhamMapper implements EntityMapper<ChiDinhDichVuKhamDTO, ChiDinhDichVuKham> {
    @Autowired
    ThongTinKhamBenhMapper thongTinKhamBenhMapper;

    @Mapping(source = "ttkb.id", target = "ttkbId")
    @Mapping(source = "ttkb.thongTinKhoaId", target = "thongTinKhoaId")
    @Mapping(source = "ttkb.dotDieuTriId", target = "dotDieuTriId")
    @Mapping(source = "ttkb.bakbId", target = "bakbId")
    @Mapping(source = "ttkb.benhNhanId", target = "benhNhanId")
    @Mapping(source = "ttkb.donViId", target = "donViId")
    @Mapping(source = "dichVuKham.id", target = "dichVuKhamId")
    public abstract ChiDinhDichVuKhamDTO toDto(ChiDinhDichVuKham chiDinhDichVuKham);

    @Mapping(expression = "java(" +
        "this.thongTinKhamBenhMapper.fromId(" +
        "new vn.vnpt.domain.ThongTinKhamBenhId(" +
        "chiDinhDichVuKhamDTO.getTtkbId(), " +
        "chiDinhDichVuKhamDTO.getThongTinKhoaId(), " +
        "chiDinhDichVuKhamDTO.getDotDieuTriId(), " +
        "chiDinhDichVuKhamDTO.getBakbId(), " +
        "chiDinhDichVuKhamDTO.getBenhNhanId(), " +
        "chiDinhDichVuKhamDTO.getDonViId()" +
        ")" +
        ")" +
        ")", target = "ttkb")
    @Mapping(source = "dichVuKhamId", target = "dichVuKham")
    public abstract ChiDinhDichVuKham toEntity(ChiDinhDichVuKhamDTO chiDinhDichVuKhamDTO);

    public ChiDinhDichVuKham fromId(Long id) {
        if (id == null) {
            return null;
        }
        ChiDinhDichVuKham chiDinhDichVuKham = new ChiDinhDichVuKham();
        chiDinhDichVuKham.setId(id);
        return chiDinhDichVuKham;
    }
}
