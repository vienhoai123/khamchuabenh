package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.TinhThanhPhoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TinhThanhPho} and its DTO {@link TinhThanhPhoDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TinhThanhPhoMapper extends EntityMapper<TinhThanhPhoDTO, TinhThanhPho> {



    default TinhThanhPho fromId(Long id) {
        if (id == null) {
            return null;
        }
        TinhThanhPho tinhThanhPho = new TinhThanhPho();
        tinhThanhPho.setId(id);
        return tinhThanhPho;
    }
}
