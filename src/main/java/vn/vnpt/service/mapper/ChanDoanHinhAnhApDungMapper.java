package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.ChanDoanHinhAnhApDungDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ChanDoanHinhAnhApDung} and its DTO {@link ChanDoanHinhAnhApDungDTO}.
 */
@Mapper(componentModel = "spring", uses = {DotGiaDichVuBhxhMapper.class, ChanDoanHinhAnhMapper.class})
public interface ChanDoanHinhAnhApDungMapper extends EntityMapper<ChanDoanHinhAnhApDungDTO, ChanDoanHinhAnhApDung> {

    @Mapping(source = "dotGia.id", target = "dotGiaId")
    @Mapping(source = "cdha.id", target = "cdhaId")
    ChanDoanHinhAnhApDungDTO toDto(ChanDoanHinhAnhApDung chanDoanHinhAnhApDung);

    @Mapping(source = "dotGiaId", target = "dotGia")
    @Mapping(source = "cdhaId", target = "cdha")
    ChanDoanHinhAnhApDung toEntity(ChanDoanHinhAnhApDungDTO chanDoanHinhAnhApDungDTO);

    default ChanDoanHinhAnhApDung fromId(Long id) {
        if (id == null) {
            return null;
        }
        ChanDoanHinhAnhApDung chanDoanHinhAnhApDung = new ChanDoanHinhAnhApDung();
        chanDoanHinhAnhApDung.setId(id);
        return chanDoanHinhAnhApDung;
    }
}
