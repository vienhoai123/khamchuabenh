package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.ThongTinBhxhDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ThongTinBhxh} and its DTO {@link ThongTinBhxhDTO}.
 */
@Mapper(componentModel = "spring", uses = {DonViMapper.class})
public interface ThongTinBhxhMapper extends EntityMapper<ThongTinBhxhDTO, ThongTinBhxh> {

    @Mapping(source = "donVi.id", target = "donViId")
    ThongTinBhxhDTO toDto(ThongTinBhxh thongTinBhxh);

    @Mapping(source = "donViId", target = "donVi")
    ThongTinBhxh toEntity(ThongTinBhxhDTO thongTinBhxhDTO);

    default ThongTinBhxh fromId(Long id) {
        if (id == null) {
            return null;
        }
        ThongTinBhxh thongTinBhxh = new ThongTinBhxh();
        thongTinBhxh.setId(id);
        return thongTinBhxh;
    }
}
