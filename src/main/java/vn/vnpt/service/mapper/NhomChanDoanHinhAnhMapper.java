package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.NhomChanDoanHinhAnhDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link NhomChanDoanHinhAnh} and its DTO {@link NhomChanDoanHinhAnhDTO}.
 */
@Mapper(componentModel = "spring", uses = {DonViMapper.class})
public interface NhomChanDoanHinhAnhMapper extends EntityMapper<NhomChanDoanHinhAnhDTO, NhomChanDoanHinhAnh> {

    @Mapping(source = "donVi.id", target = "donViId")
    NhomChanDoanHinhAnhDTO toDto(NhomChanDoanHinhAnh nhomChanDoanHinhAnh);

    @Mapping(source = "donViId", target = "donVi")
    NhomChanDoanHinhAnh toEntity(NhomChanDoanHinhAnhDTO nhomChanDoanHinhAnhDTO);

    default NhomChanDoanHinhAnh fromId(Long id) {
        if (id == null) {
            return null;
        }
        NhomChanDoanHinhAnh nhomChanDoanHinhAnh = new NhomChanDoanHinhAnh();
        nhomChanDoanHinhAnh.setId(id);
        return nhomChanDoanHinhAnh;
    }
}
