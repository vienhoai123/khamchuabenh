package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.XetNghiemApDungDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link XetNghiemApDung} and its DTO {@link XetNghiemApDungDTO}.
 */
@Mapper(componentModel = "spring", uses = {DotGiaDichVuBhxhMapper.class, XetNghiemMapper.class})
public interface XetNghiemApDungMapper extends EntityMapper<XetNghiemApDungDTO, XetNghiemApDung> {

    @Mapping(source = "dotGia.id", target = "dotGiaId")
    @Mapping(source = "XN.id", target = "XNId")
    XetNghiemApDungDTO toDto(XetNghiemApDung xetNghiemApDung);

    @Mapping(source = "dotGiaId", target = "dotGia")
    @Mapping(source = "XNId", target = "XN")
    XetNghiemApDung toEntity(XetNghiemApDungDTO xetNghiemApDungDTO);

    default XetNghiemApDung fromId(Long id) {
        if (id == null) {
            return null;
        }
        XetNghiemApDung xetNghiemApDung = new XetNghiemApDung();
        xetNghiemApDung.setId(id);
        return xetNghiemApDung;
    }
}
