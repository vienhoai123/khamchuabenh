package vn.vnpt.service.mapper;


import vn.vnpt.domain.*;
import vn.vnpt.service.dto.DonViDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link DonVi} and its DTO {@link DonViDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DonViMapper extends EntityMapper<DonViDTO, DonVi> {



    default DonVi fromId(Long id) {
        if (id == null) {
            return null;
        }
        DonVi donVi = new DonVi();
        donVi.setId(id);
        return donVi;
    }
}
