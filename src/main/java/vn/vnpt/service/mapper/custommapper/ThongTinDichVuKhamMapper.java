package vn.vnpt.service.mapper.custommapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import vn.vnpt.service.dto.DichVuKhamApDungDTO;
import vn.vnpt.service.dto.DichVuKhamDTO;
import vn.vnpt.service.dto.customdto.ThongTinDichVuKhamDTO;

import java.math.BigDecimal;

@Mapper(componentModel = "spring")
public interface ThongTinDichVuKhamMapper {
    @Mapping(target = "id", source =  "dichVuKhamDTO.id")
    @Mapping(target = "tenHienThi", source = "dichVuKhamDTO.tenHienThi")
    @Mapping(target = "maDungChung", source = "dichVuKhamDTO.maDungChung")
    @Mapping(target = "maNoiBo", source = "dichVuKhamDTO.maNoiBo")
    @Mapping(target = "donGiaBenhVien", source = "dichVuKhamDTO.donGiaBenhVien")
    @Mapping(target = "donViTinh", source = "dichVuKhamDTO.donViTinh")
    @Mapping(target = "giaBhyt", source = "dichVuKhamApDungDTO.giaBhyt")
    @Mapping(target = "giaKhongBhyt", source = "dichVuKhamApDungDTO.giaKhongBhyt")
    @Mapping(target = "tenDichVuKhongBaoHiem", source = "dichVuKhamApDungDTO.tenDichVuKhongBaoHiem")
    @Mapping(target = "tienBenhNhanChi", source = "dichVuKhamApDungDTO.tienBenhNhanChi")
    @Mapping(target = "tienBhxhChi", source = "dichVuKhamApDungDTO.tienBhxhChi")
    @Mapping(target = "tienNgoaiBhyt", source = "dichVuKhamApDungDTO.tienNgoaiBhyt")
    @Mapping(target = "tongTienThanhToan", source = "dichVuKhamApDungDTO.tongTienThanhToan")
    @Mapping(target = "tyLeBhxhThanhToan", source = "dichVuKhamApDungDTO.tyLeBhxhThanhToan")
    ThongTinDichVuKhamDTO toThongTinDichVuKhamDTO(DichVuKhamDTO dichVuKhamDTO, DichVuKhamApDungDTO dichVuKhamApDungDTO);
}
