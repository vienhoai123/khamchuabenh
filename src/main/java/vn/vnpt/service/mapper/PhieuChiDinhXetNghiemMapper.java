package vn.vnpt.service.mapper;


import org.springframework.beans.factory.annotation.Autowired;
import vn.vnpt.domain.*;
import vn.vnpt.service.dto.PhieuChiDinhXetNghiemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link PhieuChiDinhXetNghiem} and its DTO {@link PhieuChiDinhXetNghiemDTO}.
 */
@Mapper(componentModel = "spring", uses = {BenhAnKhamBenhMapper.class})
public abstract class PhieuChiDinhXetNghiemMapper implements EntityMapper<PhieuChiDinhXetNghiemDTO, PhieuChiDinhXetNghiem> {

    @Autowired
    BenhAnKhamBenhMapper benhAnKhamBenhMapper;

//    @Mapping(source = "benhNhan.id", target = "benhNhanId")
//    @Mapping(source = "donVi.id", target = "donViId")
//    @Mapping(source = "bakb.id", target = "bakbId")
    public abstract PhieuChiDinhXetNghiemDTO toDto(PhieuChiDinhXetNghiem phieuChiDinhXetNghiem);

//    @Mapping(source = "benhNhanId", target = "benhNhan")
//    @Mapping(source = "donViId", target = "donVi")
//    @Mapping(source = "bakbId", target = "bakb.id")
    public abstract PhieuChiDinhXetNghiem toEntity(PhieuChiDinhXetNghiemDTO phieuChiDinhXetNghiemDTO);

    public PhieuChiDinhXetNghiem fromId(PhieuChiDinhXetNghiemId id) {
        if (id == null) {
            return null;
        }
        PhieuChiDinhXetNghiem phieuChiDinhXetNghiem = new PhieuChiDinhXetNghiem();
        phieuChiDinhXetNghiem.setId(id.getId());
        phieuChiDinhXetNghiem.setBakbId(id.getBakbId());
        phieuChiDinhXetNghiem.setBenhNhanId(id.getBenhNhanId());
        phieuChiDinhXetNghiem.setDonViId(id.getDonViId());
        return phieuChiDinhXetNghiem;
    }
}
