package vn.vnpt.service;

import vn.vnpt.service.dto.NhomDoiTuongBhytDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.NhomDoiTuongBhyt}.
 */
public interface NhomDoiTuongBhytService {

    /**
     * Save a nhomDoiTuongBhyt.
     *
     * @param nhomDoiTuongBhytDTO the entity to save.
     * @return the persisted entity.
     */
    NhomDoiTuongBhytDTO save(NhomDoiTuongBhytDTO nhomDoiTuongBhytDTO);

    /**
     * Get all the nhomDoiTuongBhyts.
     *
     * @return the list of entities.
     */
    List<NhomDoiTuongBhytDTO> findAll();

    /**
     * Get the "id" nhomDoiTuongBhyt.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<NhomDoiTuongBhytDTO> findOne(Long id);

    /**
     * Delete the "id" nhomDoiTuongBhyt.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
