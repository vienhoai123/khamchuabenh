package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.ThuThuatPhauThuatApDung;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.ThuThuatPhauThuatApDungRepository;
import vn.vnpt.service.dto.ThuThuatPhauThuatApDungCriteria;
import vn.vnpt.service.dto.ThuThuatPhauThuatApDungDTO;
import vn.vnpt.service.mapper.ThuThuatPhauThuatApDungMapper;

/**
 * Service for executing complex queries for {@link ThuThuatPhauThuatApDung} entities in the database.
 * The main input is a {@link ThuThuatPhauThuatApDungCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ThuThuatPhauThuatApDungDTO} or a {@link Page} of {@link ThuThuatPhauThuatApDungDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ThuThuatPhauThuatApDungQueryService extends QueryService<ThuThuatPhauThuatApDung> {

    private final Logger log = LoggerFactory.getLogger(ThuThuatPhauThuatApDungQueryService.class);

    private final ThuThuatPhauThuatApDungRepository thuThuatPhauThuatApDungRepository;

    private final ThuThuatPhauThuatApDungMapper thuThuatPhauThuatApDungMapper;

    public ThuThuatPhauThuatApDungQueryService(ThuThuatPhauThuatApDungRepository thuThuatPhauThuatApDungRepository, ThuThuatPhauThuatApDungMapper thuThuatPhauThuatApDungMapper) {
        this.thuThuatPhauThuatApDungRepository = thuThuatPhauThuatApDungRepository;
        this.thuThuatPhauThuatApDungMapper = thuThuatPhauThuatApDungMapper;
    }

    /**
     * Return a {@link List} of {@link ThuThuatPhauThuatApDungDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ThuThuatPhauThuatApDungDTO> findByCriteria(ThuThuatPhauThuatApDungCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ThuThuatPhauThuatApDung> specification = createSpecification(criteria);
        return thuThuatPhauThuatApDungMapper.toDto(thuThuatPhauThuatApDungRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ThuThuatPhauThuatApDungDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ThuThuatPhauThuatApDungDTO> findByCriteria(ThuThuatPhauThuatApDungCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ThuThuatPhauThuatApDung> specification = createSpecification(criteria);
        return thuThuatPhauThuatApDungRepository.findAll(specification, page)
            .map(thuThuatPhauThuatApDungMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ThuThuatPhauThuatApDungCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ThuThuatPhauThuatApDung> specification = createSpecification(criteria);
        return thuThuatPhauThuatApDungRepository.count(specification);
    }

    /**
     * Function to convert {@link ThuThuatPhauThuatApDungCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ThuThuatPhauThuatApDung> createSpecification(ThuThuatPhauThuatApDungCriteria criteria) {
        Specification<ThuThuatPhauThuatApDung> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ThuThuatPhauThuatApDung_.id));
            }
            if (criteria.getKyThuatCao() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getKyThuatCao(), ThuThuatPhauThuatApDung_.kyThuatCao));
            }
            if (criteria.getMaBaoCaoBhxh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaBaoCaoBhxh(), ThuThuatPhauThuatApDung_.maBaoCaoBhxh));
            }
            if (criteria.getMaBaoCaoBhyt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaBaoCaoBhyt(), ThuThuatPhauThuatApDung_.maBaoCaoBhyt));
            }
            if (criteria.getNgayApDung() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayApDung(), ThuThuatPhauThuatApDung_.ngayApDung));
            }
            if (criteria.getSoCongVanBhxh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSoCongVanBhxh(), ThuThuatPhauThuatApDung_.soCongVanBhxh));
            }
            if (criteria.getSoQuyetDinh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSoQuyetDinh(), ThuThuatPhauThuatApDung_.soQuyetDinh));
            }
            if (criteria.getTenBaoCaoBhxh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenBaoCaoBhxh(), ThuThuatPhauThuatApDung_.tenBaoCaoBhxh));
            }
            if (criteria.getTenDichVuKhongBhyt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenDichVuKhongBhyt(), ThuThuatPhauThuatApDung_.tenDichVuKhongBhyt));
            }
            if (criteria.getTienBenhNhanChi() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTienBenhNhanChi(), ThuThuatPhauThuatApDung_.tienBenhNhanChi));
            }
            if (criteria.getTienBhxhChi() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTienBhxhChi(), ThuThuatPhauThuatApDung_.tienBhxhChi));
            }
            if (criteria.getTienNgoaiBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTienNgoaiBhyt(), ThuThuatPhauThuatApDung_.tienNgoaiBhyt));
            }
            if (criteria.getTongTienThanhToan() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTongTienThanhToan(), ThuThuatPhauThuatApDung_.tongTienThanhToan));
            }
            if (criteria.getTyLeBhxhThanhToan() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTyLeBhxhThanhToan(), ThuThuatPhauThuatApDung_.tyLeBhxhThanhToan));
            }
            if (criteria.getGiaBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGiaBhyt(), ThuThuatPhauThuatApDung_.giaBhyt));
            }
            if (criteria.getGiaKhongBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGiaKhongBhyt(), ThuThuatPhauThuatApDung_.giaKhongBhyt));
            }
            if (criteria.getDoiTuongDacBiet() != null) {
                specification = specification.and(buildSpecification(criteria.getDoiTuongDacBiet(), ThuThuatPhauThuatApDung_.doiTuongDacBiet));
            }
            if (criteria.getNguonChi() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNguonChi(), ThuThuatPhauThuatApDung_.nguonChi));
            }
            if (criteria.getEnable() != null) {
                specification = specification.and(buildSpecification(criteria.getEnable(), ThuThuatPhauThuatApDung_.enable));
            }
            if (criteria.getDotGiaId() != null) {
                specification = specification.and(buildSpecification(criteria.getDotGiaId(),
                    root -> root.join(ThuThuatPhauThuatApDung_.dotGia, JoinType.LEFT).get(DotGiaDichVuBhxh_.id)));
            }
            if (criteria.getTtptId() != null) {
                specification = specification.and(buildSpecification(criteria.getTtptId(),
                    root -> root.join(ThuThuatPhauThuatApDung_.ttpt, JoinType.LEFT).get(ThuThuatPhauThuat_.id)));
            }
        }
        return specification;
    }
}
