package vn.vnpt.service.impl;

import vn.vnpt.service.BenhLyService;
import vn.vnpt.domain.BenhLy;
import vn.vnpt.repository.BenhLyRepository;
import vn.vnpt.service.dto.BenhLyDTO;
import vn.vnpt.service.mapper.BenhLyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link BenhLy}.
 */
@Service
@Transactional
public class BenhLyServiceImpl implements BenhLyService {

    private final Logger log = LoggerFactory.getLogger(BenhLyServiceImpl.class);

    private final BenhLyRepository benhLyRepository;

    private final BenhLyMapper benhLyMapper;

    public BenhLyServiceImpl(BenhLyRepository benhLyRepository, BenhLyMapper benhLyMapper) {
        this.benhLyRepository = benhLyRepository;
        this.benhLyMapper = benhLyMapper;
    }

    /**
     * Save a benhLy.
     *
     * @param benhLyDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public BenhLyDTO save(BenhLyDTO benhLyDTO) {
        log.debug("Request to save BenhLy : {}", benhLyDTO);
        BenhLy benhLy = benhLyMapper.toEntity(benhLyDTO);
        benhLy = benhLyRepository.save(benhLy);
        return benhLyMapper.toDto(benhLy);
    }

    /**
     * Get all the benhLies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<BenhLyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BenhLies");
        return benhLyRepository.findAll(pageable)
            .map(benhLyMapper::toDto);
    }

    /**
     * Get one benhLy by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<BenhLyDTO> findOne(Long id) {
        log.debug("Request to get BenhLy : {}", id);
        return benhLyRepository.findById(id)
            .map(benhLyMapper::toDto);
    }

    /**
     * Delete the benhLy by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete BenhLy : {}", id);
        benhLyRepository.deleteById(id);
    }
}
