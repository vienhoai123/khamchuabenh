package vn.vnpt.service.impl;

import vn.vnpt.service.ChanDoanHinhAnhApDungService;
import vn.vnpt.domain.ChanDoanHinhAnhApDung;
import vn.vnpt.repository.ChanDoanHinhAnhApDungRepository;
import vn.vnpt.service.dto.ChanDoanHinhAnhApDungDTO;
import vn.vnpt.service.mapper.ChanDoanHinhAnhApDungMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ChanDoanHinhAnhApDung}.
 */
@Service
@Transactional
public class ChanDoanHinhAnhApDungServiceImpl implements ChanDoanHinhAnhApDungService {

    private final Logger log = LoggerFactory.getLogger(ChanDoanHinhAnhApDungServiceImpl.class);

    private final ChanDoanHinhAnhApDungRepository chanDoanHinhAnhApDungRepository;

    private final ChanDoanHinhAnhApDungMapper chanDoanHinhAnhApDungMapper;

    public ChanDoanHinhAnhApDungServiceImpl(ChanDoanHinhAnhApDungRepository chanDoanHinhAnhApDungRepository, ChanDoanHinhAnhApDungMapper chanDoanHinhAnhApDungMapper) {
        this.chanDoanHinhAnhApDungRepository = chanDoanHinhAnhApDungRepository;
        this.chanDoanHinhAnhApDungMapper = chanDoanHinhAnhApDungMapper;
    }

    /**
     * Save a chanDoanHinhAnhApDung.
     *
     * @param chanDoanHinhAnhApDungDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ChanDoanHinhAnhApDungDTO save(ChanDoanHinhAnhApDungDTO chanDoanHinhAnhApDungDTO) {
        log.debug("Request to save ChanDoanHinhAnhApDung : {}", chanDoanHinhAnhApDungDTO);
        ChanDoanHinhAnhApDung chanDoanHinhAnhApDung = chanDoanHinhAnhApDungMapper.toEntity(chanDoanHinhAnhApDungDTO);
        chanDoanHinhAnhApDung = chanDoanHinhAnhApDungRepository.save(chanDoanHinhAnhApDung);
        return chanDoanHinhAnhApDungMapper.toDto(chanDoanHinhAnhApDung);
    }

    /**
     * Get all the chanDoanHinhAnhApDungs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ChanDoanHinhAnhApDungDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ChanDoanHinhAnhApDungs");
        return chanDoanHinhAnhApDungRepository.findAll(pageable)
            .map(chanDoanHinhAnhApDungMapper::toDto);
    }

    /**
     * Get one chanDoanHinhAnhApDung by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ChanDoanHinhAnhApDungDTO> findOne(Long id) {
        log.debug("Request to get ChanDoanHinhAnhApDung : {}", id);
        return chanDoanHinhAnhApDungRepository.findById(id)
            .map(chanDoanHinhAnhApDungMapper::toDto);
    }

    /**
     * Delete the chanDoanHinhAnhApDung by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ChanDoanHinhAnhApDung : {}", id);
        chanDoanHinhAnhApDungRepository.deleteById(id);
    }
}
