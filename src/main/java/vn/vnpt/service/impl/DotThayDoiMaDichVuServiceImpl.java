package vn.vnpt.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.domain.DotThayDoiMaDichVu;
import vn.vnpt.repository.DotThayDoiMaDichVuRepository;
import vn.vnpt.service.DotThayDoiMaDichVuService;
import vn.vnpt.service.dto.DotThayDoiMaDichVuDTO;
import vn.vnpt.service.mapper.DotThayDoiMaDichVuMapper;

import java.util.Optional;

/**
 * Service Implementation for managing {@link DotThayDoiMaDichVu}.
 */
@Service
@Transactional
public class DotThayDoiMaDichVuServiceImpl implements DotThayDoiMaDichVuService {

    private final Logger log = LoggerFactory.getLogger(DotThayDoiMaDichVuServiceImpl.class);

    private final DotThayDoiMaDichVuRepository dotThayDoiMaDichVuRepository;

    private final DotThayDoiMaDichVuMapper dotThayDoiMaDichVuMapper;

    public DotThayDoiMaDichVuServiceImpl(DotThayDoiMaDichVuRepository dotThayDoiMaDichVuRepository, DotThayDoiMaDichVuMapper dotThayDoiMaDichVuMapper) {
        this.dotThayDoiMaDichVuRepository = dotThayDoiMaDichVuRepository;
        this.dotThayDoiMaDichVuMapper = dotThayDoiMaDichVuMapper;
    }

    /**
     * Save a dotThayDoiMaDichVu.
     *
     * @param dotThayDoiMaDichVuDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public DotThayDoiMaDichVuDTO save(DotThayDoiMaDichVuDTO dotThayDoiMaDichVuDTO) {
        log.debug("Request to save DotThayDoiMaDichVu : {}", dotThayDoiMaDichVuDTO);
        DotThayDoiMaDichVu dotThayDoiMaDichVu = dotThayDoiMaDichVuMapper.toEntity(dotThayDoiMaDichVuDTO);
        dotThayDoiMaDichVu = dotThayDoiMaDichVuRepository.save(dotThayDoiMaDichVu);
        return dotThayDoiMaDichVuMapper.toDto(dotThayDoiMaDichVu);
    }

    /**
     * Get all the dotThayDoiMaDichVus.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DotThayDoiMaDichVuDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DotThayDoiMaDichVus");
        return dotThayDoiMaDichVuRepository.findAll(pageable)
            .map(dotThayDoiMaDichVuMapper::toDto);
    }

    /**
     * Get one dotThayDoiMaDichVu by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DotThayDoiMaDichVuDTO> findOne(Long id) {
        log.debug("Request to get DotThayDoiMaDichVu : {}", id);
        return dotThayDoiMaDichVuRepository.findById(id)
            .map(dotThayDoiMaDichVuMapper::toDto);
    }

    /**
     * Delete the dotThayDoiMaDichVu by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DotThayDoiMaDichVu : {}", id);
        dotThayDoiMaDichVuRepository.deleteById(id);
    }
}
