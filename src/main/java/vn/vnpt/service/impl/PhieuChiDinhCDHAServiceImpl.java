package vn.vnpt.service.impl;

import vn.vnpt.domain.PhieuChiDinhCDHAId;
import vn.vnpt.service.PhieuChiDinhCDHAService;
import vn.vnpt.domain.PhieuChiDinhCDHA;
import vn.vnpt.repository.PhieuChiDinhCDHARepository;
import vn.vnpt.service.dto.PhieuChiDinhCDHADTO;
import vn.vnpt.service.mapper.PhieuChiDinhCDHAMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link PhieuChiDinhCDHA}.
 */
@Service
@Transactional
public class PhieuChiDinhCDHAServiceImpl implements PhieuChiDinhCDHAService {

    private final Logger log = LoggerFactory.getLogger(PhieuChiDinhCDHAServiceImpl.class);

    private final PhieuChiDinhCDHARepository phieuChiDinhCDHARepository;

    private final PhieuChiDinhCDHAMapper phieuChiDinhCDHAMapper;

    public PhieuChiDinhCDHAServiceImpl(PhieuChiDinhCDHARepository phieuChiDinhCDHARepository, PhieuChiDinhCDHAMapper phieuChiDinhCDHAMapper) {
        this.phieuChiDinhCDHARepository = phieuChiDinhCDHARepository;
        this.phieuChiDinhCDHAMapper = phieuChiDinhCDHAMapper;
    }

    /**
     * Save a phieuChiDinhCDHA.
     *
     * @param phieuChiDinhCDHADTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PhieuChiDinhCDHADTO save(PhieuChiDinhCDHADTO phieuChiDinhCDHADTO) {
        log.debug("Request to save PhieuChiDinhCDHA : {}", phieuChiDinhCDHADTO);
        PhieuChiDinhCDHA phieuChiDinhCDHA = phieuChiDinhCDHAMapper.toEntity(phieuChiDinhCDHADTO);
        phieuChiDinhCDHA = phieuChiDinhCDHARepository.save(phieuChiDinhCDHA);
        return phieuChiDinhCDHAMapper.toDto(phieuChiDinhCDHA);
    }

    /**
     * Get all the phieuChiDinhCDHAS.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PhieuChiDinhCDHADTO> findAll(Pageable pageable) {
        log.debug("Request to get all PhieuChiDinhCDHAS");
        return phieuChiDinhCDHARepository.findAll(pageable)
            .map(phieuChiDinhCDHAMapper::toDto);
    }

    /**
     * Get one phieuChiDinhCDHA by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PhieuChiDinhCDHADTO> findOne(PhieuChiDinhCDHAId id) {
        log.debug("Request to get PhieuChiDinhCDHA : {}", id);
        return phieuChiDinhCDHARepository.findById(id)
            .map(phieuChiDinhCDHAMapper::toDto);
    }

    /**
     * Delete the phieuChiDinhCDHA by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(PhieuChiDinhCDHAId id) {
        log.debug("Request to delete PhieuChiDinhCDHA : {}", id);
        phieuChiDinhCDHARepository.deleteById(id);
    }
}
