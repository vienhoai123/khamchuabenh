package vn.vnpt.service.impl;

import vn.vnpt.service.NhomXetNghiemService;
import vn.vnpt.domain.NhomXetNghiem;
import vn.vnpt.repository.NhomXetNghiemRepository;
import vn.vnpt.service.dto.NhomXetNghiemDTO;
import vn.vnpt.service.mapper.NhomXetNghiemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link NhomXetNghiem}.
 */
@Service
@Transactional
public class NhomXetNghiemServiceImpl implements NhomXetNghiemService {

    private final Logger log = LoggerFactory.getLogger(NhomXetNghiemServiceImpl.class);

    private final NhomXetNghiemRepository nhomXetNghiemRepository;

    private final NhomXetNghiemMapper nhomXetNghiemMapper;

    public NhomXetNghiemServiceImpl(NhomXetNghiemRepository nhomXetNghiemRepository, NhomXetNghiemMapper nhomXetNghiemMapper) {
        this.nhomXetNghiemRepository = nhomXetNghiemRepository;
        this.nhomXetNghiemMapper = nhomXetNghiemMapper;
    }

    /**
     * Save a nhomXetNghiem.
     *
     * @param nhomXetNghiemDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public NhomXetNghiemDTO save(NhomXetNghiemDTO nhomXetNghiemDTO) {
        log.debug("Request to save NhomXetNghiem : {}", nhomXetNghiemDTO);
        NhomXetNghiem nhomXetNghiem = nhomXetNghiemMapper.toEntity(nhomXetNghiemDTO);
        nhomXetNghiem = nhomXetNghiemRepository.save(nhomXetNghiem);
        return nhomXetNghiemMapper.toDto(nhomXetNghiem);
    }

    /**
     * Get all the nhomXetNghiems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<NhomXetNghiemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all NhomXetNghiems");
        return nhomXetNghiemRepository.findAll(pageable)
            .map(nhomXetNghiemMapper::toDto);
    }

    /**
     * Get one nhomXetNghiem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<NhomXetNghiemDTO> findOne(Long id) {
        log.debug("Request to get NhomXetNghiem : {}", id);
        return nhomXetNghiemRepository.findById(id)
            .map(nhomXetNghiemMapper::toDto);
    }

    /**
     * Delete the nhomXetNghiem by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete NhomXetNghiem : {}", id);
        nhomXetNghiemRepository.deleteById(id);
    }
}
