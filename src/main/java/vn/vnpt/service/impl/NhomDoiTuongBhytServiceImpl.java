package vn.vnpt.service.impl;

import vn.vnpt.service.NhomDoiTuongBhytService;
import vn.vnpt.domain.NhomDoiTuongBhyt;
import vn.vnpt.repository.NhomDoiTuongBhytRepository;
import vn.vnpt.service.dto.NhomDoiTuongBhytDTO;
import vn.vnpt.service.mapper.NhomDoiTuongBhytMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link NhomDoiTuongBhyt}.
 */
@Service
@Transactional
public class NhomDoiTuongBhytServiceImpl implements NhomDoiTuongBhytService {

    private final Logger log = LoggerFactory.getLogger(NhomDoiTuongBhytServiceImpl.class);

    private final NhomDoiTuongBhytRepository nhomDoiTuongBhytRepository;

    private final NhomDoiTuongBhytMapper nhomDoiTuongBhytMapper;

    public NhomDoiTuongBhytServiceImpl(NhomDoiTuongBhytRepository nhomDoiTuongBhytRepository, NhomDoiTuongBhytMapper nhomDoiTuongBhytMapper) {
        this.nhomDoiTuongBhytRepository = nhomDoiTuongBhytRepository;
        this.nhomDoiTuongBhytMapper = nhomDoiTuongBhytMapper;
    }

    /**
     * Save a nhomDoiTuongBhyt.
     *
     * @param nhomDoiTuongBhytDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public NhomDoiTuongBhytDTO save(NhomDoiTuongBhytDTO nhomDoiTuongBhytDTO) {
        log.debug("Request to save NhomDoiTuongBhyt : {}", nhomDoiTuongBhytDTO);
        NhomDoiTuongBhyt nhomDoiTuongBhyt = nhomDoiTuongBhytMapper.toEntity(nhomDoiTuongBhytDTO);
        nhomDoiTuongBhyt = nhomDoiTuongBhytRepository.save(nhomDoiTuongBhyt);
        return nhomDoiTuongBhytMapper.toDto(nhomDoiTuongBhyt);
    }

    /**
     * Get all the nhomDoiTuongBhyts.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<NhomDoiTuongBhytDTO> findAll() {
        log.debug("Request to get all NhomDoiTuongBhyts");
        return nhomDoiTuongBhytRepository.findAll().stream()
            .map(nhomDoiTuongBhytMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one nhomDoiTuongBhyt by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<NhomDoiTuongBhytDTO> findOne(Long id) {
        log.debug("Request to get NhomDoiTuongBhyt : {}", id);
        return nhomDoiTuongBhytRepository.findById(id)
            .map(nhomDoiTuongBhytMapper::toDto);
    }

    /**
     * Delete the nhomDoiTuongBhyt by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete NhomDoiTuongBhyt : {}", id);
        nhomDoiTuongBhytRepository.deleteById(id);
    }
}
