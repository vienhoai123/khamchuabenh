package vn.vnpt.service;

import vn.vnpt.domain.ThongTinKhoaId;
import vn.vnpt.service.dto.ThongTinKhoaDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.ThongTinKhoa}.
 */
public interface ThongTinKhoaService {

    /**
     * Save a thongTinKhoa.
     *
     * @param thongTinKhoaDTO the entity to save.
     * @return the persisted entity.
     */
    ThongTinKhoaDTO save(ThongTinKhoaDTO thongTinKhoaDTO);

    /**
     * Get all the thongTinKhoas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ThongTinKhoaDTO> findAll(Pageable pageable);

    /**
     * Get the "id" thongTinKhoa.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ThongTinKhoaDTO> findOne(ThongTinKhoaId id);

    /**
     * Delete the "id" thongTinKhoa.
     *
     * @param id the id of the entity.
     */
    void delete(ThongTinKhoaId id);
}
