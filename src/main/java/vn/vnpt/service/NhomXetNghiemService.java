package vn.vnpt.service;

import vn.vnpt.service.dto.NhomXetNghiemDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.NhomXetNghiem}.
 */
public interface NhomXetNghiemService {

    /**
     * Save a nhomXetNghiem.
     *
     * @param nhomXetNghiemDTO the entity to save.
     * @return the persisted entity.
     */
    NhomXetNghiemDTO save(NhomXetNghiemDTO nhomXetNghiemDTO);

    /**
     * Get all the nhomXetNghiems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<NhomXetNghiemDTO> findAll(Pageable pageable);

    /**
     * Get the "id" nhomXetNghiem.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<NhomXetNghiemDTO> findOne(Long id);

    /**
     * Delete the "id" nhomXetNghiem.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
