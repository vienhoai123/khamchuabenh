package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.ChanDoanHinhAnh;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.ChanDoanHinhAnhRepository;
import vn.vnpt.service.dto.ChanDoanHinhAnhCriteria;
import vn.vnpt.service.dto.ChanDoanHinhAnhDTO;
import vn.vnpt.service.mapper.ChanDoanHinhAnhMapper;

/**
 * Service for executing complex queries for {@link ChanDoanHinhAnh} entities in the database.
 * The main input is a {@link ChanDoanHinhAnhCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ChanDoanHinhAnhDTO} or a {@link Page} of {@link ChanDoanHinhAnhDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ChanDoanHinhAnhQueryService extends QueryService<ChanDoanHinhAnh> {

    private final Logger log = LoggerFactory.getLogger(ChanDoanHinhAnhQueryService.class);

    private final ChanDoanHinhAnhRepository chanDoanHinhAnhRepository;

    private final ChanDoanHinhAnhMapper chanDoanHinhAnhMapper;

    public ChanDoanHinhAnhQueryService(ChanDoanHinhAnhRepository chanDoanHinhAnhRepository, ChanDoanHinhAnhMapper chanDoanHinhAnhMapper) {
        this.chanDoanHinhAnhRepository = chanDoanHinhAnhRepository;
        this.chanDoanHinhAnhMapper = chanDoanHinhAnhMapper;
    }

    /**
     * Return a {@link List} of {@link ChanDoanHinhAnhDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ChanDoanHinhAnhDTO> findByCriteria(ChanDoanHinhAnhCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ChanDoanHinhAnh> specification = createSpecification(criteria);
        return chanDoanHinhAnhMapper.toDto(chanDoanHinhAnhRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ChanDoanHinhAnhDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ChanDoanHinhAnhDTO> findByCriteria(ChanDoanHinhAnhCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ChanDoanHinhAnh> specification = createSpecification(criteria);
        return chanDoanHinhAnhRepository.findAll(specification, page)
            .map(chanDoanHinhAnhMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ChanDoanHinhAnhCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ChanDoanHinhAnh> specification = createSpecification(criteria);
        return chanDoanHinhAnhRepository.count(specification);
    }

    /**
     * Function to convert {@link ChanDoanHinhAnhCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ChanDoanHinhAnh> createSpecification(ChanDoanHinhAnhCriteria criteria) {
        Specification<ChanDoanHinhAnh> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ChanDoanHinhAnh_.id));
            }
            if (criteria.getDeleted() != null) {
                specification = specification.and(buildSpecification(criteria.getDeleted(), ChanDoanHinhAnh_.deleted));
            }
            if (criteria.getDichVuYeuCau() != null) {
                specification = specification.and(buildSpecification(criteria.getDichVuYeuCau(), ChanDoanHinhAnh_.dichVuYeuCau));
            }
            if (criteria.getDoppler() != null) {
                specification = specification.and(buildSpecification(criteria.getDoppler(), ChanDoanHinhAnh_.doppler));
            }
            if (criteria.getDonGiaBenhVien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDonGiaBenhVien(), ChanDoanHinhAnh_.donGiaBenhVien));
            }
            if (criteria.getEnabled() != null) {
                specification = specification.and(buildSpecification(criteria.getEnabled(), ChanDoanHinhAnh_.enabled));
            }
            if (criteria.getGoiHanChiDinh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGoiHanChiDinh(), ChanDoanHinhAnh_.goiHanChiDinh));
            }
            if (criteria.getPhamViChiDinh() != null) {
                specification = specification.and(buildSpecification(criteria.getPhamViChiDinh(), ChanDoanHinhAnh_.phamViChiDinh));
            }
            if (criteria.getPhanTheoGioiTinh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPhanTheoGioiTinh(), ChanDoanHinhAnh_.phanTheoGioiTinh));
            }
            if (criteria.getSapXep() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSapXep(), ChanDoanHinhAnh_.sapXep));
            }
            if (criteria.getSoLanThucHien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLanThucHien(), ChanDoanHinhAnh_.soLanThucHien));
            }
            if (criteria.getSoLuongFilm() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLuongFilm(), ChanDoanHinhAnh_.soLuongFilm));
            }
            if (criteria.getSoLuongQuiDoi() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLuongQuiDoi(), ChanDoanHinhAnh_.soLuongQuiDoi));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), ChanDoanHinhAnh_.ten));
            }
            if (criteria.getTenHienThi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTenHienThi(), ChanDoanHinhAnh_.tenHienThi));
            }
            if (criteria.getMaNoiBo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaNoiBo(), ChanDoanHinhAnh_.maNoiBo));
            }
            if (criteria.getMaDungChung() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaDungChung(), ChanDoanHinhAnh_.maDungChung));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(),
                    root -> root.join(ChanDoanHinhAnh_.donVi, JoinType.LEFT).get(DonVi_.id)));
            }
            if (criteria.getDotMaId() != null) {
                specification = specification.and(buildSpecification(criteria.getDotMaId(),
                    root -> root.join(ChanDoanHinhAnh_.dotMa, JoinType.LEFT).get(DotThayDoiMaDichVu_.id)));
            }
            if (criteria.getLoaicdhaId() != null) {
                specification = specification.and(buildSpecification(criteria.getLoaicdhaId(),
                    root -> root.join(ChanDoanHinhAnh_.loaicdha, JoinType.LEFT).get(LoaiChanDoanHinhAnh_.id)));
            }
        }
        return specification;
    }
}
