package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.ChiDinhDichVuKham;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.ChiDinhDichVuKhamRepository;
import vn.vnpt.service.dto.ChiDinhDichVuKhamCriteria;
import vn.vnpt.service.dto.ChiDinhDichVuKhamDTO;
import vn.vnpt.service.mapper.ChiDinhDichVuKhamMapper;

/**
 * Service for executing complex queries for {@link ChiDinhDichVuKham} entities in the database.
 * The main input is a {@link ChiDinhDichVuKhamCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ChiDinhDichVuKhamDTO} or a {@link Page} of {@link ChiDinhDichVuKhamDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ChiDinhDichVuKhamQueryService extends QueryService<ChiDinhDichVuKham> {

    private final Logger log = LoggerFactory.getLogger(ChiDinhDichVuKhamQueryService.class);

    private final ChiDinhDichVuKhamRepository chiDinhDichVuKhamRepository;

    private final ChiDinhDichVuKhamMapper chiDinhDichVuKhamMapper;

    public ChiDinhDichVuKhamQueryService(ChiDinhDichVuKhamRepository chiDinhDichVuKhamRepository, ChiDinhDichVuKhamMapper chiDinhDichVuKhamMapper) {
        this.chiDinhDichVuKhamRepository = chiDinhDichVuKhamRepository;
        this.chiDinhDichVuKhamMapper = chiDinhDichVuKhamMapper;
    }

    /**
     * Return a {@link List} of {@link ChiDinhDichVuKhamDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ChiDinhDichVuKhamDTO> findByCriteria(ChiDinhDichVuKhamCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ChiDinhDichVuKham> specification = createSpecification(criteria);
        return chiDinhDichVuKhamMapper.toDto(chiDinhDichVuKhamRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ChiDinhDichVuKhamDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ChiDinhDichVuKhamDTO> findByCriteria(ChiDinhDichVuKhamCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ChiDinhDichVuKham> specification = createSpecification(criteria);
        return chiDinhDichVuKhamRepository.findAll(specification, page)
            .map(chiDinhDichVuKhamMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ChiDinhDichVuKhamCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ChiDinhDichVuKham> specification = createSpecification(criteria);
        return chiDinhDichVuKhamRepository.count(specification);
    }

    /**
     * Function to convert {@link ChiDinhDichVuKhamCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ChiDinhDichVuKham> createSpecification(ChiDinhDichVuKhamCriteria criteria) {
        Specification<ChiDinhDichVuKham> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ChiDinhDichVuKham_.id));
            }
            if (criteria.getCongKhamBanDau() != null) {
                specification = specification.and(buildSpecification(criteria.getCongKhamBanDau(), ChiDinhDichVuKham_.congKhamBanDau));
            }
            if (criteria.getDaThanhToan() != null) {
                specification = specification.and(buildSpecification(criteria.getDaThanhToan(), ChiDinhDichVuKham_.daThanhToan));
            }
            if (criteria.getDonGia() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDonGia(), ChiDinhDichVuKham_.donGia));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), ChiDinhDichVuKham_.ten));
            }
            if (criteria.getThoiGianChiDinh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThoiGianChiDinh(), ChiDinhDichVuKham_.thoiGianChiDinh));
            }
            if (criteria.getTyLeThanhToan() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTyLeThanhToan(), ChiDinhDichVuKham_.tyLeThanhToan));
            }
            if (criteria.getCoBaoHiem() != null) {
                specification = specification.and(buildSpecification(criteria.getCoBaoHiem(), ChiDinhDichVuKham_.coBaoHiem));
            }
            if (criteria.getGiaBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGiaBhyt(), ChiDinhDichVuKham_.giaBhyt));
            }
            if (criteria.getGiaKhongBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGiaKhongBhyt(), ChiDinhDichVuKham_.giaKhongBhyt));
            }
            if (criteria.getMaDungChung() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMaDungChung(), ChiDinhDichVuKham_.maDungChung));
            }
            if (criteria.getDichVuYeuCau() != null) {
                specification = specification.and(buildSpecification(criteria.getDichVuYeuCau(), ChiDinhDichVuKham_.dichVuYeuCau));
            }
            if (criteria.getNam() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNam(), ChiDinhDichVuKham_.nam));
            }
            if (criteria.getTtkbId() != null) {
                specification = specification.and(buildSpecification(criteria.getTtkbId(),
                    root -> root.join(ChiDinhDichVuKham_.ttkb, JoinType.LEFT).get(ThongTinKhamBenh_.id)));
            }
            if (criteria.getDichVuKhamId() != null) {
                specification = specification.and(buildSpecification(criteria.getDichVuKhamId(),
                    root -> root.join(ChiDinhDichVuKham_.dichVuKham, JoinType.LEFT).get(DichVuKham_.id)));
            }
        }
        return specification;
    }
}
