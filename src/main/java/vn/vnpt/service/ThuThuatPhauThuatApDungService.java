package vn.vnpt.service;

import vn.vnpt.service.dto.ThuThuatPhauThuatApDungDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.ThuThuatPhauThuatApDung}.
 */
public interface ThuThuatPhauThuatApDungService {

    /**
     * Save a thuThuatPhauThuatApDung.
     *
     * @param thuThuatPhauThuatApDungDTO the entity to save.
     * @return the persisted entity.
     */
    ThuThuatPhauThuatApDungDTO save(ThuThuatPhauThuatApDungDTO thuThuatPhauThuatApDungDTO);

    /**
     * Get all the thuThuatPhauThuatApDungs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ThuThuatPhauThuatApDungDTO> findAll(Pageable pageable);

    /**
     * Get the "id" thuThuatPhauThuatApDung.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ThuThuatPhauThuatApDungDTO> findOne(Long id);

    /**
     * Delete the "id" thuThuatPhauThuatApDung.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
