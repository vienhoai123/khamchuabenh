package vn.vnpt.service;

import vn.vnpt.service.dto.LoaiBenhLyDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.LoaiBenhLy}.
 */
public interface LoaiBenhLyService {

    /**
     * Save a loaiBenhLy.
     *
     * @param loaiBenhLyDTO the entity to save.
     * @return the persisted entity.
     */
    LoaiBenhLyDTO save(LoaiBenhLyDTO loaiBenhLyDTO);

    /**
     * Get all the loaiBenhLies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<LoaiBenhLyDTO> findAll(Pageable pageable);

    /**
     * Get the "id" loaiBenhLy.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<LoaiBenhLyDTO> findOne(Long id);

    /**
     * Delete the "id" loaiBenhLy.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
