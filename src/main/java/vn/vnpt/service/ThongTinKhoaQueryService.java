package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.ThongTinKhoa;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.ThongTinKhoaRepository;
import vn.vnpt.service.dto.ThongTinKhoaCriteria;
import vn.vnpt.service.dto.ThongTinKhoaDTO;
import vn.vnpt.service.mapper.ThongTinKhoaMapper;

/**
 * Service for executing complex queries for {@link ThongTinKhoa} entities in the database.
 * The main input is a {@link ThongTinKhoaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ThongTinKhoaDTO} or a {@link Page} of {@link ThongTinKhoaDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ThongTinKhoaQueryService extends QueryService<ThongTinKhoa> {

    private final Logger log = LoggerFactory.getLogger(ThongTinKhoaQueryService.class);

    private final ThongTinKhoaRepository thongTinKhoaRepository;

    private final ThongTinKhoaMapper thongTinKhoaMapper;

    public ThongTinKhoaQueryService(ThongTinKhoaRepository thongTinKhoaRepository, ThongTinKhoaMapper thongTinKhoaMapper) {
        this.thongTinKhoaRepository = thongTinKhoaRepository;
        this.thongTinKhoaMapper = thongTinKhoaMapper;
    }

    /**
     * Return a {@link List} of {@link ThongTinKhoaDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ThongTinKhoaDTO> findByCriteria(ThongTinKhoaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ThongTinKhoa> specification = createSpecification(criteria);
        return thongTinKhoaMapper.toDto(thongTinKhoaRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ThongTinKhoaDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ThongTinKhoaDTO> findByCriteria(ThongTinKhoaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ThongTinKhoa> specification = createSpecification(criteria);
        return thongTinKhoaRepository.findAll(specification, page)
            .map(thongTinKhoaMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ThongTinKhoaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ThongTinKhoa> specification = createSpecification(criteria);
        return thongTinKhoaRepository.count(specification);
    }

    /**
     * Function to convert {@link ThongTinKhoaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ThongTinKhoa> createSpecification(ThongTinKhoaCriteria criteria) {
        Specification<ThongTinKhoa> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ThongTinKhoa_.id));
            }
            if (criteria.getThoiGianNhanBenh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThoiGianNhanBenh(), ThongTinKhoa_.thoiGianNhanBenh));
            }
            if (criteria.getThoiGianChuyenKhoa() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThoiGianChuyenKhoa(), ThongTinKhoa_.thoiGianChuyenKhoa));
            }
            if (criteria.getKhoaId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getKhoaId(), ThongTinKhoa_.khoaId));
            }
            if (criteria.getKhoaChuyenDen() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getKhoaChuyenDen(), ThongTinKhoa_.khoaChuyenDen));
            }
            if (criteria.getKhoaChuyenDi() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getKhoaChuyenDi(), ThongTinKhoa_.khoaChuyenDi));
            }
            if (criteria.getNhanVienNhanBenh() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNhanVienNhanBenh(), ThongTinKhoa_.nhanVienNhanBenh));
            }
            if (criteria.getNhanVienChuyenKhoa() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNhanVienChuyenKhoa(), ThongTinKhoa_.nhanVienChuyenKhoa));
            }
            if (criteria.getSoNgay() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoNgay(), ThongTinKhoa_.soNgay));
            }
            if (criteria.getTrangThai() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTrangThai(), ThongTinKhoa_.trangThai));
            }
            if (criteria.getNam() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNam(), ThongTinKhoa_.nam));
            }
            if (criteria.getBakbId() != null) {
                specification = specification.and(buildSpecification(criteria.getBakbId(),
                    root -> root.join(ThongTinKhoa_.dotDieuTri,JoinType.LEFT).join(DotDieuTri_.bakb, JoinType.LEFT).get(BenhAnKhamBenh_.id)));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(),
                    root -> root.join(ThongTinKhoa_.dotDieuTri,JoinType.LEFT).join(DotDieuTri_.bakb, JoinType.LEFT).join(BenhAnKhamBenh_.donVi,JoinType.LEFT).get(DonVi_.id)));
            }
            if (criteria.getBenhNhanId() != null) {
                specification = specification.and(buildSpecification(criteria.getBenhNhanId(),
                    root -> root.join(ThongTinKhoa_.dotDieuTri,JoinType.LEFT).join(DotDieuTri_.bakb, JoinType.LEFT).join(BenhAnKhamBenh_.benhNhan,JoinType.LEFT).get(BenhNhan_.id)));
            }
            if (criteria.getDotDieuTriId() != null) {
                specification = specification.and(buildSpecification(criteria.getDotDieuTriId(),
                    root -> root.join(ThongTinKhoa_.dotDieuTri,JoinType.LEFT).get(DotDieuTri_.id)));
            }
        }
        return specification;
    }
}
