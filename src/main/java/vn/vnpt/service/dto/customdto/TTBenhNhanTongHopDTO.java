package vn.vnpt.service.dto.customdto;

import vn.vnpt.service.dto.BenhNhanDTO;
import vn.vnpt.service.dto.DoiTuongBhytDTO;
import vn.vnpt.service.dto.TheBhytDTO;

import java.io.Serializable;
import java.util.Objects;

public class TTBenhNhanTongHopDTO implements Serializable {
    private BenhNhanDTO benhNhanDTO;
    private TheBhytDTO theBhytDTO;
    private DoiTuongBhytDTO doiTuongBhytDTO;

    public TTBenhNhanTongHopDTO() {
    }

    public TTBenhNhanTongHopDTO(BenhNhanDTO benhNhanDTO, TheBhytDTO theBhytDTO, DoiTuongBhytDTO doiTuongBhytDTO) {
        this.benhNhanDTO = benhNhanDTO;
        this.theBhytDTO = theBhytDTO;
        this.doiTuongBhytDTO = doiTuongBhytDTO;
    }

    public BenhNhanDTO getBenhNhanDTO() {
        return benhNhanDTO;
    }

    public void setBenhNhanDTO(BenhNhanDTO benhNhanDTO) {
        this.benhNhanDTO = benhNhanDTO;
    }

    public TheBhytDTO getTheBhytDTO() {
        return theBhytDTO;
    }

    public void setTheBhytDTO(TheBhytDTO theBhytDTO) {
        this.theBhytDTO = theBhytDTO;
    }

    public DoiTuongBhytDTO getDoiTuongBhytDTO() {
        return doiTuongBhytDTO;
    }

    public void setDoiTuongBhytDTO(DoiTuongBhytDTO doiTuongBhytDTO) {
        this.doiTuongBhytDTO = doiTuongBhytDTO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TTBenhNhanTongHopDTO)) return false;
        TTBenhNhanTongHopDTO that = (TTBenhNhanTongHopDTO) o;
        return Objects.equals(getBenhNhanDTO(), that.getBenhNhanDTO()) &&
            Objects.equals(getTheBhytDTO(), that.getTheBhytDTO()) &&
            Objects.equals(getDoiTuongBhytDTO(), that.getDoiTuongBhytDTO());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBenhNhanDTO(), getTheBhytDTO(), getDoiTuongBhytDTO());
    }
}
