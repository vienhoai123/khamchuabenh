package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import vn.vnpt.domain.BenhAnKhamBenh;
import vn.vnpt.domain.BenhAnKhamBenhId;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.BenhAnKhamBenh} entity.
 */
public class BenhAnKhamBenhDTO implements Serializable {

    @NotNull
    private Long id;

    @NotNull
    private Long benhNhanId;

    @NotNull
    private Long donViId;

    /**
     * Trạng thái bệnh nhân đã tiếp nhận\n1: Bệnh nhân cũ\n0: Bệnh nhân mới
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái bệnh nhân đã tiếp nhận\n1: Bệnh nhân cũ\n0: Bệnh nhân mới", required = true)
    private Boolean benhNhanCu;

    /**
     * Cảnh báo còn thuộc khi bệnh nhân khám BHYT
     */
    @NotNull
    @ApiModelProperty(value = "Cảnh báo còn thuộc khi bệnh nhân khám BHYT", required = true)
    private Boolean canhBao;

    /**
     * Trạng thái cấp cứu của bệnh nhân\n1: Cấp cứu\n0: Bình thường
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái cấp cứu của bệnh nhân\n1: Cấp cứu\n0: Bình thường", required = true)
    private Boolean capCuu;

    /**
     * Trạng thái chỉ có năm sinh của bệnh nhân\n1: Chỉ có năm sinh\n0: Đủ thông tin
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái chỉ có năm sinh của bệnh nhân\n1: Chỉ có năm sinh\n0: Đủ thông tin", required = true)
    private Boolean chiCoNamSinh;

    /**
     * Thông tin chứng minh nhân dân của bệnh nhân
     */
    @Size(max = 20)
    @ApiModelProperty(value = "Thông tin chứng minh nhân dân của bệnh nhân")
    private String cmndBenhNhan;

    /**
     * Thông tin chứng minh nhân dân của người nhà bệnh nhân
     */
    @Size(max = 20)
    @ApiModelProperty(value = "Thông tin chứng minh nhân dân của người nhà bệnh nhân")
    private String cmndNguoiNha;

    /**
     * Trạng thái có bảo hiểm của
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái có bảo hiểm của", required = true)
    private Boolean coBaoHiem;

    /**
     * Thông tin địa chỉ của bệnh nhân
     */
    @NotNull
    @Size(max = 500)
    @ApiModelProperty(value = "Thông tin địa chỉ của bệnh nhân", required = true)
    private String diaChi;

    /**
     * Thông tin email của bệnh nhân
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Thông tin email của bệnh nhân")
    private String email;

    /**
     * Trạng thái giới tính của bệnh nhân:\n\n1: Nam\n.\n0: Nữ.
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái giới tính của bệnh nhân:\n\n1: Nam\n.\n0: Nữ.", required = true)
    private Integer gioiTinh;

    /**
     * Thông tin kháng thể của bệnh nhân
     */
    @Size(max = 5)
    @ApiModelProperty(value = "Thông tin kháng thể của bệnh nhân")
    private String khangThe;

    /**
     * Lần khám trong ngày của bệnh nhân
     */
    @ApiModelProperty(value = "Lần khám trong ngày của bệnh nhân")
    private Integer lanKhamTrongNgay;

    /**
     * Mã loại giấy tờ của trẻ em như giấy khai sinh, giấy chứng sinh
     */
    @ApiModelProperty(value = "Mã loại giấy tờ của trẻ em như giấy khai sinh, giấy chứng sinh")
    private Integer loaiGiayToTreEm;

    /**
     * Thông tin ngày cấp CMND của bệnh nhân
     */
    @ApiModelProperty(value = "Thông tin ngày cấp CMND của bệnh nhân")
    private LocalDate ngayCapCmnd;

    /**
     * Thời gian miễn cùng chi trả của thẻ BHYT
     */
    @ApiModelProperty(value = "Thời gian miễn cùng chi trả của thẻ BHYT")
    private LocalDate ngayMienCungChiTra;

    /**
     * Thôi tin ngày sinh của bệnh nhân
     */
    @NotNull
    @ApiModelProperty(value = "Thôi tin ngày sinh của bệnh nhân", required = true)
    private LocalDate ngaySinh;

    /**
     * Thông tin nhóm máu cảu bệnh nhân
     */
    @Size(max = 5)
    @ApiModelProperty(value = "Thông tin nhóm máu cảu bệnh nhân")
    private String nhomMau;

    /**
     * Thông tin nơi cấp của bệnh nhân
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Thông tin nơi cấp của bệnh nhân")
    private String noiCapCmnd;

    /**
     * Thông tin nơi làm việc của bệnh nhân
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Thông tin nơi làm việc của bệnh nhân")
    private String noiLamViec;

    /**
     * Thông tin số điện thoại của bệnh nhân
     */
    @Size(max = 15)
    @ApiModelProperty(value = "Thông tin số điện thoại của bệnh nhân")
    private String phone;

    /**
     * Thông tin số điện thoại người nhà
     */
    @Size(max = 15)
    @ApiModelProperty(value = "Thông tin số điện thoại người nhà")
    private String phoneNguoiNha;

    /**
     * Thông tin quốc tịch của bệnh nhân
     */
    @Size(max = 200)
    @ApiModelProperty(value = "Thông tin quốc tịch của bệnh nhân")
    private String quocTich;

    /**
     * Tên bệnh nhân
     */
    @NotNull
    @Size(max = 200)
    @ApiModelProperty(value = "Tên bệnh nhân", required = true)
    private String tenBenhNhan;

    /**
     * Tên người nhà bệnh nhân
     */
    @Size(max = 200)
    @ApiModelProperty(value = "Tên người nhà bệnh nhân")
    private String tenNguoiNha;

    /**
     * Thông tin tháng tuổi nếu là bệnh nhân nhi
     */
    @ApiModelProperty(value = "Thông tin tháng tuổi nếu là bệnh nhân nhi")
    private Integer thangTuoi;

    /**
     * Thông tin thời gian tiếp nhận bệnh nhân
     */
    @NotNull
    @ApiModelProperty(value = "Thông tin thời gian tiếp nhận bệnh nhân", required = true)
    private LocalDate thoiGianTiepNhan;

    /**
     * Thông tin buổi của bệnh nhân
     */
    @ApiModelProperty(value = "Thông tin buổi của bệnh nhân")
    private Integer tuoi;

    /**
     * Trạng thái ưu tiên của bệnh nhân:\n1: Ưu tiên\n0: Không ưu tiên
     */
    @ApiModelProperty(value = "Trạng thái ưu tiên của bệnh nhân:\n1: Ưu tiên\n0: Không ưu tiên")
    private Integer uuTien;

    /**
     * Mã bệnh nhân theo công thức UUID
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Mã bệnh nhân theo công thức UUID")
    private String uuid;

    /**
     * Trạng thái của Bệnh án khám bệnh:\n1: Chờ khám, mới vào khoa\n2: Đang khám, đang điều trị\n3: Đã khám, xuất viện\n4: Chuyển khoa phòng khám\n5: Chuyển viện, chuyển tuyến\n6: Nhập viện,\n7: Tử vong;\n8: trốn viện;
     */
    @ApiModelProperty(value = "Trạng thái của Bệnh án khám bệnh:\n1: Chờ khám, mới vào khoa\n2: Đang khám, đang điều trị\n3: Đã khám, xuất viện\n4: Chuyển khoa phòng khám\n5: Chuyển viện, chuyển tuyến\n6: Nhập viện,\n7: Tử vong;\n8: trốn viện;")
    private Integer trangThai;

    /**
     * Mã khoa hoàn tất khám
     */
    @ApiModelProperty(value = "Mã khoa hoàn tất khám")
    private Integer maKhoaHoanTatKham;

    /**
     * Mã phòng hoàn tất khám
     */
    @ApiModelProperty(value = "Mã phòng hoàn tất khám")
    private Integer maPhongHoanTatKham;

    /**
     * Tên khoa hoàn tất khám
     */
    @Size(max = 200)
    @ApiModelProperty(value = "Tên khoa hoàn tất khám")
    private String tenKhoaHoanTatKham;

    /**
     * Tên phòng hoàn tất khám
     */
    @Size(max = 200)
    @ApiModelProperty(value = "Tên phòng hoàn tất khám")
    private String tenPhongHoanTatKham;

    /**
     * Thời gian hoàn tất khám
     */
    @ApiModelProperty(value = "Thời gian hoàn tất khám")
    private LocalDate thoiGianHoanTatKham;

    /**
     * Số bệnh án
     */
    @ApiModelProperty(value = "Số bệnh án")
    private String soBenhAn;

    /**
     * Số bệnh án theo khoa
     */
    @ApiModelProperty(value = "Số bệnh án theo khoa")
    private String soBenhAnKhoa;

    @NotNull
    private Integer nam;
    /**
     * Mã nhân viên tiếp nhận
     */
    @ApiModelProperty(value = "Mã nhân viên tiếp nhận")

    private Long nhanVienTiepNhanId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Id phòng hoàn tất khám
     */
    @ApiModelProperty(value = "Id phòng hoàn tất khám")



    private Long phongTiepNhanId;

    public Long getBenhNhanId(){
        return this.benhNhanId;
    }

    public void setBenhNhanId(Long benhNhanId){
        this.benhNhanId = benhNhanId;
    }

    public Long getDonViId(){
        return this.donViId;
    }

    public void setDonViId(Long donViId){
        this.donViId = donViId;
    }

    public Boolean isBenhNhanCu() {
        return benhNhanCu;
    }

    public void setBenhNhanCu(Boolean benhNhanCu) {
        this.benhNhanCu = benhNhanCu;
    }

    public Boolean isCanhBao() {
        return canhBao;
    }

    public void setCanhBao(Boolean canhBao) {
        this.canhBao = canhBao;
    }

    public Boolean isCapCuu() {
        return capCuu;
    }

    public void setCapCuu(Boolean capCuu) {
        this.capCuu = capCuu;
    }

    public Boolean isChiCoNamSinh() {
        return chiCoNamSinh;
    }

    public void setChiCoNamSinh(Boolean chiCoNamSinh) {
        this.chiCoNamSinh = chiCoNamSinh;
    }

    public String getCmndBenhNhan() {
        return cmndBenhNhan;
    }

    public void setCmndBenhNhan(String cmndBenhNhan) {
        this.cmndBenhNhan = cmndBenhNhan;
    }

    public String getCmndNguoiNha() {
        return cmndNguoiNha;
    }

    public void setCmndNguoiNha(String cmndNguoiNha) {
        this.cmndNguoiNha = cmndNguoiNha;
    }

    public Boolean isCoBaoHiem() {
        return coBaoHiem;
    }

    public void setCoBaoHiem(Boolean coBaoHiem) {
        this.coBaoHiem = coBaoHiem;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getGioiTinh() {
        return gioiTinh;
    }

    public void setGioiTinh(Integer gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public String getKhangThe() {
        return khangThe;
    }

    public void setKhangThe(String khangThe) {
        this.khangThe = khangThe;
    }

    public Integer getLanKhamTrongNgay() {
        return lanKhamTrongNgay;
    }

    public void setLanKhamTrongNgay(Integer lanKhamTrongNgay) {
        this.lanKhamTrongNgay = lanKhamTrongNgay;
    }

    public Integer getLoaiGiayToTreEm() {
        return loaiGiayToTreEm;
    }

    public void setLoaiGiayToTreEm(Integer loaiGiayToTreEm) {
        this.loaiGiayToTreEm = loaiGiayToTreEm;
    }

    public LocalDate getNgayCapCmnd() {
        return ngayCapCmnd;
    }

    public void setNgayCapCmnd(LocalDate ngayCapCmnd) {
        this.ngayCapCmnd = ngayCapCmnd;
    }

    public LocalDate getNgayMienCungChiTra() {
        return ngayMienCungChiTra;
    }

    public void setNgayMienCungChiTra(LocalDate ngayMienCungChiTra) {
        this.ngayMienCungChiTra = ngayMienCungChiTra;
    }

    public LocalDate getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(LocalDate ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getNhomMau() {
        return nhomMau;
    }

    public void setNhomMau(String nhomMau) {
        this.nhomMau = nhomMau;
    }

    public String getNoiCapCmnd() {
        return noiCapCmnd;
    }

    public void setNoiCapCmnd(String noiCapCmnd) {
        this.noiCapCmnd = noiCapCmnd;
    }

    public String getNoiLamViec() {
        return noiLamViec;
    }

    public void setNoiLamViec(String noiLamViec) {
        this.noiLamViec = noiLamViec;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneNguoiNha() {
        return phoneNguoiNha;
    }

    public void setPhoneNguoiNha(String phoneNguoiNha) {
        this.phoneNguoiNha = phoneNguoiNha;
    }

    public String getQuocTich() {
        return quocTich;
    }

    public void setQuocTich(String quocTich) {
        this.quocTich = quocTich;
    }

    public String getTenBenhNhan() {
        return tenBenhNhan;
    }

    public void setTenBenhNhan(String tenBenhNhan) {
        this.tenBenhNhan = tenBenhNhan;
    }

    public String getTenNguoiNha() {
        return tenNguoiNha;
    }

    public void setTenNguoiNha(String tenNguoiNha) {
        this.tenNguoiNha = tenNguoiNha;
    }

    public Integer getThangTuoi() {
        return thangTuoi;
    }

    public void setThangTuoi(Integer thangTuoi) {
        this.thangTuoi = thangTuoi;
    }

    public LocalDate getThoiGianTiepNhan() {
        return thoiGianTiepNhan;
    }

    public void setThoiGianTiepNhan(LocalDate thoiGianTiepNhan) {
        this.thoiGianTiepNhan = thoiGianTiepNhan;
    }

    public Integer getTuoi() {
        return tuoi;
    }

    public void setTuoi(Integer tuoi) {
        this.tuoi = tuoi;
    }

    public Integer getUuTien() {
        return uuTien;
    }

    public void setUuTien(Integer uuTien) {
        this.uuTien = uuTien;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Integer trangThai) {
        this.trangThai = trangThai;
    }

    public Integer getMaKhoaHoanTatKham() {
        return maKhoaHoanTatKham;
    }

    public void setMaKhoaHoanTatKham(Integer maKhoaHoanTatKham) {
        this.maKhoaHoanTatKham = maKhoaHoanTatKham;
    }

    public Integer getMaPhongHoanTatKham() {
        return maPhongHoanTatKham;
    }

    public void setMaPhongHoanTatKham(Integer maPhongHoanTatKham) {
        this.maPhongHoanTatKham = maPhongHoanTatKham;
    }

    public String getTenKhoaHoanTatKham() {
        return tenKhoaHoanTatKham;
    }

    public void setTenKhoaHoanTatKham(String tenKhoaHoanTatKham) {
        this.tenKhoaHoanTatKham = tenKhoaHoanTatKham;
    }

    public String getTenPhongHoanTatKham() {
        return tenPhongHoanTatKham;
    }

    public void setTenPhongHoanTatKham(String tenPhongHoanTatKham) {
        this.tenPhongHoanTatKham = tenPhongHoanTatKham;
    }

    public LocalDate getThoiGianHoanTatKham() {
        return thoiGianHoanTatKham;
    }

    public void setThoiGianHoanTatKham(LocalDate thoiGianHoanTatKham) {
        this.thoiGianHoanTatKham = thoiGianHoanTatKham;
    }

    public String getSoBenhAn() {
        return soBenhAn;
    }

    public void setSoBenhAn(String soBenhAn) {
        this.soBenhAn = soBenhAn;
    }

    public String getSoBenhAnKhoa() {
        return soBenhAnKhoa;
    }

    public void setSoBenhAnKhoa(String soBenhAnKhoa) {
        this.soBenhAnKhoa = soBenhAnKhoa;
    }

    public Integer getNam() {
        return nam;
    }

    public void setNam(Integer nam) {
        this.nam = nam;
    }

    public Long getNhanVienTiepNhanId() {
        return nhanVienTiepNhanId;
    }

    public void setNhanVienTiepNhanId(Long nhanVienTiepNhanId) {
        this.nhanVienTiepNhanId = nhanVienTiepNhanId;
    }

    public Long getPhongTiepNhanId() {
        return phongTiepNhanId;
    }

    public void setPhongTiepNhanId(Long phongTiepNhanId) {
        this.phongTiepNhanId = phongTiepNhanId;
    }

    public BenhAnKhamBenhId getCompositeId(){
        return new BenhAnKhamBenhId(this.getId(),this.getBenhNhanId(),this.getDonViId());
    }

    public void setCompositeId(BenhAnKhamBenhId id){
        this.id = id.getId();
        this.benhNhanId = id.getBenhNhanId();
        this.donViId = id.getDonViId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BenhAnKhamBenhDTO)) return false;
        BenhAnKhamBenhDTO that = (BenhAnKhamBenhDTO) o;
        return Objects.equals(getId(), that.getId()) &&
            Objects.equals(getBenhNhanId(), that.getBenhNhanId()) &&
            Objects.equals(getDonViId(), that.getDonViId()) &&
            Objects.equals(benhNhanCu, that.benhNhanCu) &&
            Objects.equals(canhBao, that.canhBao) &&
            Objects.equals(capCuu, that.capCuu) &&
            Objects.equals(chiCoNamSinh, that.chiCoNamSinh) &&
            Objects.equals(getCmndBenhNhan(), that.getCmndBenhNhan()) &&
            Objects.equals(getCmndNguoiNha(), that.getCmndNguoiNha()) &&
            Objects.equals(coBaoHiem, that.coBaoHiem) &&
            Objects.equals(getDiaChi(), that.getDiaChi()) &&
            Objects.equals(getEmail(), that.getEmail()) &&
            Objects.equals(getGioiTinh(), that.getGioiTinh()) &&
            Objects.equals(getKhangThe(), that.getKhangThe()) &&
            Objects.equals(getLanKhamTrongNgay(), that.getLanKhamTrongNgay()) &&
            Objects.equals(getLoaiGiayToTreEm(), that.getLoaiGiayToTreEm()) &&
            Objects.equals(getNgayCapCmnd(), that.getNgayCapCmnd()) &&
            Objects.equals(getNgayMienCungChiTra(), that.getNgayMienCungChiTra()) &&
            Objects.equals(getNgaySinh(), that.getNgaySinh()) &&
            Objects.equals(getNhomMau(), that.getNhomMau()) &&
            Objects.equals(getNoiCapCmnd(), that.getNoiCapCmnd()) &&
            Objects.equals(getNoiLamViec(), that.getNoiLamViec()) &&
            Objects.equals(getPhone(), that.getPhone()) &&
            Objects.equals(getPhoneNguoiNha(), that.getPhoneNguoiNha()) &&
            Objects.equals(getQuocTich(), that.getQuocTich()) &&
            Objects.equals(getTenBenhNhan(), that.getTenBenhNhan()) &&
            Objects.equals(getTenNguoiNha(), that.getTenNguoiNha()) &&
            Objects.equals(getThangTuoi(), that.getThangTuoi()) &&
            Objects.equals(getThoiGianTiepNhan(), that.getThoiGianTiepNhan()) &&
            Objects.equals(getTuoi(), that.getTuoi()) &&
            Objects.equals(getUuTien(), that.getUuTien()) &&
            Objects.equals(getUuid(), that.getUuid()) &&
            Objects.equals(getTrangThai(), that.getTrangThai()) &&
            Objects.equals(getMaKhoaHoanTatKham(), that.getMaKhoaHoanTatKham()) &&
            Objects.equals(getMaPhongHoanTatKham(), that.getMaPhongHoanTatKham()) &&
            Objects.equals(getTenKhoaHoanTatKham(), that.getTenKhoaHoanTatKham()) &&
            Objects.equals(getTenPhongHoanTatKham(), that.getTenPhongHoanTatKham()) &&
            Objects.equals(getThoiGianHoanTatKham(), that.getThoiGianHoanTatKham()) &&
            Objects.equals(getSoBenhAn(), that.getSoBenhAn()) &&
            Objects.equals(getSoBenhAnKhoa(), that.getSoBenhAnKhoa()) &&
            Objects.equals(getNam(), that.getNam()) &&
            Objects.equals(getNhanVienTiepNhanId(), that.getNhanVienTiepNhanId()) &&
            Objects.equals(getPhongTiepNhanId(), that.getPhongTiepNhanId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBenhNhanId(), getDonViId(), benhNhanCu, canhBao, capCuu, chiCoNamSinh, getCmndBenhNhan(), getCmndNguoiNha(), coBaoHiem, getDiaChi(), getEmail(), getGioiTinh(), getKhangThe(), getLanKhamTrongNgay(), getLoaiGiayToTreEm(), getNgayCapCmnd(), getNgayMienCungChiTra(), getNgaySinh(), getNhomMau(), getNoiCapCmnd(), getNoiLamViec(), getPhone(), getPhoneNguoiNha(), getQuocTich(), getTenBenhNhan(), getTenNguoiNha(), getThangTuoi(), getThoiGianTiepNhan(), getTuoi(), getUuTien(), getUuid(), getTrangThai(), getMaKhoaHoanTatKham(), getMaPhongHoanTatKham(), getTenKhoaHoanTatKham(), getTenPhongHoanTatKham(), getThoiGianHoanTatKham(), getSoBenhAn(), getSoBenhAnKhoa(), getNam(), getNhanVienTiepNhanId(), getPhongTiepNhanId());
    }

    @Override
    public String toString() {
        return "BenhAnKhamBenhDTO{" +
            "id=" + id +
            ", benhNhanId=" + benhNhanId +
            ", donViId=" + donViId +
            ", benhNhanCu=" + benhNhanCu +
            ", canhBao=" + canhBao +
            ", capCuu=" + capCuu +
            ", chiCoNamSinh=" + chiCoNamSinh +
            ", cmndBenhNhan='" + cmndBenhNhan + '\'' +
            ", cmndNguoiNha='" + cmndNguoiNha + '\'' +
            ", coBaoHiem=" + coBaoHiem +
            ", diaChi='" + diaChi + '\'' +
            ", email='" + email + '\'' +
            ", gioiTinh=" + gioiTinh +
            ", khangThe='" + khangThe + '\'' +
            ", lanKhamTrongNgay=" + lanKhamTrongNgay +
            ", loaiGiayToTreEm=" + loaiGiayToTreEm +
            ", ngayCapCmnd=" + ngayCapCmnd +
            ", ngayMienCungChiTra=" + ngayMienCungChiTra +
            ", ngaySinh=" + ngaySinh +
            ", nhomMau='" + nhomMau + '\'' +
            ", noiCapCmnd='" + noiCapCmnd + '\'' +
            ", noiLamViec='" + noiLamViec + '\'' +
            ", phone='" + phone + '\'' +
            ", phoneNguoiNha='" + phoneNguoiNha + '\'' +
            ", quocTich='" + quocTich + '\'' +
            ", tenBenhNhan='" + tenBenhNhan + '\'' +
            ", tenNguoiNha='" + tenNguoiNha + '\'' +
            ", thangTuoi=" + thangTuoi +
            ", thoiGianTiepNhan=" + thoiGianTiepNhan +
            ", tuoi=" + tuoi +
            ", uuTien=" + uuTien +
            ", uuid='" + uuid + '\'' +
            ", trangThai=" + trangThai +
            ", maKhoaHoanTatKham=" + maKhoaHoanTatKham +
            ", maPhongHoanTatKham=" + maPhongHoanTatKham +
            ", tenKhoaHoanTatKham='" + tenKhoaHoanTatKham + '\'' +
            ", tenPhongHoanTatKham='" + tenPhongHoanTatKham + '\'' +
            ", thoiGianHoanTatKham=" + thoiGianHoanTatKham +
            ", soBenhAn='" + soBenhAn + '\'' +
            ", soBenhAnKhoa='" + soBenhAnKhoa + '\'' +
            ", nam=" + nam +
            ", nhanVienTiepNhanId=" + nhanVienTiepNhanId +
            ", phongTiepNhanId=" + phongTiepNhanId +
            '}';
    }
}
