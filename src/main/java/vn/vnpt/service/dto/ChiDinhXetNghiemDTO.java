package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.ChiDinhXetNghiem} entity.
 */
public class ChiDinhXetNghiemDTO implements Serializable {

    private Long id;

    /**
     * Trạng thái được thanh toán bảo hiểm của chỉ định: \r\n1: Có bảo hiểm. \r\n0: Không bảo hiểm. \r\n
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái được thanh toán bảo hiểm của chỉ định: \r\n1: Có bảo hiểm. \r\n0: Không bảo hiểm. \r\n", required = true)
    private Boolean coBaoHiem;

    /**
     * Trạng thái có kết quả của chỉ định: \r\n1: Có kết quả. \r\n0: Chưa có kết quả. \r\n
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái có kết quả của chỉ định: \r\n1: Có kết quả. \r\n0: Chưa có kết quả. \r\n", required = true)
    private Boolean coKetQua;

    /**
     * Trạng thái đã thanh toán của chỉ định: \r\n0: Chưa thanh toán. \r\n1: Đã thanh toán. \r\n-1: Đã hủy thanh toán
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái đã thanh toán của chỉ định: \r\n0: Chưa thanh toán. \r\n1: Đã thanh toán. \r\n-1: Đã hủy thanh toán", required = true)
    private Integer daThanhToan;

    /**
     * Trạng thái đã thanh toán chênh lệch của chỉ định: \r\n0: Chưa thanh toán. \r\n1: Đã thanh toán. \r\n-1: Đã hủy thanh toán
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái đã thanh toán chênh lệch của chỉ định: \r\n0: Chưa thanh toán. \r\n1: Đã thanh toán. \r\n-1: Đã hủy thanh toán", required = true)
    private Integer daThanhToanChenhLech;

    /**
     * Trạng thái đã thực hiện của chỉ định: \r\n0: Chưa thực hiện. \r\n1: Đã thực hiện. \r\n-1: Đã hủy thực hiện
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái đã thực hiện của chỉ định: \r\n0: Chưa thực hiện. \r\n1: Đã thực hiện. \r\n-1: Đã hủy thực hiện", required = true)
    private Integer daThucHien;

    /**
     * Đơn giá
     */
    @NotNull
    @ApiModelProperty(value = "Đơn giá", required = true)
    private BigDecimal donGia;

    /**
     * Đơn giá Bảo hiểm y tế
     */
    @NotNull
    @ApiModelProperty(value = "Đơn giá Bảo hiểm y tế", required = true)
    private BigDecimal donGiaBhyt;

    /**
     * Đơn giá không Bảo hiểm y tế
     */
    @ApiModelProperty(value = "Đơn giá không Bảo hiểm y tế")
    private BigDecimal donGiaKhongBhyt;

    /**
     * Ghi chú chỉ định
     */
    @Size(max = 2000)
    @ApiModelProperty(value = "Ghi chú chỉ định")
    private String ghiChuChiDinh;

    /**
     * Mô tả chỉ định
     */
    @Size(max = 2000)
    @ApiModelProperty(value = "Mô tả chỉ định")
    private String moTa;

    /**
     * Mã nhân viên chỉ định
     */
    @ApiModelProperty(value = "Mã nhân viên chỉ định")
    private Long nguoiChiDinhId;

    /**
     * Số lượng
     */
    @ApiModelProperty(value = "Số lượng")
    private Integer soLuong;

    /**
     * Tên chỉ định
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Tên chỉ định")
    private String ten;

    /**
     * Thành tiền
     */
    @NotNull
    @ApiModelProperty(value = "Thành tiền", required = true)
    private BigDecimal thanhTien;

    /**
     * Thành tiền BHYT
     */
    @ApiModelProperty(value = "Thành tiền BHYT")
    private BigDecimal thanhTienBhyt;

    /**
     * Thành tiền không BHYT
     */
    @ApiModelProperty(value = "Thành tiền không BHYT")
    private BigDecimal thanhTienKhongBHYT;

    /**
     * Thời gian chỉ định
     */
    @ApiModelProperty(value = "Thời gian chỉ định")
    private LocalDate thoiGianChiDinh;

    /**
     * Thời gian tạo
     */
    @NotNull
    @ApiModelProperty(value = "Thời gian tạo", required = true)
    private LocalDate thoiGianTao;

    /**
     * Tiền ngoài BHYT
     */
    @NotNull
    @ApiModelProperty(value = "Tiền ngoài BHYT", required = true)
    private BigDecimal tienNgoaiBHYT;

    /**
     * Trạng thái thanh toán chênh lệch: \r\n0: Không có thanh toán chênh lệch. \r\n1: Có thanh toán chênh lệch
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái thanh toán chênh lệch: \r\n0: Không có thanh toán chênh lệch. \r\n1: Có thanh toán chênh lệch", required = true)
    private Boolean thanhToanChenhLech;

    /**
     * Tỷ lệ thanh toán
     */
    @ApiModelProperty(value = "Tỷ lệ thanh toán")
    private Integer tyLeThanhToan;

    /**
     * Mã dùng chung cho dịch vụ
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Mã dùng chung cho dịch vụ")
    private String maDungChung;

    /**
     * Trạng thái theo yêu cầu của dịch vụ: 0: Bình thường. 1: Theo yêu cầu
     */
    @ApiModelProperty(value = "Trạng thái theo yêu cầu của dịch vụ: 0: Bình thường. 1: Theo yêu cầu")
    private Boolean dichVuYeuCau;

    @NotNull
    private Integer nam;


    private Long donViId;

    private Long benhNhanId;

    private Long bakbId;

    private Long phieuCDId;

    private Long phongId;

    private Long xetNghiemId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isCoBaoHiem() {
        return coBaoHiem;
    }

    public void setCoBaoHiem(Boolean coBaoHiem) {
        this.coBaoHiem = coBaoHiem;
    }

    public Boolean isCoKetQua() {
        return coKetQua;
    }

    public void setCoKetQua(Boolean coKetQua) {
        this.coKetQua = coKetQua;
    }

    public Integer getDaThanhToan() {
        return daThanhToan;
    }

    public void setDaThanhToan(Integer daThanhToan) {
        this.daThanhToan = daThanhToan;
    }

    public Integer getDaThanhToanChenhLech() {
        return daThanhToanChenhLech;
    }

    public void setDaThanhToanChenhLech(Integer daThanhToanChenhLech) {
        this.daThanhToanChenhLech = daThanhToanChenhLech;
    }

    public Integer getDaThucHien() {
        return daThucHien;
    }

    public void setDaThucHien(Integer daThucHien) {
        this.daThucHien = daThucHien;
    }

    public BigDecimal getDonGia() {
        return donGia;
    }

    public void setDonGia(BigDecimal donGia) {
        this.donGia = donGia;
    }

    public BigDecimal getDonGiaBhyt() {
        return donGiaBhyt;
    }

    public void setDonGiaBhyt(BigDecimal donGiaBhyt) {
        this.donGiaBhyt = donGiaBhyt;
    }

    public BigDecimal getDonGiaKhongBhyt() {
        return donGiaKhongBhyt;
    }

    public void setDonGiaKhongBhyt(BigDecimal donGiaKhongBhyt) {
        this.donGiaKhongBhyt = donGiaKhongBhyt;
    }

    public String getGhiChuChiDinh() {
        return ghiChuChiDinh;
    }

    public void setGhiChuChiDinh(String ghiChuChiDinh) {
        this.ghiChuChiDinh = ghiChuChiDinh;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Long getNguoiChiDinhId() {
        return nguoiChiDinhId;
    }

    public void setNguoiChiDinhId(Long nguoiChiDinhId) {
        this.nguoiChiDinhId = nguoiChiDinhId;
    }

    public Integer getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(Integer soLuong) {
        this.soLuong = soLuong;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public BigDecimal getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(BigDecimal thanhTien) {
        this.thanhTien = thanhTien;
    }

    public BigDecimal getThanhTienBhyt() {
        return thanhTienBhyt;
    }

    public void setThanhTienBhyt(BigDecimal thanhTienBhyt) {
        this.thanhTienBhyt = thanhTienBhyt;
    }

    public BigDecimal getThanhTienKhongBHYT() {
        return thanhTienKhongBHYT;
    }

    public void setThanhTienKhongBHYT(BigDecimal thanhTienKhongBHYT) {
        this.thanhTienKhongBHYT = thanhTienKhongBHYT;
    }

    public LocalDate getThoiGianChiDinh() {
        return thoiGianChiDinh;
    }

    public void setThoiGianChiDinh(LocalDate thoiGianChiDinh) {
        this.thoiGianChiDinh = thoiGianChiDinh;
    }

    public LocalDate getThoiGianTao() {
        return thoiGianTao;
    }

    public void setThoiGianTao(LocalDate thoiGianTao) {
        this.thoiGianTao = thoiGianTao;
    }

    public BigDecimal getTienNgoaiBHYT() {
        return tienNgoaiBHYT;
    }

    public void setTienNgoaiBHYT(BigDecimal tienNgoaiBHYT) {
        this.tienNgoaiBHYT = tienNgoaiBHYT;
    }

    public Boolean isThanhToanChenhLech() {
        return thanhToanChenhLech;
    }

    public void setThanhToanChenhLech(Boolean thanhToanChenhLech) {
        this.thanhToanChenhLech = thanhToanChenhLech;
    }

    public Integer getTyLeThanhToan() {
        return tyLeThanhToan;
    }

    public void setTyLeThanhToan(Integer tyLeThanhToan) {
        this.tyLeThanhToan = tyLeThanhToan;
    }

    public String getMaDungChung() {
        return maDungChung;
    }

    public void setMaDungChung(String maDungChung) {
        this.maDungChung = maDungChung;
    }

    public Boolean isDichVuYeuCau() {
        return dichVuYeuCau;
    }

    public void setDichVuYeuCau(Boolean dichVuYeuCau) {
        this.dichVuYeuCau = dichVuYeuCau;
    }

    public Integer getNam() {
        return nam;
    }

    public void setNam(Integer nam) {
        this.nam = nam;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public Long getBakbId() {
        return bakbId;
    }

    public void setBakbId(Long bakbId) {
        this.bakbId = bakbId;
    }

    public Long getPhieuCDId() {
        return phieuCDId;
    }

    public void setPhieuCDId(Long phieuCDId) {
        this.phieuCDId = phieuCDId;
    }

    public Long getPhongId() {
        return phongId;
    }

    public void setPhongId(Long phongId) {
        this.phongId = phongId;
    }

    public Long getXetNghiemId() {
        return xetNghiemId;
    }

    public void setXetNghiemId(Long xetNghiemId) {
        this.xetNghiemId = xetNghiemId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ChiDinhXetNghiemDTO chiDinhXetNghiemDTO = (ChiDinhXetNghiemDTO) o;
        if (chiDinhXetNghiemDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), chiDinhXetNghiemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ChiDinhXetNghiemDTO{" +
            "id=" + getId() +
            ", coBaoHiem='" + isCoBaoHiem() + "'" +
            ", coKetQua='" + isCoKetQua() + "'" +
            ", daThanhToan=" + getDaThanhToan() +
            ", daThanhToanChenhLech=" + getDaThanhToanChenhLech() +
            ", daThucHien=" + getDaThucHien() +
            ", donGia=" + getDonGia() +
            ", donGiaBhyt=" + getDonGiaBhyt() +
            ", donGiaKhongBhyt=" + getDonGiaKhongBhyt() +
            ", ghiChuChiDinh='" + getGhiChuChiDinh() + "'" +
            ", moTa='" + getMoTa() + "'" +
            ", nguoiChiDinhId=" + getNguoiChiDinhId() +
            ", soLuong=" + getSoLuong() +
            ", ten='" + getTen() + "'" +
            ", thanhTien=" + getThanhTien() +
            ", thanhTienBhyt=" + getThanhTienBhyt() +
            ", thanhTienKhongBHYT=" + getThanhTienKhongBHYT() +
            ", thoiGianChiDinh='" + getThoiGianChiDinh() + "'" +
            ", thoiGianTao='" + getThoiGianTao() + "'" +
            ", tienNgoaiBHYT=" + getTienNgoaiBHYT() +
            ", thanhToanChenhLech='" + isThanhToanChenhLech() + "'" +
            ", tyLeThanhToan=" + getTyLeThanhToan() +
            ", maDungChung='" + getMaDungChung() + "'" +
            ", dichVuYeuCau='" + isDichVuYeuCau() + "'" +
            ", nam=" + getNam() +
            ", donViId=" + getDonViId() +
            ", benhNhanId=" + getBenhNhanId() +
            ", bakbId=" + getBakbId() +
            ", phieuCDId=" + getPhieuCDId() +
            ", phongId=" + getPhongId() +
            ", xetNghiemId=" + getXetNghiemId() +
            "}";
    }
}
