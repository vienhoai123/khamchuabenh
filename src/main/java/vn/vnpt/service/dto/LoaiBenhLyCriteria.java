package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.LoaiBenhLy} entity. This class is used
 * in {@link vn.vnpt.web.rest.LoaiBenhLyResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /loai-benh-lies?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class LoaiBenhLyCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter kyHieu;

    private StringFilter moTa;

    private StringFilter ten;

    private StringFilter tenTiengAnh;

    public LoaiBenhLyCriteria() {
    }

    public LoaiBenhLyCriteria(LoaiBenhLyCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.kyHieu = other.kyHieu == null ? null : other.kyHieu.copy();
        this.moTa = other.moTa == null ? null : other.moTa.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.tenTiengAnh = other.tenTiengAnh == null ? null : other.tenTiengAnh.copy();
    }

    @Override
    public LoaiBenhLyCriteria copy() {
        return new LoaiBenhLyCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getKyHieu() {
        return kyHieu;
    }

    public void setKyHieu(StringFilter kyHieu) {
        this.kyHieu = kyHieu;
    }

    public StringFilter getMoTa() {
        return moTa;
    }

    public void setMoTa(StringFilter moTa) {
        this.moTa = moTa;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public StringFilter getTenTiengAnh() {
        return tenTiengAnh;
    }

    public void setTenTiengAnh(StringFilter tenTiengAnh) {
        this.tenTiengAnh = tenTiengAnh;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final LoaiBenhLyCriteria that = (LoaiBenhLyCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(kyHieu, that.kyHieu) &&
            Objects.equals(moTa, that.moTa) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(tenTiengAnh, that.tenTiengAnh);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        kyHieu,
        moTa,
        ten,
        tenTiengAnh
        );
    }

    @Override
    public String toString() {
        return "LoaiBenhLyCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (kyHieu != null ? "kyHieu=" + kyHieu + ", " : "") +
                (moTa != null ? "moTa=" + moTa + ", " : "") +
                (ten != null ? "ten=" + ten + ", " : "") +
                (tenTiengAnh != null ? "tenTiengAnh=" + tenTiengAnh + ", " : "") +
            "}";
    }

}
