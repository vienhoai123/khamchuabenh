package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.*;

/**
 * Criteria class for the {@link vn.vnpt.domain.TheBhyt} entity. This class is used
 * in {@link vn.vnpt.web.rest.TheBhytResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /the-bhyts?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TheBhytCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter enabled;

    private StringFilter maKhuVuc;

    private StringFilter maSoBhxh;

    private StringFilter maTheCu;

    private LocalDateFilter ngayBatDau;

    private LocalDateFilter ngayDuNamNam;

    private LocalDateFilter ngayHetHan;

    private StringFilter qr;

    private StringFilter soThe;

    private LocalDateFilter ngayMienCungChiTra;

    private LongFilter benhNhanId;

    private LongFilter doiTuongBhytId;

    private LongFilter thongTinBhxhId;

    public TheBhytCriteria() {
    }

    public TheBhytCriteria(TheBhytCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.enabled = other.enabled == null ? null : other.enabled.copy();
        this.maKhuVuc = other.maKhuVuc == null ? null : other.maKhuVuc.copy();
        this.maSoBhxh = other.maSoBhxh == null ? null : other.maSoBhxh.copy();
        this.maTheCu = other.maTheCu == null ? null : other.maTheCu.copy();
        this.ngayBatDau = other.ngayBatDau == null ? null : other.ngayBatDau.copy();
        this.ngayDuNamNam = other.ngayDuNamNam == null ? null : other.ngayDuNamNam.copy();
        this.ngayHetHan = other.ngayHetHan == null ? null : other.ngayHetHan.copy();
        this.qr = other.qr == null ? null : other.qr.copy();
        this.soThe = other.soThe == null ? null : other.soThe.copy();
        this.ngayMienCungChiTra = other.ngayMienCungChiTra == null ? null : other.ngayMienCungChiTra.copy();
        this.benhNhanId = other.benhNhanId == null ? null : other.benhNhanId.copy();
        this.doiTuongBhytId = other.doiTuongBhytId == null ? null : other.doiTuongBhytId.copy();
        this.thongTinBhxhId = other.thongTinBhxhId == null ? null : other.thongTinBhxhId.copy();
    }

    @Override
    public TheBhytCriteria copy() {
        return new TheBhytCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getEnabled() {
        return enabled;
    }

    public void setEnabled(BooleanFilter enabled) {
        this.enabled = enabled;
    }

    public StringFilter getMaKhuVuc() {
        return maKhuVuc;
    }

    public void setMaKhuVuc(StringFilter maKhuVuc) {
        this.maKhuVuc = maKhuVuc;
    }

    public StringFilter getMaSoBhxh() {
        return maSoBhxh;
    }

    public void setMaSoBhxh(StringFilter maSoBhxh) {
        this.maSoBhxh = maSoBhxh;
    }

    public StringFilter getMaTheCu() {
        return maTheCu;
    }

    public void setMaTheCu(StringFilter maTheCu) {
        this.maTheCu = maTheCu;
    }

    public LocalDateFilter getNgayBatDau() {
        return ngayBatDau;
    }

    public void setNgayBatDau(LocalDateFilter ngayBatDau) {
        this.ngayBatDau = ngayBatDau;
    }

    public LocalDateFilter getNgayDuNamNam() {
        return ngayDuNamNam;
    }

    public void setNgayDuNamNam(LocalDateFilter ngayDuNamNam) {
        this.ngayDuNamNam = ngayDuNamNam;
    }

    public LocalDateFilter getNgayHetHan() {
        return ngayHetHan;
    }

    public void setNgayHetHan(LocalDateFilter ngayHetHan) {
        this.ngayHetHan = ngayHetHan;
    }

    public StringFilter getQr() {
        return qr;
    }

    public void setQr(StringFilter qr) {
        this.qr = qr;
    }

    public StringFilter getSoThe() {
        return soThe;
    }

    public void setSoThe(StringFilter soThe) {
        this.soThe = soThe;
    }

    public LocalDateFilter getNgayMienCungChiTra() {
        return ngayMienCungChiTra;
    }

    public void setNgayMienCungChiTra(LocalDateFilter ngayMienCungChiTra) {
        this.ngayMienCungChiTra = ngayMienCungChiTra;
    }

    public LongFilter getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(LongFilter benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public LongFilter getDoiTuongBhytId() {
        return doiTuongBhytId;
    }

    public void setDoiTuongBhytId(LongFilter doiTuongBhytId) {
        this.doiTuongBhytId = doiTuongBhytId;
    }

    public LongFilter getThongTinBhxhId() {
        return thongTinBhxhId;
    }

    public void setThongTinBhxhId(LongFilter thongTinBhxhId) {
        this.thongTinBhxhId = thongTinBhxhId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TheBhytCriteria that = (TheBhytCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(enabled, that.enabled) &&
            Objects.equals(maKhuVuc, that.maKhuVuc) &&
            Objects.equals(maSoBhxh, that.maSoBhxh) &&
            Objects.equals(maTheCu, that.maTheCu) &&
            Objects.equals(ngayBatDau, that.ngayBatDau) &&
            Objects.equals(ngayDuNamNam, that.ngayDuNamNam) &&
            Objects.equals(ngayHetHan, that.ngayHetHan) &&
            Objects.equals(qr, that.qr) &&
            Objects.equals(soThe, that.soThe) &&
            Objects.equals(ngayMienCungChiTra, that.ngayMienCungChiTra) &&
            Objects.equals(benhNhanId, that.benhNhanId) &&
            Objects.equals(doiTuongBhytId, that.doiTuongBhytId) &&
            Objects.equals(thongTinBhxhId, that.thongTinBhxhId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        enabled,
        maKhuVuc,
        maSoBhxh,
        maTheCu,
        ngayBatDau,
        ngayDuNamNam,
        ngayHetHan,
        qr,
        soThe,
        ngayMienCungChiTra,
        benhNhanId,
        doiTuongBhytId,
        thongTinBhxhId
        );
    }

    @Override
    public String toString() {
        return "TheBhytCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (enabled != null ? "enabled=" + enabled + ", " : "") +
                (maKhuVuc != null ? "maKhuVuc=" + maKhuVuc + ", " : "") +
                (maSoBhxh != null ? "maSoBhxh=" + maSoBhxh + ", " : "") +
                (maTheCu != null ? "maTheCu=" + maTheCu + ", " : "") +
                (ngayBatDau != null ? "ngayBatDau=" + ngayBatDau + ", " : "") +
                (ngayDuNamNam != null ? "ngayDuNamNam=" + ngayDuNamNam + ", " : "") +
                (ngayHetHan != null ? "ngayHetHan=" + ngayHetHan + ", " : "") +
                (qr != null ? "qr=" + qr + ", " : "") +
                (soThe != null ? "soThe=" + soThe + ", " : "") +
                (ngayMienCungChiTra != null ? "ngayMienCungChiTra=" + ngayMienCungChiTra + ", " : "") +
                (benhNhanId != null ? "benhNhanId=" + benhNhanId + ", " : "") +
                (doiTuongBhytId != null ? "doiTuongBhytId=" + doiTuongBhytId + ", " : "") +
                (thongTinBhxhId != null ? "thongTinBhxhId=" + thongTinBhxhId + ", " : "") +
            "}";
    }

}
