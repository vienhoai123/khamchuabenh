package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.BenhYhct} entity.
 */
@ApiModel(description = "NDB_TABLE=READ_BACKUP=1 Danh mục tên bệnh lý theo Y học cổ truyền")
public class BenhYhctDTO implements Serializable {
    
    private Long id;

    /**
     * Mã bệnh lý YHCT
     */
    @NotNull
    @Size(max = 50)
    @ApiModelProperty(value = "Mã bệnh lý YHCT", required = true)
    private String ma;

    /**
     * Tên YHCT
     */
    @NotNull
    @Size(max = 500)
    @ApiModelProperty(value = "Tên YHCT", required = true)
    private String ten;

    /**
     * Mã bệnh lý
     */
    @ApiModelProperty(value = "Mã bệnh lý")

    private Long benhLyId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Long getBenhLyId() {
        return benhLyId;
    }

    public void setBenhLyId(Long benhLyId) {
        this.benhLyId = benhLyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BenhYhctDTO benhYhctDTO = (BenhYhctDTO) o;
        if (benhYhctDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), benhYhctDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BenhYhctDTO{" +
            "id=" + getId() +
            ", ma='" + getMa() + "'" +
            ", ten='" + getTen() + "'" +
            ", benhLyId=" + getBenhLyId() +
            "}";
    }
}
