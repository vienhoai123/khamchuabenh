package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.QuanHuyen} entity.
 */
public class QuanHuyenDTO implements Serializable {
    
    private Long id;

    /**
     * Cấp
     */
    @NotNull
    @Size(max = 255)
    @ApiModelProperty(value = "Cấp", required = true)
    private String cap;

    /**
     * Đoạn cụm từ gợi ý
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Đoạn cụm từ gợi ý")
    private String guessPhrase;

    /**
     * Tên quận huyện
     */
    @NotNull
    @Size(max = 255)
    @ApiModelProperty(value = "Tên quận huyện", required = true)
    private String ten;

    /**
     * Tên không dấu
     */
    @NotNull
    @Size(max = 255)
    @ApiModelProperty(value = "Tên không dấu", required = true)
    private String tenKhongDau;

    /**
     * Mã tỉnh thành phố
     */
    @ApiModelProperty(value = "Mã tỉnh thành phố")

    private Long tinhThanhPhoId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCap() {
        return cap;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    public String getGuessPhrase() {
        return guessPhrase;
    }

    public void setGuessPhrase(String guessPhrase) {
        this.guessPhrase = guessPhrase;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getTenKhongDau() {
        return tenKhongDau;
    }

    public void setTenKhongDau(String tenKhongDau) {
        this.tenKhongDau = tenKhongDau;
    }

    public Long getTinhThanhPhoId() {
        return tinhThanhPhoId;
    }

    public void setTinhThanhPhoId(Long tinhThanhPhoId) {
        this.tinhThanhPhoId = tinhThanhPhoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        QuanHuyenDTO quanHuyenDTO = (QuanHuyenDTO) o;
        if (quanHuyenDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), quanHuyenDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "QuanHuyenDTO{" +
            "id=" + getId() +
            ", cap='" + getCap() + "'" +
            ", guessPhrase='" + getGuessPhrase() + "'" +
            ", ten='" + getTen() + "'" +
            ", tenKhongDau='" + getTenKhongDau() + "'" +
            ", tinhThanhPhoId=" + getTinhThanhPhoId() +
            "}";
    }
}
