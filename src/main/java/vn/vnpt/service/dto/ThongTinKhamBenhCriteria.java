package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.ThongTinKhamBenh} entity. This class is used
 * in {@link vn.vnpt.web.rest.ThongTinKhamBenhResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /thong-tin-kham-benhs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ThongTinKhamBenhCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BigDecimalFilter bmi;

    private BigDecimalFilter canNang;

    private IntegerFilter chieuCao;

    private BigDecimalFilter creatinin;

    private StringFilter dauHieuLamSang;

    private BigDecimalFilter doThanhThai;

    private IntegerFilter huyetApCao;

    private IntegerFilter huyetApThap;

    private StringFilter icd;

    private IntegerFilter lanHen;

    private StringFilter loiDan;

    private StringFilter lyDoChuyen;

    private StringFilter maBenhYhct;

    private IntegerFilter mach;

    private IntegerFilter maxNgayRaToa;

    private LocalDateFilter ngayHen;

    private StringFilter nhanDinhBmi;

    private StringFilter nhanDinhDoThanhThai;

    private BooleanFilter nhapVien;

    private BigDecimalFilter nhietDo;

    private IntegerFilter nhipTho;

    private StringFilter ppDieuTriYtct;

    private IntegerFilter soLanInBangKe;

    private IntegerFilter soLanInToaThuoc;

    private StringFilter tenBenhIcd;

    private StringFilter tenBenhTheoBacSi;

    private StringFilter tenBenhYhct;

    private BooleanFilter trangThaiCdha;

    private BooleanFilter trangThaiTtpt;

    private BooleanFilter trangThaiXetNghiem;

    private StringFilter trieuChungLamSang;

    private IntegerFilter vongBung;

    private IntegerFilter trangThai;

    private LocalDateFilter thoiGianKetThucKham;

    private LongFilter phongIdChuyenTu;

    private LocalDateFilter thoiGianKhamBenh;

    private StringFilter yLenh;

    private IntegerFilter loaiYLenh;

    private StringFilter chanDoan;

    private IntegerFilter loaiChanDoan;

    private IntegerFilter nam;

    private LongFilter bakbId;

    private LongFilter donViId;

    private LongFilter benhNhanId;

    private LongFilter dotDieuTriId;

    private LongFilter thongTinKhoaId;

    private LongFilter nhanVienId;

    private LongFilter huongDieuTriId;

    private LongFilter phongId;

    public ThongTinKhamBenhCriteria() {
    }

    public ThongTinKhamBenhCriteria(ThongTinKhamBenhCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.bmi = other.bmi == null ? null : other.bmi.copy();
        this.canNang = other.canNang == null ? null : other.canNang.copy();
        this.chieuCao = other.chieuCao == null ? null : other.chieuCao.copy();
        this.creatinin = other.creatinin == null ? null : other.creatinin.copy();
        this.dauHieuLamSang = other.dauHieuLamSang == null ? null : other.dauHieuLamSang.copy();
        this.doThanhThai = other.doThanhThai == null ? null : other.doThanhThai.copy();
        this.huyetApCao = other.huyetApCao == null ? null : other.huyetApCao.copy();
        this.huyetApThap = other.huyetApThap == null ? null : other.huyetApThap.copy();
        this.icd = other.icd == null ? null : other.icd.copy();
        this.lanHen = other.lanHen == null ? null : other.lanHen.copy();
        this.loiDan = other.loiDan == null ? null : other.loiDan.copy();
        this.lyDoChuyen = other.lyDoChuyen == null ? null : other.lyDoChuyen.copy();
        this.maBenhYhct = other.maBenhYhct == null ? null : other.maBenhYhct.copy();
        this.mach = other.mach == null ? null : other.mach.copy();
        this.maxNgayRaToa = other.maxNgayRaToa == null ? null : other.maxNgayRaToa.copy();
        this.ngayHen = other.ngayHen == null ? null : other.ngayHen.copy();
        this.nhanDinhBmi = other.nhanDinhBmi == null ? null : other.nhanDinhBmi.copy();
        this.nhanDinhDoThanhThai = other.nhanDinhDoThanhThai == null ? null : other.nhanDinhDoThanhThai.copy();
        this.nhapVien = other.nhapVien == null ? null : other.nhapVien.copy();
        this.nhietDo = other.nhietDo == null ? null : other.nhietDo.copy();
        this.nhipTho = other.nhipTho == null ? null : other.nhipTho.copy();
        this.ppDieuTriYtct = other.ppDieuTriYtct == null ? null : other.ppDieuTriYtct.copy();
        this.soLanInBangKe = other.soLanInBangKe == null ? null : other.soLanInBangKe.copy();
        this.soLanInToaThuoc = other.soLanInToaThuoc == null ? null : other.soLanInToaThuoc.copy();
        this.tenBenhIcd = other.tenBenhIcd == null ? null : other.tenBenhIcd.copy();
        this.tenBenhTheoBacSi = other.tenBenhTheoBacSi == null ? null : other.tenBenhTheoBacSi.copy();
        this.tenBenhYhct = other.tenBenhYhct == null ? null : other.tenBenhYhct.copy();
        this.trangThaiCdha = other.trangThaiCdha == null ? null : other.trangThaiCdha.copy();
        this.trangThaiTtpt = other.trangThaiTtpt == null ? null : other.trangThaiTtpt.copy();
        this.trangThaiXetNghiem = other.trangThaiXetNghiem == null ? null : other.trangThaiXetNghiem.copy();
        this.trieuChungLamSang = other.trieuChungLamSang == null ? null : other.trieuChungLamSang.copy();
        this.vongBung = other.vongBung == null ? null : other.vongBung.copy();
        this.trangThai = other.trangThai == null ? null : other.trangThai.copy();
        this.thoiGianKetThucKham = other.thoiGianKetThucKham == null ? null : other.thoiGianKetThucKham.copy();
        this.phongIdChuyenTu = other.phongIdChuyenTu == null ? null : other.phongIdChuyenTu.copy();
        this.thoiGianKhamBenh = other.thoiGianKhamBenh == null ? null : other.thoiGianKhamBenh.copy();
        this.yLenh = other.yLenh == null ? null : other.yLenh.copy();
        this.loaiYLenh = other.loaiYLenh == null ? null : other.loaiYLenh.copy();
        this.chanDoan = other.chanDoan == null ? null : other.chanDoan.copy();
        this.loaiChanDoan = other.loaiChanDoan == null ? null : other.loaiChanDoan.copy();
        this.nam = other.nam == null ? null : other.nam.copy();
        this.bakbId = other.bakbId == null ? null : other.bakbId.copy();
        this.donViId = other.donViId == null ? null : other.donViId.copy();
        this.benhNhanId = other.benhNhanId == null ? null : other.benhNhanId.copy();
        this.dotDieuTriId = other.dotDieuTriId == null ? null : other.dotDieuTriId.copy();
        this.thongTinKhoaId = other.thongTinKhoaId == null ? null : other.thongTinKhoaId.copy();
        this.nhanVienId = other.nhanVienId == null ? null : other.nhanVienId.copy();
        this.huongDieuTriId = other.huongDieuTriId == null ? null : other.huongDieuTriId.copy();
        this.phongId = other.phongId == null ? null : other.phongId.copy();
    }

    @Override
    public ThongTinKhamBenhCriteria copy() {
        return new ThongTinKhamBenhCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BigDecimalFilter getBmi() {
        return bmi;
    }

    public void setBmi(BigDecimalFilter bmi) {
        this.bmi = bmi;
    }

    public BigDecimalFilter getCanNang() {
        return canNang;
    }

    public void setCanNang(BigDecimalFilter canNang) {
        this.canNang = canNang;
    }

    public IntegerFilter getChieuCao() {
        return chieuCao;
    }

    public void setChieuCao(IntegerFilter chieuCao) {
        this.chieuCao = chieuCao;
    }

    public BigDecimalFilter getCreatinin() {
        return creatinin;
    }

    public void setCreatinin(BigDecimalFilter creatinin) {
        this.creatinin = creatinin;
    }

    public StringFilter getDauHieuLamSang() {
        return dauHieuLamSang;
    }

    public void setDauHieuLamSang(StringFilter dauHieuLamSang) {
        this.dauHieuLamSang = dauHieuLamSang;
    }

    public BigDecimalFilter getDoThanhThai() {
        return doThanhThai;
    }

    public void setDoThanhThai(BigDecimalFilter doThanhThai) {
        this.doThanhThai = doThanhThai;
    }

    public IntegerFilter getHuyetApCao() {
        return huyetApCao;
    }

    public void setHuyetApCao(IntegerFilter huyetApCao) {
        this.huyetApCao = huyetApCao;
    }

    public IntegerFilter getHuyetApThap() {
        return huyetApThap;
    }

    public void setHuyetApThap(IntegerFilter huyetApThap) {
        this.huyetApThap = huyetApThap;
    }

    public StringFilter getIcd() {
        return icd;
    }

    public void setIcd(StringFilter icd) {
        this.icd = icd;
    }

    public IntegerFilter getLanHen() {
        return lanHen;
    }

    public void setLanHen(IntegerFilter lanHen) {
        this.lanHen = lanHen;
    }

    public StringFilter getLoiDan() {
        return loiDan;
    }

    public void setLoiDan(StringFilter loiDan) {
        this.loiDan = loiDan;
    }

    public StringFilter getLyDoChuyen() {
        return lyDoChuyen;
    }

    public void setLyDoChuyen(StringFilter lyDoChuyen) {
        this.lyDoChuyen = lyDoChuyen;
    }

    public StringFilter getMaBenhYhct() {
        return maBenhYhct;
    }

    public void setMaBenhYhct(StringFilter maBenhYhct) {
        this.maBenhYhct = maBenhYhct;
    }

    public IntegerFilter getMach() {
        return mach;
    }

    public void setMach(IntegerFilter mach) {
        this.mach = mach;
    }

    public IntegerFilter getMaxNgayRaToa() {
        return maxNgayRaToa;
    }

    public void setMaxNgayRaToa(IntegerFilter maxNgayRaToa) {
        this.maxNgayRaToa = maxNgayRaToa;
    }

    public LocalDateFilter getNgayHen() {
        return ngayHen;
    }

    public void setNgayHen(LocalDateFilter ngayHen) {
        this.ngayHen = ngayHen;
    }

    public StringFilter getNhanDinhBmi() {
        return nhanDinhBmi;
    }

    public void setNhanDinhBmi(StringFilter nhanDinhBmi) {
        this.nhanDinhBmi = nhanDinhBmi;
    }

    public StringFilter getNhanDinhDoThanhThai() {
        return nhanDinhDoThanhThai;
    }

    public void setNhanDinhDoThanhThai(StringFilter nhanDinhDoThanhThai) {
        this.nhanDinhDoThanhThai = nhanDinhDoThanhThai;
    }

    public BooleanFilter getNhapVien() {
        return nhapVien;
    }

    public void setNhapVien(BooleanFilter nhapVien) {
        this.nhapVien = nhapVien;
    }

    public BigDecimalFilter getNhietDo() {
        return nhietDo;
    }

    public void setNhietDo(BigDecimalFilter nhietDo) {
        this.nhietDo = nhietDo;
    }

    public IntegerFilter getNhipTho() {
        return nhipTho;
    }

    public void setNhipTho(IntegerFilter nhipTho) {
        this.nhipTho = nhipTho;
    }

    public StringFilter getPpDieuTriYtct() {
        return ppDieuTriYtct;
    }

    public void setPpDieuTriYtct(StringFilter ppDieuTriYtct) {
        this.ppDieuTriYtct = ppDieuTriYtct;
    }

    public IntegerFilter getSoLanInBangKe() {
        return soLanInBangKe;
    }

    public void setSoLanInBangKe(IntegerFilter soLanInBangKe) {
        this.soLanInBangKe = soLanInBangKe;
    }

    public IntegerFilter getSoLanInToaThuoc() {
        return soLanInToaThuoc;
    }

    public void setSoLanInToaThuoc(IntegerFilter soLanInToaThuoc) {
        this.soLanInToaThuoc = soLanInToaThuoc;
    }

    public StringFilter getTenBenhIcd() {
        return tenBenhIcd;
    }

    public void setTenBenhIcd(StringFilter tenBenhIcd) {
        this.tenBenhIcd = tenBenhIcd;
    }

    public StringFilter getTenBenhTheoBacSi() {
        return tenBenhTheoBacSi;
    }

    public void setTenBenhTheoBacSi(StringFilter tenBenhTheoBacSi) {
        this.tenBenhTheoBacSi = tenBenhTheoBacSi;
    }

    public StringFilter getTenBenhYhct() {
        return tenBenhYhct;
    }

    public void setTenBenhYhct(StringFilter tenBenhYhct) {
        this.tenBenhYhct = tenBenhYhct;
    }

    public BooleanFilter getTrangThaiCdha() {
        return trangThaiCdha;
    }

    public void setTrangThaiCdha(BooleanFilter trangThaiCdha) {
        this.trangThaiCdha = trangThaiCdha;
    }

    public BooleanFilter getTrangThaiTtpt() {
        return trangThaiTtpt;
    }

    public void setTrangThaiTtpt(BooleanFilter trangThaiTtpt) {
        this.trangThaiTtpt = trangThaiTtpt;
    }

    public BooleanFilter getTrangThaiXetNghiem() {
        return trangThaiXetNghiem;
    }

    public void setTrangThaiXetNghiem(BooleanFilter trangThaiXetNghiem) {
        this.trangThaiXetNghiem = trangThaiXetNghiem;
    }

    public StringFilter getTrieuChungLamSang() {
        return trieuChungLamSang;
    }

    public void setTrieuChungLamSang(StringFilter trieuChungLamSang) {
        this.trieuChungLamSang = trieuChungLamSang;
    }

    public IntegerFilter getVongBung() {
        return vongBung;
    }

    public void setVongBung(IntegerFilter vongBung) {
        this.vongBung = vongBung;
    }

    public IntegerFilter getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(IntegerFilter trangThai) {
        this.trangThai = trangThai;
    }

    public LocalDateFilter getThoiGianKetThucKham() {
        return thoiGianKetThucKham;
    }

    public void setThoiGianKetThucKham(LocalDateFilter thoiGianKetThucKham) {
        this.thoiGianKetThucKham = thoiGianKetThucKham;
    }

    public LongFilter getPhongIdChuyenTu() {
        return phongIdChuyenTu;
    }

    public void setPhongIdChuyenTu(LongFilter phongIdChuyenTu) {
        this.phongIdChuyenTu = phongIdChuyenTu;
    }

    public LocalDateFilter getThoiGianKhamBenh() {
        return thoiGianKhamBenh;
    }

    public void setThoiGianKhamBenh(LocalDateFilter thoiGianKhamBenh) {
        this.thoiGianKhamBenh = thoiGianKhamBenh;
    }

    public StringFilter getyLenh() {
        return yLenh;
    }

    public void setyLenh(StringFilter yLenh) {
        this.yLenh = yLenh;
    }

    public IntegerFilter getLoaiYLenh() {
        return loaiYLenh;
    }

    public void setLoaiYLenh(IntegerFilter loaiYLenh) {
        this.loaiYLenh = loaiYLenh;
    }

    public StringFilter getChanDoan() {
        return chanDoan;
    }

    public void setChanDoan(StringFilter chanDoan) {
        this.chanDoan = chanDoan;
    }

    public IntegerFilter getLoaiChanDoan() {
        return loaiChanDoan;
    }

    public void setLoaiChanDoan(IntegerFilter loaiChanDoan) {
        this.loaiChanDoan = loaiChanDoan;
    }

    public IntegerFilter getNam() {
        return nam;
    }

    public void setNam(IntegerFilter nam) {
        this.nam = nam;
    }

    public LongFilter getBakbId() {
        return bakbId;
    }

    public void setBakbId(LongFilter bakbId) {
        this.bakbId = bakbId;
    }

    public LongFilter getDonViId() {
        return donViId;
    }

    public void setDonViId(LongFilter donViId) {
        this.donViId = donViId;
    }

    public LongFilter getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(LongFilter benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public LongFilter getDotDieuTriId() {
        return dotDieuTriId;
    }

    public void setDotDieuTriId(LongFilter dotDieuTriId) {
        this.dotDieuTriId = dotDieuTriId;
    }

    public LongFilter getThongTinKhoaId() {
        return thongTinKhoaId;
    }

    public void setThongTinKhoaId(LongFilter thongTinKhoaId) {
        this.thongTinKhoaId = thongTinKhoaId;
    }

    public LongFilter getNhanVienId() {
        return nhanVienId;
    }

    public void setNhanVienId(LongFilter nhanVienId) {
        this.nhanVienId = nhanVienId;
    }

    public LongFilter getHuongDieuTriId() {
        return huongDieuTriId;
    }

    public void setHuongDieuTriId(LongFilter huongDieuTriId) {
        this.huongDieuTriId = huongDieuTriId;
    }

    public LongFilter getPhongId() {
        return phongId;
    }

    public void setPhongId(LongFilter phongId) {
        this.phongId = phongId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ThongTinKhamBenhCriteria that = (ThongTinKhamBenhCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(bmi, that.bmi) &&
            Objects.equals(canNang, that.canNang) &&
            Objects.equals(chieuCao, that.chieuCao) &&
            Objects.equals(creatinin, that.creatinin) &&
            Objects.equals(dauHieuLamSang, that.dauHieuLamSang) &&
            Objects.equals(doThanhThai, that.doThanhThai) &&
            Objects.equals(huyetApCao, that.huyetApCao) &&
            Objects.equals(huyetApThap, that.huyetApThap) &&
            Objects.equals(icd, that.icd) &&
            Objects.equals(lanHen, that.lanHen) &&
            Objects.equals(loiDan, that.loiDan) &&
            Objects.equals(lyDoChuyen, that.lyDoChuyen) &&
            Objects.equals(maBenhYhct, that.maBenhYhct) &&
            Objects.equals(mach, that.mach) &&
            Objects.equals(maxNgayRaToa, that.maxNgayRaToa) &&
            Objects.equals(ngayHen, that.ngayHen) &&
            Objects.equals(nhanDinhBmi, that.nhanDinhBmi) &&
            Objects.equals(nhanDinhDoThanhThai, that.nhanDinhDoThanhThai) &&
            Objects.equals(nhapVien, that.nhapVien) &&
            Objects.equals(nhietDo, that.nhietDo) &&
            Objects.equals(nhipTho, that.nhipTho) &&
            Objects.equals(ppDieuTriYtct, that.ppDieuTriYtct) &&
            Objects.equals(soLanInBangKe, that.soLanInBangKe) &&
            Objects.equals(soLanInToaThuoc, that.soLanInToaThuoc) &&
            Objects.equals(tenBenhIcd, that.tenBenhIcd) &&
            Objects.equals(tenBenhTheoBacSi, that.tenBenhTheoBacSi) &&
            Objects.equals(tenBenhYhct, that.tenBenhYhct) &&
            Objects.equals(trangThaiCdha, that.trangThaiCdha) &&
            Objects.equals(trangThaiTtpt, that.trangThaiTtpt) &&
            Objects.equals(trangThaiXetNghiem, that.trangThaiXetNghiem) &&
            Objects.equals(trieuChungLamSang, that.trieuChungLamSang) &&
            Objects.equals(vongBung, that.vongBung) &&
            Objects.equals(trangThai, that.trangThai) &&
            Objects.equals(thoiGianKetThucKham, that.thoiGianKetThucKham) &&
            Objects.equals(phongIdChuyenTu, that.phongIdChuyenTu) &&
            Objects.equals(thoiGianKhamBenh, that.thoiGianKhamBenh) &&
            Objects.equals(yLenh, that.yLenh) &&
            Objects.equals(loaiYLenh, that.loaiYLenh) &&
            Objects.equals(chanDoan, that.chanDoan) &&
            Objects.equals(loaiChanDoan, that.loaiChanDoan) &&
            Objects.equals(nam, that.nam) &&
            Objects.equals(bakbId, that.bakbId) &&
            Objects.equals(donViId, that.donViId) &&
            Objects.equals(benhNhanId, that.benhNhanId) &&
            Objects.equals(dotDieuTriId, that.dotDieuTriId) &&
            Objects.equals(thongTinKhoaId, that.thongTinKhoaId) &&
            Objects.equals(nhanVienId, that.nhanVienId) &&
            Objects.equals(huongDieuTriId, that.huongDieuTriId) &&
            Objects.equals(phongId, that.phongId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        bmi,
        canNang,
        chieuCao,
        creatinin,
        dauHieuLamSang,
        doThanhThai,
        huyetApCao,
        huyetApThap,
        icd,
        lanHen,
        loiDan,
        lyDoChuyen,
        maBenhYhct,
        mach,
        maxNgayRaToa,
        ngayHen,
        nhanDinhBmi,
        nhanDinhDoThanhThai,
        nhapVien,
        nhietDo,
        nhipTho,
        ppDieuTriYtct,
        soLanInBangKe,
        soLanInToaThuoc,
        tenBenhIcd,
        tenBenhTheoBacSi,
        tenBenhYhct,
        trangThaiCdha,
        trangThaiTtpt,
        trangThaiXetNghiem,
        trieuChungLamSang,
        vongBung,
        trangThai,
        thoiGianKetThucKham,
        phongIdChuyenTu,
        thoiGianKhamBenh,
        yLenh,
        loaiYLenh,
        chanDoan,
        loaiChanDoan,
        nam,
        bakbId,
        donViId,
        benhNhanId,
        dotDieuTriId,
        thongTinKhoaId,
        nhanVienId,
        huongDieuTriId,
        phongId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ThongTinKhamBenhCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (bmi != null ? "bmi=" + bmi + ", " : "") +
                (canNang != null ? "canNang=" + canNang + ", " : "") +
                (chieuCao != null ? "chieuCao=" + chieuCao + ", " : "") +
                (creatinin != null ? "creatinin=" + creatinin + ", " : "") +
                (dauHieuLamSang != null ? "dauHieuLamSang=" + dauHieuLamSang + ", " : "") +
                (doThanhThai != null ? "doThanhThai=" + doThanhThai + ", " : "") +
                (huyetApCao != null ? "huyetApCao=" + huyetApCao + ", " : "") +
                (huyetApThap != null ? "huyetApThap=" + huyetApThap + ", " : "") +
                (icd != null ? "icd=" + icd + ", " : "") +
                (lanHen != null ? "lanHen=" + lanHen + ", " : "") +
                (loiDan != null ? "loiDan=" + loiDan + ", " : "") +
                (lyDoChuyen != null ? "lyDoChuyen=" + lyDoChuyen + ", " : "") +
                (maBenhYhct != null ? "maBenhYhct=" + maBenhYhct + ", " : "") +
                (mach != null ? "mach=" + mach + ", " : "") +
                (maxNgayRaToa != null ? "maxNgayRaToa=" + maxNgayRaToa + ", " : "") +
                (ngayHen != null ? "ngayHen=" + ngayHen + ", " : "") +
                (nhanDinhBmi != null ? "nhanDinhBmi=" + nhanDinhBmi + ", " : "") +
                (nhanDinhDoThanhThai != null ? "nhanDinhDoThanhThai=" + nhanDinhDoThanhThai + ", " : "") +
                (nhapVien != null ? "nhapVien=" + nhapVien + ", " : "") +
                (nhietDo != null ? "nhietDo=" + nhietDo + ", " : "") +
                (nhipTho != null ? "nhipTho=" + nhipTho + ", " : "") +
                (ppDieuTriYtct != null ? "ppDieuTriYtct=" + ppDieuTriYtct + ", " : "") +
                (soLanInBangKe != null ? "soLanInBangKe=" + soLanInBangKe + ", " : "") +
                (soLanInToaThuoc != null ? "soLanInToaThuoc=" + soLanInToaThuoc + ", " : "") +
                (tenBenhIcd != null ? "tenBenhIcd=" + tenBenhIcd + ", " : "") +
                (tenBenhTheoBacSi != null ? "tenBenhTheoBacSi=" + tenBenhTheoBacSi + ", " : "") +
                (tenBenhYhct != null ? "tenBenhYhct=" + tenBenhYhct + ", " : "") +
                (trangThaiCdha != null ? "trangThaiCdha=" + trangThaiCdha + ", " : "") +
                (trangThaiTtpt != null ? "trangThaiTtpt=" + trangThaiTtpt + ", " : "") +
                (trangThaiXetNghiem != null ? "trangThaiXetNghiem=" + trangThaiXetNghiem + ", " : "") +
                (trieuChungLamSang != null ? "trieuChungLamSang=" + trieuChungLamSang + ", " : "") +
                (vongBung != null ? "vongBung=" + vongBung + ", " : "") +
                (trangThai != null ? "trangThai=" + trangThai + ", " : "") +
                (thoiGianKetThucKham != null ? "thoiGianKetThucKham=" + thoiGianKetThucKham + ", " : "") +
                (phongIdChuyenTu != null ? "phongIdChuyenTu=" + phongIdChuyenTu + ", " : "") +
                (thoiGianKhamBenh != null ? "thoiGianKhamBenh=" + thoiGianKhamBenh + ", " : "") +
                (yLenh != null ? "yLenh=" + yLenh + ", " : "") +
                (loaiYLenh != null ? "loaiYLenh=" + loaiYLenh + ", " : "") +
                (chanDoan != null ? "chanDoan=" + chanDoan + ", " : "") +
                (loaiChanDoan != null ? "loaiChanDoan=" + loaiChanDoan + ", " : "") +
                (nam != null ? "nam=" + nam + ", " : "") +
                (bakbId != null ? "bakbId=" + bakbId + ", " : "") +
                (donViId != null ? "donViId=" + donViId + ", " : "") +
                (benhNhanId != null ? "benhNhanId=" + benhNhanId + ", " : "") +
                (dotDieuTriId != null ? "dotDieuTriId=" + dotDieuTriId + ", " : "") +
                (thongTinKhoaId != null ? "thongTinKhoaId=" + thongTinKhoaId + ", " : "") +
                (nhanVienId != null ? "nhanVienId=" + nhanVienId + ", " : "") +
                (huongDieuTriId != null ? "huongDieuTriId=" + huongDieuTriId + ", " : "") +
                (phongId != null ? "phongId=" + phongId + ", " : "") +
            "}";
    }

}
