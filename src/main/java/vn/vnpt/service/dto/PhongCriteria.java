package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.Phong} entity. This class is used
 * in {@link vn.vnpt.web.rest.PhongResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /phongs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PhongCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter enabled;

    private StringFilter kyHieu;

    private BigDecimalFilter loai;

    private StringFilter phone;

    private StringFilter ten;

    private StringFilter viTri;

    private LongFilter khoaId;

    private IntegerFilter trangThai;

    public void setTrangThai(IntegerFilter trangThai) {
        this.trangThai = trangThai;
    }

    public IntegerFilter getTrangThai() {
        return trangThai;
    }



    public PhongCriteria() {
    }

    public PhongCriteria(PhongCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.enabled = other.enabled == null ? null : other.enabled.copy();
        this.kyHieu = other.kyHieu == null ? null : other.kyHieu.copy();
        this.loai = other.loai == null ? null : other.loai.copy();
        this.phone = other.phone == null ? null : other.phone.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.viTri = other.viTri == null ? null : other.viTri.copy();
        this.khoaId = other.khoaId == null ? null : other.khoaId.copy();
    }

    @Override
    public PhongCriteria copy() {
        return new PhongCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getEnabled() {
        return enabled;
    }

    public void setEnabled(BooleanFilter enabled) {
        this.enabled = enabled;
    }

    public StringFilter getKyHieu() {
        return kyHieu;
    }

    public void setKyHieu(StringFilter kyHieu) {
        this.kyHieu = kyHieu;
    }

    public BigDecimalFilter getLoai() {
        return loai;
    }

    public void setLoai(BigDecimalFilter loai) {
        this.loai = loai;
    }

    public StringFilter getPhone() {
        return phone;
    }

    public void setPhone(StringFilter phone) {
        this.phone = phone;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public StringFilter getViTri() {
        return viTri;
    }

    public void setViTri(StringFilter viTri) {
        this.viTri = viTri;
    }

    public LongFilter getKhoaId() {
        return khoaId;
    }

    public void setKhoaId(LongFilter khoaId) {
        this.khoaId = khoaId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PhongCriteria that = (PhongCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(enabled, that.enabled) &&
            Objects.equals(kyHieu, that.kyHieu) &&
            Objects.equals(loai, that.loai) &&
            Objects.equals(phone, that.phone) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(viTri, that.viTri) &&
            Objects.equals(khoaId, that.khoaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        enabled,
        kyHieu,
        loai,
        phone,
        ten,
        viTri,
        khoaId
        );
    }

    @Override
    public String toString() {
        return "PhongCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (enabled != null ? "enabled=" + enabled + ", " : "") +
                (kyHieu != null ? "kyHieu=" + kyHieu + ", " : "") +
                (loai != null ? "loai=" + loai + ", " : "") +
                (phone != null ? "phone=" + phone + ", " : "") +
                (ten != null ? "ten=" + ten + ", " : "") +
                (viTri != null ? "viTri=" + viTri + ", " : "") +
                (khoaId != null ? "khoaId=" + khoaId + ", " : "") +
            "}";
    }

}
