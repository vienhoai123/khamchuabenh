package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.ThongTinKhoa} entity. This class is used
 * in {@link vn.vnpt.web.rest.ThongTinKhoaResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /thong-tin-khoas?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ThongTinKhoaCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LocalDateFilter thoiGianNhanBenh;

    private LocalDateFilter thoiGianChuyenKhoa;

    private IntegerFilter khoaId;

    private IntegerFilter khoaChuyenDen;

    private IntegerFilter khoaChuyenDi;

    private IntegerFilter nhanVienNhanBenh;

    private IntegerFilter nhanVienChuyenKhoa;

    private BigDecimalFilter soNgay;

    private IntegerFilter trangThai;

    private IntegerFilter nam;

    private LongFilter bakbId;

    private LongFilter donViId;

    private LongFilter benhNhanId;

    private LongFilter dotDieuTriId;

    public ThongTinKhoaCriteria() {
    }

    public ThongTinKhoaCriteria(ThongTinKhoaCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.thoiGianNhanBenh = other.thoiGianNhanBenh == null ? null : other.thoiGianNhanBenh.copy();
        this.thoiGianChuyenKhoa = other.thoiGianChuyenKhoa == null ? null : other.thoiGianChuyenKhoa.copy();
        this.khoaId = other.khoaId == null ? null : other.khoaId.copy();
        this.khoaChuyenDen = other.khoaChuyenDen == null ? null : other.khoaChuyenDen.copy();
        this.khoaChuyenDi = other.khoaChuyenDi == null ? null : other.khoaChuyenDi.copy();
        this.nhanVienNhanBenh = other.nhanVienNhanBenh == null ? null : other.nhanVienNhanBenh.copy();
        this.nhanVienChuyenKhoa = other.nhanVienChuyenKhoa == null ? null : other.nhanVienChuyenKhoa.copy();
        this.soNgay = other.soNgay == null ? null : other.soNgay.copy();
        this.trangThai = other.trangThai == null ? null : other.trangThai.copy();
        this.nam = other.nam == null ? null : other.nam.copy();
        this.bakbId = other.bakbId == null ? null : other.bakbId.copy();
        this.donViId = other.donViId == null ? null : other.donViId.copy();
        this.benhNhanId = other.benhNhanId == null ? null : other.benhNhanId.copy();
        this.dotDieuTriId = other.dotDieuTriId == null ? null : other.dotDieuTriId.copy();
    }

    @Override
    public ThongTinKhoaCriteria copy() {
        return new ThongTinKhoaCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LocalDateFilter getThoiGianNhanBenh() {
        return thoiGianNhanBenh;
    }

    public void setThoiGianNhanBenh(LocalDateFilter thoiGianNhanBenh) {
        this.thoiGianNhanBenh = thoiGianNhanBenh;
    }

    public LocalDateFilter getThoiGianChuyenKhoa() {
        return thoiGianChuyenKhoa;
    }

    public void setThoiGianChuyenKhoa(LocalDateFilter thoiGianChuyenKhoa) {
        this.thoiGianChuyenKhoa = thoiGianChuyenKhoa;
    }

    public IntegerFilter getKhoaId() {
        return khoaId;
    }

    public void setKhoaId(IntegerFilter khoaId) {
        this.khoaId = khoaId;
    }

    public IntegerFilter getKhoaChuyenDen() {
        return khoaChuyenDen;
    }

    public void setKhoaChuyenDen(IntegerFilter khoaChuyenDen) {
        this.khoaChuyenDen = khoaChuyenDen;
    }

    public IntegerFilter getKhoaChuyenDi() {
        return khoaChuyenDi;
    }

    public void setKhoaChuyenDi(IntegerFilter khoaChuyenDi) {
        this.khoaChuyenDi = khoaChuyenDi;
    }

    public IntegerFilter getNhanVienNhanBenh() {
        return nhanVienNhanBenh;
    }

    public void setNhanVienNhanBenh(IntegerFilter nhanVienNhanBenh) {
        this.nhanVienNhanBenh = nhanVienNhanBenh;
    }

    public IntegerFilter getNhanVienChuyenKhoa() {
        return nhanVienChuyenKhoa;
    }

    public void setNhanVienChuyenKhoa(IntegerFilter nhanVienChuyenKhoa) {
        this.nhanVienChuyenKhoa = nhanVienChuyenKhoa;
    }

    public BigDecimalFilter getSoNgay() {
        return soNgay;
    }

    public void setSoNgay(BigDecimalFilter soNgay) {
        this.soNgay = soNgay;
    }

    public IntegerFilter getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(IntegerFilter trangThai) {
        this.trangThai = trangThai;
    }

    public IntegerFilter getNam() {
        return nam;
    }

    public void setNam(IntegerFilter nam) {
        this.nam = nam;
    }

    public LongFilter getBakbId() {
        return bakbId;
    }

    public void setBakbId(LongFilter bakbId) {
        this.bakbId = bakbId;
    }

    public LongFilter getDonViId() {
        return donViId;
    }

    public void setDonViId(LongFilter donViId) {
        this.donViId = donViId;
    }

    public LongFilter getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(LongFilter benhNhanId) {
        this.benhNhanId = benhNhanId;
    }

    public LongFilter getDotDieuTriId() {
        return dotDieuTriId;
    }

    public void setDotDieuTriId(LongFilter dotDieuTriId) {
        this.dotDieuTriId = dotDieuTriId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ThongTinKhoaCriteria that = (ThongTinKhoaCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(thoiGianNhanBenh, that.thoiGianNhanBenh) &&
            Objects.equals(thoiGianChuyenKhoa, that.thoiGianChuyenKhoa) &&
            Objects.equals(khoaId, that.khoaId) &&
            Objects.equals(khoaChuyenDen, that.khoaChuyenDen) &&
            Objects.equals(khoaChuyenDi, that.khoaChuyenDi) &&
            Objects.equals(nhanVienNhanBenh, that.nhanVienNhanBenh) &&
            Objects.equals(nhanVienChuyenKhoa, that.nhanVienChuyenKhoa) &&
            Objects.equals(soNgay, that.soNgay) &&
            Objects.equals(trangThai, that.trangThai) &&
            Objects.equals(nam, that.nam) &&
            Objects.equals(bakbId, that.bakbId) &&
            Objects.equals(donViId, that.donViId) &&
            Objects.equals(benhNhanId, that.benhNhanId) &&
            Objects.equals(dotDieuTriId, that.dotDieuTriId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        thoiGianNhanBenh,
        thoiGianChuyenKhoa,
        khoaId,
        khoaChuyenDen,
        khoaChuyenDi,
        nhanVienNhanBenh,
        nhanVienChuyenKhoa,
        soNgay,
        trangThai,
        nam,
        bakbId,
        donViId,
        benhNhanId,
        dotDieuTriId
        );
    }

    @Override
    public String toString() {
        return "ThongTinKhoaCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (thoiGianNhanBenh != null ? "thoiGianNhanBenh=" + thoiGianNhanBenh + ", " : "") +
                (thoiGianChuyenKhoa != null ? "thoiGianChuyenKhoa=" + thoiGianChuyenKhoa + ", " : "") +
                (khoaId != null ? "khoaId=" + khoaId + ", " : "") +
                (khoaChuyenDen != null ? "khoaChuyenDen=" + khoaChuyenDen + ", " : "") +
                (khoaChuyenDi != null ? "khoaChuyenDi=" + khoaChuyenDi + ", " : "") +
                (nhanVienNhanBenh != null ? "nhanVienNhanBenh=" + nhanVienNhanBenh + ", " : "") +
                (nhanVienChuyenKhoa != null ? "nhanVienChuyenKhoa=" + nhanVienChuyenKhoa + ", " : "") +
                (soNgay != null ? "soNgay=" + soNgay + ", " : "") +
                (trangThai != null ? "trangThai=" + trangThai + ", " : "") +
                (nam != null ? "nam=" + nam + ", " : "") +
                (bakbId != null ? "bakbId=" + bakbId + ", " : "") +
                (donViId != null ? "donViId=" + donViId + ", " : "") +
                (benhNhanId != null ? "benhNhanId=" + benhNhanId + ", " : "") +
                (dotDieuTriId != null ? "dotDieuTriId=" + dotDieuTriId + ", " : "") +
            "}";
    }

}
