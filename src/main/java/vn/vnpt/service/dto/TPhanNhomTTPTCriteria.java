package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.TPhanNhomTTPT} entity. This class is used
 * in {@link vn.vnpt.web.rest.TPhanNhomTTPTResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /t-phan-nhom-ttpts?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TPhanNhomTTPTCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter nhomttptId;

    private LongFilter ttptId;

    public TPhanNhomTTPTCriteria() {
    }

    public TPhanNhomTTPTCriteria(TPhanNhomTTPTCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.nhomttptId = other.nhomttptId == null ? null : other.nhomttptId.copy();
        this.ttptId = other.ttptId == null ? null : other.ttptId.copy();
    }

    @Override
    public TPhanNhomTTPTCriteria copy() {
        return new TPhanNhomTTPTCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getNhomttptId() {
        return nhomttptId;
    }

    public void setNhomttptId(LongFilter nhomttptId) {
        this.nhomttptId = nhomttptId;
    }

    public LongFilter getTtptId() {
        return ttptId;
    }

    public void setTtptId(LongFilter ttptId) {
        this.ttptId = ttptId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TPhanNhomTTPTCriteria that = (TPhanNhomTTPTCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(nhomttptId, that.nhomttptId) &&
            Objects.equals(ttptId, that.ttptId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        nhomttptId,
        ttptId
        );
    }

    @Override
    public String toString() {
        return "TPhanNhomTTPTCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (nhomttptId != null ? "nhomttptId=" + nhomttptId + ", " : "") +
                (ttptId != null ? "ttptId=" + ttptId + ", " : "") +
            "}";
    }

}
