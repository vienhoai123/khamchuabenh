package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.ChiDinhCDHA} entity.
 */
public class ChiDinhCDHADTO implements Serializable {
    
    private Long id;

    /**
     * Trạng thái được thanh toán bảo hiểm của chỉ định: \r\n1: Có bảo hiểm. \r\n0: Không bảo hiểm. \r\n
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái được thanh toán bảo hiểm của chỉ định: \r\n1: Có bảo hiểm. \r\n0: Không bảo hiểm. \r\n", required = true)
    private Boolean coBaoHiem;

    /**
     * Trạng thái có kết quả của chỉ định: \r\n1: Có kết quả. \r\n0: Chưa có kết quả. \r\n
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái có kết quả của chỉ định: \r\n1: Có kết quả. \r\n0: Chưa có kết quả. \r\n", required = true)
    private Boolean coKetQua;

    /**
     * Trạng thái đã thanh toán của chỉ định: \r\n0: Chưa thanh toán. \r\n1: Đã thanh toán. \r\n-1: Đã hủy thanh toán
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái đã thanh toán của chỉ định: \r\n0: Chưa thanh toán. \r\n1: Đã thanh toán. \r\n-1: Đã hủy thanh toán", required = true)
    private Integer daThanhToan;

    /**
     * Trạng thái đã thanh toán chênh lệch của chỉ định: \r\n0: Chưa thanh toán. \r\n1: Đã thanh toán. \r\n-1: Đã hủy thanh toán
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái đã thanh toán chênh lệch của chỉ định: \r\n0: Chưa thanh toán. \r\n1: Đã thanh toán. \r\n-1: Đã hủy thanh toán", required = true)
    private Integer daThanhToanChenhLech;

    /**
     * Trạng thái đã thực hiện của chỉ định: \r\n0: Chưa thực hiện. \r\n1: Đã thực hiện. \r\n-1: Đã hủy thực hiện
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái đã thực hiện của chỉ định: \r\n0: Chưa thực hiện. \r\n1: Đã thực hiện. \r\n-1: Đã hủy thực hiện", required = true)
    private Integer daThucHien;

    /**
     * Đơn giá
     */
    @NotNull
    @ApiModelProperty(value = "Đơn giá", required = true)
    private BigDecimal donGia;

    /**
     * Đơn giá Bảo hiểm y tế
     */
    @NotNull
    @ApiModelProperty(value = "Đơn giá Bảo hiểm y tế", required = true)
    private BigDecimal donGiaBhyt;

    /**
     * Đơn giá không Bảo hiểm y tế
     */
    @ApiModelProperty(value = "Đơn giá không Bảo hiểm y tế")
    private BigDecimal donGiaKhongBhyt;

    /**
     * Ghi chú chỉ định
     */
    @Size(max = 2000)
    @ApiModelProperty(value = "Ghi chú chỉ định")
    private String ghiChuChiDinh;

    /**
     * Mô tả chỉ định
     */
    @Size(max = 2000)
    @ApiModelProperty(value = "Mô tả chỉ định")
    private String moTa;

    /**
     * Mô tả chỉ định theo xm15
     */
    @ApiModelProperty(value = "Mô tả chỉ định theo xm15")
    private String moTaXm15;

    /**
     * Mã người chỉ định
     */
    @ApiModelProperty(value = "Mã người chỉ định")
    private Long nguoiChiDinhId;

    /**
     * Tên người chỉ định
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Tên người chỉ định")
    private String nguoiChiDinh;

    /**
     * Số lần chụp
     */
    @ApiModelProperty(value = "Số lần chụp")
    private Integer soLanChup;

    /**
     * Số lượng
     */
    @ApiModelProperty(value = "Số lượng")
    private BigDecimal soLuong;

    /**
     * Số lượng có film
     */
    @ApiModelProperty(value = "Số lượng có film")
    private BigDecimal soLuongCoFilm;

    /**
     * Số lượng có film 1318
     */
    @ApiModelProperty(value = "Số lượng có film 1318")
    private BigDecimal soLuongCoFilm1318;

    /**
     * Số lượng có film 1820
     */
    @ApiModelProperty(value = "Số lượng có film 1820")
    private BigDecimal soLuongCoFilm1820;

    /**
     * Số lượng có film 2025
     */
    @ApiModelProperty(value = "Số lượng có film 2025")
    private BigDecimal soLuongCoFilm2025;

    /**
     * Số lượng có film 2430
     */
    @ApiModelProperty(value = "Số lượng có film 2430")
    private BigDecimal soLuongCoFilm2430;

    /**
     * Số lượng có film 2530
     */
    @ApiModelProperty(value = "Số lượng có film 2530")
    private BigDecimal soLuongCoFilm2530;

    /**
     * Số lượng có film 3040
     */
    @ApiModelProperty(value = "Số lượng có film 3040")
    private BigDecimal soLuongCoFilm3040;

    /**
     * Số lượng film
     */
    @ApiModelProperty(value = "Số lượng film")
    private BigDecimal soLuongFilm;

    /**
     * Tên chỉ định
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Tên chỉ định")
    private String ten;

    /**
     * Thành tiền
     */
    @NotNull
    @ApiModelProperty(value = "Thành tiền", required = true)
    private BigDecimal thanhTien;

    /**
     * Thành tiền BHYT
     */
    @ApiModelProperty(value = "Thành tiền BHYT")
    private BigDecimal thanhTienBhyt;

    /**
     * Thành tiền không BHYT
     */
    @ApiModelProperty(value = "Thành tiền không BHYT")
    private BigDecimal thanhTienKhongBHYT;

    /**
     * Thời gian chỉ định
     */
    @ApiModelProperty(value = "Thời gian chỉ định")
    private LocalDate thoiGianChiDinh;

    /**
     * Thời gian tạo
     */
    @NotNull
    @ApiModelProperty(value = "Thời gian tạo", required = true)
    private LocalDate thoiGianTao;

    /**
     * Tiền ngoài BHYT
     */
    @NotNull
    @ApiModelProperty(value = "Tiền ngoài BHYT", required = true)
    private BigDecimal tienNgoaiBHYT;

    /**
     * Trạng thái thanh toán chênh lệch: \r\n0: Không có thanh toán chênh lệch. \r\n1: Có thanh toán chênh lệch
     */
    @NotNull
    @ApiModelProperty(value = "Trạng thái thanh toán chênh lệch: \r\n0: Không có thanh toán chênh lệch. \r\n1: Có thanh toán chênh lệch", required = true)
    private Boolean thanhToanChenhLech;

    /**
     * Tỷ lệ thanh toán
     */
    @ApiModelProperty(value = "Tỷ lệ thanh toán")
    private Integer tyLeThanhToan;

    /**
     * Mã dùng chung cho dịch vụ
     */
    @Size(max = 255)
    @ApiModelProperty(value = "Mã dùng chung cho dịch vụ")
    private String maDungChung;

    /**
     * Trạng thái theo yêu cầu của dịch vụ: 0: Bình thường. 1: Theo yêu cầu
     */
    @ApiModelProperty(value = "Trạng thái theo yêu cầu của dịch vụ: 0: Bình thường. 1: Theo yêu cầu")
    private Boolean dichVuYeuCau;

    @NotNull
    private Integer nam;


    private Long donViId;

    private Long benhNhanId;

    private Long bakbId;

    private Long phieuCDId;

    private Long phongId;

    private Long cdhaId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isCoBaoHiem() {
        return coBaoHiem;
    }

    public void setCoBaoHiem(Boolean coBaoHiem) {
        this.coBaoHiem = coBaoHiem;
    }

    public Boolean isCoKetQua() {
        return coKetQua;
    }

    public void setCoKetQua(Boolean coKetQua) {
        this.coKetQua = coKetQua;
    }

    public Integer getDaThanhToan() {
        return daThanhToan;
    }

    public void setDaThanhToan(Integer daThanhToan) {
        this.daThanhToan = daThanhToan;
    }

    public Integer getDaThanhToanChenhLech() {
        return daThanhToanChenhLech;
    }

    public void setDaThanhToanChenhLech(Integer daThanhToanChenhLech) {
        this.daThanhToanChenhLech = daThanhToanChenhLech;
    }

    public Integer getDaThucHien() {
        return daThucHien;
    }

    public void setDaThucHien(Integer daThucHien) {
        this.daThucHien = daThucHien;
    }

    public BigDecimal getDonGia() {
        return donGia;
    }

    public void setDonGia(BigDecimal donGia) {
        this.donGia = donGia;
    }

    public BigDecimal getDonGiaBhyt() {
        return donGiaBhyt;
    }

    public void setDonGiaBhyt(BigDecimal donGiaBhyt) {
        this.donGiaBhyt = donGiaBhyt;
    }

    public BigDecimal getDonGiaKhongBhyt() {
        return donGiaKhongBhyt;
    }

    public void setDonGiaKhongBhyt(BigDecimal donGiaKhongBhyt) {
        this.donGiaKhongBhyt = donGiaKhongBhyt;
    }

    public String getGhiChuChiDinh() {
        return ghiChuChiDinh;
    }

    public void setGhiChuChiDinh(String ghiChuChiDinh) {
        this.ghiChuChiDinh = ghiChuChiDinh;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public String getMoTaXm15() {
        return moTaXm15;
    }

    public void setMoTaXm15(String moTaXm15) {
        this.moTaXm15 = moTaXm15;
    }

    public Long getNguoiChiDinhId() {
        return nguoiChiDinhId;
    }

    public void setNguoiChiDinhId(Long nguoiChiDinhId) {
        this.nguoiChiDinhId = nguoiChiDinhId;
    }

    public String getNguoiChiDinh() {
        return nguoiChiDinh;
    }

    public void setNguoiChiDinh(String nguoiChiDinh) {
        this.nguoiChiDinh = nguoiChiDinh;
    }

    public Integer getSoLanChup() {
        return soLanChup;
    }

    public void setSoLanChup(Integer soLanChup) {
        this.soLanChup = soLanChup;
    }

    public BigDecimal getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(BigDecimal soLuong) {
        this.soLuong = soLuong;
    }

    public BigDecimal getSoLuongCoFilm() {
        return soLuongCoFilm;
    }

    public void setSoLuongCoFilm(BigDecimal soLuongCoFilm) {
        this.soLuongCoFilm = soLuongCoFilm;
    }

    public BigDecimal getSoLuongCoFilm1318() {
        return soLuongCoFilm1318;
    }

    public void setSoLuongCoFilm1318(BigDecimal soLuongCoFilm1318) {
        this.soLuongCoFilm1318 = soLuongCoFilm1318;
    }

    public BigDecimal getSoLuongCoFilm1820() {
        return soLuongCoFilm1820;
    }

    public void setSoLuongCoFilm1820(BigDecimal soLuongCoFilm1820) {
        this.soLuongCoFilm1820 = soLuongCoFilm1820;
    }

    public BigDecimal getSoLuongCoFilm2025() {
        return soLuongCoFilm2025;
    }

    public void setSoLuongCoFilm2025(BigDecimal soLuongCoFilm2025) {
        this.soLuongCoFilm2025 = soLuongCoFilm2025;
    }

    public BigDecimal getSoLuongCoFilm2430() {
        return soLuongCoFilm2430;
    }

    public void setSoLuongCoFilm2430(BigDecimal soLuongCoFilm2430) {
        this.soLuongCoFilm2430 = soLuongCoFilm2430;
    }

    public BigDecimal getSoLuongCoFilm2530() {
        return soLuongCoFilm2530;
    }

    public void setSoLuongCoFilm2530(BigDecimal soLuongCoFilm2530) {
        this.soLuongCoFilm2530 = soLuongCoFilm2530;
    }

    public BigDecimal getSoLuongCoFilm3040() {
        return soLuongCoFilm3040;
    }

    public void setSoLuongCoFilm3040(BigDecimal soLuongCoFilm3040) {
        this.soLuongCoFilm3040 = soLuongCoFilm3040;
    }

    public BigDecimal getSoLuongFilm() {
        return soLuongFilm;
    }

    public void setSoLuongFilm(BigDecimal soLuongFilm) {
        this.soLuongFilm = soLuongFilm;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public BigDecimal getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(BigDecimal thanhTien) {
        this.thanhTien = thanhTien;
    }

    public BigDecimal getThanhTienBhyt() {
        return thanhTienBhyt;
    }

    public void setThanhTienBhyt(BigDecimal thanhTienBhyt) {
        this.thanhTienBhyt = thanhTienBhyt;
    }

    public BigDecimal getThanhTienKhongBHYT() {
        return thanhTienKhongBHYT;
    }

    public void setThanhTienKhongBHYT(BigDecimal thanhTienKhongBHYT) {
        this.thanhTienKhongBHYT = thanhTienKhongBHYT;
    }

    public LocalDate getThoiGianChiDinh() {
        return thoiGianChiDinh;
    }

    public void setThoiGianChiDinh(LocalDate thoiGianChiDinh) {
        this.thoiGianChiDinh = thoiGianChiDinh;
    }

    public LocalDate getThoiGianTao() {
        return thoiGianTao;
    }

    public void setThoiGianTao(LocalDate thoiGianTao) {
        this.thoiGianTao = thoiGianTao;
    }

    public BigDecimal getTienNgoaiBHYT() {
        return tienNgoaiBHYT;
    }

    public void setTienNgoaiBHYT(BigDecimal tienNgoaiBHYT) {
        this.tienNgoaiBHYT = tienNgoaiBHYT;
    }

    public Boolean isThanhToanChenhLech() {
        return thanhToanChenhLech;
    }

    public void setThanhToanChenhLech(Boolean thanhToanChenhLech) {
        this.thanhToanChenhLech = thanhToanChenhLech;
    }

    public Integer getTyLeThanhToan() {
        return tyLeThanhToan;
    }

    public void setTyLeThanhToan(Integer tyLeThanhToan) {
        this.tyLeThanhToan = tyLeThanhToan;
    }

    public String getMaDungChung() {
        return maDungChung;
    }

    public void setMaDungChung(String maDungChung) {
        this.maDungChung = maDungChung;
    }

    public Boolean isDichVuYeuCau() {
        return dichVuYeuCau;
    }

    public void setDichVuYeuCau(Boolean dichVuYeuCau) {
        this.dichVuYeuCau = dichVuYeuCau;
    }

    public Integer getNam() {
        return nam;
    }

    public void setNam(Integer nam) {
        this.nam = nam;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long phieuChiDinhCDHAId) {
        this.donViId = phieuChiDinhCDHAId;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long phieuChiDinhCDHAId) {
        this.benhNhanId = phieuChiDinhCDHAId;
    }

    public Long getBakbId() {
        return bakbId;
    }

    public void setBakbId(Long phieuChiDinhCDHAId) {
        this.bakbId = phieuChiDinhCDHAId;
    }

    public Long getPhieuCDId() {
        return phieuCDId;
    }

    public void setPhieuCDId(Long phieuChiDinhCDHAId) {
        this.phieuCDId = phieuChiDinhCDHAId;
    }

    public Long getPhongId() {
        return phongId;
    }

    public void setPhongId(Long phongId) {
        this.phongId = phongId;
    }

    public Long getCdhaId() {
        return cdhaId;
    }

    public void setCdhaId(Long chanDoanHinhAnhId) {
        this.cdhaId = chanDoanHinhAnhId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ChiDinhCDHADTO chiDinhCDHADTO = (ChiDinhCDHADTO) o;
        if (chiDinhCDHADTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), chiDinhCDHADTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ChiDinhCDHADTO{" +
            "id=" + getId() +
            ", coBaoHiem='" + isCoBaoHiem() + "'" +
            ", coKetQua='" + isCoKetQua() + "'" +
            ", daThanhToan=" + getDaThanhToan() +
            ", daThanhToanChenhLech=" + getDaThanhToanChenhLech() +
            ", daThucHien=" + getDaThucHien() +
            ", donGia=" + getDonGia() +
            ", donGiaBhyt=" + getDonGiaBhyt() +
            ", donGiaKhongBhyt=" + getDonGiaKhongBhyt() +
            ", ghiChuChiDinh='" + getGhiChuChiDinh() + "'" +
            ", moTa='" + getMoTa() + "'" +
            ", moTaXm15='" + getMoTaXm15() + "'" +
            ", nguoiChiDinhId=" + getNguoiChiDinhId() +
            ", nguoiChiDinh='" + getNguoiChiDinh() + "'" +
            ", soLanChup=" + getSoLanChup() +
            ", soLuong=" + getSoLuong() +
            ", soLuongCoFilm=" + getSoLuongCoFilm() +
            ", soLuongCoFilm1318=" + getSoLuongCoFilm1318() +
            ", soLuongCoFilm1820=" + getSoLuongCoFilm1820() +
            ", soLuongCoFilm2025=" + getSoLuongCoFilm2025() +
            ", soLuongCoFilm2430=" + getSoLuongCoFilm2430() +
            ", soLuongCoFilm2530=" + getSoLuongCoFilm2530() +
            ", soLuongCoFilm3040=" + getSoLuongCoFilm3040() +
            ", soLuongFilm=" + getSoLuongFilm() +
            ", ten='" + getTen() + "'" +
            ", thanhTien=" + getThanhTien() +
            ", thanhTienBhyt=" + getThanhTienBhyt() +
            ", thanhTienKhongBHYT=" + getThanhTienKhongBHYT() +
            ", thoiGianChiDinh='" + getThoiGianChiDinh() + "'" +
            ", thoiGianTao='" + getThoiGianTao() + "'" +
            ", tienNgoaiBHYT=" + getTienNgoaiBHYT() +
            ", thanhToanChenhLech='" + isThanhToanChenhLech() + "'" +
            ", tyLeThanhToan=" + getTyLeThanhToan() +
            ", maDungChung='" + getMaDungChung() + "'" +
            ", dichVuYeuCau='" + isDichVuYeuCau() + "'" +
            ", nam=" + getNam() +
            ", donViId=" + getDonViId() +
            ", benhNhanId=" + getBenhNhanId() +
            ", bakbId=" + getBakbId() +
            ", phieuCDId=" + getPhieuCDId() +
            ", phongId=" + getPhongId() +
            ", cdhaId=" + getCdhaId() +
            "}";
    }
}
