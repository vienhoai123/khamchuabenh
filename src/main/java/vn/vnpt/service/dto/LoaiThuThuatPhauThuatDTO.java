package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.LoaiThuThuatPhauThuat} entity.
 */
public class LoaiThuThuatPhauThuatDTO implements Serializable {
    
    private Long id;

    /**
     * Tên thủ thuật phẫu thuật
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Tên thủ thuật phẫu thuật")
    private String ten;

    /**
     * Mô tả loại thủ thuật phẫu thuật
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Mô tả loại thủ thuật phẫu thuật")
    private String moTa;

    /**
     * Trạng thái có hiệu lực: 1: Có hiệu lực. 0: không có hiệu lực
     */
    @ApiModelProperty(value = "Trạng thái có hiệu lực: 1: Có hiệu lực. 0: không có hiệu lực")
    private Boolean enable;

    /**
     * Số càng nhỏ thì độ ưu tiên lớn
     */
    @NotNull
    @ApiModelProperty(value = "Số càng nhỏ thì độ ưu tiên lớn", required = true)
    private Integer uuTien;

    /**
     * Tên loại nhỏ hơn chẩn đoán hình ảnh
     */
    @Size(max = 500)
    @ApiModelProperty(value = "Tên loại nhỏ hơn chẩn đoán hình ảnh")
    private String maPhanLoai;


    private Long donViId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Boolean isEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public Integer getUuTien() {
        return uuTien;
    }

    public void setUuTien(Integer uuTien) {
        this.uuTien = uuTien;
    }

    public String getMaPhanLoai() {
        return maPhanLoai;
    }

    public void setMaPhanLoai(String maPhanLoai) {
        this.maPhanLoai = maPhanLoai;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long donViId) {
        this.donViId = donViId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LoaiThuThuatPhauThuatDTO loaiThuThuatPhauThuatDTO = (LoaiThuThuatPhauThuatDTO) o;
        if (loaiThuThuatPhauThuatDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), loaiThuThuatPhauThuatDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LoaiThuThuatPhauThuatDTO{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", moTa='" + getMoTa() + "'" +
            ", enable='" + isEnable() + "'" +
            ", uuTien=" + getUuTien() +
            ", maPhanLoai='" + getMaPhanLoai() + "'" +
            ", donViId=" + getDonViId() +
            "}";
    }
}
