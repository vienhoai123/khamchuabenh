package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.NgheNghiep} entity.
 */
@ApiModel(description = "NDB_TABLE=READ_BACKUP=1 Thông tin nghề nghiệp theo công văn 4069")
public class NgheNghiepDTO implements Serializable {
    
    private Long id;

    /**
     * Mã nghề nghiệp 4069
     */
    @NotNull
    @ApiModelProperty(value = "Mã nghề nghiệp 4069", required = true)
    private Integer ma4069;

    /**
     * Tên nghề nghiệp 4069
     */
    @NotNull
    @Size(max = 500)
    @ApiModelProperty(value = "Tên nghề nghiệp 4069", required = true)
    private String ten4069;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMa4069() {
        return ma4069;
    }

    public void setMa4069(Integer ma4069) {
        this.ma4069 = ma4069;
    }

    public String getTen4069() {
        return ten4069;
    }

    public void setTen4069(String ten4069) {
        this.ten4069 = ten4069;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NgheNghiepDTO ngheNghiepDTO = (NgheNghiepDTO) o;
        if (ngheNghiepDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ngheNghiepDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NgheNghiepDTO{" +
            "id=" + getId() +
            ", ma4069=" + getMa4069() +
            ", ten4069='" + getTen4069() + "'" +
            "}";
    }
}
