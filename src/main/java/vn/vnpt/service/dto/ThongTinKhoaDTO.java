package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import vn.vnpt.domain.ThongTinKhoaId;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.ThongTinKhoa} entity.
 */
@ApiModel(description = "The ThongTinKhoa entity.")
public class ThongTinKhoaDTO implements Serializable {

    private Long id;

    /**
     * Thời gian Khoa nhận bệnh
     */
    @ApiModelProperty(value = "Thời gian Khoa nhận bệnh")
    private LocalDate thoiGianNhanBenh;

    /**
     * Thời gian chuyển bệnh nhân chuyển sang Khoa khác
     */
    @ApiModelProperty(value = "Thời gian chuyển bệnh nhân chuyển sang Khoa khác")
    private LocalDate thoiGianChuyenKhoa;

    /**
     * Mã Khoa phòng hiện tại
     */
    @ApiModelProperty(value = "Mã Khoa phòng hiện tại")
    private Integer khoaId;

    /**
     * khoaChuyenDen Mã khoa chuyển bệnh nhân đến
     */
    @ApiModelProperty(value = "khoaChuyenDen Mã khoa chuyển bệnh nhân đến")
    private Integer khoaChuyenDen;

    /**
     * khoaChuyenDi Mã khoa chuyển đi
     */
    @ApiModelProperty(value = "khoaChuyenDi Mã khoa chuyển đi")
    private Integer khoaChuyenDi;

    /**
     * nhanVienNhanBenh nhân viên nhận bệnh
     */
    @ApiModelProperty(value = "nhanVienNhanBenh nhân viên nhận bệnh")
    private Integer nhanVienNhanBenh;

    /**
     * nhanVienChuyenKhoa
     */
    @ApiModelProperty(value = "nhanVienChuyenKhoa")
    private Integer nhanVienChuyenKhoa;

    /**
     * soNgay: số ngày nằm viện trong khoa
     */
    @ApiModelProperty(value = "soNgay: số ngày nằm viện trong khoa")
    private BigDecimal soNgay;

    /**
     * trangThai:\n0: mới vào khoa, chờ khám;\n1: điều trị, khám bệnh có giường;\n2: điều trị, khám bệnh không giường;\n3: xuất viện, hoàn tất khám;\n4: Chuyển khoa phòng khám\n5: Chuyển viện, chuyển tuyến;\n6: Nhập viện,\n7: Tử vong\n8: Kết thúc đợt ĐT, thêm đợt ĐT mới;
     */
    @ApiModelProperty(value = "trangThai:\n0: mới vào khoa, chờ khám;\n1: điều trị, khám bệnh có giường;\n2: điều trị, khám bệnh không giường;\n3: xuất viện, hoàn tất khám;\n4: Chuyển khoa phòng khám\n5: Chuyển viện, chuyển tuyến;\n6: Nhập viện,\n7: Tử vong\n8: Kết thúc đợt ĐT, thêm đợt ĐT mới;")
    private Integer trangThai;

    @NotNull
    private Integer nam;


    private Long bakbId;

    private Long donViId;

    private Long benhNhanId;

    private Long dotDieuTriId;

    public ThongTinKhoaId getCompositeId(){
        ThongTinKhoaId result = new ThongTinKhoaId();
        result.setId(this.id);
        result.setDotDieuTriId(this.dotDieuTriId);
        result.setBakbId(this.bakbId);
        result.setBenhNhanId(this.benhNhanId);
        result.setDonViId(this.donViId);
        return result;
    }


    public void setCompositeId(ThongTinKhoaId id){
        this.id = id.getId();
        this.dotDieuTriId = id.getDotDieuTriId();
        this.bakbId = id.getBakbId();
        this.benhNhanId = id.getBenhNhanId();
        this.donViId = id.getDonViId();
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getThoiGianNhanBenh() {
        return thoiGianNhanBenh;
    }

    public void setThoiGianNhanBenh(LocalDate thoiGianNhanBenh) {
        this.thoiGianNhanBenh = thoiGianNhanBenh;
    }

    public LocalDate getThoiGianChuyenKhoa() {
        return thoiGianChuyenKhoa;
    }

    public void setThoiGianChuyenKhoa(LocalDate thoiGianChuyenKhoa) {
        this.thoiGianChuyenKhoa = thoiGianChuyenKhoa;
    }

    public Integer getKhoaId() {
        return khoaId;
    }

    public void setKhoaId(Integer khoaId) {
        this.khoaId = khoaId;
    }

    public Integer getKhoaChuyenDen() {
        return khoaChuyenDen;
    }

    public void setKhoaChuyenDen(Integer khoaChuyenDen) {
        this.khoaChuyenDen = khoaChuyenDen;
    }

    public Integer getKhoaChuyenDi() {
        return khoaChuyenDi;
    }

    public void setKhoaChuyenDi(Integer khoaChuyenDi) {
        this.khoaChuyenDi = khoaChuyenDi;
    }

    public Integer getNhanVienNhanBenh() {
        return nhanVienNhanBenh;
    }

    public void setNhanVienNhanBenh(Integer nhanVienNhanBenh) {
        this.nhanVienNhanBenh = nhanVienNhanBenh;
    }

    public Integer getNhanVienChuyenKhoa() {
        return nhanVienChuyenKhoa;
    }

    public void setNhanVienChuyenKhoa(Integer nhanVienChuyenKhoa) {
        this.nhanVienChuyenKhoa = nhanVienChuyenKhoa;
    }

    public BigDecimal getSoNgay() {
        return soNgay;
    }

    public void setSoNgay(BigDecimal soNgay) {
        this.soNgay = soNgay;
    }

    public Integer getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Integer trangThai) {
        this.trangThai = trangThai;
    }

    public Integer getNam() {
        return nam;
    }

    public void setNam(Integer nam) {
        this.nam = nam;
    }

    public Long getBakbId() {
        return bakbId;
    }

    public void setBakbId(Long dotDieuTriId) {
        this.bakbId = dotDieuTriId;
    }

    public Long getDonViId() {
        return donViId;
    }

    public void setDonViId(Long dotDieuTriId) {
        this.donViId = dotDieuTriId;
    }

    public Long getBenhNhanId() {
        return benhNhanId;
    }

    public void setBenhNhanId(Long dotDieuTriId) {
        this.benhNhanId = dotDieuTriId;
    }

    public Long getDotDieuTriId() {
        return dotDieuTriId;
    }

    public void setDotDieuTriId(Long dotDieuTriId) {
        this.dotDieuTriId = dotDieuTriId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ThongTinKhoaDTO thongTinKhoaDTO = (ThongTinKhoaDTO) o;
        if (thongTinKhoaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), thongTinKhoaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ThongTinKhoaDTO{" +
            "id=" + getId() +
            ", thoiGianNhanBenh='" + getThoiGianNhanBenh() + "'" +
            ", thoiGianChuyenKhoa='" + getThoiGianChuyenKhoa() + "'" +
            ", khoaId=" + getKhoaId() +
            ", khoaChuyenDen=" + getKhoaChuyenDen() +
            ", khoaChuyenDi=" + getKhoaChuyenDi() +
            ", nhanVienNhanBenh=" + getNhanVienNhanBenh() +
            ", nhanVienChuyenKhoa=" + getNhanVienChuyenKhoa() +
            ", soNgay=" + getSoNgay() +
            ", trangThai=" + getTrangThai() +
            ", nam=" + getNam() +
            ", bakbId=" + getBakbId() +
            ", donViId=" + getDonViId() +
            ", benhNhanId=" + getBenhNhanId() +
            ", dotDieuTriId=" + getDotDieuTriId() +
            "}";
    }
}
