package vn.vnpt.service.dto;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link vn.vnpt.domain.DoiTuongBhyt} entity.
 */
public class DoiTuongBhytDTO implements Serializable {
    
    private Long id;

    /**
     * Cận trên BHYT
     */
    @NotNull
    @ApiModelProperty(value = "Cận trên BHYT", required = true)
    private Integer canTren;

    /**
     * Cận trên kỹ thuật cao
     */
    @NotNull
    @ApiModelProperty(value = "Cận trên kỹ thuật cao", required = true)
    private Integer canTrenKtc;

    /**
     * Mã đối tượng BHYT
     */
    @NotNull
    @Size(max = 10)
    @ApiModelProperty(value = "Mã đối tượng BHYT", required = true)
    private String ma;

    /**
     * Tên đối tượng
     */
    @NotNull
    @Size(max = 500)
    @ApiModelProperty(value = "Tên đối tượng", required = true)
    private String ten;

    /**
     * Tỷ lệ miễn giảm
     */
    @NotNull
    @ApiModelProperty(value = "Tỷ lệ miễn giảm", required = true)
    private Integer tyLeMienGiam;

    /**
     * Tỷ lệ miển giảm kỹ thuật cao
     */
    @NotNull
    @ApiModelProperty(value = "Tỷ lệ miển giảm kỹ thuật cao", required = true)
    private Integer tyLeMienGiamKtc;

    /**
     * Mã nhóm đối tượng BHYT
     */
    @ApiModelProperty(value = "Mã nhóm đối tượng BHYT")

    private Long nhomDoiTuongBhytId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCanTren() {
        return canTren;
    }

    public void setCanTren(Integer canTren) {
        this.canTren = canTren;
    }

    public Integer getCanTrenKtc() {
        return canTrenKtc;
    }

    public void setCanTrenKtc(Integer canTrenKtc) {
        this.canTrenKtc = canTrenKtc;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Integer getTyLeMienGiam() {
        return tyLeMienGiam;
    }

    public void setTyLeMienGiam(Integer tyLeMienGiam) {
        this.tyLeMienGiam = tyLeMienGiam;
    }

    public Integer getTyLeMienGiamKtc() {
        return tyLeMienGiamKtc;
    }

    public void setTyLeMienGiamKtc(Integer tyLeMienGiamKtc) {
        this.tyLeMienGiamKtc = tyLeMienGiamKtc;
    }

    public Long getNhomDoiTuongBhytId() {
        return nhomDoiTuongBhytId;
    }

    public void setNhomDoiTuongBhytId(Long nhomDoiTuongBhytId) {
        this.nhomDoiTuongBhytId = nhomDoiTuongBhytId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DoiTuongBhytDTO doiTuongBhytDTO = (DoiTuongBhytDTO) o;
        if (doiTuongBhytDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), doiTuongBhytDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DoiTuongBhytDTO{" +
            "id=" + getId() +
            ", canTren=" + getCanTren() +
            ", canTrenKtc=" + getCanTrenKtc() +
            ", ma='" + getMa() + "'" +
            ", ten='" + getTen() + "'" +
            ", tyLeMienGiam=" + getTyLeMienGiam() +
            ", tyLeMienGiamKtc=" + getTyLeMienGiamKtc() +
            ", nhomDoiTuongBhytId=" + getNhomDoiTuongBhytId() +
            "}";
    }
}
