package vn.vnpt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;

/**
 * Criteria class for the {@link vn.vnpt.domain.XetNghiem} entity. This class is used
 * in {@link vn.vnpt.web.rest.XetNghiemResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /xet-nghiems?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class XetNghiemCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BigDecimalFilter canDuoiNam;

    private BigDecimalFilter canDuoiNu;

    private BigDecimalFilter canTrenNam;

    private BigDecimalFilter canTrenNu;

    private StringFilter chiSoBinhThuongNam;

    private BigDecimalFilter chiSoMax;

    private BigDecimalFilter chiSoMin;

    private StringFilter congThuc;

    private BooleanFilter deleted;

    private StringFilter doPhaLoang;

    private BigDecimalFilter donGiaBenhVien;

    private StringFilter donViTinh;

    private BooleanFilter enabled;

    private BigDecimalFilter gioiHanChiDinh;

    private StringFilter ketQuaBatThuong;

    private StringFilter maDungChung;

    private StringFilter ketQuaMacDinh;

    private BooleanFilter phamViChiDinh;

    private BooleanFilter phanTheoGioiTInh;

    private IntegerFilter soLeLamTron;

    private BigDecimalFilter soLuongThucHien;

    private StringFilter ten;

    private StringFilter tenHienThi;

    private StringFilter maNoiBo;

    private LongFilter donViId;

    private LongFilter dotMaId;

    private LongFilter loaiXNId;

    public XetNghiemCriteria() {
    }

    public XetNghiemCriteria(XetNghiemCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.canDuoiNam = other.canDuoiNam == null ? null : other.canDuoiNam.copy();
        this.canDuoiNu = other.canDuoiNu == null ? null : other.canDuoiNu.copy();
        this.canTrenNam = other.canTrenNam == null ? null : other.canTrenNam.copy();
        this.canTrenNu = other.canTrenNu == null ? null : other.canTrenNu.copy();
        this.chiSoBinhThuongNam = other.chiSoBinhThuongNam == null ? null : other.chiSoBinhThuongNam.copy();
        this.chiSoMax = other.chiSoMax == null ? null : other.chiSoMax.copy();
        this.chiSoMin = other.chiSoMin == null ? null : other.chiSoMin.copy();
        this.congThuc = other.congThuc == null ? null : other.congThuc.copy();
        this.deleted = other.deleted == null ? null : other.deleted.copy();
        this.doPhaLoang = other.doPhaLoang == null ? null : other.doPhaLoang.copy();
        this.donGiaBenhVien = other.donGiaBenhVien == null ? null : other.donGiaBenhVien.copy();
        this.donViTinh = other.donViTinh == null ? null : other.donViTinh.copy();
        this.enabled = other.enabled == null ? null : other.enabled.copy();
        this.gioiHanChiDinh = other.gioiHanChiDinh == null ? null : other.gioiHanChiDinh.copy();
        this.ketQuaBatThuong = other.ketQuaBatThuong == null ? null : other.ketQuaBatThuong.copy();
        this.maDungChung = other.maDungChung == null ? null : other.maDungChung.copy();
        this.ketQuaMacDinh = other.ketQuaMacDinh == null ? null : other.ketQuaMacDinh.copy();
        this.phamViChiDinh = other.phamViChiDinh == null ? null : other.phamViChiDinh.copy();
        this.phanTheoGioiTInh = other.phanTheoGioiTInh == null ? null : other.phanTheoGioiTInh.copy();
        this.soLeLamTron = other.soLeLamTron == null ? null : other.soLeLamTron.copy();
        this.soLuongThucHien = other.soLuongThucHien == null ? null : other.soLuongThucHien.copy();
        this.ten = other.ten == null ? null : other.ten.copy();
        this.tenHienThi = other.tenHienThi == null ? null : other.tenHienThi.copy();
        this.maNoiBo = other.maNoiBo == null ? null : other.maNoiBo.copy();
        this.donViId = other.donViId == null ? null : other.donViId.copy();
        this.dotMaId = other.dotMaId == null ? null : other.dotMaId.copy();
        this.loaiXNId = other.loaiXNId == null ? null : other.loaiXNId.copy();
    }

    @Override
    public XetNghiemCriteria copy() {
        return new XetNghiemCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BigDecimalFilter getCanDuoiNam() {
        return canDuoiNam;
    }

    public void setCanDuoiNam(BigDecimalFilter canDuoiNam) {
        this.canDuoiNam = canDuoiNam;
    }

    public BigDecimalFilter getCanDuoiNu() {
        return canDuoiNu;
    }

    public void setCanDuoiNu(BigDecimalFilter canDuoiNu) {
        this.canDuoiNu = canDuoiNu;
    }

    public BigDecimalFilter getCanTrenNam() {
        return canTrenNam;
    }

    public void setCanTrenNam(BigDecimalFilter canTrenNam) {
        this.canTrenNam = canTrenNam;
    }

    public BigDecimalFilter getCanTrenNu() {
        return canTrenNu;
    }

    public void setCanTrenNu(BigDecimalFilter canTrenNu) {
        this.canTrenNu = canTrenNu;
    }

    public StringFilter getChiSoBinhThuongNam() {
        return chiSoBinhThuongNam;
    }

    public void setChiSoBinhThuongNam(StringFilter chiSoBinhThuongNam) {
        this.chiSoBinhThuongNam = chiSoBinhThuongNam;
    }

    public BigDecimalFilter getChiSoMax() {
        return chiSoMax;
    }

    public void setChiSoMax(BigDecimalFilter chiSoMax) {
        this.chiSoMax = chiSoMax;
    }

    public BigDecimalFilter getChiSoMin() {
        return chiSoMin;
    }

    public void setChiSoMin(BigDecimalFilter chiSoMin) {
        this.chiSoMin = chiSoMin;
    }

    public StringFilter getCongThuc() {
        return congThuc;
    }

    public void setCongThuc(StringFilter congThuc) {
        this.congThuc = congThuc;
    }

    public BooleanFilter getDeleted() {
        return deleted;
    }

    public void setDeleted(BooleanFilter deleted) {
        this.deleted = deleted;
    }

    public StringFilter getDoPhaLoang() {
        return doPhaLoang;
    }

    public void setDoPhaLoang(StringFilter doPhaLoang) {
        this.doPhaLoang = doPhaLoang;
    }

    public BigDecimalFilter getDonGiaBenhVien() {
        return donGiaBenhVien;
    }

    public void setDonGiaBenhVien(BigDecimalFilter donGiaBenhVien) {
        this.donGiaBenhVien = donGiaBenhVien;
    }

    public StringFilter getDonViTinh() {
        return donViTinh;
    }

    public void setDonViTinh(StringFilter donViTinh) {
        this.donViTinh = donViTinh;
    }

    public BooleanFilter getEnabled() {
        return enabled;
    }

    public void setEnabled(BooleanFilter enabled) {
        this.enabled = enabled;
    }

    public BigDecimalFilter getGioiHanChiDinh() {
        return gioiHanChiDinh;
    }

    public void setGioiHanChiDinh(BigDecimalFilter gioiHanChiDinh) {
        this.gioiHanChiDinh = gioiHanChiDinh;
    }

    public StringFilter getKetQuaBatThuong() {
        return ketQuaBatThuong;
    }

    public void setKetQuaBatThuong(StringFilter ketQuaBatThuong) {
        this.ketQuaBatThuong = ketQuaBatThuong;
    }

    public StringFilter getMaDungChung() {
        return maDungChung;
    }

    public void setMaDungChung(StringFilter maDungChung) {
        this.maDungChung = maDungChung;
    }

    public StringFilter getKetQuaMacDinh() {
        return ketQuaMacDinh;
    }

    public void setKetQuaMacDinh(StringFilter ketQuaMacDinh) {
        this.ketQuaMacDinh = ketQuaMacDinh;
    }

    public BooleanFilter getPhamViChiDinh() {
        return phamViChiDinh;
    }

    public void setPhamViChiDinh(BooleanFilter phamViChiDinh) {
        this.phamViChiDinh = phamViChiDinh;
    }

    public BooleanFilter getPhanTheoGioiTInh() {
        return phanTheoGioiTInh;
    }

    public void setPhanTheoGioiTInh(BooleanFilter phanTheoGioiTInh) {
        this.phanTheoGioiTInh = phanTheoGioiTInh;
    }

    public IntegerFilter getSoLeLamTron() {
        return soLeLamTron;
    }

    public void setSoLeLamTron(IntegerFilter soLeLamTron) {
        this.soLeLamTron = soLeLamTron;
    }

    public BigDecimalFilter getSoLuongThucHien() {
        return soLuongThucHien;
    }

    public void setSoLuongThucHien(BigDecimalFilter soLuongThucHien) {
        this.soLuongThucHien = soLuongThucHien;
    }

    public StringFilter getTen() {
        return ten;
    }

    public void setTen(StringFilter ten) {
        this.ten = ten;
    }

    public StringFilter getTenHienThi() {
        return tenHienThi;
    }

    public void setTenHienThi(StringFilter tenHienThi) {
        this.tenHienThi = tenHienThi;
    }

    public StringFilter getMaNoiBo() {
        return maNoiBo;
    }

    public void setMaNoiBo(StringFilter maNoiBo) {
        this.maNoiBo = maNoiBo;
    }

    public LongFilter getDonViId() {
        return donViId;
    }

    public void setDonViId(LongFilter donViId) {
        this.donViId = donViId;
    }

    public LongFilter getDotMaId() {
        return dotMaId;
    }

    public void setDotMaId(LongFilter dotMaId) {
        this.dotMaId = dotMaId;
    }

    public LongFilter getLoaiXNId() {
        return loaiXNId;
    }

    public void setLoaiXNId(LongFilter loaiXNId) {
        this.loaiXNId = loaiXNId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final XetNghiemCriteria that = (XetNghiemCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(canDuoiNam, that.canDuoiNam) &&
            Objects.equals(canDuoiNu, that.canDuoiNu) &&
            Objects.equals(canTrenNam, that.canTrenNam) &&
            Objects.equals(canTrenNu, that.canTrenNu) &&
            Objects.equals(chiSoBinhThuongNam, that.chiSoBinhThuongNam) &&
            Objects.equals(chiSoMax, that.chiSoMax) &&
            Objects.equals(chiSoMin, that.chiSoMin) &&
            Objects.equals(congThuc, that.congThuc) &&
            Objects.equals(deleted, that.deleted) &&
            Objects.equals(doPhaLoang, that.doPhaLoang) &&
            Objects.equals(donGiaBenhVien, that.donGiaBenhVien) &&
            Objects.equals(donViTinh, that.donViTinh) &&
            Objects.equals(enabled, that.enabled) &&
            Objects.equals(gioiHanChiDinh, that.gioiHanChiDinh) &&
            Objects.equals(ketQuaBatThuong, that.ketQuaBatThuong) &&
            Objects.equals(maDungChung, that.maDungChung) &&
            Objects.equals(ketQuaMacDinh, that.ketQuaMacDinh) &&
            Objects.equals(phamViChiDinh, that.phamViChiDinh) &&
            Objects.equals(phanTheoGioiTInh, that.phanTheoGioiTInh) &&
            Objects.equals(soLeLamTron, that.soLeLamTron) &&
            Objects.equals(soLuongThucHien, that.soLuongThucHien) &&
            Objects.equals(ten, that.ten) &&
            Objects.equals(tenHienThi, that.tenHienThi) &&
            Objects.equals(maNoiBo, that.maNoiBo) &&
            Objects.equals(donViId, that.donViId) &&
            Objects.equals(dotMaId, that.dotMaId) &&
            Objects.equals(loaiXNId, that.loaiXNId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        canDuoiNam,
        canDuoiNu,
        canTrenNam,
        canTrenNu,
        chiSoBinhThuongNam,
        chiSoMax,
        chiSoMin,
        congThuc,
        deleted,
        doPhaLoang,
        donGiaBenhVien,
        donViTinh,
        enabled,
        gioiHanChiDinh,
        ketQuaBatThuong,
        maDungChung,
        ketQuaMacDinh,
        phamViChiDinh,
        phanTheoGioiTInh,
        soLeLamTron,
        soLuongThucHien,
        ten,
        tenHienThi,
        maNoiBo,
        donViId,
        dotMaId,
        loaiXNId
        );
    }

    @Override
    public String toString() {
        return "XetNghiemCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (canDuoiNam != null ? "canDuoiNam=" + canDuoiNam + ", " : "") +
                (canDuoiNu != null ? "canDuoiNu=" + canDuoiNu + ", " : "") +
                (canTrenNam != null ? "canTrenNam=" + canTrenNam + ", " : "") +
                (canTrenNu != null ? "canTrenNu=" + canTrenNu + ", " : "") +
                (chiSoBinhThuongNam != null ? "chiSoBinhThuongNam=" + chiSoBinhThuongNam + ", " : "") +
                (chiSoMax != null ? "chiSoMax=" + chiSoMax + ", " : "") +
                (chiSoMin != null ? "chiSoMin=" + chiSoMin + ", " : "") +
                (congThuc != null ? "congThuc=" + congThuc + ", " : "") +
                (deleted != null ? "deleted=" + deleted + ", " : "") +
                (doPhaLoang != null ? "doPhaLoang=" + doPhaLoang + ", " : "") +
                (donGiaBenhVien != null ? "donGiaBenhVien=" + donGiaBenhVien + ", " : "") +
                (donViTinh != null ? "donViTinh=" + donViTinh + ", " : "") +
                (enabled != null ? "enabled=" + enabled + ", " : "") +
                (gioiHanChiDinh != null ? "gioiHanChiDinh=" + gioiHanChiDinh + ", " : "") +
                (ketQuaBatThuong != null ? "ketQuaBatThuong=" + ketQuaBatThuong + ", " : "") +
                (maDungChung != null ? "maDungChung=" + maDungChung + ", " : "") +
                (ketQuaMacDinh != null ? "ketQuaMacDinh=" + ketQuaMacDinh + ", " : "") +
                (phamViChiDinh != null ? "phamViChiDinh=" + phamViChiDinh + ", " : "") +
                (phanTheoGioiTInh != null ? "phanTheoGioiTInh=" + phanTheoGioiTInh + ", " : "") +
                (soLeLamTron != null ? "soLeLamTron=" + soLeLamTron + ", " : "") +
                (soLuongThucHien != null ? "soLuongThucHien=" + soLuongThucHien + ", " : "") +
                (ten != null ? "ten=" + ten + ", " : "") +
                (tenHienThi != null ? "tenHienThi=" + tenHienThi + ", " : "") +
                (maNoiBo != null ? "maNoiBo=" + maNoiBo + ", " : "") +
                (donViId != null ? "donViId=" + donViId + ", " : "") +
                (dotMaId != null ? "dotMaId=" + dotMaId + ", " : "") +
                (loaiXNId != null ? "loaiXNId=" + loaiXNId + ", " : "") +
            "}";
    }

}
