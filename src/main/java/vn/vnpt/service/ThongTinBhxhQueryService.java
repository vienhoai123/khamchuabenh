package vn.vnpt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import vn.vnpt.domain.ThongTinBhxh;
import vn.vnpt.domain.*; // for static metamodels
import vn.vnpt.repository.ThongTinBhxhRepository;
import vn.vnpt.service.dto.ThongTinBhxhCriteria;
import vn.vnpt.service.dto.ThongTinBhxhDTO;
import vn.vnpt.service.mapper.ThongTinBhxhMapper;

/**
 * Service for executing complex queries for {@link ThongTinBhxh} entities in the database.
 * The main input is a {@link ThongTinBhxhCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ThongTinBhxhDTO} or a {@link Page} of {@link ThongTinBhxhDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ThongTinBhxhQueryService extends QueryService<ThongTinBhxh> {

    private final Logger log = LoggerFactory.getLogger(ThongTinBhxhQueryService.class);

    private final ThongTinBhxhRepository thongTinBhxhRepository;

    private final ThongTinBhxhMapper thongTinBhxhMapper;

    public ThongTinBhxhQueryService(ThongTinBhxhRepository thongTinBhxhRepository, ThongTinBhxhMapper thongTinBhxhMapper) {
        this.thongTinBhxhRepository = thongTinBhxhRepository;
        this.thongTinBhxhMapper = thongTinBhxhMapper;
    }

    /**
     * Return a {@link List} of {@link ThongTinBhxhDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ThongTinBhxhDTO> findByCriteria(ThongTinBhxhCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ThongTinBhxh> specification = createSpecification(criteria);
        return thongTinBhxhMapper.toDto(thongTinBhxhRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ThongTinBhxhDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ThongTinBhxhDTO> findByCriteria(ThongTinBhxhCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ThongTinBhxh> specification = createSpecification(criteria);
        return thongTinBhxhRepository.findAll(specification, page)
            .map(thongTinBhxhMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ThongTinBhxhCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ThongTinBhxh> specification = createSpecification(criteria);
        return thongTinBhxhRepository.count(specification);
    }

    /**
     * Function to convert {@link ThongTinBhxhCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ThongTinBhxh> createSpecification(ThongTinBhxhCriteria criteria) {
        Specification<ThongTinBhxh> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ThongTinBhxh_.id));
            }
            if (criteria.getDvtt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDvtt(), ThongTinBhxh_.dvtt));
            }
            if (criteria.getEnabled() != null) {
                specification = specification.and(buildSpecification(criteria.getEnabled(), ThongTinBhxh_.enabled));
            }
            if (criteria.getNgayApDungBhyt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayApDungBhyt(), ThongTinBhxh_.ngayApDungBhyt));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), ThongTinBhxh_.ten));
            }
            if (criteria.getDonViId() != null) {
                specification = specification.and(buildSpecification(criteria.getDonViId(),
                    root -> root.join(ThongTinBhxh_.donVi, JoinType.LEFT).get(DonVi_.id)));
            }
        }
        return specification;
    }
}
