package vn.vnpt.service;

import vn.vnpt.service.dto.NhomBenhLyDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.NhomBenhLy}.
 */
public interface NhomBenhLyService {

    /**
     * Save a nhomBenhLy.
     *
     * @param nhomBenhLyDTO the entity to save.
     * @return the persisted entity.
     */
    NhomBenhLyDTO save(NhomBenhLyDTO nhomBenhLyDTO);

    /**
     * Get all the nhomBenhLies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<NhomBenhLyDTO> findAll(Pageable pageable);

    /**
     * Get the "id" nhomBenhLy.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<NhomBenhLyDTO> findOne(Long id);

    /**
     * Delete the "id" nhomBenhLy.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
