package vn.vnpt.service;

import vn.vnpt.service.dto.NhomChanDoanHinhAnhDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.NhomChanDoanHinhAnh}.
 */
public interface NhomChanDoanHinhAnhService {

    /**
     * Save a nhomChanDoanHinhAnh.
     *
     * @param nhomChanDoanHinhAnhDTO the entity to save.
     * @return the persisted entity.
     */
    NhomChanDoanHinhAnhDTO save(NhomChanDoanHinhAnhDTO nhomChanDoanHinhAnhDTO);

    /**
     * Get all the nhomChanDoanHinhAnhs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<NhomChanDoanHinhAnhDTO> findAll(Pageable pageable);

    /**
     * Get the "id" nhomChanDoanHinhAnh.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<NhomChanDoanHinhAnhDTO> findOne(Long id);

    /**
     * Delete the "id" nhomChanDoanHinhAnh.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
