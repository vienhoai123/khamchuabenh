package vn.vnpt.service;

import vn.vnpt.service.dto.DoiTuongBhytDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link vn.vnpt.domain.DoiTuongBhyt}.
 */
public interface DoiTuongBhytService {

    /**
     * Save a doiTuongBhyt.
     *
     * @param doiTuongBhytDTO the entity to save.
     * @return the persisted entity.
     */
    DoiTuongBhytDTO save(DoiTuongBhytDTO doiTuongBhytDTO);

    /**
     * Get all the doiTuongBhyts.
     *
     * @return the list of entities.
     */
    List<DoiTuongBhytDTO> findAll();

    /**
     * Get the "id" doiTuongBhyt.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DoiTuongBhytDTO> findOne(Long id);

    /**
     * Delete the "id" doiTuongBhyt.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
