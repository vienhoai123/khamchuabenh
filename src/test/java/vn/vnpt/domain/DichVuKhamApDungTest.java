package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class DichVuKhamApDungTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DichVuKhamApDung.class);
        DichVuKhamApDung dichVuKhamApDung1 = new DichVuKhamApDung();
        dichVuKhamApDung1.setId(1L);
        DichVuKhamApDung dichVuKhamApDung2 = new DichVuKhamApDung();
        dichVuKhamApDung2.setId(1L);
        assertThat(dichVuKhamApDung1).isEqualTo(dichVuKhamApDung2);
        dichVuKhamApDung2.setId(2L);
        assertThat(dichVuKhamApDung1).isNotEqualTo(dichVuKhamApDung2);
        dichVuKhamApDung1.setId(null);
        assertThat(dichVuKhamApDung1).isNotEqualTo(dichVuKhamApDung2);
    }
}
