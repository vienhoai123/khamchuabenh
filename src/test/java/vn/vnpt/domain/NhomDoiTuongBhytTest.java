package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class NhomDoiTuongBhytTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NhomDoiTuongBhyt.class);
        NhomDoiTuongBhyt nhomDoiTuongBhyt1 = new NhomDoiTuongBhyt();
        nhomDoiTuongBhyt1.setId(1L);
        NhomDoiTuongBhyt nhomDoiTuongBhyt2 = new NhomDoiTuongBhyt();
        nhomDoiTuongBhyt2.setId(nhomDoiTuongBhyt1.getId());
        assertThat(nhomDoiTuongBhyt1).isEqualTo(nhomDoiTuongBhyt2);
        nhomDoiTuongBhyt2.setId(2L);
        assertThat(nhomDoiTuongBhyt1).isNotEqualTo(nhomDoiTuongBhyt2);
        nhomDoiTuongBhyt1.setId(null);
        assertThat(nhomDoiTuongBhyt1).isNotEqualTo(nhomDoiTuongBhyt2);
    }
}
