package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ThuThuatPhauThuatTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ThuThuatPhauThuat.class);
        ThuThuatPhauThuat thuThuatPhauThuat1 = new ThuThuatPhauThuat();
        thuThuatPhauThuat1.setId(1L);
        ThuThuatPhauThuat thuThuatPhauThuat2 = new ThuThuatPhauThuat();
        thuThuatPhauThuat2.setId(thuThuatPhauThuat1.getId());
        assertThat(thuThuatPhauThuat1).isEqualTo(thuThuatPhauThuat2);
        thuThuatPhauThuat2.setId(2L);
        assertThat(thuThuatPhauThuat1).isNotEqualTo(thuThuatPhauThuat2);
        thuThuatPhauThuat1.setId(null);
        assertThat(thuThuatPhauThuat1).isNotEqualTo(thuThuatPhauThuat2);
    }
}
