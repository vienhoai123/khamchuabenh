package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class TinhThanhPhoTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TinhThanhPho.class);
        TinhThanhPho tinhThanhPho1 = new TinhThanhPho();
        tinhThanhPho1.setId(1L);
        TinhThanhPho tinhThanhPho2 = new TinhThanhPho();
        tinhThanhPho2.setId(tinhThanhPho1.getId());
        assertThat(tinhThanhPho1).isEqualTo(tinhThanhPho2);
        tinhThanhPho2.setId(2L);
        assertThat(tinhThanhPho1).isNotEqualTo(tinhThanhPho2);
        tinhThanhPho1.setId(null);
        assertThat(tinhThanhPho1).isNotEqualTo(tinhThanhPho2);
    }
}
