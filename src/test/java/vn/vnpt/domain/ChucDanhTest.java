package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ChucDanhTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChucDanh.class);
        ChucDanh chucDanh1 = new ChucDanh();
        chucDanh1.setId(1L);
        ChucDanh chucDanh2 = new ChucDanh();
        chucDanh2.setId(chucDanh1.getId());
        assertThat(chucDanh1).isEqualTo(chucDanh2);
        chucDanh2.setId(2L);
        assertThat(chucDanh1).isNotEqualTo(chucDanh2);
        chucDanh1.setId(null);
        assertThat(chucDanh1).isNotEqualTo(chucDanh2);
    }
}
