package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ThongTinKhoaTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ThongTinKhoa.class);
        ThongTinKhoa thongTinKhoa1 = new ThongTinKhoa();
        thongTinKhoa1.setId(1L);
        ThongTinKhoa thongTinKhoa2 = new ThongTinKhoa();
        thongTinKhoa2.setId(thongTinKhoa1.getId());
        assertThat(thongTinKhoa1).isEqualTo(thongTinKhoa2);
        thongTinKhoa2.setId(2L);
        assertThat(thongTinKhoa1).isNotEqualTo(thongTinKhoa2);
        thongTinKhoa1.setId(null);
        assertThat(thongTinKhoa1).isNotEqualTo(thongTinKhoa2);
    }
}
