package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class XetNghiemApDungTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(XetNghiemApDung.class);
        XetNghiemApDung xetNghiemApDung1 = new XetNghiemApDung();
        xetNghiemApDung1.setId(1L);
        XetNghiemApDung xetNghiemApDung2 = new XetNghiemApDung();
        xetNghiemApDung2.setId(xetNghiemApDung1.getId());
        assertThat(xetNghiemApDung1).isEqualTo(xetNghiemApDung2);
        xetNghiemApDung2.setId(2L);
        assertThat(xetNghiemApDung1).isNotEqualTo(xetNghiemApDung2);
        xetNghiemApDung1.setId(null);
        assertThat(xetNghiemApDung1).isNotEqualTo(xetNghiemApDung2);
    }
}
