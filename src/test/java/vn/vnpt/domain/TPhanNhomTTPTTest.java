package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class TPhanNhomTTPTTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TPhanNhomTTPT.class);
        TPhanNhomTTPT tPhanNhomTTPT1 = new TPhanNhomTTPT();
        tPhanNhomTTPT1.setId(1L);
        TPhanNhomTTPT tPhanNhomTTPT2 = new TPhanNhomTTPT();
        tPhanNhomTTPT2.setId(tPhanNhomTTPT1.getId());
        assertThat(tPhanNhomTTPT1).isEqualTo(tPhanNhomTTPT2);
        tPhanNhomTTPT2.setId(2L);
        assertThat(tPhanNhomTTPT1).isNotEqualTo(tPhanNhomTTPT2);
        tPhanNhomTTPT1.setId(null);
        assertThat(tPhanNhomTTPT1).isNotEqualTo(tPhanNhomTTPT2);
    }
}
