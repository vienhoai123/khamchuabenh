package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class BenhYhctTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BenhYhct.class);
        BenhYhct benhYhct1 = new BenhYhct();
        benhYhct1.setId(1L);
        BenhYhct benhYhct2 = new BenhYhct();
        benhYhct2.setId(benhYhct1.getId());
        assertThat(benhYhct1).isEqualTo(benhYhct2);
        benhYhct2.setId(2L);
        assertThat(benhYhct1).isNotEqualTo(benhYhct2);
        benhYhct1.setId(null);
        assertThat(benhYhct1).isNotEqualTo(benhYhct2);
    }
}
