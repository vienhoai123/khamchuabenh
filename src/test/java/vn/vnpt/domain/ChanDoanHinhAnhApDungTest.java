package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ChanDoanHinhAnhApDungTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChanDoanHinhAnhApDung.class);
        ChanDoanHinhAnhApDung chanDoanHinhAnhApDung1 = new ChanDoanHinhAnhApDung();
        chanDoanHinhAnhApDung1.setId(1L);
        ChanDoanHinhAnhApDung chanDoanHinhAnhApDung2 = new ChanDoanHinhAnhApDung();
        chanDoanHinhAnhApDung2.setId(chanDoanHinhAnhApDung1.getId());
        assertThat(chanDoanHinhAnhApDung1).isEqualTo(chanDoanHinhAnhApDung2);
        chanDoanHinhAnhApDung2.setId(2L);
        assertThat(chanDoanHinhAnhApDung1).isNotEqualTo(chanDoanHinhAnhApDung2);
        chanDoanHinhAnhApDung1.setId(null);
        assertThat(chanDoanHinhAnhApDung1).isNotEqualTo(chanDoanHinhAnhApDung2);
    }
}
