package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class PhieuChiDinhCDHATest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhieuChiDinhCDHA.class);
        PhieuChiDinhCDHA phieuChiDinhCDHA1 = new PhieuChiDinhCDHA();
        phieuChiDinhCDHA1.setId(1L);
        PhieuChiDinhCDHA phieuChiDinhCDHA2 = new PhieuChiDinhCDHA();
        phieuChiDinhCDHA2.setId(phieuChiDinhCDHA1.getId());
        assertThat(phieuChiDinhCDHA1).isEqualTo(phieuChiDinhCDHA2);
        phieuChiDinhCDHA2.setId(2L);
        assertThat(phieuChiDinhCDHA1).isNotEqualTo(phieuChiDinhCDHA2);
        phieuChiDinhCDHA1.setId(null);
        assertThat(phieuChiDinhCDHA1).isNotEqualTo(phieuChiDinhCDHA2);
    }
}
