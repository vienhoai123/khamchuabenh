package vn.vnpt.domain;

import org.junit.jupiter.api.Test;
import vn.vnpt.web.rest.TestUtil;

import static org.assertj.core.api.Assertions.assertThat;

public class DotThayDoiMaDichVuTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DotThayDoiMaDichVu.class);
        DotThayDoiMaDichVu dotThayDoiMaDichVu1 = new DotThayDoiMaDichVu();
        dotThayDoiMaDichVu1.setId(1L);
        DotThayDoiMaDichVu dotThayDoiMaDichVu2 = new DotThayDoiMaDichVu();
        dotThayDoiMaDichVu2.setId(dotThayDoiMaDichVu1.getId());
        assertThat(dotThayDoiMaDichVu1).isEqualTo(dotThayDoiMaDichVu2);
        dotThayDoiMaDichVu2.setId(2L);
        assertThat(dotThayDoiMaDichVu1).isNotEqualTo(dotThayDoiMaDichVu2);
        dotThayDoiMaDichVu1.setId(null);
        assertThat(dotThayDoiMaDichVu1).isNotEqualTo(dotThayDoiMaDichVu2);
    }
}
