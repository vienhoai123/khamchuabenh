package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TinhThanhPhoMapperTest {

    private TinhThanhPhoMapper tinhThanhPhoMapper;

    @BeforeEach
    public void setUp() {
        tinhThanhPhoMapper = new TinhThanhPhoMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(tinhThanhPhoMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(tinhThanhPhoMapper.fromId(null)).isNull();
    }
}
