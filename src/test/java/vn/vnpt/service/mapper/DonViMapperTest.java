package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class DonViMapperTest {

    private DonViMapper donViMapper;

    @BeforeEach
    public void setUp() {
        donViMapper = new DonViMapperImpl();
    }

    @Test
        public void testEntityFromId() {
        assertThat(donViMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(donViMapper.fromId(null)).isNull();
    }
}
