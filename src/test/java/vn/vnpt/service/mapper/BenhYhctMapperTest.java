package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class BenhYhctMapperTest {

    private BenhYhctMapper benhYhctMapper;

    @BeforeEach
    public void setUp() {
        benhYhctMapper = new BenhYhctMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(benhYhctMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(benhYhctMapper.fromId(null)).isNull();
    }
}
