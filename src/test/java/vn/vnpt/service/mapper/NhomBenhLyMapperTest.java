package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class NhomBenhLyMapperTest {

    private NhomBenhLyMapper nhomBenhLyMapper;

    @BeforeEach
    public void setUp() {
        nhomBenhLyMapper = new NhomBenhLyMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(nhomBenhLyMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(nhomBenhLyMapper.fromId(null)).isNull();
    }
}
