package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ChiDinhDichVuKhamMapperTest {

    private ChiDinhDichVuKhamMapper chiDinhDichVuKhamMapper;

    @BeforeEach
    public void setUp() {
        chiDinhDichVuKhamMapper = new ChiDinhDichVuKhamMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(chiDinhDichVuKhamMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(chiDinhDichVuKhamMapper.fromId(null)).isNull();
    }
}
