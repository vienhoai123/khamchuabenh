package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ChanDoanHinhAnhApDungMapperTest {

    private ChanDoanHinhAnhApDungMapper chanDoanHinhAnhApDungMapper;

    @BeforeEach
    public void setUp() {
        chanDoanHinhAnhApDungMapper = new ChanDoanHinhAnhApDungMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(chanDoanHinhAnhApDungMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(chanDoanHinhAnhApDungMapper.fromId(null)).isNull();
    }
}
