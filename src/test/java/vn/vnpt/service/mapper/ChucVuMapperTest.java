package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ChucVuMapperTest {

    private ChucVuMapper chucVuMapper;

    @BeforeEach
    public void setUp() {
        chucVuMapper = new ChucVuMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(chucVuMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(chucVuMapper.fromId(null)).isNull();
    }
}
