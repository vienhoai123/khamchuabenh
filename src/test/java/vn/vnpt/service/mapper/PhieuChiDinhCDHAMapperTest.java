package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class PhieuChiDinhCDHAMapperTest {

    private PhieuChiDinhCDHAMapper phieuChiDinhCDHAMapper;

    @BeforeEach
    public void setUp() {
        phieuChiDinhCDHAMapper = new PhieuChiDinhCDHAMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(phieuChiDinhCDHAMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(phieuChiDinhCDHAMapper.fromId(null)).isNull();
    }
}
