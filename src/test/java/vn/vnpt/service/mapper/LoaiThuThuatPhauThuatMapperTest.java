package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class LoaiThuThuatPhauThuatMapperTest {

    private LoaiThuThuatPhauThuatMapper loaiThuThuatPhauThuatMapper;

    @BeforeEach
    public void setUp() {
        loaiThuThuatPhauThuatMapper = new LoaiThuThuatPhauThuatMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(loaiThuThuatPhauThuatMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(loaiThuThuatPhauThuatMapper.fromId(null)).isNull();
    }
}
