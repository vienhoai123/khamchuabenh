package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ThuThuatPhauThuatApDungMapperTest {

    private ThuThuatPhauThuatApDungMapper thuThuatPhauThuatApDungMapper;

    @BeforeEach
    public void setUp() {
        thuThuatPhauThuatApDungMapper = new ThuThuatPhauThuatApDungMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(thuThuatPhauThuatApDungMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(thuThuatPhauThuatApDungMapper.fromId(null)).isNull();
    }
}
