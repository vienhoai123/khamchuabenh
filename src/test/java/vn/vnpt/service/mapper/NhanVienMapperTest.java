package vn.vnpt.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class NhanVienMapperTest {

    private NhanVienMapper nhanVienMapper;

    @BeforeEach
    public void setUp() {
        nhanVienMapper = new NhanVienMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(nhanVienMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(nhanVienMapper.fromId(null)).isNull();
    }
}
