package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class PhieuChiDinhTTPTDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhieuChiDinhTTPTDTO.class);
        PhieuChiDinhTTPTDTO phieuChiDinhTTPTDTO1 = new PhieuChiDinhTTPTDTO();
        phieuChiDinhTTPTDTO1.setId(1L);
        PhieuChiDinhTTPTDTO phieuChiDinhTTPTDTO2 = new PhieuChiDinhTTPTDTO();
        assertThat(phieuChiDinhTTPTDTO1).isNotEqualTo(phieuChiDinhTTPTDTO2);
        phieuChiDinhTTPTDTO2.setId(phieuChiDinhTTPTDTO1.getId());
        assertThat(phieuChiDinhTTPTDTO1).isEqualTo(phieuChiDinhTTPTDTO2);
        phieuChiDinhTTPTDTO2.setId(2L);
        assertThat(phieuChiDinhTTPTDTO1).isNotEqualTo(phieuChiDinhTTPTDTO2);
        phieuChiDinhTTPTDTO1.setId(null);
        assertThat(phieuChiDinhTTPTDTO1).isNotEqualTo(phieuChiDinhTTPTDTO2);
    }
}
