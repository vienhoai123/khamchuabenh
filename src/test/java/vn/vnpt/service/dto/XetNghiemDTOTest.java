package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class XetNghiemDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(XetNghiemDTO.class);
        XetNghiemDTO xetNghiemDTO1 = new XetNghiemDTO();
        xetNghiemDTO1.setId(1L);
        XetNghiemDTO xetNghiemDTO2 = new XetNghiemDTO();
        assertThat(xetNghiemDTO1).isNotEqualTo(xetNghiemDTO2);
        xetNghiemDTO2.setId(xetNghiemDTO1.getId());
        assertThat(xetNghiemDTO1).isEqualTo(xetNghiemDTO2);
        xetNghiemDTO2.setId(2L);
        assertThat(xetNghiemDTO1).isNotEqualTo(xetNghiemDTO2);
        xetNghiemDTO1.setId(null);
        assertThat(xetNghiemDTO1).isNotEqualTo(xetNghiemDTO2);
    }
}
