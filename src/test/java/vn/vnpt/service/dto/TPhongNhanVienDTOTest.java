package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class TPhongNhanVienDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TPhongNhanVienDTO.class);
        TPhongNhanVienDTO tPhongNhanVienDTO1 = new TPhongNhanVienDTO();
        tPhongNhanVienDTO1.setId(1L);
        TPhongNhanVienDTO tPhongNhanVienDTO2 = new TPhongNhanVienDTO();
        assertThat(tPhongNhanVienDTO1).isNotEqualTo(tPhongNhanVienDTO2);
        tPhongNhanVienDTO2.setId(tPhongNhanVienDTO1.getId());
        assertThat(tPhongNhanVienDTO1).isEqualTo(tPhongNhanVienDTO2);
        tPhongNhanVienDTO2.setId(2L);
        assertThat(tPhongNhanVienDTO1).isNotEqualTo(tPhongNhanVienDTO2);
        tPhongNhanVienDTO1.setId(null);
        assertThat(tPhongNhanVienDTO1).isNotEqualTo(tPhongNhanVienDTO2);
    }
}
