package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ThuThuatPhauThuatDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ThuThuatPhauThuatDTO.class);
        ThuThuatPhauThuatDTO thuThuatPhauThuatDTO1 = new ThuThuatPhauThuatDTO();
        thuThuatPhauThuatDTO1.setId(1L);
        ThuThuatPhauThuatDTO thuThuatPhauThuatDTO2 = new ThuThuatPhauThuatDTO();
        assertThat(thuThuatPhauThuatDTO1).isNotEqualTo(thuThuatPhauThuatDTO2);
        thuThuatPhauThuatDTO2.setId(thuThuatPhauThuatDTO1.getId());
        assertThat(thuThuatPhauThuatDTO1).isEqualTo(thuThuatPhauThuatDTO2);
        thuThuatPhauThuatDTO2.setId(2L);
        assertThat(thuThuatPhauThuatDTO1).isNotEqualTo(thuThuatPhauThuatDTO2);
        thuThuatPhauThuatDTO1.setId(null);
        assertThat(thuThuatPhauThuatDTO1).isNotEqualTo(thuThuatPhauThuatDTO2);
    }
}
