package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class ChucVuDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChucVuDTO.class);
        ChucVuDTO chucVuDTO1 = new ChucVuDTO();
        chucVuDTO1.setId(1L);
        ChucVuDTO chucVuDTO2 = new ChucVuDTO();
        assertThat(chucVuDTO1).isNotEqualTo(chucVuDTO2);
        chucVuDTO2.setId(chucVuDTO1.getId());
        assertThat(chucVuDTO1).isEqualTo(chucVuDTO2);
        chucVuDTO2.setId(2L);
        assertThat(chucVuDTO1).isNotEqualTo(chucVuDTO2);
        chucVuDTO1.setId(null);
        assertThat(chucVuDTO1).isNotEqualTo(chucVuDTO2);
    }
}
