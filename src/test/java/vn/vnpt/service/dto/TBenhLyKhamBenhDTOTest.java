package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class TBenhLyKhamBenhDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TBenhLyKhamBenhDTO.class);
        TBenhLyKhamBenhDTO tBenhLyKhamBenhDTO1 = new TBenhLyKhamBenhDTO();
        tBenhLyKhamBenhDTO1.setId(1L);
        TBenhLyKhamBenhDTO tBenhLyKhamBenhDTO2 = new TBenhLyKhamBenhDTO();
        assertThat(tBenhLyKhamBenhDTO1).isNotEqualTo(tBenhLyKhamBenhDTO2);
        tBenhLyKhamBenhDTO2.setId(tBenhLyKhamBenhDTO1.getId());
        assertThat(tBenhLyKhamBenhDTO1).isEqualTo(tBenhLyKhamBenhDTO2);
        tBenhLyKhamBenhDTO2.setId(2L);
        assertThat(tBenhLyKhamBenhDTO1).isNotEqualTo(tBenhLyKhamBenhDTO2);
        tBenhLyKhamBenhDTO1.setId(null);
        assertThat(tBenhLyKhamBenhDTO1).isNotEqualTo(tBenhLyKhamBenhDTO2);
    }
}
