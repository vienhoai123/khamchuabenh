package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class NgheNghiepDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(NgheNghiepDTO.class);
        NgheNghiepDTO ngheNghiepDTO1 = new NgheNghiepDTO();
        ngheNghiepDTO1.setId(1L);
        NgheNghiepDTO ngheNghiepDTO2 = new NgheNghiepDTO();
        assertThat(ngheNghiepDTO1).isNotEqualTo(ngheNghiepDTO2);
        ngheNghiepDTO2.setId(ngheNghiepDTO1.getId());
        assertThat(ngheNghiepDTO1).isEqualTo(ngheNghiepDTO2);
        ngheNghiepDTO2.setId(2L);
        assertThat(ngheNghiepDTO1).isNotEqualTo(ngheNghiepDTO2);
        ngheNghiepDTO1.setId(null);
        assertThat(ngheNghiepDTO1).isNotEqualTo(ngheNghiepDTO2);
    }
}
