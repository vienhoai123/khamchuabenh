package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class LoaiXetNghiemDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoaiXetNghiemDTO.class);
        LoaiXetNghiemDTO loaiXetNghiemDTO1 = new LoaiXetNghiemDTO();
        loaiXetNghiemDTO1.setId(1L);
        LoaiXetNghiemDTO loaiXetNghiemDTO2 = new LoaiXetNghiemDTO();
        assertThat(loaiXetNghiemDTO1).isNotEqualTo(loaiXetNghiemDTO2);
        loaiXetNghiemDTO2.setId(loaiXetNghiemDTO1.getId());
        assertThat(loaiXetNghiemDTO1).isEqualTo(loaiXetNghiemDTO2);
        loaiXetNghiemDTO2.setId(2L);
        assertThat(loaiXetNghiemDTO1).isNotEqualTo(loaiXetNghiemDTO2);
        loaiXetNghiemDTO1.setId(null);
        assertThat(loaiXetNghiemDTO1).isNotEqualTo(loaiXetNghiemDTO2);
    }
}
