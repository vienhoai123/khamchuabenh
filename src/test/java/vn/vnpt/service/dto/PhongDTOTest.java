package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class PhongDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhongDTO.class);
        PhongDTO phongDTO1 = new PhongDTO();
        phongDTO1.setId(1L);
        PhongDTO phongDTO2 = new PhongDTO();
        assertThat(phongDTO1).isNotEqualTo(phongDTO2);
        phongDTO2.setId(phongDTO1.getId());
        assertThat(phongDTO1).isEqualTo(phongDTO2);
        phongDTO2.setId(2L);
        assertThat(phongDTO1).isNotEqualTo(phongDTO2);
        phongDTO1.setId(null);
        assertThat(phongDTO1).isNotEqualTo(phongDTO2);
    }
}
