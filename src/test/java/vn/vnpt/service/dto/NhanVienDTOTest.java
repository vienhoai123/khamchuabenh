package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class NhanVienDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(NhanVienDTO.class);
        NhanVienDTO nhanVienDTO1 = new NhanVienDTO();
        nhanVienDTO1.setId(1L);
        NhanVienDTO nhanVienDTO2 = new NhanVienDTO();
        assertThat(nhanVienDTO1).isNotEqualTo(nhanVienDTO2);
        nhanVienDTO2.setId(nhanVienDTO1.getId());
        assertThat(nhanVienDTO1).isEqualTo(nhanVienDTO2);
        nhanVienDTO2.setId(2L);
        assertThat(nhanVienDTO1).isNotEqualTo(nhanVienDTO2);
        nhanVienDTO1.setId(null);
        assertThat(nhanVienDTO1).isNotEqualTo(nhanVienDTO2);
    }
}
