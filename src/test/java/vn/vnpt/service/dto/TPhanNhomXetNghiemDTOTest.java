package vn.vnpt.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import vn.vnpt.web.rest.TestUtil;

public class TPhanNhomXetNghiemDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TPhanNhomXetNghiemDTO.class);
        TPhanNhomXetNghiemDTO tPhanNhomXetNghiemDTO1 = new TPhanNhomXetNghiemDTO();
        tPhanNhomXetNghiemDTO1.setId(1L);
        TPhanNhomXetNghiemDTO tPhanNhomXetNghiemDTO2 = new TPhanNhomXetNghiemDTO();
        assertThat(tPhanNhomXetNghiemDTO1).isNotEqualTo(tPhanNhomXetNghiemDTO2);
        tPhanNhomXetNghiemDTO2.setId(tPhanNhomXetNghiemDTO1.getId());
        assertThat(tPhanNhomXetNghiemDTO1).isEqualTo(tPhanNhomXetNghiemDTO2);
        tPhanNhomXetNghiemDTO2.setId(2L);
        assertThat(tPhanNhomXetNghiemDTO1).isNotEqualTo(tPhanNhomXetNghiemDTO2);
        tPhanNhomXetNghiemDTO1.setId(null);
        assertThat(tPhanNhomXetNghiemDTO1).isNotEqualTo(tPhanNhomXetNghiemDTO2);
    }
}
