package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.PhieuChiDinhTTPT;
import vn.vnpt.domain.BenhAnKhamBenh;
import vn.vnpt.repository.PhieuChiDinhTTPTRepository;
import vn.vnpt.service.PhieuChiDinhTTPTService;
import vn.vnpt.service.dto.PhieuChiDinhTTPTDTO;
import vn.vnpt.service.mapper.PhieuChiDinhTTPTMapper;
import vn.vnpt.service.dto.PhieuChiDinhTTPTCriteria;
import vn.vnpt.service.PhieuChiDinhTTPTQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PhieuChiDinhTTPTResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class PhieuChiDinhTTPTResourceIT {

    private static final String DEFAULT_CHAN_DOAN_TONG_QUAT = "AAAAAAAAAA";
    private static final String UPDATED_CHAN_DOAN_TONG_QUAT = "BBBBBBBBBB";

    private static final String DEFAULT_GHI_CHU = "AAAAAAAAAA";
    private static final String UPDATED_GHI_CHU = "BBBBBBBBBB";

    private static final String DEFAULT_KET_QUA_TONG_QUAT = "AAAAAAAAAA";
    private static final String UPDATED_KET_QUA_TONG_QUAT = "BBBBBBBBBB";

    private static final Integer DEFAULT_NAM = 1;
    private static final Integer UPDATED_NAM = 2;
    private static final Integer SMALLER_NAM = 1 - 1;

    @Autowired
    private PhieuChiDinhTTPTRepository phieuChiDinhTTPTRepository;

    @Autowired
    private PhieuChiDinhTTPTMapper phieuChiDinhTTPTMapper;

    @Autowired
    private PhieuChiDinhTTPTService phieuChiDinhTTPTService;

    @Autowired
    private PhieuChiDinhTTPTQueryService phieuChiDinhTTPTQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPhieuChiDinhTTPTMockMvc;

    private PhieuChiDinhTTPT phieuChiDinhTTPT;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhieuChiDinhTTPT createEntity(EntityManager em) {
        PhieuChiDinhTTPT phieuChiDinhTTPT = new PhieuChiDinhTTPT()
            .chanDoanTongQuat(DEFAULT_CHAN_DOAN_TONG_QUAT)
            .ghiChu(DEFAULT_GHI_CHU)
            .ketQuaTongQuat(DEFAULT_KET_QUA_TONG_QUAT)
            .nam(DEFAULT_NAM);
        // Add required entity
        BenhAnKhamBenh benhAnKhamBenh;
        if (TestUtil.findAll(em, BenhAnKhamBenh.class).isEmpty()) {
            benhAnKhamBenh = BenhAnKhamBenhResourceIT.createEntity(em);
            em.persist(benhAnKhamBenh);
            em.flush();
        } else {
            benhAnKhamBenh = TestUtil.findAll(em, BenhAnKhamBenh.class).get(0);
        }
        phieuChiDinhTTPT.setDonVi(benhAnKhamBenh);
        // Add required entity
        phieuChiDinhTTPT.setBenhNhan(benhAnKhamBenh);
        // Add required entity
        phieuChiDinhTTPT.setBakb(benhAnKhamBenh);
        return phieuChiDinhTTPT;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhieuChiDinhTTPT createUpdatedEntity(EntityManager em) {
        PhieuChiDinhTTPT phieuChiDinhTTPT = new PhieuChiDinhTTPT()
            .chanDoanTongQuat(UPDATED_CHAN_DOAN_TONG_QUAT)
            .ghiChu(UPDATED_GHI_CHU)
            .ketQuaTongQuat(UPDATED_KET_QUA_TONG_QUAT)
            .nam(UPDATED_NAM);
        // Add required entity
        BenhAnKhamBenh benhAnKhamBenh;
        if (TestUtil.findAll(em, BenhAnKhamBenh.class).isEmpty()) {
            benhAnKhamBenh = BenhAnKhamBenhResourceIT.createUpdatedEntity(em);
            em.persist(benhAnKhamBenh);
            em.flush();
        } else {
            benhAnKhamBenh = TestUtil.findAll(em, BenhAnKhamBenh.class).get(0);
        }
        phieuChiDinhTTPT.setDonVi(benhAnKhamBenh);
        // Add required entity
        phieuChiDinhTTPT.setBenhNhan(benhAnKhamBenh);
        // Add required entity
        phieuChiDinhTTPT.setBakb(benhAnKhamBenh);
        return phieuChiDinhTTPT;
    }

    @BeforeEach
    public void initTest() {
        phieuChiDinhTTPT = createEntity(em);
    }

    @Test
    @Transactional
    public void createPhieuChiDinhTTPT() throws Exception {
        int databaseSizeBeforeCreate = phieuChiDinhTTPTRepository.findAll().size();

        // Create the PhieuChiDinhTTPT
        PhieuChiDinhTTPTDTO phieuChiDinhTTPTDTO = phieuChiDinhTTPTMapper.toDto(phieuChiDinhTTPT);
        restPhieuChiDinhTTPTMockMvc.perform(post("/api/phieu-chi-dinh-ttpts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phieuChiDinhTTPTDTO)))
            .andExpect(status().isCreated());

        // Validate the PhieuChiDinhTTPT in the database
        List<PhieuChiDinhTTPT> phieuChiDinhTTPTList = phieuChiDinhTTPTRepository.findAll();
        assertThat(phieuChiDinhTTPTList).hasSize(databaseSizeBeforeCreate + 1);
        PhieuChiDinhTTPT testPhieuChiDinhTTPT = phieuChiDinhTTPTList.get(phieuChiDinhTTPTList.size() - 1);
        assertThat(testPhieuChiDinhTTPT.getChanDoanTongQuat()).isEqualTo(DEFAULT_CHAN_DOAN_TONG_QUAT);
        assertThat(testPhieuChiDinhTTPT.getGhiChu()).isEqualTo(DEFAULT_GHI_CHU);
        assertThat(testPhieuChiDinhTTPT.getKetQuaTongQuat()).isEqualTo(DEFAULT_KET_QUA_TONG_QUAT);
        assertThat(testPhieuChiDinhTTPT.getNam()).isEqualTo(DEFAULT_NAM);
    }

    @Test
    @Transactional
    public void createPhieuChiDinhTTPTWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = phieuChiDinhTTPTRepository.findAll().size();

        // Create the PhieuChiDinhTTPT with an existing ID
        phieuChiDinhTTPT.setId(1L);
        PhieuChiDinhTTPTDTO phieuChiDinhTTPTDTO = phieuChiDinhTTPTMapper.toDto(phieuChiDinhTTPT);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPhieuChiDinhTTPTMockMvc.perform(post("/api/phieu-chi-dinh-ttpts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phieuChiDinhTTPTDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PhieuChiDinhTTPT in the database
        List<PhieuChiDinhTTPT> phieuChiDinhTTPTList = phieuChiDinhTTPTRepository.findAll();
        assertThat(phieuChiDinhTTPTList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNamIsRequired() throws Exception {
        int databaseSizeBeforeTest = phieuChiDinhTTPTRepository.findAll().size();
        // set the field null
        phieuChiDinhTTPT.setNam(null);

        // Create the PhieuChiDinhTTPT, which fails.
        PhieuChiDinhTTPTDTO phieuChiDinhTTPTDTO = phieuChiDinhTTPTMapper.toDto(phieuChiDinhTTPT);

        restPhieuChiDinhTTPTMockMvc.perform(post("/api/phieu-chi-dinh-ttpts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phieuChiDinhTTPTDTO)))
            .andExpect(status().isBadRequest());

        List<PhieuChiDinhTTPT> phieuChiDinhTTPTList = phieuChiDinhTTPTRepository.findAll();
        assertThat(phieuChiDinhTTPTList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTS() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList
        restPhieuChiDinhTTPTMockMvc.perform(get("/api/phieu-chi-dinh-ttpts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phieuChiDinhTTPT.getId().intValue())))
            .andExpect(jsonPath("$.[*].chanDoanTongQuat").value(hasItem(DEFAULT_CHAN_DOAN_TONG_QUAT)))
            .andExpect(jsonPath("$.[*].ghiChu").value(hasItem(DEFAULT_GHI_CHU)))
            .andExpect(jsonPath("$.[*].ketQuaTongQuat").value(hasItem(DEFAULT_KET_QUA_TONG_QUAT)))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));
    }
    
    @Test
    @Transactional
    public void getPhieuChiDinhTTPT() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get the phieuChiDinhTTPT
        restPhieuChiDinhTTPTMockMvc.perform(get("/api/phieu-chi-dinh-ttpts/{id}", phieuChiDinhTTPT.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(phieuChiDinhTTPT.getId().intValue()))
            .andExpect(jsonPath("$.chanDoanTongQuat").value(DEFAULT_CHAN_DOAN_TONG_QUAT))
            .andExpect(jsonPath("$.ghiChu").value(DEFAULT_GHI_CHU))
            .andExpect(jsonPath("$.ketQuaTongQuat").value(DEFAULT_KET_QUA_TONG_QUAT))
            .andExpect(jsonPath("$.nam").value(DEFAULT_NAM));
    }


    @Test
    @Transactional
    public void getPhieuChiDinhTTPTSByIdFiltering() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        Long id = phieuChiDinhTTPT.getId();

        defaultPhieuChiDinhTTPTShouldBeFound("id.equals=" + id);
        defaultPhieuChiDinhTTPTShouldNotBeFound("id.notEquals=" + id);

        defaultPhieuChiDinhTTPTShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPhieuChiDinhTTPTShouldNotBeFound("id.greaterThan=" + id);

        defaultPhieuChiDinhTTPTShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPhieuChiDinhTTPTShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByChanDoanTongQuatIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where chanDoanTongQuat equals to DEFAULT_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhTTPTShouldBeFound("chanDoanTongQuat.equals=" + DEFAULT_CHAN_DOAN_TONG_QUAT);

        // Get all the phieuChiDinhTTPTList where chanDoanTongQuat equals to UPDATED_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhTTPTShouldNotBeFound("chanDoanTongQuat.equals=" + UPDATED_CHAN_DOAN_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByChanDoanTongQuatIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where chanDoanTongQuat not equals to DEFAULT_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhTTPTShouldNotBeFound("chanDoanTongQuat.notEquals=" + DEFAULT_CHAN_DOAN_TONG_QUAT);

        // Get all the phieuChiDinhTTPTList where chanDoanTongQuat not equals to UPDATED_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhTTPTShouldBeFound("chanDoanTongQuat.notEquals=" + UPDATED_CHAN_DOAN_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByChanDoanTongQuatIsInShouldWork() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where chanDoanTongQuat in DEFAULT_CHAN_DOAN_TONG_QUAT or UPDATED_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhTTPTShouldBeFound("chanDoanTongQuat.in=" + DEFAULT_CHAN_DOAN_TONG_QUAT + "," + UPDATED_CHAN_DOAN_TONG_QUAT);

        // Get all the phieuChiDinhTTPTList where chanDoanTongQuat equals to UPDATED_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhTTPTShouldNotBeFound("chanDoanTongQuat.in=" + UPDATED_CHAN_DOAN_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByChanDoanTongQuatIsNullOrNotNull() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where chanDoanTongQuat is not null
        defaultPhieuChiDinhTTPTShouldBeFound("chanDoanTongQuat.specified=true");

        // Get all the phieuChiDinhTTPTList where chanDoanTongQuat is null
        defaultPhieuChiDinhTTPTShouldNotBeFound("chanDoanTongQuat.specified=false");
    }
                @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByChanDoanTongQuatContainsSomething() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where chanDoanTongQuat contains DEFAULT_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhTTPTShouldBeFound("chanDoanTongQuat.contains=" + DEFAULT_CHAN_DOAN_TONG_QUAT);

        // Get all the phieuChiDinhTTPTList where chanDoanTongQuat contains UPDATED_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhTTPTShouldNotBeFound("chanDoanTongQuat.contains=" + UPDATED_CHAN_DOAN_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByChanDoanTongQuatNotContainsSomething() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where chanDoanTongQuat does not contain DEFAULT_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhTTPTShouldNotBeFound("chanDoanTongQuat.doesNotContain=" + DEFAULT_CHAN_DOAN_TONG_QUAT);

        // Get all the phieuChiDinhTTPTList where chanDoanTongQuat does not contain UPDATED_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhTTPTShouldBeFound("chanDoanTongQuat.doesNotContain=" + UPDATED_CHAN_DOAN_TONG_QUAT);
    }


    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByGhiChuIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where ghiChu equals to DEFAULT_GHI_CHU
        defaultPhieuChiDinhTTPTShouldBeFound("ghiChu.equals=" + DEFAULT_GHI_CHU);

        // Get all the phieuChiDinhTTPTList where ghiChu equals to UPDATED_GHI_CHU
        defaultPhieuChiDinhTTPTShouldNotBeFound("ghiChu.equals=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByGhiChuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where ghiChu not equals to DEFAULT_GHI_CHU
        defaultPhieuChiDinhTTPTShouldNotBeFound("ghiChu.notEquals=" + DEFAULT_GHI_CHU);

        // Get all the phieuChiDinhTTPTList where ghiChu not equals to UPDATED_GHI_CHU
        defaultPhieuChiDinhTTPTShouldBeFound("ghiChu.notEquals=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByGhiChuIsInShouldWork() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where ghiChu in DEFAULT_GHI_CHU or UPDATED_GHI_CHU
        defaultPhieuChiDinhTTPTShouldBeFound("ghiChu.in=" + DEFAULT_GHI_CHU + "," + UPDATED_GHI_CHU);

        // Get all the phieuChiDinhTTPTList where ghiChu equals to UPDATED_GHI_CHU
        defaultPhieuChiDinhTTPTShouldNotBeFound("ghiChu.in=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByGhiChuIsNullOrNotNull() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where ghiChu is not null
        defaultPhieuChiDinhTTPTShouldBeFound("ghiChu.specified=true");

        // Get all the phieuChiDinhTTPTList where ghiChu is null
        defaultPhieuChiDinhTTPTShouldNotBeFound("ghiChu.specified=false");
    }
                @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByGhiChuContainsSomething() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where ghiChu contains DEFAULT_GHI_CHU
        defaultPhieuChiDinhTTPTShouldBeFound("ghiChu.contains=" + DEFAULT_GHI_CHU);

        // Get all the phieuChiDinhTTPTList where ghiChu contains UPDATED_GHI_CHU
        defaultPhieuChiDinhTTPTShouldNotBeFound("ghiChu.contains=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByGhiChuNotContainsSomething() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where ghiChu does not contain DEFAULT_GHI_CHU
        defaultPhieuChiDinhTTPTShouldNotBeFound("ghiChu.doesNotContain=" + DEFAULT_GHI_CHU);

        // Get all the phieuChiDinhTTPTList where ghiChu does not contain UPDATED_GHI_CHU
        defaultPhieuChiDinhTTPTShouldBeFound("ghiChu.doesNotContain=" + UPDATED_GHI_CHU);
    }


    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByKetQuaTongQuatIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where ketQuaTongQuat equals to DEFAULT_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhTTPTShouldBeFound("ketQuaTongQuat.equals=" + DEFAULT_KET_QUA_TONG_QUAT);

        // Get all the phieuChiDinhTTPTList where ketQuaTongQuat equals to UPDATED_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhTTPTShouldNotBeFound("ketQuaTongQuat.equals=" + UPDATED_KET_QUA_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByKetQuaTongQuatIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where ketQuaTongQuat not equals to DEFAULT_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhTTPTShouldNotBeFound("ketQuaTongQuat.notEquals=" + DEFAULT_KET_QUA_TONG_QUAT);

        // Get all the phieuChiDinhTTPTList where ketQuaTongQuat not equals to UPDATED_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhTTPTShouldBeFound("ketQuaTongQuat.notEquals=" + UPDATED_KET_QUA_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByKetQuaTongQuatIsInShouldWork() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where ketQuaTongQuat in DEFAULT_KET_QUA_TONG_QUAT or UPDATED_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhTTPTShouldBeFound("ketQuaTongQuat.in=" + DEFAULT_KET_QUA_TONG_QUAT + "," + UPDATED_KET_QUA_TONG_QUAT);

        // Get all the phieuChiDinhTTPTList where ketQuaTongQuat equals to UPDATED_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhTTPTShouldNotBeFound("ketQuaTongQuat.in=" + UPDATED_KET_QUA_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByKetQuaTongQuatIsNullOrNotNull() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where ketQuaTongQuat is not null
        defaultPhieuChiDinhTTPTShouldBeFound("ketQuaTongQuat.specified=true");

        // Get all the phieuChiDinhTTPTList where ketQuaTongQuat is null
        defaultPhieuChiDinhTTPTShouldNotBeFound("ketQuaTongQuat.specified=false");
    }
                @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByKetQuaTongQuatContainsSomething() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where ketQuaTongQuat contains DEFAULT_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhTTPTShouldBeFound("ketQuaTongQuat.contains=" + DEFAULT_KET_QUA_TONG_QUAT);

        // Get all the phieuChiDinhTTPTList where ketQuaTongQuat contains UPDATED_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhTTPTShouldNotBeFound("ketQuaTongQuat.contains=" + UPDATED_KET_QUA_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByKetQuaTongQuatNotContainsSomething() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where ketQuaTongQuat does not contain DEFAULT_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhTTPTShouldNotBeFound("ketQuaTongQuat.doesNotContain=" + DEFAULT_KET_QUA_TONG_QUAT);

        // Get all the phieuChiDinhTTPTList where ketQuaTongQuat does not contain UPDATED_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhTTPTShouldBeFound("ketQuaTongQuat.doesNotContain=" + UPDATED_KET_QUA_TONG_QUAT);
    }


    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByNamIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where nam equals to DEFAULT_NAM
        defaultPhieuChiDinhTTPTShouldBeFound("nam.equals=" + DEFAULT_NAM);

        // Get all the phieuChiDinhTTPTList where nam equals to UPDATED_NAM
        defaultPhieuChiDinhTTPTShouldNotBeFound("nam.equals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByNamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where nam not equals to DEFAULT_NAM
        defaultPhieuChiDinhTTPTShouldNotBeFound("nam.notEquals=" + DEFAULT_NAM);

        // Get all the phieuChiDinhTTPTList where nam not equals to UPDATED_NAM
        defaultPhieuChiDinhTTPTShouldBeFound("nam.notEquals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByNamIsInShouldWork() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where nam in DEFAULT_NAM or UPDATED_NAM
        defaultPhieuChiDinhTTPTShouldBeFound("nam.in=" + DEFAULT_NAM + "," + UPDATED_NAM);

        // Get all the phieuChiDinhTTPTList where nam equals to UPDATED_NAM
        defaultPhieuChiDinhTTPTShouldNotBeFound("nam.in=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByNamIsNullOrNotNull() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where nam is not null
        defaultPhieuChiDinhTTPTShouldBeFound("nam.specified=true");

        // Get all the phieuChiDinhTTPTList where nam is null
        defaultPhieuChiDinhTTPTShouldNotBeFound("nam.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByNamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where nam is greater than or equal to DEFAULT_NAM
        defaultPhieuChiDinhTTPTShouldBeFound("nam.greaterThanOrEqual=" + DEFAULT_NAM);

        // Get all the phieuChiDinhTTPTList where nam is greater than or equal to UPDATED_NAM
        defaultPhieuChiDinhTTPTShouldNotBeFound("nam.greaterThanOrEqual=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByNamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where nam is less than or equal to DEFAULT_NAM
        defaultPhieuChiDinhTTPTShouldBeFound("nam.lessThanOrEqual=" + DEFAULT_NAM);

        // Get all the phieuChiDinhTTPTList where nam is less than or equal to SMALLER_NAM
        defaultPhieuChiDinhTTPTShouldNotBeFound("nam.lessThanOrEqual=" + SMALLER_NAM);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByNamIsLessThanSomething() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where nam is less than DEFAULT_NAM
        defaultPhieuChiDinhTTPTShouldNotBeFound("nam.lessThan=" + DEFAULT_NAM);

        // Get all the phieuChiDinhTTPTList where nam is less than UPDATED_NAM
        defaultPhieuChiDinhTTPTShouldBeFound("nam.lessThan=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByNamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        // Get all the phieuChiDinhTTPTList where nam is greater than DEFAULT_NAM
        defaultPhieuChiDinhTTPTShouldNotBeFound("nam.greaterThan=" + DEFAULT_NAM);

        // Get all the phieuChiDinhTTPTList where nam is greater than SMALLER_NAM
        defaultPhieuChiDinhTTPTShouldBeFound("nam.greaterThan=" + SMALLER_NAM);
    }


    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        BenhAnKhamBenh donVi = phieuChiDinhTTPT.getDonVi();
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);
        Long donViId = donVi.getId();

        // Get all the phieuChiDinhTTPTList where donVi equals to donViId
        defaultPhieuChiDinhTTPTShouldBeFound("donViId.equals=" + donViId);

        // Get all the phieuChiDinhTTPTList where donVi equals to donViId + 1
        defaultPhieuChiDinhTTPTShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }


    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByBenhNhanIsEqualToSomething() throws Exception {
        // Get already existing entity
        BenhAnKhamBenh benhNhan = phieuChiDinhTTPT.getBenhNhan();
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);
        Long benhNhanId = benhNhan.getId();

        // Get all the phieuChiDinhTTPTList where benhNhan equals to benhNhanId
        defaultPhieuChiDinhTTPTShouldBeFound("benhNhanId.equals=" + benhNhanId);

        // Get all the phieuChiDinhTTPTList where benhNhan equals to benhNhanId + 1
        defaultPhieuChiDinhTTPTShouldNotBeFound("benhNhanId.equals=" + (benhNhanId + 1));
    }


    @Test
    @Transactional
    public void getAllPhieuChiDinhTTPTSByBakbIsEqualToSomething() throws Exception {
        // Get already existing entity
        BenhAnKhamBenh bakb = phieuChiDinhTTPT.getBakb();
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);
        Long bakbId = bakb.getId();

        // Get all the phieuChiDinhTTPTList where bakb equals to bakbId
        defaultPhieuChiDinhTTPTShouldBeFound("bakbId.equals=" + bakbId);

        // Get all the phieuChiDinhTTPTList where bakb equals to bakbId + 1
        defaultPhieuChiDinhTTPTShouldNotBeFound("bakbId.equals=" + (bakbId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPhieuChiDinhTTPTShouldBeFound(String filter) throws Exception {
        restPhieuChiDinhTTPTMockMvc.perform(get("/api/phieu-chi-dinh-ttpts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phieuChiDinhTTPT.getId().intValue())))
            .andExpect(jsonPath("$.[*].chanDoanTongQuat").value(hasItem(DEFAULT_CHAN_DOAN_TONG_QUAT)))
            .andExpect(jsonPath("$.[*].ghiChu").value(hasItem(DEFAULT_GHI_CHU)))
            .andExpect(jsonPath("$.[*].ketQuaTongQuat").value(hasItem(DEFAULT_KET_QUA_TONG_QUAT)))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));

        // Check, that the count call also returns 1
        restPhieuChiDinhTTPTMockMvc.perform(get("/api/phieu-chi-dinh-ttpts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPhieuChiDinhTTPTShouldNotBeFound(String filter) throws Exception {
        restPhieuChiDinhTTPTMockMvc.perform(get("/api/phieu-chi-dinh-ttpts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPhieuChiDinhTTPTMockMvc.perform(get("/api/phieu-chi-dinh-ttpts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingPhieuChiDinhTTPT() throws Exception {
        // Get the phieuChiDinhTTPT
        restPhieuChiDinhTTPTMockMvc.perform(get("/api/phieu-chi-dinh-ttpts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePhieuChiDinhTTPT() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        int databaseSizeBeforeUpdate = phieuChiDinhTTPTRepository.findAll().size();

        // Update the phieuChiDinhTTPT
        PhieuChiDinhTTPT updatedPhieuChiDinhTTPT = phieuChiDinhTTPTRepository.findById(phieuChiDinhTTPT.getId()).get();
        // Disconnect from session so that the updates on updatedPhieuChiDinhTTPT are not directly saved in db
        em.detach(updatedPhieuChiDinhTTPT);
        updatedPhieuChiDinhTTPT
            .chanDoanTongQuat(UPDATED_CHAN_DOAN_TONG_QUAT)
            .ghiChu(UPDATED_GHI_CHU)
            .ketQuaTongQuat(UPDATED_KET_QUA_TONG_QUAT)
            .nam(UPDATED_NAM);
        PhieuChiDinhTTPTDTO phieuChiDinhTTPTDTO = phieuChiDinhTTPTMapper.toDto(updatedPhieuChiDinhTTPT);

        restPhieuChiDinhTTPTMockMvc.perform(put("/api/phieu-chi-dinh-ttpts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phieuChiDinhTTPTDTO)))
            .andExpect(status().isOk());

        // Validate the PhieuChiDinhTTPT in the database
        List<PhieuChiDinhTTPT> phieuChiDinhTTPTList = phieuChiDinhTTPTRepository.findAll();
        assertThat(phieuChiDinhTTPTList).hasSize(databaseSizeBeforeUpdate);
        PhieuChiDinhTTPT testPhieuChiDinhTTPT = phieuChiDinhTTPTList.get(phieuChiDinhTTPTList.size() - 1);
        assertThat(testPhieuChiDinhTTPT.getChanDoanTongQuat()).isEqualTo(UPDATED_CHAN_DOAN_TONG_QUAT);
        assertThat(testPhieuChiDinhTTPT.getGhiChu()).isEqualTo(UPDATED_GHI_CHU);
        assertThat(testPhieuChiDinhTTPT.getKetQuaTongQuat()).isEqualTo(UPDATED_KET_QUA_TONG_QUAT);
        assertThat(testPhieuChiDinhTTPT.getNam()).isEqualTo(UPDATED_NAM);
    }

    @Test
    @Transactional
    public void updateNonExistingPhieuChiDinhTTPT() throws Exception {
        int databaseSizeBeforeUpdate = phieuChiDinhTTPTRepository.findAll().size();

        // Create the PhieuChiDinhTTPT
        PhieuChiDinhTTPTDTO phieuChiDinhTTPTDTO = phieuChiDinhTTPTMapper.toDto(phieuChiDinhTTPT);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPhieuChiDinhTTPTMockMvc.perform(put("/api/phieu-chi-dinh-ttpts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phieuChiDinhTTPTDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PhieuChiDinhTTPT in the database
        List<PhieuChiDinhTTPT> phieuChiDinhTTPTList = phieuChiDinhTTPTRepository.findAll();
        assertThat(phieuChiDinhTTPTList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePhieuChiDinhTTPT() throws Exception {
        // Initialize the database
        phieuChiDinhTTPTRepository.saveAndFlush(phieuChiDinhTTPT);

        int databaseSizeBeforeDelete = phieuChiDinhTTPTRepository.findAll().size();

        // Delete the phieuChiDinhTTPT
        restPhieuChiDinhTTPTMockMvc.perform(delete("/api/phieu-chi-dinh-ttpts/{id}", phieuChiDinhTTPT.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PhieuChiDinhTTPT> phieuChiDinhTTPTList = phieuChiDinhTTPTRepository.findAll();
        assertThat(phieuChiDinhTTPTList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
