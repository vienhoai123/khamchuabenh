package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.TinhThanhPho;
import vn.vnpt.repository.TinhThanhPhoRepository;
import vn.vnpt.service.TinhThanhPhoService;
import vn.vnpt.service.dto.TinhThanhPhoDTO;
import vn.vnpt.service.mapper.TinhThanhPhoMapper;
import vn.vnpt.service.dto.TinhThanhPhoCriteria;
import vn.vnpt.service.TinhThanhPhoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TinhThanhPhoResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class TinhThanhPhoResourceIT {

    private static final String DEFAULT_CAP = "AAAAAAAAAA";
    private static final String UPDATED_CAP = "BBBBBBBBBB";

    private static final String DEFAULT_GUESS_PHRASE = "AAAAAAAAAA";
    private static final String UPDATED_GUESS_PHRASE = "BBBBBBBBBB";

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String DEFAULT_TEN_KHONG_DAU = "AAAAAAAAAA";
    private static final String UPDATED_TEN_KHONG_DAU = "BBBBBBBBBB";

    @Autowired
    private TinhThanhPhoRepository tinhThanhPhoRepository;

    @Autowired
    private TinhThanhPhoMapper tinhThanhPhoMapper;

    @Autowired
    private TinhThanhPhoService tinhThanhPhoService;

    @Autowired
    private TinhThanhPhoQueryService tinhThanhPhoQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTinhThanhPhoMockMvc;

    private TinhThanhPho tinhThanhPho;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TinhThanhPho createEntity(EntityManager em) {
        TinhThanhPho tinhThanhPho = new TinhThanhPho()
            .cap(DEFAULT_CAP)
            .guessPhrase(DEFAULT_GUESS_PHRASE)
            .ten(DEFAULT_TEN)
            .tenKhongDau(DEFAULT_TEN_KHONG_DAU);
        return tinhThanhPho;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TinhThanhPho createUpdatedEntity(EntityManager em) {
        TinhThanhPho tinhThanhPho = new TinhThanhPho()
            .cap(UPDATED_CAP)
            .guessPhrase(UPDATED_GUESS_PHRASE)
            .ten(UPDATED_TEN)
            .tenKhongDau(UPDATED_TEN_KHONG_DAU);
        return tinhThanhPho;
    }

    @BeforeEach
    public void initTest() {
        tinhThanhPho = createEntity(em);
    }

    @Test
    @Transactional
    public void createTinhThanhPho() throws Exception {
        int databaseSizeBeforeCreate = tinhThanhPhoRepository.findAll().size();

        // Create the TinhThanhPho
        TinhThanhPhoDTO tinhThanhPhoDTO = tinhThanhPhoMapper.toDto(tinhThanhPho);
        restTinhThanhPhoMockMvc.perform(post("/api/tinh-thanh-phos").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tinhThanhPhoDTO)))
            .andExpect(status().isCreated());

        // Validate the TinhThanhPho in the database
        List<TinhThanhPho> tinhThanhPhoList = tinhThanhPhoRepository.findAll();
        assertThat(tinhThanhPhoList).hasSize(databaseSizeBeforeCreate + 1);
        TinhThanhPho testTinhThanhPho = tinhThanhPhoList.get(tinhThanhPhoList.size() - 1);
        assertThat(testTinhThanhPho.getCap()).isEqualTo(DEFAULT_CAP);
        assertThat(testTinhThanhPho.getGuessPhrase()).isEqualTo(DEFAULT_GUESS_PHRASE);
        assertThat(testTinhThanhPho.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testTinhThanhPho.getTenKhongDau()).isEqualTo(DEFAULT_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void createTinhThanhPhoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tinhThanhPhoRepository.findAll().size();

        // Create the TinhThanhPho with an existing ID
        tinhThanhPho.setId(1L);
        TinhThanhPhoDTO tinhThanhPhoDTO = tinhThanhPhoMapper.toDto(tinhThanhPho);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTinhThanhPhoMockMvc.perform(post("/api/tinh-thanh-phos").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tinhThanhPhoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TinhThanhPho in the database
        List<TinhThanhPho> tinhThanhPhoList = tinhThanhPhoRepository.findAll();
        assertThat(tinhThanhPhoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCapIsRequired() throws Exception {
        int databaseSizeBeforeTest = tinhThanhPhoRepository.findAll().size();
        // set the field null
        tinhThanhPho.setCap(null);

        // Create the TinhThanhPho, which fails.
        TinhThanhPhoDTO tinhThanhPhoDTO = tinhThanhPhoMapper.toDto(tinhThanhPho);

        restTinhThanhPhoMockMvc.perform(post("/api/tinh-thanh-phos").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tinhThanhPhoDTO)))
            .andExpect(status().isBadRequest());

        List<TinhThanhPho> tinhThanhPhoList = tinhThanhPhoRepository.findAll();
        assertThat(tinhThanhPhoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = tinhThanhPhoRepository.findAll().size();
        // set the field null
        tinhThanhPho.setTen(null);

        // Create the TinhThanhPho, which fails.
        TinhThanhPhoDTO tinhThanhPhoDTO = tinhThanhPhoMapper.toDto(tinhThanhPho);

        restTinhThanhPhoMockMvc.perform(post("/api/tinh-thanh-phos").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tinhThanhPhoDTO)))
            .andExpect(status().isBadRequest());

        List<TinhThanhPho> tinhThanhPhoList = tinhThanhPhoRepository.findAll();
        assertThat(tinhThanhPhoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTenKhongDauIsRequired() throws Exception {
        int databaseSizeBeforeTest = tinhThanhPhoRepository.findAll().size();
        // set the field null
        tinhThanhPho.setTenKhongDau(null);

        // Create the TinhThanhPho, which fails.
        TinhThanhPhoDTO tinhThanhPhoDTO = tinhThanhPhoMapper.toDto(tinhThanhPho);

        restTinhThanhPhoMockMvc.perform(post("/api/tinh-thanh-phos").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tinhThanhPhoDTO)))
            .andExpect(status().isBadRequest());

        List<TinhThanhPho> tinhThanhPhoList = tinhThanhPhoRepository.findAll();
        assertThat(tinhThanhPhoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTinhThanhPhos() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList
        restTinhThanhPhoMockMvc.perform(get("/api/tinh-thanh-phos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tinhThanhPho.getId().intValue())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].guessPhrase").value(hasItem(DEFAULT_GUESS_PHRASE)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].tenKhongDau").value(hasItem(DEFAULT_TEN_KHONG_DAU)));
    }
    
    @Test
    @Transactional
    public void getTinhThanhPho() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get the tinhThanhPho
        restTinhThanhPhoMockMvc.perform(get("/api/tinh-thanh-phos/{id}", tinhThanhPho.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(tinhThanhPho.getId().intValue()))
            .andExpect(jsonPath("$.cap").value(DEFAULT_CAP))
            .andExpect(jsonPath("$.guessPhrase").value(DEFAULT_GUESS_PHRASE))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.tenKhongDau").value(DEFAULT_TEN_KHONG_DAU));
    }


    @Test
    @Transactional
    public void getTinhThanhPhosByIdFiltering() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        Long id = tinhThanhPho.getId();

        defaultTinhThanhPhoShouldBeFound("id.equals=" + id);
        defaultTinhThanhPhoShouldNotBeFound("id.notEquals=" + id);

        defaultTinhThanhPhoShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTinhThanhPhoShouldNotBeFound("id.greaterThan=" + id);

        defaultTinhThanhPhoShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTinhThanhPhoShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllTinhThanhPhosByCapIsEqualToSomething() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where cap equals to DEFAULT_CAP
        defaultTinhThanhPhoShouldBeFound("cap.equals=" + DEFAULT_CAP);

        // Get all the tinhThanhPhoList where cap equals to UPDATED_CAP
        defaultTinhThanhPhoShouldNotBeFound("cap.equals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllTinhThanhPhosByCapIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where cap not equals to DEFAULT_CAP
        defaultTinhThanhPhoShouldNotBeFound("cap.notEquals=" + DEFAULT_CAP);

        // Get all the tinhThanhPhoList where cap not equals to UPDATED_CAP
        defaultTinhThanhPhoShouldBeFound("cap.notEquals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllTinhThanhPhosByCapIsInShouldWork() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where cap in DEFAULT_CAP or UPDATED_CAP
        defaultTinhThanhPhoShouldBeFound("cap.in=" + DEFAULT_CAP + "," + UPDATED_CAP);

        // Get all the tinhThanhPhoList where cap equals to UPDATED_CAP
        defaultTinhThanhPhoShouldNotBeFound("cap.in=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllTinhThanhPhosByCapIsNullOrNotNull() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where cap is not null
        defaultTinhThanhPhoShouldBeFound("cap.specified=true");

        // Get all the tinhThanhPhoList where cap is null
        defaultTinhThanhPhoShouldNotBeFound("cap.specified=false");
    }
                @Test
    @Transactional
    public void getAllTinhThanhPhosByCapContainsSomething() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where cap contains DEFAULT_CAP
        defaultTinhThanhPhoShouldBeFound("cap.contains=" + DEFAULT_CAP);

        // Get all the tinhThanhPhoList where cap contains UPDATED_CAP
        defaultTinhThanhPhoShouldNotBeFound("cap.contains=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllTinhThanhPhosByCapNotContainsSomething() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where cap does not contain DEFAULT_CAP
        defaultTinhThanhPhoShouldNotBeFound("cap.doesNotContain=" + DEFAULT_CAP);

        // Get all the tinhThanhPhoList where cap does not contain UPDATED_CAP
        defaultTinhThanhPhoShouldBeFound("cap.doesNotContain=" + UPDATED_CAP);
    }


    @Test
    @Transactional
    public void getAllTinhThanhPhosByGuessPhraseIsEqualToSomething() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where guessPhrase equals to DEFAULT_GUESS_PHRASE
        defaultTinhThanhPhoShouldBeFound("guessPhrase.equals=" + DEFAULT_GUESS_PHRASE);

        // Get all the tinhThanhPhoList where guessPhrase equals to UPDATED_GUESS_PHRASE
        defaultTinhThanhPhoShouldNotBeFound("guessPhrase.equals=" + UPDATED_GUESS_PHRASE);
    }

    @Test
    @Transactional
    public void getAllTinhThanhPhosByGuessPhraseIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where guessPhrase not equals to DEFAULT_GUESS_PHRASE
        defaultTinhThanhPhoShouldNotBeFound("guessPhrase.notEquals=" + DEFAULT_GUESS_PHRASE);

        // Get all the tinhThanhPhoList where guessPhrase not equals to UPDATED_GUESS_PHRASE
        defaultTinhThanhPhoShouldBeFound("guessPhrase.notEquals=" + UPDATED_GUESS_PHRASE);
    }

    @Test
    @Transactional
    public void getAllTinhThanhPhosByGuessPhraseIsInShouldWork() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where guessPhrase in DEFAULT_GUESS_PHRASE or UPDATED_GUESS_PHRASE
        defaultTinhThanhPhoShouldBeFound("guessPhrase.in=" + DEFAULT_GUESS_PHRASE + "," + UPDATED_GUESS_PHRASE);

        // Get all the tinhThanhPhoList where guessPhrase equals to UPDATED_GUESS_PHRASE
        defaultTinhThanhPhoShouldNotBeFound("guessPhrase.in=" + UPDATED_GUESS_PHRASE);
    }

    @Test
    @Transactional
    public void getAllTinhThanhPhosByGuessPhraseIsNullOrNotNull() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where guessPhrase is not null
        defaultTinhThanhPhoShouldBeFound("guessPhrase.specified=true");

        // Get all the tinhThanhPhoList where guessPhrase is null
        defaultTinhThanhPhoShouldNotBeFound("guessPhrase.specified=false");
    }
                @Test
    @Transactional
    public void getAllTinhThanhPhosByGuessPhraseContainsSomething() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where guessPhrase contains DEFAULT_GUESS_PHRASE
        defaultTinhThanhPhoShouldBeFound("guessPhrase.contains=" + DEFAULT_GUESS_PHRASE);

        // Get all the tinhThanhPhoList where guessPhrase contains UPDATED_GUESS_PHRASE
        defaultTinhThanhPhoShouldNotBeFound("guessPhrase.contains=" + UPDATED_GUESS_PHRASE);
    }

    @Test
    @Transactional
    public void getAllTinhThanhPhosByGuessPhraseNotContainsSomething() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where guessPhrase does not contain DEFAULT_GUESS_PHRASE
        defaultTinhThanhPhoShouldNotBeFound("guessPhrase.doesNotContain=" + DEFAULT_GUESS_PHRASE);

        // Get all the tinhThanhPhoList where guessPhrase does not contain UPDATED_GUESS_PHRASE
        defaultTinhThanhPhoShouldBeFound("guessPhrase.doesNotContain=" + UPDATED_GUESS_PHRASE);
    }


    @Test
    @Transactional
    public void getAllTinhThanhPhosByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where ten equals to DEFAULT_TEN
        defaultTinhThanhPhoShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the tinhThanhPhoList where ten equals to UPDATED_TEN
        defaultTinhThanhPhoShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllTinhThanhPhosByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where ten not equals to DEFAULT_TEN
        defaultTinhThanhPhoShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the tinhThanhPhoList where ten not equals to UPDATED_TEN
        defaultTinhThanhPhoShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllTinhThanhPhosByTenIsInShouldWork() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultTinhThanhPhoShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the tinhThanhPhoList where ten equals to UPDATED_TEN
        defaultTinhThanhPhoShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllTinhThanhPhosByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where ten is not null
        defaultTinhThanhPhoShouldBeFound("ten.specified=true");

        // Get all the tinhThanhPhoList where ten is null
        defaultTinhThanhPhoShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllTinhThanhPhosByTenContainsSomething() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where ten contains DEFAULT_TEN
        defaultTinhThanhPhoShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the tinhThanhPhoList where ten contains UPDATED_TEN
        defaultTinhThanhPhoShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllTinhThanhPhosByTenNotContainsSomething() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where ten does not contain DEFAULT_TEN
        defaultTinhThanhPhoShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the tinhThanhPhoList where ten does not contain UPDATED_TEN
        defaultTinhThanhPhoShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllTinhThanhPhosByTenKhongDauIsEqualToSomething() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where tenKhongDau equals to DEFAULT_TEN_KHONG_DAU
        defaultTinhThanhPhoShouldBeFound("tenKhongDau.equals=" + DEFAULT_TEN_KHONG_DAU);

        // Get all the tinhThanhPhoList where tenKhongDau equals to UPDATED_TEN_KHONG_DAU
        defaultTinhThanhPhoShouldNotBeFound("tenKhongDau.equals=" + UPDATED_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void getAllTinhThanhPhosByTenKhongDauIsNotEqualToSomething() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where tenKhongDau not equals to DEFAULT_TEN_KHONG_DAU
        defaultTinhThanhPhoShouldNotBeFound("tenKhongDau.notEquals=" + DEFAULT_TEN_KHONG_DAU);

        // Get all the tinhThanhPhoList where tenKhongDau not equals to UPDATED_TEN_KHONG_DAU
        defaultTinhThanhPhoShouldBeFound("tenKhongDau.notEquals=" + UPDATED_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void getAllTinhThanhPhosByTenKhongDauIsInShouldWork() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where tenKhongDau in DEFAULT_TEN_KHONG_DAU or UPDATED_TEN_KHONG_DAU
        defaultTinhThanhPhoShouldBeFound("tenKhongDau.in=" + DEFAULT_TEN_KHONG_DAU + "," + UPDATED_TEN_KHONG_DAU);

        // Get all the tinhThanhPhoList where tenKhongDau equals to UPDATED_TEN_KHONG_DAU
        defaultTinhThanhPhoShouldNotBeFound("tenKhongDau.in=" + UPDATED_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void getAllTinhThanhPhosByTenKhongDauIsNullOrNotNull() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where tenKhongDau is not null
        defaultTinhThanhPhoShouldBeFound("tenKhongDau.specified=true");

        // Get all the tinhThanhPhoList where tenKhongDau is null
        defaultTinhThanhPhoShouldNotBeFound("tenKhongDau.specified=false");
    }
                @Test
    @Transactional
    public void getAllTinhThanhPhosByTenKhongDauContainsSomething() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where tenKhongDau contains DEFAULT_TEN_KHONG_DAU
        defaultTinhThanhPhoShouldBeFound("tenKhongDau.contains=" + DEFAULT_TEN_KHONG_DAU);

        // Get all the tinhThanhPhoList where tenKhongDau contains UPDATED_TEN_KHONG_DAU
        defaultTinhThanhPhoShouldNotBeFound("tenKhongDau.contains=" + UPDATED_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void getAllTinhThanhPhosByTenKhongDauNotContainsSomething() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        // Get all the tinhThanhPhoList where tenKhongDau does not contain DEFAULT_TEN_KHONG_DAU
        defaultTinhThanhPhoShouldNotBeFound("tenKhongDau.doesNotContain=" + DEFAULT_TEN_KHONG_DAU);

        // Get all the tinhThanhPhoList where tenKhongDau does not contain UPDATED_TEN_KHONG_DAU
        defaultTinhThanhPhoShouldBeFound("tenKhongDau.doesNotContain=" + UPDATED_TEN_KHONG_DAU);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTinhThanhPhoShouldBeFound(String filter) throws Exception {
        restTinhThanhPhoMockMvc.perform(get("/api/tinh-thanh-phos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tinhThanhPho.getId().intValue())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].guessPhrase").value(hasItem(DEFAULT_GUESS_PHRASE)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].tenKhongDau").value(hasItem(DEFAULT_TEN_KHONG_DAU)));

        // Check, that the count call also returns 1
        restTinhThanhPhoMockMvc.perform(get("/api/tinh-thanh-phos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTinhThanhPhoShouldNotBeFound(String filter) throws Exception {
        restTinhThanhPhoMockMvc.perform(get("/api/tinh-thanh-phos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTinhThanhPhoMockMvc.perform(get("/api/tinh-thanh-phos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTinhThanhPho() throws Exception {
        // Get the tinhThanhPho
        restTinhThanhPhoMockMvc.perform(get("/api/tinh-thanh-phos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTinhThanhPho() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        int databaseSizeBeforeUpdate = tinhThanhPhoRepository.findAll().size();

        // Update the tinhThanhPho
        TinhThanhPho updatedTinhThanhPho = tinhThanhPhoRepository.findById(tinhThanhPho.getId()).get();
        // Disconnect from session so that the updates on updatedTinhThanhPho are not directly saved in db
        em.detach(updatedTinhThanhPho);
        updatedTinhThanhPho
            .cap(UPDATED_CAP)
            .guessPhrase(UPDATED_GUESS_PHRASE)
            .ten(UPDATED_TEN)
            .tenKhongDau(UPDATED_TEN_KHONG_DAU);
        TinhThanhPhoDTO tinhThanhPhoDTO = tinhThanhPhoMapper.toDto(updatedTinhThanhPho);

        restTinhThanhPhoMockMvc.perform(put("/api/tinh-thanh-phos").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tinhThanhPhoDTO)))
            .andExpect(status().isOk());

        // Validate the TinhThanhPho in the database
        List<TinhThanhPho> tinhThanhPhoList = tinhThanhPhoRepository.findAll();
        assertThat(tinhThanhPhoList).hasSize(databaseSizeBeforeUpdate);
        TinhThanhPho testTinhThanhPho = tinhThanhPhoList.get(tinhThanhPhoList.size() - 1);
        assertThat(testTinhThanhPho.getCap()).isEqualTo(UPDATED_CAP);
        assertThat(testTinhThanhPho.getGuessPhrase()).isEqualTo(UPDATED_GUESS_PHRASE);
        assertThat(testTinhThanhPho.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testTinhThanhPho.getTenKhongDau()).isEqualTo(UPDATED_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void updateNonExistingTinhThanhPho() throws Exception {
        int databaseSizeBeforeUpdate = tinhThanhPhoRepository.findAll().size();

        // Create the TinhThanhPho
        TinhThanhPhoDTO tinhThanhPhoDTO = tinhThanhPhoMapper.toDto(tinhThanhPho);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTinhThanhPhoMockMvc.perform(put("/api/tinh-thanh-phos").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tinhThanhPhoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TinhThanhPho in the database
        List<TinhThanhPho> tinhThanhPhoList = tinhThanhPhoRepository.findAll();
        assertThat(tinhThanhPhoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTinhThanhPho() throws Exception {
        // Initialize the database
        tinhThanhPhoRepository.saveAndFlush(tinhThanhPho);

        int databaseSizeBeforeDelete = tinhThanhPhoRepository.findAll().size();

        // Delete the tinhThanhPho
        restTinhThanhPhoMockMvc.perform(delete("/api/tinh-thanh-phos/{id}", tinhThanhPho.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TinhThanhPho> tinhThanhPhoList = tinhThanhPhoRepository.findAll();
        assertThat(tinhThanhPhoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
