package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.Phong;
import vn.vnpt.domain.Khoa;
import vn.vnpt.repository.PhongRepository;
import vn.vnpt.service.PhongService;
import vn.vnpt.service.dto.PhongDTO;
import vn.vnpt.service.mapper.PhongMapper;
import vn.vnpt.service.dto.PhongCriteria;
import vn.vnpt.service.PhongQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PhongResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class PhongResourceIT {

    private static final Boolean DEFAULT_ENABLED = false;
    private static final Boolean UPDATED_ENABLED = true;

    private static final String DEFAULT_KY_HIEU = "AAAAAAAAAA";
    private static final String UPDATED_KY_HIEU = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_LOAI = new BigDecimal(1);
    private static final BigDecimal UPDATED_LOAI = new BigDecimal(2);
    private static final BigDecimal SMALLER_LOAI = new BigDecimal(1 - 1);

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String DEFAULT_VI_TRI = "AAAAAAAAAA";
    private static final String UPDATED_VI_TRI = "BBBBBBBBBB";

    @Autowired
    private PhongRepository phongRepository;

    @Autowired
    private PhongMapper phongMapper;

    @Autowired
    private PhongService phongService;

    @Autowired
    private PhongQueryService phongQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPhongMockMvc;

    private Phong phong;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Phong createEntity(EntityManager em) {
        Phong phong = new Phong()
            .enabled(DEFAULT_ENABLED)
            .kyHieu(DEFAULT_KY_HIEU)
            .loai(DEFAULT_LOAI)
            .phone(DEFAULT_PHONE)
            .ten(DEFAULT_TEN)
            .viTri(DEFAULT_VI_TRI);
        // Add required entity
        Khoa khoa;
        if (TestUtil.findAll(em, Khoa.class).isEmpty()) {
            khoa = KhoaResourceIT.createEntity(em);
            em.persist(khoa);
            em.flush();
        } else {
            khoa = TestUtil.findAll(em, Khoa.class).get(0);
        }
        phong.setKhoa(khoa);
        return phong;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Phong createUpdatedEntity(EntityManager em) {
        Phong phong = new Phong()
            .enabled(UPDATED_ENABLED)
            .kyHieu(UPDATED_KY_HIEU)
            .loai(UPDATED_LOAI)
            .phone(UPDATED_PHONE)
            .ten(UPDATED_TEN)
            .viTri(UPDATED_VI_TRI);
        // Add required entity
        Khoa khoa;
        if (TestUtil.findAll(em, Khoa.class).isEmpty()) {
            khoa = KhoaResourceIT.createUpdatedEntity(em);
            em.persist(khoa);
            em.flush();
        } else {
            khoa = TestUtil.findAll(em, Khoa.class).get(0);
        }
        phong.setKhoa(khoa);
        return phong;
    }

    @BeforeEach
    public void initTest() {
        phong = createEntity(em);
    }

    @Test
    @Transactional
    public void createPhong() throws Exception {
        int databaseSizeBeforeCreate = phongRepository.findAll().size();

        // Create the Phong
        PhongDTO phongDTO = phongMapper.toDto(phong);
        restPhongMockMvc.perform(post("/api/phongs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phongDTO)))
            .andExpect(status().isCreated());

        // Validate the Phong in the database
        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeCreate + 1);
        Phong testPhong = phongList.get(phongList.size() - 1);
        assertThat(testPhong.isEnabled()).isEqualTo(DEFAULT_ENABLED);
        assertThat(testPhong.getKyHieu()).isEqualTo(DEFAULT_KY_HIEU);
        assertThat(testPhong.getLoai()).isEqualTo(DEFAULT_LOAI);
        assertThat(testPhong.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testPhong.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testPhong.getViTri()).isEqualTo(DEFAULT_VI_TRI);
    }

    @Test
    @Transactional
    public void createPhongWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = phongRepository.findAll().size();

        // Create the Phong with an existing ID
        phong.setId(1L);
        PhongDTO phongDTO = phongMapper.toDto(phong);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPhongMockMvc.perform(post("/api/phongs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phongDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Phong in the database
        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkEnabledIsRequired() throws Exception {
        int databaseSizeBeforeTest = phongRepository.findAll().size();
        // set the field null
        phong.setEnabled(null);

        // Create the Phong, which fails.
        PhongDTO phongDTO = phongMapper.toDto(phong);

        restPhongMockMvc.perform(post("/api/phongs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phongDTO)))
            .andExpect(status().isBadRequest());

        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkKyHieuIsRequired() throws Exception {
        int databaseSizeBeforeTest = phongRepository.findAll().size();
        // set the field null
        phong.setKyHieu(null);

        // Create the Phong, which fails.
        PhongDTO phongDTO = phongMapper.toDto(phong);

        restPhongMockMvc.perform(post("/api/phongs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phongDTO)))
            .andExpect(status().isBadRequest());

        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLoaiIsRequired() throws Exception {
        int databaseSizeBeforeTest = phongRepository.findAll().size();
        // set the field null
        phong.setLoai(null);

        // Create the Phong, which fails.
        PhongDTO phongDTO = phongMapper.toDto(phong);

        restPhongMockMvc.perform(post("/api/phongs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phongDTO)))
            .andExpect(status().isBadRequest());

        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = phongRepository.findAll().size();
        // set the field null
        phong.setTen(null);

        // Create the Phong, which fails.
        PhongDTO phongDTO = phongMapper.toDto(phong);

        restPhongMockMvc.perform(post("/api/phongs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phongDTO)))
            .andExpect(status().isBadRequest());

        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPhongs() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList
        restPhongMockMvc.perform(get("/api/phongs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phong.getId().intValue())))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].kyHieu").value(hasItem(DEFAULT_KY_HIEU)))
            .andExpect(jsonPath("$.[*].loai").value(hasItem(DEFAULT_LOAI.intValue())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].viTri").value(hasItem(DEFAULT_VI_TRI)));
    }
    
    @Test
    @Transactional
    public void getPhong() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get the phong
        restPhongMockMvc.perform(get("/api/phongs/{id}", phong.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(phong.getId().intValue()))
            .andExpect(jsonPath("$.enabled").value(DEFAULT_ENABLED.booleanValue()))
            .andExpect(jsonPath("$.kyHieu").value(DEFAULT_KY_HIEU))
            .andExpect(jsonPath("$.loai").value(DEFAULT_LOAI.intValue()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.viTri").value(DEFAULT_VI_TRI));
    }


    @Test
    @Transactional
    public void getPhongsByIdFiltering() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        Long id = phong.getId();

        defaultPhongShouldBeFound("id.equals=" + id);
        defaultPhongShouldNotBeFound("id.notEquals=" + id);

        defaultPhongShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPhongShouldNotBeFound("id.greaterThan=" + id);

        defaultPhongShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPhongShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllPhongsByEnabledIsEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where enabled equals to DEFAULT_ENABLED
        defaultPhongShouldBeFound("enabled.equals=" + DEFAULT_ENABLED);

        // Get all the phongList where enabled equals to UPDATED_ENABLED
        defaultPhongShouldNotBeFound("enabled.equals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllPhongsByEnabledIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where enabled not equals to DEFAULT_ENABLED
        defaultPhongShouldNotBeFound("enabled.notEquals=" + DEFAULT_ENABLED);

        // Get all the phongList where enabled not equals to UPDATED_ENABLED
        defaultPhongShouldBeFound("enabled.notEquals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllPhongsByEnabledIsInShouldWork() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where enabled in DEFAULT_ENABLED or UPDATED_ENABLED
        defaultPhongShouldBeFound("enabled.in=" + DEFAULT_ENABLED + "," + UPDATED_ENABLED);

        // Get all the phongList where enabled equals to UPDATED_ENABLED
        defaultPhongShouldNotBeFound("enabled.in=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllPhongsByEnabledIsNullOrNotNull() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where enabled is not null
        defaultPhongShouldBeFound("enabled.specified=true");

        // Get all the phongList where enabled is null
        defaultPhongShouldNotBeFound("enabled.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhongsByKyHieuIsEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where kyHieu equals to DEFAULT_KY_HIEU
        defaultPhongShouldBeFound("kyHieu.equals=" + DEFAULT_KY_HIEU);

        // Get all the phongList where kyHieu equals to UPDATED_KY_HIEU
        defaultPhongShouldNotBeFound("kyHieu.equals=" + UPDATED_KY_HIEU);
    }

    @Test
    @Transactional
    public void getAllPhongsByKyHieuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where kyHieu not equals to DEFAULT_KY_HIEU
        defaultPhongShouldNotBeFound("kyHieu.notEquals=" + DEFAULT_KY_HIEU);

        // Get all the phongList where kyHieu not equals to UPDATED_KY_HIEU
        defaultPhongShouldBeFound("kyHieu.notEquals=" + UPDATED_KY_HIEU);
    }

    @Test
    @Transactional
    public void getAllPhongsByKyHieuIsInShouldWork() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where kyHieu in DEFAULT_KY_HIEU or UPDATED_KY_HIEU
        defaultPhongShouldBeFound("kyHieu.in=" + DEFAULT_KY_HIEU + "," + UPDATED_KY_HIEU);

        // Get all the phongList where kyHieu equals to UPDATED_KY_HIEU
        defaultPhongShouldNotBeFound("kyHieu.in=" + UPDATED_KY_HIEU);
    }

    @Test
    @Transactional
    public void getAllPhongsByKyHieuIsNullOrNotNull() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where kyHieu is not null
        defaultPhongShouldBeFound("kyHieu.specified=true");

        // Get all the phongList where kyHieu is null
        defaultPhongShouldNotBeFound("kyHieu.specified=false");
    }
                @Test
    @Transactional
    public void getAllPhongsByKyHieuContainsSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where kyHieu contains DEFAULT_KY_HIEU
        defaultPhongShouldBeFound("kyHieu.contains=" + DEFAULT_KY_HIEU);

        // Get all the phongList where kyHieu contains UPDATED_KY_HIEU
        defaultPhongShouldNotBeFound("kyHieu.contains=" + UPDATED_KY_HIEU);
    }

    @Test
    @Transactional
    public void getAllPhongsByKyHieuNotContainsSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where kyHieu does not contain DEFAULT_KY_HIEU
        defaultPhongShouldNotBeFound("kyHieu.doesNotContain=" + DEFAULT_KY_HIEU);

        // Get all the phongList where kyHieu does not contain UPDATED_KY_HIEU
        defaultPhongShouldBeFound("kyHieu.doesNotContain=" + UPDATED_KY_HIEU);
    }


    @Test
    @Transactional
    public void getAllPhongsByLoaiIsEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where loai equals to DEFAULT_LOAI
        defaultPhongShouldBeFound("loai.equals=" + DEFAULT_LOAI);

        // Get all the phongList where loai equals to UPDATED_LOAI
        defaultPhongShouldNotBeFound("loai.equals=" + UPDATED_LOAI);
    }

    @Test
    @Transactional
    public void getAllPhongsByLoaiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where loai not equals to DEFAULT_LOAI
        defaultPhongShouldNotBeFound("loai.notEquals=" + DEFAULT_LOAI);

        // Get all the phongList where loai not equals to UPDATED_LOAI
        defaultPhongShouldBeFound("loai.notEquals=" + UPDATED_LOAI);
    }

    @Test
    @Transactional
    public void getAllPhongsByLoaiIsInShouldWork() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where loai in DEFAULT_LOAI or UPDATED_LOAI
        defaultPhongShouldBeFound("loai.in=" + DEFAULT_LOAI + "," + UPDATED_LOAI);

        // Get all the phongList where loai equals to UPDATED_LOAI
        defaultPhongShouldNotBeFound("loai.in=" + UPDATED_LOAI);
    }

    @Test
    @Transactional
    public void getAllPhongsByLoaiIsNullOrNotNull() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where loai is not null
        defaultPhongShouldBeFound("loai.specified=true");

        // Get all the phongList where loai is null
        defaultPhongShouldNotBeFound("loai.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhongsByLoaiIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where loai is greater than or equal to DEFAULT_LOAI
        defaultPhongShouldBeFound("loai.greaterThanOrEqual=" + DEFAULT_LOAI);

        // Get all the phongList where loai is greater than or equal to UPDATED_LOAI
        defaultPhongShouldNotBeFound("loai.greaterThanOrEqual=" + UPDATED_LOAI);
    }

    @Test
    @Transactional
    public void getAllPhongsByLoaiIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where loai is less than or equal to DEFAULT_LOAI
        defaultPhongShouldBeFound("loai.lessThanOrEqual=" + DEFAULT_LOAI);

        // Get all the phongList where loai is less than or equal to SMALLER_LOAI
        defaultPhongShouldNotBeFound("loai.lessThanOrEqual=" + SMALLER_LOAI);
    }

    @Test
    @Transactional
    public void getAllPhongsByLoaiIsLessThanSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where loai is less than DEFAULT_LOAI
        defaultPhongShouldNotBeFound("loai.lessThan=" + DEFAULT_LOAI);

        // Get all the phongList where loai is less than UPDATED_LOAI
        defaultPhongShouldBeFound("loai.lessThan=" + UPDATED_LOAI);
    }

    @Test
    @Transactional
    public void getAllPhongsByLoaiIsGreaterThanSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where loai is greater than DEFAULT_LOAI
        defaultPhongShouldNotBeFound("loai.greaterThan=" + DEFAULT_LOAI);

        // Get all the phongList where loai is greater than SMALLER_LOAI
        defaultPhongShouldBeFound("loai.greaterThan=" + SMALLER_LOAI);
    }


    @Test
    @Transactional
    public void getAllPhongsByPhoneIsEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where phone equals to DEFAULT_PHONE
        defaultPhongShouldBeFound("phone.equals=" + DEFAULT_PHONE);

        // Get all the phongList where phone equals to UPDATED_PHONE
        defaultPhongShouldNotBeFound("phone.equals=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllPhongsByPhoneIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where phone not equals to DEFAULT_PHONE
        defaultPhongShouldNotBeFound("phone.notEquals=" + DEFAULT_PHONE);

        // Get all the phongList where phone not equals to UPDATED_PHONE
        defaultPhongShouldBeFound("phone.notEquals=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllPhongsByPhoneIsInShouldWork() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where phone in DEFAULT_PHONE or UPDATED_PHONE
        defaultPhongShouldBeFound("phone.in=" + DEFAULT_PHONE + "," + UPDATED_PHONE);

        // Get all the phongList where phone equals to UPDATED_PHONE
        defaultPhongShouldNotBeFound("phone.in=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllPhongsByPhoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where phone is not null
        defaultPhongShouldBeFound("phone.specified=true");

        // Get all the phongList where phone is null
        defaultPhongShouldNotBeFound("phone.specified=false");
    }
                @Test
    @Transactional
    public void getAllPhongsByPhoneContainsSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where phone contains DEFAULT_PHONE
        defaultPhongShouldBeFound("phone.contains=" + DEFAULT_PHONE);

        // Get all the phongList where phone contains UPDATED_PHONE
        defaultPhongShouldNotBeFound("phone.contains=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllPhongsByPhoneNotContainsSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where phone does not contain DEFAULT_PHONE
        defaultPhongShouldNotBeFound("phone.doesNotContain=" + DEFAULT_PHONE);

        // Get all the phongList where phone does not contain UPDATED_PHONE
        defaultPhongShouldBeFound("phone.doesNotContain=" + UPDATED_PHONE);
    }


    @Test
    @Transactional
    public void getAllPhongsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where ten equals to DEFAULT_TEN
        defaultPhongShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the phongList where ten equals to UPDATED_TEN
        defaultPhongShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllPhongsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where ten not equals to DEFAULT_TEN
        defaultPhongShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the phongList where ten not equals to UPDATED_TEN
        defaultPhongShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllPhongsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultPhongShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the phongList where ten equals to UPDATED_TEN
        defaultPhongShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllPhongsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where ten is not null
        defaultPhongShouldBeFound("ten.specified=true");

        // Get all the phongList where ten is null
        defaultPhongShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllPhongsByTenContainsSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where ten contains DEFAULT_TEN
        defaultPhongShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the phongList where ten contains UPDATED_TEN
        defaultPhongShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllPhongsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where ten does not contain DEFAULT_TEN
        defaultPhongShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the phongList where ten does not contain UPDATED_TEN
        defaultPhongShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllPhongsByViTriIsEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where viTri equals to DEFAULT_VI_TRI
        defaultPhongShouldBeFound("viTri.equals=" + DEFAULT_VI_TRI);

        // Get all the phongList where viTri equals to UPDATED_VI_TRI
        defaultPhongShouldNotBeFound("viTri.equals=" + UPDATED_VI_TRI);
    }

    @Test
    @Transactional
    public void getAllPhongsByViTriIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where viTri not equals to DEFAULT_VI_TRI
        defaultPhongShouldNotBeFound("viTri.notEquals=" + DEFAULT_VI_TRI);

        // Get all the phongList where viTri not equals to UPDATED_VI_TRI
        defaultPhongShouldBeFound("viTri.notEquals=" + UPDATED_VI_TRI);
    }

    @Test
    @Transactional
    public void getAllPhongsByViTriIsInShouldWork() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where viTri in DEFAULT_VI_TRI or UPDATED_VI_TRI
        defaultPhongShouldBeFound("viTri.in=" + DEFAULT_VI_TRI + "," + UPDATED_VI_TRI);

        // Get all the phongList where viTri equals to UPDATED_VI_TRI
        defaultPhongShouldNotBeFound("viTri.in=" + UPDATED_VI_TRI);
    }

    @Test
    @Transactional
    public void getAllPhongsByViTriIsNullOrNotNull() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where viTri is not null
        defaultPhongShouldBeFound("viTri.specified=true");

        // Get all the phongList where viTri is null
        defaultPhongShouldNotBeFound("viTri.specified=false");
    }
                @Test
    @Transactional
    public void getAllPhongsByViTriContainsSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where viTri contains DEFAULT_VI_TRI
        defaultPhongShouldBeFound("viTri.contains=" + DEFAULT_VI_TRI);

        // Get all the phongList where viTri contains UPDATED_VI_TRI
        defaultPhongShouldNotBeFound("viTri.contains=" + UPDATED_VI_TRI);
    }

    @Test
    @Transactional
    public void getAllPhongsByViTriNotContainsSomething() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        // Get all the phongList where viTri does not contain DEFAULT_VI_TRI
        defaultPhongShouldNotBeFound("viTri.doesNotContain=" + DEFAULT_VI_TRI);

        // Get all the phongList where viTri does not contain UPDATED_VI_TRI
        defaultPhongShouldBeFound("viTri.doesNotContain=" + UPDATED_VI_TRI);
    }


    @Test
    @Transactional
    public void getAllPhongsByKhoaIsEqualToSomething() throws Exception {
        // Get already existing entity
        Khoa khoa = phong.getKhoa();
        phongRepository.saveAndFlush(phong);
        Long khoaId = khoa.getId();

        // Get all the phongList where khoa equals to khoaId
        defaultPhongShouldBeFound("khoaId.equals=" + khoaId);

        // Get all the phongList where khoa equals to khoaId + 1
        defaultPhongShouldNotBeFound("khoaId.equals=" + (khoaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPhongShouldBeFound(String filter) throws Exception {
        restPhongMockMvc.perform(get("/api/phongs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phong.getId().intValue())))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].kyHieu").value(hasItem(DEFAULT_KY_HIEU)))
            .andExpect(jsonPath("$.[*].loai").value(hasItem(DEFAULT_LOAI.intValue())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].viTri").value(hasItem(DEFAULT_VI_TRI)));

        // Check, that the count call also returns 1
        restPhongMockMvc.perform(get("/api/phongs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPhongShouldNotBeFound(String filter) throws Exception {
        restPhongMockMvc.perform(get("/api/phongs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPhongMockMvc.perform(get("/api/phongs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingPhong() throws Exception {
        // Get the phong
        restPhongMockMvc.perform(get("/api/phongs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePhong() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        int databaseSizeBeforeUpdate = phongRepository.findAll().size();

        // Update the phong
        Phong updatedPhong = phongRepository.findById(phong.getId()).get();
        // Disconnect from session so that the updates on updatedPhong are not directly saved in db
        em.detach(updatedPhong);
        updatedPhong
            .enabled(UPDATED_ENABLED)
            .kyHieu(UPDATED_KY_HIEU)
            .loai(UPDATED_LOAI)
            .phone(UPDATED_PHONE)
            .ten(UPDATED_TEN)
            .viTri(UPDATED_VI_TRI);
        PhongDTO phongDTO = phongMapper.toDto(updatedPhong);

        restPhongMockMvc.perform(put("/api/phongs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phongDTO)))
            .andExpect(status().isOk());

        // Validate the Phong in the database
        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeUpdate);
        Phong testPhong = phongList.get(phongList.size() - 1);
        assertThat(testPhong.isEnabled()).isEqualTo(UPDATED_ENABLED);
        assertThat(testPhong.getKyHieu()).isEqualTo(UPDATED_KY_HIEU);
        assertThat(testPhong.getLoai()).isEqualTo(UPDATED_LOAI);
        assertThat(testPhong.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testPhong.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testPhong.getViTri()).isEqualTo(UPDATED_VI_TRI);
    }

    @Test
    @Transactional
    public void updateNonExistingPhong() throws Exception {
        int databaseSizeBeforeUpdate = phongRepository.findAll().size();

        // Create the Phong
        PhongDTO phongDTO = phongMapper.toDto(phong);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPhongMockMvc.perform(put("/api/phongs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phongDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Phong in the database
        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePhong() throws Exception {
        // Initialize the database
        phongRepository.saveAndFlush(phong);

        int databaseSizeBeforeDelete = phongRepository.findAll().size();

        // Delete the phong
        restPhongMockMvc.perform(delete("/api/phongs/{id}", phong.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Phong> phongList = phongRepository.findAll();
        assertThat(phongList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
