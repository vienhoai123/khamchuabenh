package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.PhieuChiDinhCDHA;
import vn.vnpt.domain.BenhAnKhamBenh;
import vn.vnpt.repository.PhieuChiDinhCDHARepository;
import vn.vnpt.service.PhieuChiDinhCDHAService;
import vn.vnpt.service.dto.PhieuChiDinhCDHADTO;
import vn.vnpt.service.mapper.PhieuChiDinhCDHAMapper;
import vn.vnpt.service.dto.PhieuChiDinhCDHACriteria;
import vn.vnpt.service.PhieuChiDinhCDHAQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PhieuChiDinhCDHAResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class PhieuChiDinhCDHAResourceIT {

    private static final String DEFAULT_CHAN_DOAN_TONG_QUAT = "AAAAAAAAAA";
    private static final String UPDATED_CHAN_DOAN_TONG_QUAT = "BBBBBBBBBB";

    private static final String DEFAULT_GHI_CHU = "AAAAAAAAAA";
    private static final String UPDATED_GHI_CHU = "BBBBBBBBBB";

    private static final String DEFAULT_KET_QUA_TONG_QUAT = "AAAAAAAAAA";
    private static final String UPDATED_KET_QUA_TONG_QUAT = "BBBBBBBBBB";

    private static final Integer DEFAULT_NAM = 1;
    private static final Integer UPDATED_NAM = 2;
    private static final Integer SMALLER_NAM = 1 - 1;

    @Autowired
    private PhieuChiDinhCDHARepository phieuChiDinhCDHARepository;

    @Autowired
    private PhieuChiDinhCDHAMapper phieuChiDinhCDHAMapper;

    @Autowired
    private PhieuChiDinhCDHAService phieuChiDinhCDHAService;

    @Autowired
    private PhieuChiDinhCDHAQueryService phieuChiDinhCDHAQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPhieuChiDinhCDHAMockMvc;

    private PhieuChiDinhCDHA phieuChiDinhCDHA;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhieuChiDinhCDHA createEntity(EntityManager em) {
        PhieuChiDinhCDHA phieuChiDinhCDHA = new PhieuChiDinhCDHA()
            .chanDoanTongQuat(DEFAULT_CHAN_DOAN_TONG_QUAT)
            .ghiChu(DEFAULT_GHI_CHU)
            .ketQuaTongQuat(DEFAULT_KET_QUA_TONG_QUAT)
            .nam(DEFAULT_NAM);
        // Add required entity
        BenhAnKhamBenh benhAnKhamBenh;
        if (TestUtil.findAll(em, BenhAnKhamBenh.class).isEmpty()) {
            benhAnKhamBenh = BenhAnKhamBenhResourceIT.createEntity(em);
            em.persist(benhAnKhamBenh);
            em.flush();
        } else {
            benhAnKhamBenh = TestUtil.findAll(em, BenhAnKhamBenh.class).get(0);
        }
        phieuChiDinhCDHA.setDonVi(benhAnKhamBenh);
        // Add required entity
        phieuChiDinhCDHA.setBenhNhan(benhAnKhamBenh);
        // Add required entity
        phieuChiDinhCDHA.setBakb(benhAnKhamBenh);
        return phieuChiDinhCDHA;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhieuChiDinhCDHA createUpdatedEntity(EntityManager em) {
        PhieuChiDinhCDHA phieuChiDinhCDHA = new PhieuChiDinhCDHA()
            .chanDoanTongQuat(UPDATED_CHAN_DOAN_TONG_QUAT)
            .ghiChu(UPDATED_GHI_CHU)
            .ketQuaTongQuat(UPDATED_KET_QUA_TONG_QUAT)
            .nam(UPDATED_NAM);
        // Add required entity
        BenhAnKhamBenh benhAnKhamBenh;
        if (TestUtil.findAll(em, BenhAnKhamBenh.class).isEmpty()) {
            benhAnKhamBenh = BenhAnKhamBenhResourceIT.createUpdatedEntity(em);
            em.persist(benhAnKhamBenh);
            em.flush();
        } else {
            benhAnKhamBenh = TestUtil.findAll(em, BenhAnKhamBenh.class).get(0);
        }
        phieuChiDinhCDHA.setDonVi(benhAnKhamBenh);
        // Add required entity
        phieuChiDinhCDHA.setBenhNhan(benhAnKhamBenh);
        // Add required entity
        phieuChiDinhCDHA.setBakb(benhAnKhamBenh);
        return phieuChiDinhCDHA;
    }

    @BeforeEach
    public void initTest() {
        phieuChiDinhCDHA = createEntity(em);
    }

    @Test
    @Transactional
    public void createPhieuChiDinhCDHA() throws Exception {
        int databaseSizeBeforeCreate = phieuChiDinhCDHARepository.findAll().size();

        // Create the PhieuChiDinhCDHA
        PhieuChiDinhCDHADTO phieuChiDinhCDHADTO = phieuChiDinhCDHAMapper.toDto(phieuChiDinhCDHA);
        restPhieuChiDinhCDHAMockMvc.perform(post("/api/phieu-chi-dinh-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phieuChiDinhCDHADTO)))
            .andExpect(status().isCreated());

        // Validate the PhieuChiDinhCDHA in the database
        List<PhieuChiDinhCDHA> phieuChiDinhCDHAList = phieuChiDinhCDHARepository.findAll();
        assertThat(phieuChiDinhCDHAList).hasSize(databaseSizeBeforeCreate + 1);
        PhieuChiDinhCDHA testPhieuChiDinhCDHA = phieuChiDinhCDHAList.get(phieuChiDinhCDHAList.size() - 1);
        assertThat(testPhieuChiDinhCDHA.getChanDoanTongQuat()).isEqualTo(DEFAULT_CHAN_DOAN_TONG_QUAT);
        assertThat(testPhieuChiDinhCDHA.getGhiChu()).isEqualTo(DEFAULT_GHI_CHU);
        assertThat(testPhieuChiDinhCDHA.getKetQuaTongQuat()).isEqualTo(DEFAULT_KET_QUA_TONG_QUAT);
        assertThat(testPhieuChiDinhCDHA.getNam()).isEqualTo(DEFAULT_NAM);
    }

    @Test
    @Transactional
    public void createPhieuChiDinhCDHAWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = phieuChiDinhCDHARepository.findAll().size();

        // Create the PhieuChiDinhCDHA with an existing ID
        phieuChiDinhCDHA.setId(1L);
        PhieuChiDinhCDHADTO phieuChiDinhCDHADTO = phieuChiDinhCDHAMapper.toDto(phieuChiDinhCDHA);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPhieuChiDinhCDHAMockMvc.perform(post("/api/phieu-chi-dinh-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phieuChiDinhCDHADTO)))
            .andExpect(status().isBadRequest());

        // Validate the PhieuChiDinhCDHA in the database
        List<PhieuChiDinhCDHA> phieuChiDinhCDHAList = phieuChiDinhCDHARepository.findAll();
        assertThat(phieuChiDinhCDHAList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNamIsRequired() throws Exception {
        int databaseSizeBeforeTest = phieuChiDinhCDHARepository.findAll().size();
        // set the field null
        phieuChiDinhCDHA.setNam(null);

        // Create the PhieuChiDinhCDHA, which fails.
        PhieuChiDinhCDHADTO phieuChiDinhCDHADTO = phieuChiDinhCDHAMapper.toDto(phieuChiDinhCDHA);

        restPhieuChiDinhCDHAMockMvc.perform(post("/api/phieu-chi-dinh-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phieuChiDinhCDHADTO)))
            .andExpect(status().isBadRequest());

        List<PhieuChiDinhCDHA> phieuChiDinhCDHAList = phieuChiDinhCDHARepository.findAll();
        assertThat(phieuChiDinhCDHAList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHAS() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList
        restPhieuChiDinhCDHAMockMvc.perform(get("/api/phieu-chi-dinh-cdhas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phieuChiDinhCDHA.getId().intValue())))
            .andExpect(jsonPath("$.[*].chanDoanTongQuat").value(hasItem(DEFAULT_CHAN_DOAN_TONG_QUAT)))
            .andExpect(jsonPath("$.[*].ghiChu").value(hasItem(DEFAULT_GHI_CHU)))
            .andExpect(jsonPath("$.[*].ketQuaTongQuat").value(hasItem(DEFAULT_KET_QUA_TONG_QUAT)))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));
    }
    
    @Test
    @Transactional
    public void getPhieuChiDinhCDHA() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get the phieuChiDinhCDHA
        restPhieuChiDinhCDHAMockMvc.perform(get("/api/phieu-chi-dinh-cdhas/{id}", phieuChiDinhCDHA.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(phieuChiDinhCDHA.getId().intValue()))
            .andExpect(jsonPath("$.chanDoanTongQuat").value(DEFAULT_CHAN_DOAN_TONG_QUAT))
            .andExpect(jsonPath("$.ghiChu").value(DEFAULT_GHI_CHU))
            .andExpect(jsonPath("$.ketQuaTongQuat").value(DEFAULT_KET_QUA_TONG_QUAT))
            .andExpect(jsonPath("$.nam").value(DEFAULT_NAM));
    }


    @Test
    @Transactional
    public void getPhieuChiDinhCDHASByIdFiltering() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        Long id = phieuChiDinhCDHA.getId();

        defaultPhieuChiDinhCDHAShouldBeFound("id.equals=" + id);
        defaultPhieuChiDinhCDHAShouldNotBeFound("id.notEquals=" + id);

        defaultPhieuChiDinhCDHAShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPhieuChiDinhCDHAShouldNotBeFound("id.greaterThan=" + id);

        defaultPhieuChiDinhCDHAShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPhieuChiDinhCDHAShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByChanDoanTongQuatIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where chanDoanTongQuat equals to DEFAULT_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhCDHAShouldBeFound("chanDoanTongQuat.equals=" + DEFAULT_CHAN_DOAN_TONG_QUAT);

        // Get all the phieuChiDinhCDHAList where chanDoanTongQuat equals to UPDATED_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhCDHAShouldNotBeFound("chanDoanTongQuat.equals=" + UPDATED_CHAN_DOAN_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByChanDoanTongQuatIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where chanDoanTongQuat not equals to DEFAULT_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhCDHAShouldNotBeFound("chanDoanTongQuat.notEquals=" + DEFAULT_CHAN_DOAN_TONG_QUAT);

        // Get all the phieuChiDinhCDHAList where chanDoanTongQuat not equals to UPDATED_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhCDHAShouldBeFound("chanDoanTongQuat.notEquals=" + UPDATED_CHAN_DOAN_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByChanDoanTongQuatIsInShouldWork() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where chanDoanTongQuat in DEFAULT_CHAN_DOAN_TONG_QUAT or UPDATED_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhCDHAShouldBeFound("chanDoanTongQuat.in=" + DEFAULT_CHAN_DOAN_TONG_QUAT + "," + UPDATED_CHAN_DOAN_TONG_QUAT);

        // Get all the phieuChiDinhCDHAList where chanDoanTongQuat equals to UPDATED_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhCDHAShouldNotBeFound("chanDoanTongQuat.in=" + UPDATED_CHAN_DOAN_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByChanDoanTongQuatIsNullOrNotNull() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where chanDoanTongQuat is not null
        defaultPhieuChiDinhCDHAShouldBeFound("chanDoanTongQuat.specified=true");

        // Get all the phieuChiDinhCDHAList where chanDoanTongQuat is null
        defaultPhieuChiDinhCDHAShouldNotBeFound("chanDoanTongQuat.specified=false");
    }
                @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByChanDoanTongQuatContainsSomething() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where chanDoanTongQuat contains DEFAULT_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhCDHAShouldBeFound("chanDoanTongQuat.contains=" + DEFAULT_CHAN_DOAN_TONG_QUAT);

        // Get all the phieuChiDinhCDHAList where chanDoanTongQuat contains UPDATED_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhCDHAShouldNotBeFound("chanDoanTongQuat.contains=" + UPDATED_CHAN_DOAN_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByChanDoanTongQuatNotContainsSomething() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where chanDoanTongQuat does not contain DEFAULT_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhCDHAShouldNotBeFound("chanDoanTongQuat.doesNotContain=" + DEFAULT_CHAN_DOAN_TONG_QUAT);

        // Get all the phieuChiDinhCDHAList where chanDoanTongQuat does not contain UPDATED_CHAN_DOAN_TONG_QUAT
        defaultPhieuChiDinhCDHAShouldBeFound("chanDoanTongQuat.doesNotContain=" + UPDATED_CHAN_DOAN_TONG_QUAT);
    }


    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByGhiChuIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where ghiChu equals to DEFAULT_GHI_CHU
        defaultPhieuChiDinhCDHAShouldBeFound("ghiChu.equals=" + DEFAULT_GHI_CHU);

        // Get all the phieuChiDinhCDHAList where ghiChu equals to UPDATED_GHI_CHU
        defaultPhieuChiDinhCDHAShouldNotBeFound("ghiChu.equals=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByGhiChuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where ghiChu not equals to DEFAULT_GHI_CHU
        defaultPhieuChiDinhCDHAShouldNotBeFound("ghiChu.notEquals=" + DEFAULT_GHI_CHU);

        // Get all the phieuChiDinhCDHAList where ghiChu not equals to UPDATED_GHI_CHU
        defaultPhieuChiDinhCDHAShouldBeFound("ghiChu.notEquals=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByGhiChuIsInShouldWork() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where ghiChu in DEFAULT_GHI_CHU or UPDATED_GHI_CHU
        defaultPhieuChiDinhCDHAShouldBeFound("ghiChu.in=" + DEFAULT_GHI_CHU + "," + UPDATED_GHI_CHU);

        // Get all the phieuChiDinhCDHAList where ghiChu equals to UPDATED_GHI_CHU
        defaultPhieuChiDinhCDHAShouldNotBeFound("ghiChu.in=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByGhiChuIsNullOrNotNull() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where ghiChu is not null
        defaultPhieuChiDinhCDHAShouldBeFound("ghiChu.specified=true");

        // Get all the phieuChiDinhCDHAList where ghiChu is null
        defaultPhieuChiDinhCDHAShouldNotBeFound("ghiChu.specified=false");
    }
                @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByGhiChuContainsSomething() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where ghiChu contains DEFAULT_GHI_CHU
        defaultPhieuChiDinhCDHAShouldBeFound("ghiChu.contains=" + DEFAULT_GHI_CHU);

        // Get all the phieuChiDinhCDHAList where ghiChu contains UPDATED_GHI_CHU
        defaultPhieuChiDinhCDHAShouldNotBeFound("ghiChu.contains=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByGhiChuNotContainsSomething() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where ghiChu does not contain DEFAULT_GHI_CHU
        defaultPhieuChiDinhCDHAShouldNotBeFound("ghiChu.doesNotContain=" + DEFAULT_GHI_CHU);

        // Get all the phieuChiDinhCDHAList where ghiChu does not contain UPDATED_GHI_CHU
        defaultPhieuChiDinhCDHAShouldBeFound("ghiChu.doesNotContain=" + UPDATED_GHI_CHU);
    }


    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByKetQuaTongQuatIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where ketQuaTongQuat equals to DEFAULT_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhCDHAShouldBeFound("ketQuaTongQuat.equals=" + DEFAULT_KET_QUA_TONG_QUAT);

        // Get all the phieuChiDinhCDHAList where ketQuaTongQuat equals to UPDATED_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhCDHAShouldNotBeFound("ketQuaTongQuat.equals=" + UPDATED_KET_QUA_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByKetQuaTongQuatIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where ketQuaTongQuat not equals to DEFAULT_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhCDHAShouldNotBeFound("ketQuaTongQuat.notEquals=" + DEFAULT_KET_QUA_TONG_QUAT);

        // Get all the phieuChiDinhCDHAList where ketQuaTongQuat not equals to UPDATED_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhCDHAShouldBeFound("ketQuaTongQuat.notEquals=" + UPDATED_KET_QUA_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByKetQuaTongQuatIsInShouldWork() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where ketQuaTongQuat in DEFAULT_KET_QUA_TONG_QUAT or UPDATED_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhCDHAShouldBeFound("ketQuaTongQuat.in=" + DEFAULT_KET_QUA_TONG_QUAT + "," + UPDATED_KET_QUA_TONG_QUAT);

        // Get all the phieuChiDinhCDHAList where ketQuaTongQuat equals to UPDATED_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhCDHAShouldNotBeFound("ketQuaTongQuat.in=" + UPDATED_KET_QUA_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByKetQuaTongQuatIsNullOrNotNull() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where ketQuaTongQuat is not null
        defaultPhieuChiDinhCDHAShouldBeFound("ketQuaTongQuat.specified=true");

        // Get all the phieuChiDinhCDHAList where ketQuaTongQuat is null
        defaultPhieuChiDinhCDHAShouldNotBeFound("ketQuaTongQuat.specified=false");
    }
                @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByKetQuaTongQuatContainsSomething() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where ketQuaTongQuat contains DEFAULT_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhCDHAShouldBeFound("ketQuaTongQuat.contains=" + DEFAULT_KET_QUA_TONG_QUAT);

        // Get all the phieuChiDinhCDHAList where ketQuaTongQuat contains UPDATED_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhCDHAShouldNotBeFound("ketQuaTongQuat.contains=" + UPDATED_KET_QUA_TONG_QUAT);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByKetQuaTongQuatNotContainsSomething() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where ketQuaTongQuat does not contain DEFAULT_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhCDHAShouldNotBeFound("ketQuaTongQuat.doesNotContain=" + DEFAULT_KET_QUA_TONG_QUAT);

        // Get all the phieuChiDinhCDHAList where ketQuaTongQuat does not contain UPDATED_KET_QUA_TONG_QUAT
        defaultPhieuChiDinhCDHAShouldBeFound("ketQuaTongQuat.doesNotContain=" + UPDATED_KET_QUA_TONG_QUAT);
    }


    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByNamIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where nam equals to DEFAULT_NAM
        defaultPhieuChiDinhCDHAShouldBeFound("nam.equals=" + DEFAULT_NAM);

        // Get all the phieuChiDinhCDHAList where nam equals to UPDATED_NAM
        defaultPhieuChiDinhCDHAShouldNotBeFound("nam.equals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByNamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where nam not equals to DEFAULT_NAM
        defaultPhieuChiDinhCDHAShouldNotBeFound("nam.notEquals=" + DEFAULT_NAM);

        // Get all the phieuChiDinhCDHAList where nam not equals to UPDATED_NAM
        defaultPhieuChiDinhCDHAShouldBeFound("nam.notEquals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByNamIsInShouldWork() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where nam in DEFAULT_NAM or UPDATED_NAM
        defaultPhieuChiDinhCDHAShouldBeFound("nam.in=" + DEFAULT_NAM + "," + UPDATED_NAM);

        // Get all the phieuChiDinhCDHAList where nam equals to UPDATED_NAM
        defaultPhieuChiDinhCDHAShouldNotBeFound("nam.in=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByNamIsNullOrNotNull() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where nam is not null
        defaultPhieuChiDinhCDHAShouldBeFound("nam.specified=true");

        // Get all the phieuChiDinhCDHAList where nam is null
        defaultPhieuChiDinhCDHAShouldNotBeFound("nam.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByNamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where nam is greater than or equal to DEFAULT_NAM
        defaultPhieuChiDinhCDHAShouldBeFound("nam.greaterThanOrEqual=" + DEFAULT_NAM);

        // Get all the phieuChiDinhCDHAList where nam is greater than or equal to UPDATED_NAM
        defaultPhieuChiDinhCDHAShouldNotBeFound("nam.greaterThanOrEqual=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByNamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where nam is less than or equal to DEFAULT_NAM
        defaultPhieuChiDinhCDHAShouldBeFound("nam.lessThanOrEqual=" + DEFAULT_NAM);

        // Get all the phieuChiDinhCDHAList where nam is less than or equal to SMALLER_NAM
        defaultPhieuChiDinhCDHAShouldNotBeFound("nam.lessThanOrEqual=" + SMALLER_NAM);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByNamIsLessThanSomething() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where nam is less than DEFAULT_NAM
        defaultPhieuChiDinhCDHAShouldNotBeFound("nam.lessThan=" + DEFAULT_NAM);

        // Get all the phieuChiDinhCDHAList where nam is less than UPDATED_NAM
        defaultPhieuChiDinhCDHAShouldBeFound("nam.lessThan=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByNamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        // Get all the phieuChiDinhCDHAList where nam is greater than DEFAULT_NAM
        defaultPhieuChiDinhCDHAShouldNotBeFound("nam.greaterThan=" + DEFAULT_NAM);

        // Get all the phieuChiDinhCDHAList where nam is greater than SMALLER_NAM
        defaultPhieuChiDinhCDHAShouldBeFound("nam.greaterThan=" + SMALLER_NAM);
    }


    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        BenhAnKhamBenh donVi = phieuChiDinhCDHA.getDonVi();
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);
        Long donViId = donVi.getId();

        // Get all the phieuChiDinhCDHAList where donVi equals to donViId
        defaultPhieuChiDinhCDHAShouldBeFound("donViId.equals=" + donViId);

        // Get all the phieuChiDinhCDHAList where donVi equals to donViId + 1
        defaultPhieuChiDinhCDHAShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }


    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByBenhNhanIsEqualToSomething() throws Exception {
        // Get already existing entity
        BenhAnKhamBenh benhNhan = phieuChiDinhCDHA.getBenhNhan();
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);
        Long benhNhanId = benhNhan.getId();

        // Get all the phieuChiDinhCDHAList where benhNhan equals to benhNhanId
        defaultPhieuChiDinhCDHAShouldBeFound("benhNhanId.equals=" + benhNhanId);

        // Get all the phieuChiDinhCDHAList where benhNhan equals to benhNhanId + 1
        defaultPhieuChiDinhCDHAShouldNotBeFound("benhNhanId.equals=" + (benhNhanId + 1));
    }


    @Test
    @Transactional
    public void getAllPhieuChiDinhCDHASByBakbIsEqualToSomething() throws Exception {
        // Get already existing entity
        BenhAnKhamBenh bakb = phieuChiDinhCDHA.getBakb();
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);
        Long bakbId = bakb.getId();

        // Get all the phieuChiDinhCDHAList where bakb equals to bakbId
        defaultPhieuChiDinhCDHAShouldBeFound("bakbId.equals=" + bakbId);

        // Get all the phieuChiDinhCDHAList where bakb equals to bakbId + 1
        defaultPhieuChiDinhCDHAShouldNotBeFound("bakbId.equals=" + (bakbId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPhieuChiDinhCDHAShouldBeFound(String filter) throws Exception {
        restPhieuChiDinhCDHAMockMvc.perform(get("/api/phieu-chi-dinh-cdhas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phieuChiDinhCDHA.getId().intValue())))
            .andExpect(jsonPath("$.[*].chanDoanTongQuat").value(hasItem(DEFAULT_CHAN_DOAN_TONG_QUAT)))
            .andExpect(jsonPath("$.[*].ghiChu").value(hasItem(DEFAULT_GHI_CHU)))
            .andExpect(jsonPath("$.[*].ketQuaTongQuat").value(hasItem(DEFAULT_KET_QUA_TONG_QUAT)))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)));

        // Check, that the count call also returns 1
        restPhieuChiDinhCDHAMockMvc.perform(get("/api/phieu-chi-dinh-cdhas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPhieuChiDinhCDHAShouldNotBeFound(String filter) throws Exception {
        restPhieuChiDinhCDHAMockMvc.perform(get("/api/phieu-chi-dinh-cdhas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPhieuChiDinhCDHAMockMvc.perform(get("/api/phieu-chi-dinh-cdhas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingPhieuChiDinhCDHA() throws Exception {
        // Get the phieuChiDinhCDHA
        restPhieuChiDinhCDHAMockMvc.perform(get("/api/phieu-chi-dinh-cdhas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePhieuChiDinhCDHA() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        int databaseSizeBeforeUpdate = phieuChiDinhCDHARepository.findAll().size();

        // Update the phieuChiDinhCDHA
        PhieuChiDinhCDHA updatedPhieuChiDinhCDHA = phieuChiDinhCDHARepository.findById(phieuChiDinhCDHA.getId()).get();
        // Disconnect from session so that the updates on updatedPhieuChiDinhCDHA are not directly saved in db
        em.detach(updatedPhieuChiDinhCDHA);
        updatedPhieuChiDinhCDHA
            .chanDoanTongQuat(UPDATED_CHAN_DOAN_TONG_QUAT)
            .ghiChu(UPDATED_GHI_CHU)
            .ketQuaTongQuat(UPDATED_KET_QUA_TONG_QUAT)
            .nam(UPDATED_NAM);
        PhieuChiDinhCDHADTO phieuChiDinhCDHADTO = phieuChiDinhCDHAMapper.toDto(updatedPhieuChiDinhCDHA);

        restPhieuChiDinhCDHAMockMvc.perform(put("/api/phieu-chi-dinh-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phieuChiDinhCDHADTO)))
            .andExpect(status().isOk());

        // Validate the PhieuChiDinhCDHA in the database
        List<PhieuChiDinhCDHA> phieuChiDinhCDHAList = phieuChiDinhCDHARepository.findAll();
        assertThat(phieuChiDinhCDHAList).hasSize(databaseSizeBeforeUpdate);
        PhieuChiDinhCDHA testPhieuChiDinhCDHA = phieuChiDinhCDHAList.get(phieuChiDinhCDHAList.size() - 1);
        assertThat(testPhieuChiDinhCDHA.getChanDoanTongQuat()).isEqualTo(UPDATED_CHAN_DOAN_TONG_QUAT);
        assertThat(testPhieuChiDinhCDHA.getGhiChu()).isEqualTo(UPDATED_GHI_CHU);
        assertThat(testPhieuChiDinhCDHA.getKetQuaTongQuat()).isEqualTo(UPDATED_KET_QUA_TONG_QUAT);
        assertThat(testPhieuChiDinhCDHA.getNam()).isEqualTo(UPDATED_NAM);
    }

    @Test
    @Transactional
    public void updateNonExistingPhieuChiDinhCDHA() throws Exception {
        int databaseSizeBeforeUpdate = phieuChiDinhCDHARepository.findAll().size();

        // Create the PhieuChiDinhCDHA
        PhieuChiDinhCDHADTO phieuChiDinhCDHADTO = phieuChiDinhCDHAMapper.toDto(phieuChiDinhCDHA);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPhieuChiDinhCDHAMockMvc.perform(put("/api/phieu-chi-dinh-cdhas").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phieuChiDinhCDHADTO)))
            .andExpect(status().isBadRequest());

        // Validate the PhieuChiDinhCDHA in the database
        List<PhieuChiDinhCDHA> phieuChiDinhCDHAList = phieuChiDinhCDHARepository.findAll();
        assertThat(phieuChiDinhCDHAList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePhieuChiDinhCDHA() throws Exception {
        // Initialize the database
        phieuChiDinhCDHARepository.saveAndFlush(phieuChiDinhCDHA);

        int databaseSizeBeforeDelete = phieuChiDinhCDHARepository.findAll().size();

        // Delete the phieuChiDinhCDHA
        restPhieuChiDinhCDHAMockMvc.perform(delete("/api/phieu-chi-dinh-cdhas/{id}", phieuChiDinhCDHA.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PhieuChiDinhCDHA> phieuChiDinhCDHAList = phieuChiDinhCDHARepository.findAll();
        assertThat(phieuChiDinhCDHAList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
