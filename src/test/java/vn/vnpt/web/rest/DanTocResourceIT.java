package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.DanToc;
import vn.vnpt.repository.DanTocRepository;
import vn.vnpt.service.DanTocService;
import vn.vnpt.service.dto.DanTocDTO;
import vn.vnpt.service.mapper.DanTocMapper;
import vn.vnpt.service.dto.DanTocCriteria;
import vn.vnpt.service.DanTocQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DanTocResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class DanTocResourceIT {

    private static final Integer DEFAULT_MA_4069_BYT = 1;
    private static final Integer UPDATED_MA_4069_BYT = 2;
    private static final Integer SMALLER_MA_4069_BYT = 1 - 1;

    private static final Integer DEFAULT_MA_CUC_THONG_KE = 1;
    private static final Integer UPDATED_MA_CUC_THONG_KE = 2;
    private static final Integer SMALLER_MA_CUC_THONG_KE = 1 - 1;

    private static final String DEFAULT_TEN_4069_BYT = "AAAAAAAAAA";
    private static final String UPDATED_TEN_4069_BYT = "BBBBBBBBBB";

    private static final String DEFAULT_TEN_CUC_THONG_KE = "AAAAAAAAAA";
    private static final String UPDATED_TEN_CUC_THONG_KE = "BBBBBBBBBB";

    @Autowired
    private DanTocRepository danTocRepository;

    @Autowired
    private DanTocMapper danTocMapper;

    @Autowired
    private DanTocService danTocService;

    @Autowired
    private DanTocQueryService danTocQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDanTocMockMvc;

    private DanToc danToc;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DanToc createEntity(EntityManager em) {
        DanToc danToc = new DanToc()
            .ma4069Byt(DEFAULT_MA_4069_BYT)
            .maCucThongKe(DEFAULT_MA_CUC_THONG_KE)
            .ten4069Byt(DEFAULT_TEN_4069_BYT)
            .tenCucThongKe(DEFAULT_TEN_CUC_THONG_KE);
        return danToc;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DanToc createUpdatedEntity(EntityManager em) {
        DanToc danToc = new DanToc()
            .ma4069Byt(UPDATED_MA_4069_BYT)
            .maCucThongKe(UPDATED_MA_CUC_THONG_KE)
            .ten4069Byt(UPDATED_TEN_4069_BYT)
            .tenCucThongKe(UPDATED_TEN_CUC_THONG_KE);
        return danToc;
    }

    @BeforeEach
    public void initTest() {
        danToc = createEntity(em);
    }

    @Test
    @Transactional
    public void createDanToc() throws Exception {
        int databaseSizeBeforeCreate = danTocRepository.findAll().size();

        // Create the DanToc
        DanTocDTO danTocDTO = danTocMapper.toDto(danToc);
        restDanTocMockMvc.perform(post("/api/dan-tocs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(danTocDTO)))
            .andExpect(status().isCreated());

        // Validate the DanToc in the database
        List<DanToc> danTocList = danTocRepository.findAll();
        assertThat(danTocList).hasSize(databaseSizeBeforeCreate + 1);
        DanToc testDanToc = danTocList.get(danTocList.size() - 1);
        assertThat(testDanToc.getMa4069Byt()).isEqualTo(DEFAULT_MA_4069_BYT);
        assertThat(testDanToc.getMaCucThongKe()).isEqualTo(DEFAULT_MA_CUC_THONG_KE);
        assertThat(testDanToc.getTen4069Byt()).isEqualTo(DEFAULT_TEN_4069_BYT);
        assertThat(testDanToc.getTenCucThongKe()).isEqualTo(DEFAULT_TEN_CUC_THONG_KE);
    }

    @Test
    @Transactional
    public void createDanTocWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = danTocRepository.findAll().size();

        // Create the DanToc with an existing ID
        danToc.setId(1L);
        DanTocDTO danTocDTO = danTocMapper.toDto(danToc);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDanTocMockMvc.perform(post("/api/dan-tocs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(danTocDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DanToc in the database
        List<DanToc> danTocList = danTocRepository.findAll();
        assertThat(danTocList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkMa4069BytIsRequired() throws Exception {
        int databaseSizeBeforeTest = danTocRepository.findAll().size();
        // set the field null
        danToc.setMa4069Byt(null);

        // Create the DanToc, which fails.
        DanTocDTO danTocDTO = danTocMapper.toDto(danToc);

        restDanTocMockMvc.perform(post("/api/dan-tocs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(danTocDTO)))
            .andExpect(status().isBadRequest());

        List<DanToc> danTocList = danTocRepository.findAll();
        assertThat(danTocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMaCucThongKeIsRequired() throws Exception {
        int databaseSizeBeforeTest = danTocRepository.findAll().size();
        // set the field null
        danToc.setMaCucThongKe(null);

        // Create the DanToc, which fails.
        DanTocDTO danTocDTO = danTocMapper.toDto(danToc);

        restDanTocMockMvc.perform(post("/api/dan-tocs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(danTocDTO)))
            .andExpect(status().isBadRequest());

        List<DanToc> danTocList = danTocRepository.findAll();
        assertThat(danTocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTen4069BytIsRequired() throws Exception {
        int databaseSizeBeforeTest = danTocRepository.findAll().size();
        // set the field null
        danToc.setTen4069Byt(null);

        // Create the DanToc, which fails.
        DanTocDTO danTocDTO = danTocMapper.toDto(danToc);

        restDanTocMockMvc.perform(post("/api/dan-tocs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(danTocDTO)))
            .andExpect(status().isBadRequest());

        List<DanToc> danTocList = danTocRepository.findAll();
        assertThat(danTocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTenCucThongKeIsRequired() throws Exception {
        int databaseSizeBeforeTest = danTocRepository.findAll().size();
        // set the field null
        danToc.setTenCucThongKe(null);

        // Create the DanToc, which fails.
        DanTocDTO danTocDTO = danTocMapper.toDto(danToc);

        restDanTocMockMvc.perform(post("/api/dan-tocs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(danTocDTO)))
            .andExpect(status().isBadRequest());

        List<DanToc> danTocList = danTocRepository.findAll();
        assertThat(danTocList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDanTocs() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList
        restDanTocMockMvc.perform(get("/api/dan-tocs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(danToc.getId().intValue())))
            .andExpect(jsonPath("$.[*].ma4069Byt").value(hasItem(DEFAULT_MA_4069_BYT)))
            .andExpect(jsonPath("$.[*].maCucThongKe").value(hasItem(DEFAULT_MA_CUC_THONG_KE)))
            .andExpect(jsonPath("$.[*].ten4069Byt").value(hasItem(DEFAULT_TEN_4069_BYT)))
            .andExpect(jsonPath("$.[*].tenCucThongKe").value(hasItem(DEFAULT_TEN_CUC_THONG_KE)));
    }
    
    @Test
    @Transactional
    public void getDanToc() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get the danToc
        restDanTocMockMvc.perform(get("/api/dan-tocs/{id}", danToc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(danToc.getId().intValue()))
            .andExpect(jsonPath("$.ma4069Byt").value(DEFAULT_MA_4069_BYT))
            .andExpect(jsonPath("$.maCucThongKe").value(DEFAULT_MA_CUC_THONG_KE))
            .andExpect(jsonPath("$.ten4069Byt").value(DEFAULT_TEN_4069_BYT))
            .andExpect(jsonPath("$.tenCucThongKe").value(DEFAULT_TEN_CUC_THONG_KE));
    }


    @Test
    @Transactional
    public void getDanTocsByIdFiltering() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        Long id = danToc.getId();

        defaultDanTocShouldBeFound("id.equals=" + id);
        defaultDanTocShouldNotBeFound("id.notEquals=" + id);

        defaultDanTocShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultDanTocShouldNotBeFound("id.greaterThan=" + id);

        defaultDanTocShouldBeFound("id.lessThanOrEqual=" + id);
        defaultDanTocShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllDanTocsByMa4069BytIsEqualToSomething() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where ma4069Byt equals to DEFAULT_MA_4069_BYT
        defaultDanTocShouldBeFound("ma4069Byt.equals=" + DEFAULT_MA_4069_BYT);

        // Get all the danTocList where ma4069Byt equals to UPDATED_MA_4069_BYT
        defaultDanTocShouldNotBeFound("ma4069Byt.equals=" + UPDATED_MA_4069_BYT);
    }

    @Test
    @Transactional
    public void getAllDanTocsByMa4069BytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where ma4069Byt not equals to DEFAULT_MA_4069_BYT
        defaultDanTocShouldNotBeFound("ma4069Byt.notEquals=" + DEFAULT_MA_4069_BYT);

        // Get all the danTocList where ma4069Byt not equals to UPDATED_MA_4069_BYT
        defaultDanTocShouldBeFound("ma4069Byt.notEquals=" + UPDATED_MA_4069_BYT);
    }

    @Test
    @Transactional
    public void getAllDanTocsByMa4069BytIsInShouldWork() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where ma4069Byt in DEFAULT_MA_4069_BYT or UPDATED_MA_4069_BYT
        defaultDanTocShouldBeFound("ma4069Byt.in=" + DEFAULT_MA_4069_BYT + "," + UPDATED_MA_4069_BYT);

        // Get all the danTocList where ma4069Byt equals to UPDATED_MA_4069_BYT
        defaultDanTocShouldNotBeFound("ma4069Byt.in=" + UPDATED_MA_4069_BYT);
    }

    @Test
    @Transactional
    public void getAllDanTocsByMa4069BytIsNullOrNotNull() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where ma4069Byt is not null
        defaultDanTocShouldBeFound("ma4069Byt.specified=true");

        // Get all the danTocList where ma4069Byt is null
        defaultDanTocShouldNotBeFound("ma4069Byt.specified=false");
    }

    @Test
    @Transactional
    public void getAllDanTocsByMa4069BytIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where ma4069Byt is greater than or equal to DEFAULT_MA_4069_BYT
        defaultDanTocShouldBeFound("ma4069Byt.greaterThanOrEqual=" + DEFAULT_MA_4069_BYT);

        // Get all the danTocList where ma4069Byt is greater than or equal to UPDATED_MA_4069_BYT
        defaultDanTocShouldNotBeFound("ma4069Byt.greaterThanOrEqual=" + UPDATED_MA_4069_BYT);
    }

    @Test
    @Transactional
    public void getAllDanTocsByMa4069BytIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where ma4069Byt is less than or equal to DEFAULT_MA_4069_BYT
        defaultDanTocShouldBeFound("ma4069Byt.lessThanOrEqual=" + DEFAULT_MA_4069_BYT);

        // Get all the danTocList where ma4069Byt is less than or equal to SMALLER_MA_4069_BYT
        defaultDanTocShouldNotBeFound("ma4069Byt.lessThanOrEqual=" + SMALLER_MA_4069_BYT);
    }

    @Test
    @Transactional
    public void getAllDanTocsByMa4069BytIsLessThanSomething() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where ma4069Byt is less than DEFAULT_MA_4069_BYT
        defaultDanTocShouldNotBeFound("ma4069Byt.lessThan=" + DEFAULT_MA_4069_BYT);

        // Get all the danTocList where ma4069Byt is less than UPDATED_MA_4069_BYT
        defaultDanTocShouldBeFound("ma4069Byt.lessThan=" + UPDATED_MA_4069_BYT);
    }

    @Test
    @Transactional
    public void getAllDanTocsByMa4069BytIsGreaterThanSomething() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where ma4069Byt is greater than DEFAULT_MA_4069_BYT
        defaultDanTocShouldNotBeFound("ma4069Byt.greaterThan=" + DEFAULT_MA_4069_BYT);

        // Get all the danTocList where ma4069Byt is greater than SMALLER_MA_4069_BYT
        defaultDanTocShouldBeFound("ma4069Byt.greaterThan=" + SMALLER_MA_4069_BYT);
    }


    @Test
    @Transactional
    public void getAllDanTocsByMaCucThongKeIsEqualToSomething() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where maCucThongKe equals to DEFAULT_MA_CUC_THONG_KE
        defaultDanTocShouldBeFound("maCucThongKe.equals=" + DEFAULT_MA_CUC_THONG_KE);

        // Get all the danTocList where maCucThongKe equals to UPDATED_MA_CUC_THONG_KE
        defaultDanTocShouldNotBeFound("maCucThongKe.equals=" + UPDATED_MA_CUC_THONG_KE);
    }

    @Test
    @Transactional
    public void getAllDanTocsByMaCucThongKeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where maCucThongKe not equals to DEFAULT_MA_CUC_THONG_KE
        defaultDanTocShouldNotBeFound("maCucThongKe.notEquals=" + DEFAULT_MA_CUC_THONG_KE);

        // Get all the danTocList where maCucThongKe not equals to UPDATED_MA_CUC_THONG_KE
        defaultDanTocShouldBeFound("maCucThongKe.notEquals=" + UPDATED_MA_CUC_THONG_KE);
    }

    @Test
    @Transactional
    public void getAllDanTocsByMaCucThongKeIsInShouldWork() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where maCucThongKe in DEFAULT_MA_CUC_THONG_KE or UPDATED_MA_CUC_THONG_KE
        defaultDanTocShouldBeFound("maCucThongKe.in=" + DEFAULT_MA_CUC_THONG_KE + "," + UPDATED_MA_CUC_THONG_KE);

        // Get all the danTocList where maCucThongKe equals to UPDATED_MA_CUC_THONG_KE
        defaultDanTocShouldNotBeFound("maCucThongKe.in=" + UPDATED_MA_CUC_THONG_KE);
    }

    @Test
    @Transactional
    public void getAllDanTocsByMaCucThongKeIsNullOrNotNull() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where maCucThongKe is not null
        defaultDanTocShouldBeFound("maCucThongKe.specified=true");

        // Get all the danTocList where maCucThongKe is null
        defaultDanTocShouldNotBeFound("maCucThongKe.specified=false");
    }

    @Test
    @Transactional
    public void getAllDanTocsByMaCucThongKeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where maCucThongKe is greater than or equal to DEFAULT_MA_CUC_THONG_KE
        defaultDanTocShouldBeFound("maCucThongKe.greaterThanOrEqual=" + DEFAULT_MA_CUC_THONG_KE);

        // Get all the danTocList where maCucThongKe is greater than or equal to UPDATED_MA_CUC_THONG_KE
        defaultDanTocShouldNotBeFound("maCucThongKe.greaterThanOrEqual=" + UPDATED_MA_CUC_THONG_KE);
    }

    @Test
    @Transactional
    public void getAllDanTocsByMaCucThongKeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where maCucThongKe is less than or equal to DEFAULT_MA_CUC_THONG_KE
        defaultDanTocShouldBeFound("maCucThongKe.lessThanOrEqual=" + DEFAULT_MA_CUC_THONG_KE);

        // Get all the danTocList where maCucThongKe is less than or equal to SMALLER_MA_CUC_THONG_KE
        defaultDanTocShouldNotBeFound("maCucThongKe.lessThanOrEqual=" + SMALLER_MA_CUC_THONG_KE);
    }

    @Test
    @Transactional
    public void getAllDanTocsByMaCucThongKeIsLessThanSomething() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where maCucThongKe is less than DEFAULT_MA_CUC_THONG_KE
        defaultDanTocShouldNotBeFound("maCucThongKe.lessThan=" + DEFAULT_MA_CUC_THONG_KE);

        // Get all the danTocList where maCucThongKe is less than UPDATED_MA_CUC_THONG_KE
        defaultDanTocShouldBeFound("maCucThongKe.lessThan=" + UPDATED_MA_CUC_THONG_KE);
    }

    @Test
    @Transactional
    public void getAllDanTocsByMaCucThongKeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where maCucThongKe is greater than DEFAULT_MA_CUC_THONG_KE
        defaultDanTocShouldNotBeFound("maCucThongKe.greaterThan=" + DEFAULT_MA_CUC_THONG_KE);

        // Get all the danTocList where maCucThongKe is greater than SMALLER_MA_CUC_THONG_KE
        defaultDanTocShouldBeFound("maCucThongKe.greaterThan=" + SMALLER_MA_CUC_THONG_KE);
    }


    @Test
    @Transactional
    public void getAllDanTocsByTen4069BytIsEqualToSomething() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where ten4069Byt equals to DEFAULT_TEN_4069_BYT
        defaultDanTocShouldBeFound("ten4069Byt.equals=" + DEFAULT_TEN_4069_BYT);

        // Get all the danTocList where ten4069Byt equals to UPDATED_TEN_4069_BYT
        defaultDanTocShouldNotBeFound("ten4069Byt.equals=" + UPDATED_TEN_4069_BYT);
    }

    @Test
    @Transactional
    public void getAllDanTocsByTen4069BytIsNotEqualToSomething() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where ten4069Byt not equals to DEFAULT_TEN_4069_BYT
        defaultDanTocShouldNotBeFound("ten4069Byt.notEquals=" + DEFAULT_TEN_4069_BYT);

        // Get all the danTocList where ten4069Byt not equals to UPDATED_TEN_4069_BYT
        defaultDanTocShouldBeFound("ten4069Byt.notEquals=" + UPDATED_TEN_4069_BYT);
    }

    @Test
    @Transactional
    public void getAllDanTocsByTen4069BytIsInShouldWork() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where ten4069Byt in DEFAULT_TEN_4069_BYT or UPDATED_TEN_4069_BYT
        defaultDanTocShouldBeFound("ten4069Byt.in=" + DEFAULT_TEN_4069_BYT + "," + UPDATED_TEN_4069_BYT);

        // Get all the danTocList where ten4069Byt equals to UPDATED_TEN_4069_BYT
        defaultDanTocShouldNotBeFound("ten4069Byt.in=" + UPDATED_TEN_4069_BYT);
    }

    @Test
    @Transactional
    public void getAllDanTocsByTen4069BytIsNullOrNotNull() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where ten4069Byt is not null
        defaultDanTocShouldBeFound("ten4069Byt.specified=true");

        // Get all the danTocList where ten4069Byt is null
        defaultDanTocShouldNotBeFound("ten4069Byt.specified=false");
    }
                @Test
    @Transactional
    public void getAllDanTocsByTen4069BytContainsSomething() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where ten4069Byt contains DEFAULT_TEN_4069_BYT
        defaultDanTocShouldBeFound("ten4069Byt.contains=" + DEFAULT_TEN_4069_BYT);

        // Get all the danTocList where ten4069Byt contains UPDATED_TEN_4069_BYT
        defaultDanTocShouldNotBeFound("ten4069Byt.contains=" + UPDATED_TEN_4069_BYT);
    }

    @Test
    @Transactional
    public void getAllDanTocsByTen4069BytNotContainsSomething() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where ten4069Byt does not contain DEFAULT_TEN_4069_BYT
        defaultDanTocShouldNotBeFound("ten4069Byt.doesNotContain=" + DEFAULT_TEN_4069_BYT);

        // Get all the danTocList where ten4069Byt does not contain UPDATED_TEN_4069_BYT
        defaultDanTocShouldBeFound("ten4069Byt.doesNotContain=" + UPDATED_TEN_4069_BYT);
    }


    @Test
    @Transactional
    public void getAllDanTocsByTenCucThongKeIsEqualToSomething() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where tenCucThongKe equals to DEFAULT_TEN_CUC_THONG_KE
        defaultDanTocShouldBeFound("tenCucThongKe.equals=" + DEFAULT_TEN_CUC_THONG_KE);

        // Get all the danTocList where tenCucThongKe equals to UPDATED_TEN_CUC_THONG_KE
        defaultDanTocShouldNotBeFound("tenCucThongKe.equals=" + UPDATED_TEN_CUC_THONG_KE);
    }

    @Test
    @Transactional
    public void getAllDanTocsByTenCucThongKeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where tenCucThongKe not equals to DEFAULT_TEN_CUC_THONG_KE
        defaultDanTocShouldNotBeFound("tenCucThongKe.notEquals=" + DEFAULT_TEN_CUC_THONG_KE);

        // Get all the danTocList where tenCucThongKe not equals to UPDATED_TEN_CUC_THONG_KE
        defaultDanTocShouldBeFound("tenCucThongKe.notEquals=" + UPDATED_TEN_CUC_THONG_KE);
    }

    @Test
    @Transactional
    public void getAllDanTocsByTenCucThongKeIsInShouldWork() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where tenCucThongKe in DEFAULT_TEN_CUC_THONG_KE or UPDATED_TEN_CUC_THONG_KE
        defaultDanTocShouldBeFound("tenCucThongKe.in=" + DEFAULT_TEN_CUC_THONG_KE + "," + UPDATED_TEN_CUC_THONG_KE);

        // Get all the danTocList where tenCucThongKe equals to UPDATED_TEN_CUC_THONG_KE
        defaultDanTocShouldNotBeFound("tenCucThongKe.in=" + UPDATED_TEN_CUC_THONG_KE);
    }

    @Test
    @Transactional
    public void getAllDanTocsByTenCucThongKeIsNullOrNotNull() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where tenCucThongKe is not null
        defaultDanTocShouldBeFound("tenCucThongKe.specified=true");

        // Get all the danTocList where tenCucThongKe is null
        defaultDanTocShouldNotBeFound("tenCucThongKe.specified=false");
    }
                @Test
    @Transactional
    public void getAllDanTocsByTenCucThongKeContainsSomething() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where tenCucThongKe contains DEFAULT_TEN_CUC_THONG_KE
        defaultDanTocShouldBeFound("tenCucThongKe.contains=" + DEFAULT_TEN_CUC_THONG_KE);

        // Get all the danTocList where tenCucThongKe contains UPDATED_TEN_CUC_THONG_KE
        defaultDanTocShouldNotBeFound("tenCucThongKe.contains=" + UPDATED_TEN_CUC_THONG_KE);
    }

    @Test
    @Transactional
    public void getAllDanTocsByTenCucThongKeNotContainsSomething() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        // Get all the danTocList where tenCucThongKe does not contain DEFAULT_TEN_CUC_THONG_KE
        defaultDanTocShouldNotBeFound("tenCucThongKe.doesNotContain=" + DEFAULT_TEN_CUC_THONG_KE);

        // Get all the danTocList where tenCucThongKe does not contain UPDATED_TEN_CUC_THONG_KE
        defaultDanTocShouldBeFound("tenCucThongKe.doesNotContain=" + UPDATED_TEN_CUC_THONG_KE);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDanTocShouldBeFound(String filter) throws Exception {
        restDanTocMockMvc.perform(get("/api/dan-tocs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(danToc.getId().intValue())))
            .andExpect(jsonPath("$.[*].ma4069Byt").value(hasItem(DEFAULT_MA_4069_BYT)))
            .andExpect(jsonPath("$.[*].maCucThongKe").value(hasItem(DEFAULT_MA_CUC_THONG_KE)))
            .andExpect(jsonPath("$.[*].ten4069Byt").value(hasItem(DEFAULT_TEN_4069_BYT)))
            .andExpect(jsonPath("$.[*].tenCucThongKe").value(hasItem(DEFAULT_TEN_CUC_THONG_KE)));

        // Check, that the count call also returns 1
        restDanTocMockMvc.perform(get("/api/dan-tocs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDanTocShouldNotBeFound(String filter) throws Exception {
        restDanTocMockMvc.perform(get("/api/dan-tocs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDanTocMockMvc.perform(get("/api/dan-tocs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingDanToc() throws Exception {
        // Get the danToc
        restDanTocMockMvc.perform(get("/api/dan-tocs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDanToc() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        int databaseSizeBeforeUpdate = danTocRepository.findAll().size();

        // Update the danToc
        DanToc updatedDanToc = danTocRepository.findById(danToc.getId()).get();
        // Disconnect from session so that the updates on updatedDanToc are not directly saved in db
        em.detach(updatedDanToc);
        updatedDanToc
            .ma4069Byt(UPDATED_MA_4069_BYT)
            .maCucThongKe(UPDATED_MA_CUC_THONG_KE)
            .ten4069Byt(UPDATED_TEN_4069_BYT)
            .tenCucThongKe(UPDATED_TEN_CUC_THONG_KE);
        DanTocDTO danTocDTO = danTocMapper.toDto(updatedDanToc);

        restDanTocMockMvc.perform(put("/api/dan-tocs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(danTocDTO)))
            .andExpect(status().isOk());

        // Validate the DanToc in the database
        List<DanToc> danTocList = danTocRepository.findAll();
        assertThat(danTocList).hasSize(databaseSizeBeforeUpdate);
        DanToc testDanToc = danTocList.get(danTocList.size() - 1);
        assertThat(testDanToc.getMa4069Byt()).isEqualTo(UPDATED_MA_4069_BYT);
        assertThat(testDanToc.getMaCucThongKe()).isEqualTo(UPDATED_MA_CUC_THONG_KE);
        assertThat(testDanToc.getTen4069Byt()).isEqualTo(UPDATED_TEN_4069_BYT);
        assertThat(testDanToc.getTenCucThongKe()).isEqualTo(UPDATED_TEN_CUC_THONG_KE);
    }

    @Test
    @Transactional
    public void updateNonExistingDanToc() throws Exception {
        int databaseSizeBeforeUpdate = danTocRepository.findAll().size();

        // Create the DanToc
        DanTocDTO danTocDTO = danTocMapper.toDto(danToc);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDanTocMockMvc.perform(put("/api/dan-tocs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(danTocDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DanToc in the database
        List<DanToc> danTocList = danTocRepository.findAll();
        assertThat(danTocList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDanToc() throws Exception {
        // Initialize the database
        danTocRepository.saveAndFlush(danToc);

        int databaseSizeBeforeDelete = danTocRepository.findAll().size();

        // Delete the danToc
        restDanTocMockMvc.perform(delete("/api/dan-tocs/{id}", danToc.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DanToc> danTocList = danTocRepository.findAll();
        assertThat(danTocList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
