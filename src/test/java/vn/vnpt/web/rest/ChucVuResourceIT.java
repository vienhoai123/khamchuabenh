package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.ChucVu;
import vn.vnpt.domain.NhanVien;
import vn.vnpt.repository.ChucVuRepository;
import vn.vnpt.service.ChucVuService;
import vn.vnpt.service.dto.ChucVuDTO;
import vn.vnpt.service.mapper.ChucVuMapper;
import vn.vnpt.service.dto.ChucVuCriteria;
import vn.vnpt.service.ChucVuQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ChucVuResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class ChucVuResourceIT {

    private static final String DEFAULT_MO_TA = "AAAAAAAAAA";
    private static final String UPDATED_MO_TA = "BBBBBBBBBB";

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    @Autowired
    private ChucVuRepository chucVuRepository;

    @Autowired
    private ChucVuMapper chucVuMapper;

    @Autowired
    private ChucVuService chucVuService;

    @Autowired
    private ChucVuQueryService chucVuQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restChucVuMockMvc;

    private ChucVu chucVu;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChucVu createEntity(EntityManager em) {
        ChucVu chucVu = new ChucVu()
            .moTa(DEFAULT_MO_TA)
            .ten(DEFAULT_TEN);
        return chucVu;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChucVu createUpdatedEntity(EntityManager em) {
        ChucVu chucVu = new ChucVu()
            .moTa(UPDATED_MO_TA)
            .ten(UPDATED_TEN);
        return chucVu;
    }

    @BeforeEach
    public void initTest() {
        chucVu = createEntity(em);
    }

    @Test
    @Transactional
    public void createChucVu() throws Exception {
        int databaseSizeBeforeCreate = chucVuRepository.findAll().size();

        // Create the ChucVu
        ChucVuDTO chucVuDTO = chucVuMapper.toDto(chucVu);
        restChucVuMockMvc.perform(post("/api/chuc-vus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chucVuDTO)))
            .andExpect(status().isCreated());

        // Validate the ChucVu in the database
        List<ChucVu> chucVuList = chucVuRepository.findAll();
        assertThat(chucVuList).hasSize(databaseSizeBeforeCreate + 1);
        ChucVu testChucVu = chucVuList.get(chucVuList.size() - 1);
        assertThat(testChucVu.getMoTa()).isEqualTo(DEFAULT_MO_TA);
        assertThat(testChucVu.getTen()).isEqualTo(DEFAULT_TEN);
    }

    @Test
    @Transactional
    public void createChucVuWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = chucVuRepository.findAll().size();

        // Create the ChucVu with an existing ID
        chucVu.setId(1L);
        ChucVuDTO chucVuDTO = chucVuMapper.toDto(chucVu);

        // An entity with an existing ID cannot be created, so this API call must fail
        restChucVuMockMvc.perform(post("/api/chuc-vus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chucVuDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ChucVu in the database
        List<ChucVu> chucVuList = chucVuRepository.findAll();
        assertThat(chucVuList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = chucVuRepository.findAll().size();
        // set the field null
        chucVu.setTen(null);

        // Create the ChucVu, which fails.
        ChucVuDTO chucVuDTO = chucVuMapper.toDto(chucVu);

        restChucVuMockMvc.perform(post("/api/chuc-vus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chucVuDTO)))
            .andExpect(status().isBadRequest());

        List<ChucVu> chucVuList = chucVuRepository.findAll();
        assertThat(chucVuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllChucVus() throws Exception {
        // Initialize the database
        chucVuRepository.saveAndFlush(chucVu);

        // Get all the chucVuList
        restChucVuMockMvc.perform(get("/api/chuc-vus?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chucVu.getId().intValue())))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));
    }
    
    @Test
    @Transactional
    public void getChucVu() throws Exception {
        // Initialize the database
        chucVuRepository.saveAndFlush(chucVu);

        // Get the chucVu
        restChucVuMockMvc.perform(get("/api/chuc-vus/{id}", chucVu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(chucVu.getId().intValue()))
            .andExpect(jsonPath("$.moTa").value(DEFAULT_MO_TA))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN));
    }


    @Test
    @Transactional
    public void getChucVusByIdFiltering() throws Exception {
        // Initialize the database
        chucVuRepository.saveAndFlush(chucVu);

        Long id = chucVu.getId();

        defaultChucVuShouldBeFound("id.equals=" + id);
        defaultChucVuShouldNotBeFound("id.notEquals=" + id);

        defaultChucVuShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultChucVuShouldNotBeFound("id.greaterThan=" + id);

        defaultChucVuShouldBeFound("id.lessThanOrEqual=" + id);
        defaultChucVuShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllChucVusByMoTaIsEqualToSomething() throws Exception {
        // Initialize the database
        chucVuRepository.saveAndFlush(chucVu);

        // Get all the chucVuList where moTa equals to DEFAULT_MO_TA
        defaultChucVuShouldBeFound("moTa.equals=" + DEFAULT_MO_TA);

        // Get all the chucVuList where moTa equals to UPDATED_MO_TA
        defaultChucVuShouldNotBeFound("moTa.equals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllChucVusByMoTaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chucVuRepository.saveAndFlush(chucVu);

        // Get all the chucVuList where moTa not equals to DEFAULT_MO_TA
        defaultChucVuShouldNotBeFound("moTa.notEquals=" + DEFAULT_MO_TA);

        // Get all the chucVuList where moTa not equals to UPDATED_MO_TA
        defaultChucVuShouldBeFound("moTa.notEquals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllChucVusByMoTaIsInShouldWork() throws Exception {
        // Initialize the database
        chucVuRepository.saveAndFlush(chucVu);

        // Get all the chucVuList where moTa in DEFAULT_MO_TA or UPDATED_MO_TA
        defaultChucVuShouldBeFound("moTa.in=" + DEFAULT_MO_TA + "," + UPDATED_MO_TA);

        // Get all the chucVuList where moTa equals to UPDATED_MO_TA
        defaultChucVuShouldNotBeFound("moTa.in=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllChucVusByMoTaIsNullOrNotNull() throws Exception {
        // Initialize the database
        chucVuRepository.saveAndFlush(chucVu);

        // Get all the chucVuList where moTa is not null
        defaultChucVuShouldBeFound("moTa.specified=true");

        // Get all the chucVuList where moTa is null
        defaultChucVuShouldNotBeFound("moTa.specified=false");
    }
                @Test
    @Transactional
    public void getAllChucVusByMoTaContainsSomething() throws Exception {
        // Initialize the database
        chucVuRepository.saveAndFlush(chucVu);

        // Get all the chucVuList where moTa contains DEFAULT_MO_TA
        defaultChucVuShouldBeFound("moTa.contains=" + DEFAULT_MO_TA);

        // Get all the chucVuList where moTa contains UPDATED_MO_TA
        defaultChucVuShouldNotBeFound("moTa.contains=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllChucVusByMoTaNotContainsSomething() throws Exception {
        // Initialize the database
        chucVuRepository.saveAndFlush(chucVu);

        // Get all the chucVuList where moTa does not contain DEFAULT_MO_TA
        defaultChucVuShouldNotBeFound("moTa.doesNotContain=" + DEFAULT_MO_TA);

        // Get all the chucVuList where moTa does not contain UPDATED_MO_TA
        defaultChucVuShouldBeFound("moTa.doesNotContain=" + UPDATED_MO_TA);
    }


    @Test
    @Transactional
    public void getAllChucVusByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        chucVuRepository.saveAndFlush(chucVu);

        // Get all the chucVuList where ten equals to DEFAULT_TEN
        defaultChucVuShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the chucVuList where ten equals to UPDATED_TEN
        defaultChucVuShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChucVusByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chucVuRepository.saveAndFlush(chucVu);

        // Get all the chucVuList where ten not equals to DEFAULT_TEN
        defaultChucVuShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the chucVuList where ten not equals to UPDATED_TEN
        defaultChucVuShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChucVusByTenIsInShouldWork() throws Exception {
        // Initialize the database
        chucVuRepository.saveAndFlush(chucVu);

        // Get all the chucVuList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultChucVuShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the chucVuList where ten equals to UPDATED_TEN
        defaultChucVuShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChucVusByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        chucVuRepository.saveAndFlush(chucVu);

        // Get all the chucVuList where ten is not null
        defaultChucVuShouldBeFound("ten.specified=true");

        // Get all the chucVuList where ten is null
        defaultChucVuShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllChucVusByTenContainsSomething() throws Exception {
        // Initialize the database
        chucVuRepository.saveAndFlush(chucVu);

        // Get all the chucVuList where ten contains DEFAULT_TEN
        defaultChucVuShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the chucVuList where ten contains UPDATED_TEN
        defaultChucVuShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllChucVusByTenNotContainsSomething() throws Exception {
        // Initialize the database
        chucVuRepository.saveAndFlush(chucVu);

        // Get all the chucVuList where ten does not contain DEFAULT_TEN
        defaultChucVuShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the chucVuList where ten does not contain UPDATED_TEN
        defaultChucVuShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllChucVusByNhanVienIsEqualToSomething() throws Exception {
        // Initialize the database
        chucVuRepository.saveAndFlush(chucVu);
        NhanVien nhanVien = NhanVienResourceIT.createEntity(em);
        em.persist(nhanVien);
        em.flush();
        chucVu.addNhanVien(nhanVien);
        chucVuRepository.saveAndFlush(chucVu);
        Long nhanVienId = nhanVien.getId();

        // Get all the chucVuList where nhanVien equals to nhanVienId
        defaultChucVuShouldBeFound("nhanVienId.equals=" + nhanVienId);

        // Get all the chucVuList where nhanVien equals to nhanVienId + 1
        defaultChucVuShouldNotBeFound("nhanVienId.equals=" + (nhanVienId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultChucVuShouldBeFound(String filter) throws Exception {
        restChucVuMockMvc.perform(get("/api/chuc-vus?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chucVu.getId().intValue())))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));

        // Check, that the count call also returns 1
        restChucVuMockMvc.perform(get("/api/chuc-vus/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultChucVuShouldNotBeFound(String filter) throws Exception {
        restChucVuMockMvc.perform(get("/api/chuc-vus?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restChucVuMockMvc.perform(get("/api/chuc-vus/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingChucVu() throws Exception {
        // Get the chucVu
        restChucVuMockMvc.perform(get("/api/chuc-vus/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChucVu() throws Exception {
        // Initialize the database
        chucVuRepository.saveAndFlush(chucVu);

        int databaseSizeBeforeUpdate = chucVuRepository.findAll().size();

        // Update the chucVu
        ChucVu updatedChucVu = chucVuRepository.findById(chucVu.getId()).get();
        // Disconnect from session so that the updates on updatedChucVu are not directly saved in db
        em.detach(updatedChucVu);
        updatedChucVu
            .moTa(UPDATED_MO_TA)
            .ten(UPDATED_TEN);
        ChucVuDTO chucVuDTO = chucVuMapper.toDto(updatedChucVu);

        restChucVuMockMvc.perform(put("/api/chuc-vus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chucVuDTO)))
            .andExpect(status().isOk());

        // Validate the ChucVu in the database
        List<ChucVu> chucVuList = chucVuRepository.findAll();
        assertThat(chucVuList).hasSize(databaseSizeBeforeUpdate);
        ChucVu testChucVu = chucVuList.get(chucVuList.size() - 1);
        assertThat(testChucVu.getMoTa()).isEqualTo(UPDATED_MO_TA);
        assertThat(testChucVu.getTen()).isEqualTo(UPDATED_TEN);
    }

    @Test
    @Transactional
    public void updateNonExistingChucVu() throws Exception {
        int databaseSizeBeforeUpdate = chucVuRepository.findAll().size();

        // Create the ChucVu
        ChucVuDTO chucVuDTO = chucVuMapper.toDto(chucVu);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChucVuMockMvc.perform(put("/api/chuc-vus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(chucVuDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ChucVu in the database
        List<ChucVu> chucVuList = chucVuRepository.findAll();
        assertThat(chucVuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteChucVu() throws Exception {
        // Initialize the database
        chucVuRepository.saveAndFlush(chucVu);

        int databaseSizeBeforeDelete = chucVuRepository.findAll().size();

        // Delete the chucVu
        restChucVuMockMvc.perform(delete("/api/chuc-vus/{id}", chucVu.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ChucVu> chucVuList = chucVuRepository.findAll();
        assertThat(chucVuList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
