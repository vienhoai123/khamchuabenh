package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.NhanVien;
import vn.vnpt.domain.ChucDanh;
import vn.vnpt.domain.UserExtra;
import vn.vnpt.domain.ChucVu;
import vn.vnpt.repository.NhanVienRepository;
import vn.vnpt.service.NhanVienService;
import vn.vnpt.service.dto.NhanVienDTO;
import vn.vnpt.service.mapper.NhanVienMapper;
import vn.vnpt.service.dto.NhanVienCriteria;
import vn.vnpt.service.NhanVienQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link NhanVienResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class NhanVienResourceIT {

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String DEFAULT_CHUNG_CHI_HANH_NGHE = "AAAAAAAAAA";
    private static final String UPDATED_CHUNG_CHI_HANH_NGHE = "BBBBBBBBBB";

    private static final String DEFAULT_CMND = "AAAAAAAAAA";
    private static final String UPDATED_CMND = "BBBBBBBBBB";

    private static final Integer DEFAULT_GIOI_TINH = 1;
    private static final Integer UPDATED_GIOI_TINH = 2;
    private static final Integer SMALLER_GIOI_TINH = 1 - 1;

    private static final LocalDate DEFAULT_NGAY_SINH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_SINH = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_NGAY_SINH = LocalDate.ofEpochDay(-1L);

    @Autowired
    private NhanVienRepository nhanVienRepository;

    @Mock
    private NhanVienRepository nhanVienRepositoryMock;

    @Autowired
    private NhanVienMapper nhanVienMapper;

    @Mock
    private NhanVienService nhanVienServiceMock;

    @Autowired
    private NhanVienService nhanVienService;

    @Autowired
    private NhanVienQueryService nhanVienQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restNhanVienMockMvc;

    private NhanVien nhanVien;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NhanVien createEntity(EntityManager em) {
        NhanVien nhanVien = new NhanVien()
            .ten(DEFAULT_TEN)
            .chungChiHanhNghe(DEFAULT_CHUNG_CHI_HANH_NGHE)
            .cmnd(DEFAULT_CMND)
            .gioiTinh(DEFAULT_GIOI_TINH)
            .ngaySinh(DEFAULT_NGAY_SINH);
        // Add required entity
        ChucDanh chucDanh;
        if (TestUtil.findAll(em, ChucDanh.class).isEmpty()) {
            chucDanh = ChucDanhResourceIT.createEntity(em);
            em.persist(chucDanh);
            em.flush();
        } else {
            chucDanh = TestUtil.findAll(em, ChucDanh.class).get(0);
        }
        nhanVien.setChucDanh(chucDanh);
        // Add required entity
        ChucVu chucVu;
        if (TestUtil.findAll(em, ChucVu.class).isEmpty()) {
            chucVu = ChucVuResourceIT.createEntity(em);
            em.persist(chucVu);
            em.flush();
        } else {
            chucVu = TestUtil.findAll(em, ChucVu.class).get(0);
        }
        nhanVien.getChucVus().add(chucVu);
        return nhanVien;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NhanVien createUpdatedEntity(EntityManager em) {
        NhanVien nhanVien = new NhanVien()
            .ten(UPDATED_TEN)
            .chungChiHanhNghe(UPDATED_CHUNG_CHI_HANH_NGHE)
            .cmnd(UPDATED_CMND)
            .gioiTinh(UPDATED_GIOI_TINH)
            .ngaySinh(UPDATED_NGAY_SINH);
        // Add required entity
        ChucDanh chucDanh;
        if (TestUtil.findAll(em, ChucDanh.class).isEmpty()) {
            chucDanh = ChucDanhResourceIT.createUpdatedEntity(em);
            em.persist(chucDanh);
            em.flush();
        } else {
            chucDanh = TestUtil.findAll(em, ChucDanh.class).get(0);
        }
        nhanVien.setChucDanh(chucDanh);
        // Add required entity
        ChucVu chucVu;
        if (TestUtil.findAll(em, ChucVu.class).isEmpty()) {
            chucVu = ChucVuResourceIT.createUpdatedEntity(em);
            em.persist(chucVu);
            em.flush();
        } else {
            chucVu = TestUtil.findAll(em, ChucVu.class).get(0);
        }
        nhanVien.getChucVus().add(chucVu);
        return nhanVien;
    }

    @BeforeEach
    public void initTest() {
        nhanVien = createEntity(em);
    }

    @Test
    @Transactional
    public void createNhanVien() throws Exception {
        int databaseSizeBeforeCreate = nhanVienRepository.findAll().size();
        // Create the NhanVien
        NhanVienDTO nhanVienDTO = nhanVienMapper.toDto(nhanVien);
        restNhanVienMockMvc.perform(post("/api/nhan-viens").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhanVienDTO)))
            .andExpect(status().isCreated());

        // Validate the NhanVien in the database
        List<NhanVien> nhanVienList = nhanVienRepository.findAll();
        assertThat(nhanVienList).hasSize(databaseSizeBeforeCreate + 1);
        NhanVien testNhanVien = nhanVienList.get(nhanVienList.size() - 1);
        assertThat(testNhanVien.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testNhanVien.getChungChiHanhNghe()).isEqualTo(DEFAULT_CHUNG_CHI_HANH_NGHE);
        assertThat(testNhanVien.getCmnd()).isEqualTo(DEFAULT_CMND);
        assertThat(testNhanVien.getGioiTinh()).isEqualTo(DEFAULT_GIOI_TINH);
        assertThat(testNhanVien.getNgaySinh()).isEqualTo(DEFAULT_NGAY_SINH);
    }

    @Test
    @Transactional
    public void createNhanVienWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = nhanVienRepository.findAll().size();

        // Create the NhanVien with an existing ID
        nhanVien.setId(1L);
        NhanVienDTO nhanVienDTO = nhanVienMapper.toDto(nhanVien);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNhanVienMockMvc.perform(post("/api/nhan-viens").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhanVienDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NhanVien in the database
        List<NhanVien> nhanVienList = nhanVienRepository.findAll();
        assertThat(nhanVienList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = nhanVienRepository.findAll().size();
        // set the field null
        nhanVien.setTen(null);

        // Create the NhanVien, which fails.
        NhanVienDTO nhanVienDTO = nhanVienMapper.toDto(nhanVien);


        restNhanVienMockMvc.perform(post("/api/nhan-viens").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhanVienDTO)))
            .andExpect(status().isBadRequest());

        List<NhanVien> nhanVienList = nhanVienRepository.findAll();
        assertThat(nhanVienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkChungChiHanhNgheIsRequired() throws Exception {
        int databaseSizeBeforeTest = nhanVienRepository.findAll().size();
        // set the field null
        nhanVien.setChungChiHanhNghe(null);

        // Create the NhanVien, which fails.
        NhanVienDTO nhanVienDTO = nhanVienMapper.toDto(nhanVien);


        restNhanVienMockMvc.perform(post("/api/nhan-viens").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhanVienDTO)))
            .andExpect(status().isBadRequest());

        List<NhanVien> nhanVienList = nhanVienRepository.findAll();
        assertThat(nhanVienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCmndIsRequired() throws Exception {
        int databaseSizeBeforeTest = nhanVienRepository.findAll().size();
        // set the field null
        nhanVien.setCmnd(null);

        // Create the NhanVien, which fails.
        NhanVienDTO nhanVienDTO = nhanVienMapper.toDto(nhanVien);


        restNhanVienMockMvc.perform(post("/api/nhan-viens").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhanVienDTO)))
            .andExpect(status().isBadRequest());

        List<NhanVien> nhanVienList = nhanVienRepository.findAll();
        assertThat(nhanVienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGioiTinhIsRequired() throws Exception {
        int databaseSizeBeforeTest = nhanVienRepository.findAll().size();
        // set the field null
        nhanVien.setGioiTinh(null);

        // Create the NhanVien, which fails.
        NhanVienDTO nhanVienDTO = nhanVienMapper.toDto(nhanVien);


        restNhanVienMockMvc.perform(post("/api/nhan-viens").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhanVienDTO)))
            .andExpect(status().isBadRequest());

        List<NhanVien> nhanVienList = nhanVienRepository.findAll();
        assertThat(nhanVienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNgaySinhIsRequired() throws Exception {
        int databaseSizeBeforeTest = nhanVienRepository.findAll().size();
        // set the field null
        nhanVien.setNgaySinh(null);

        // Create the NhanVien, which fails.
        NhanVienDTO nhanVienDTO = nhanVienMapper.toDto(nhanVien);


        restNhanVienMockMvc.perform(post("/api/nhan-viens").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhanVienDTO)))
            .andExpect(status().isBadRequest());

        List<NhanVien> nhanVienList = nhanVienRepository.findAll();
        assertThat(nhanVienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNhanViens() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList
        restNhanVienMockMvc.perform(get("/api/nhan-viens?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nhanVien.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].chungChiHanhNghe").value(hasItem(DEFAULT_CHUNG_CHI_HANH_NGHE)))
            .andExpect(jsonPath("$.[*].cmnd").value(hasItem(DEFAULT_CMND)))
            .andExpect(jsonPath("$.[*].gioiTinh").value(hasItem(DEFAULT_GIOI_TINH)))
            .andExpect(jsonPath("$.[*].ngaySinh").value(hasItem(DEFAULT_NGAY_SINH.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllNhanViensWithEagerRelationshipsIsEnabled() throws Exception {
        when(nhanVienServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restNhanVienMockMvc.perform(get("/api/nhan-viens?eagerload=true"))
            .andExpect(status().isOk());

        verify(nhanVienServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllNhanViensWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(nhanVienServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restNhanVienMockMvc.perform(get("/api/nhan-viens?eagerload=true"))
            .andExpect(status().isOk());

        verify(nhanVienServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getNhanVien() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get the nhanVien
        restNhanVienMockMvc.perform(get("/api/nhan-viens/{id}", nhanVien.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(nhanVien.getId().intValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.chungChiHanhNghe").value(DEFAULT_CHUNG_CHI_HANH_NGHE))
            .andExpect(jsonPath("$.cmnd").value(DEFAULT_CMND))
            .andExpect(jsonPath("$.gioiTinh").value(DEFAULT_GIOI_TINH))
            .andExpect(jsonPath("$.ngaySinh").value(DEFAULT_NGAY_SINH.toString()));
    }


    @Test
    @Transactional
    public void getNhanViensByIdFiltering() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        Long id = nhanVien.getId();

        defaultNhanVienShouldBeFound("id.equals=" + id);
        defaultNhanVienShouldNotBeFound("id.notEquals=" + id);

        defaultNhanVienShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultNhanVienShouldNotBeFound("id.greaterThan=" + id);

        defaultNhanVienShouldBeFound("id.lessThanOrEqual=" + id);
        defaultNhanVienShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllNhanViensByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where ten equals to DEFAULT_TEN
        defaultNhanVienShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the nhanVienList where ten equals to UPDATED_TEN
        defaultNhanVienShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhanViensByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where ten not equals to DEFAULT_TEN
        defaultNhanVienShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the nhanVienList where ten not equals to UPDATED_TEN
        defaultNhanVienShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhanViensByTenIsInShouldWork() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultNhanVienShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the nhanVienList where ten equals to UPDATED_TEN
        defaultNhanVienShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhanViensByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where ten is not null
        defaultNhanVienShouldBeFound("ten.specified=true");

        // Get all the nhanVienList where ten is null
        defaultNhanVienShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllNhanViensByTenContainsSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where ten contains DEFAULT_TEN
        defaultNhanVienShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the nhanVienList where ten contains UPDATED_TEN
        defaultNhanVienShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhanViensByTenNotContainsSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where ten does not contain DEFAULT_TEN
        defaultNhanVienShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the nhanVienList where ten does not contain UPDATED_TEN
        defaultNhanVienShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllNhanViensByChungChiHanhNgheIsEqualToSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where chungChiHanhNghe equals to DEFAULT_CHUNG_CHI_HANH_NGHE
        defaultNhanVienShouldBeFound("chungChiHanhNghe.equals=" + DEFAULT_CHUNG_CHI_HANH_NGHE);

        // Get all the nhanVienList where chungChiHanhNghe equals to UPDATED_CHUNG_CHI_HANH_NGHE
        defaultNhanVienShouldNotBeFound("chungChiHanhNghe.equals=" + UPDATED_CHUNG_CHI_HANH_NGHE);
    }

    @Test
    @Transactional
    public void getAllNhanViensByChungChiHanhNgheIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where chungChiHanhNghe not equals to DEFAULT_CHUNG_CHI_HANH_NGHE
        defaultNhanVienShouldNotBeFound("chungChiHanhNghe.notEquals=" + DEFAULT_CHUNG_CHI_HANH_NGHE);

        // Get all the nhanVienList where chungChiHanhNghe not equals to UPDATED_CHUNG_CHI_HANH_NGHE
        defaultNhanVienShouldBeFound("chungChiHanhNghe.notEquals=" + UPDATED_CHUNG_CHI_HANH_NGHE);
    }

    @Test
    @Transactional
    public void getAllNhanViensByChungChiHanhNgheIsInShouldWork() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where chungChiHanhNghe in DEFAULT_CHUNG_CHI_HANH_NGHE or UPDATED_CHUNG_CHI_HANH_NGHE
        defaultNhanVienShouldBeFound("chungChiHanhNghe.in=" + DEFAULT_CHUNG_CHI_HANH_NGHE + "," + UPDATED_CHUNG_CHI_HANH_NGHE);

        // Get all the nhanVienList where chungChiHanhNghe equals to UPDATED_CHUNG_CHI_HANH_NGHE
        defaultNhanVienShouldNotBeFound("chungChiHanhNghe.in=" + UPDATED_CHUNG_CHI_HANH_NGHE);
    }

    @Test
    @Transactional
    public void getAllNhanViensByChungChiHanhNgheIsNullOrNotNull() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where chungChiHanhNghe is not null
        defaultNhanVienShouldBeFound("chungChiHanhNghe.specified=true");

        // Get all the nhanVienList where chungChiHanhNghe is null
        defaultNhanVienShouldNotBeFound("chungChiHanhNghe.specified=false");
    }
                @Test
    @Transactional
    public void getAllNhanViensByChungChiHanhNgheContainsSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where chungChiHanhNghe contains DEFAULT_CHUNG_CHI_HANH_NGHE
        defaultNhanVienShouldBeFound("chungChiHanhNghe.contains=" + DEFAULT_CHUNG_CHI_HANH_NGHE);

        // Get all the nhanVienList where chungChiHanhNghe contains UPDATED_CHUNG_CHI_HANH_NGHE
        defaultNhanVienShouldNotBeFound("chungChiHanhNghe.contains=" + UPDATED_CHUNG_CHI_HANH_NGHE);
    }

    @Test
    @Transactional
    public void getAllNhanViensByChungChiHanhNgheNotContainsSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where chungChiHanhNghe does not contain DEFAULT_CHUNG_CHI_HANH_NGHE
        defaultNhanVienShouldNotBeFound("chungChiHanhNghe.doesNotContain=" + DEFAULT_CHUNG_CHI_HANH_NGHE);

        // Get all the nhanVienList where chungChiHanhNghe does not contain UPDATED_CHUNG_CHI_HANH_NGHE
        defaultNhanVienShouldBeFound("chungChiHanhNghe.doesNotContain=" + UPDATED_CHUNG_CHI_HANH_NGHE);
    }


    @Test
    @Transactional
    public void getAllNhanViensByCmndIsEqualToSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where cmnd equals to DEFAULT_CMND
        defaultNhanVienShouldBeFound("cmnd.equals=" + DEFAULT_CMND);

        // Get all the nhanVienList where cmnd equals to UPDATED_CMND
        defaultNhanVienShouldNotBeFound("cmnd.equals=" + UPDATED_CMND);
    }

    @Test
    @Transactional
    public void getAllNhanViensByCmndIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where cmnd not equals to DEFAULT_CMND
        defaultNhanVienShouldNotBeFound("cmnd.notEquals=" + DEFAULT_CMND);

        // Get all the nhanVienList where cmnd not equals to UPDATED_CMND
        defaultNhanVienShouldBeFound("cmnd.notEquals=" + UPDATED_CMND);
    }

    @Test
    @Transactional
    public void getAllNhanViensByCmndIsInShouldWork() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where cmnd in DEFAULT_CMND or UPDATED_CMND
        defaultNhanVienShouldBeFound("cmnd.in=" + DEFAULT_CMND + "," + UPDATED_CMND);

        // Get all the nhanVienList where cmnd equals to UPDATED_CMND
        defaultNhanVienShouldNotBeFound("cmnd.in=" + UPDATED_CMND);
    }

    @Test
    @Transactional
    public void getAllNhanViensByCmndIsNullOrNotNull() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where cmnd is not null
        defaultNhanVienShouldBeFound("cmnd.specified=true");

        // Get all the nhanVienList where cmnd is null
        defaultNhanVienShouldNotBeFound("cmnd.specified=false");
    }
                @Test
    @Transactional
    public void getAllNhanViensByCmndContainsSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where cmnd contains DEFAULT_CMND
        defaultNhanVienShouldBeFound("cmnd.contains=" + DEFAULT_CMND);

        // Get all the nhanVienList where cmnd contains UPDATED_CMND
        defaultNhanVienShouldNotBeFound("cmnd.contains=" + UPDATED_CMND);
    }

    @Test
    @Transactional
    public void getAllNhanViensByCmndNotContainsSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where cmnd does not contain DEFAULT_CMND
        defaultNhanVienShouldNotBeFound("cmnd.doesNotContain=" + DEFAULT_CMND);

        // Get all the nhanVienList where cmnd does not contain UPDATED_CMND
        defaultNhanVienShouldBeFound("cmnd.doesNotContain=" + UPDATED_CMND);
    }


    @Test
    @Transactional
    public void getAllNhanViensByGioiTinhIsEqualToSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where gioiTinh equals to DEFAULT_GIOI_TINH
        defaultNhanVienShouldBeFound("gioiTinh.equals=" + DEFAULT_GIOI_TINH);

        // Get all the nhanVienList where gioiTinh equals to UPDATED_GIOI_TINH
        defaultNhanVienShouldNotBeFound("gioiTinh.equals=" + UPDATED_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllNhanViensByGioiTinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where gioiTinh not equals to DEFAULT_GIOI_TINH
        defaultNhanVienShouldNotBeFound("gioiTinh.notEquals=" + DEFAULT_GIOI_TINH);

        // Get all the nhanVienList where gioiTinh not equals to UPDATED_GIOI_TINH
        defaultNhanVienShouldBeFound("gioiTinh.notEquals=" + UPDATED_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllNhanViensByGioiTinhIsInShouldWork() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where gioiTinh in DEFAULT_GIOI_TINH or UPDATED_GIOI_TINH
        defaultNhanVienShouldBeFound("gioiTinh.in=" + DEFAULT_GIOI_TINH + "," + UPDATED_GIOI_TINH);

        // Get all the nhanVienList where gioiTinh equals to UPDATED_GIOI_TINH
        defaultNhanVienShouldNotBeFound("gioiTinh.in=" + UPDATED_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllNhanViensByGioiTinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where gioiTinh is not null
        defaultNhanVienShouldBeFound("gioiTinh.specified=true");

        // Get all the nhanVienList where gioiTinh is null
        defaultNhanVienShouldNotBeFound("gioiTinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllNhanViensByGioiTinhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where gioiTinh is greater than or equal to DEFAULT_GIOI_TINH
        defaultNhanVienShouldBeFound("gioiTinh.greaterThanOrEqual=" + DEFAULT_GIOI_TINH);

        // Get all the nhanVienList where gioiTinh is greater than or equal to UPDATED_GIOI_TINH
        defaultNhanVienShouldNotBeFound("gioiTinh.greaterThanOrEqual=" + UPDATED_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllNhanViensByGioiTinhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where gioiTinh is less than or equal to DEFAULT_GIOI_TINH
        defaultNhanVienShouldBeFound("gioiTinh.lessThanOrEqual=" + DEFAULT_GIOI_TINH);

        // Get all the nhanVienList where gioiTinh is less than or equal to SMALLER_GIOI_TINH
        defaultNhanVienShouldNotBeFound("gioiTinh.lessThanOrEqual=" + SMALLER_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllNhanViensByGioiTinhIsLessThanSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where gioiTinh is less than DEFAULT_GIOI_TINH
        defaultNhanVienShouldNotBeFound("gioiTinh.lessThan=" + DEFAULT_GIOI_TINH);

        // Get all the nhanVienList where gioiTinh is less than UPDATED_GIOI_TINH
        defaultNhanVienShouldBeFound("gioiTinh.lessThan=" + UPDATED_GIOI_TINH);
    }

    @Test
    @Transactional
    public void getAllNhanViensByGioiTinhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where gioiTinh is greater than DEFAULT_GIOI_TINH
        defaultNhanVienShouldNotBeFound("gioiTinh.greaterThan=" + DEFAULT_GIOI_TINH);

        // Get all the nhanVienList where gioiTinh is greater than SMALLER_GIOI_TINH
        defaultNhanVienShouldBeFound("gioiTinh.greaterThan=" + SMALLER_GIOI_TINH);
    }


    @Test
    @Transactional
    public void getAllNhanViensByNgaySinhIsEqualToSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where ngaySinh equals to DEFAULT_NGAY_SINH
        defaultNhanVienShouldBeFound("ngaySinh.equals=" + DEFAULT_NGAY_SINH);

        // Get all the nhanVienList where ngaySinh equals to UPDATED_NGAY_SINH
        defaultNhanVienShouldNotBeFound("ngaySinh.equals=" + UPDATED_NGAY_SINH);
    }

    @Test
    @Transactional
    public void getAllNhanViensByNgaySinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where ngaySinh not equals to DEFAULT_NGAY_SINH
        defaultNhanVienShouldNotBeFound("ngaySinh.notEquals=" + DEFAULT_NGAY_SINH);

        // Get all the nhanVienList where ngaySinh not equals to UPDATED_NGAY_SINH
        defaultNhanVienShouldBeFound("ngaySinh.notEquals=" + UPDATED_NGAY_SINH);
    }

    @Test
    @Transactional
    public void getAllNhanViensByNgaySinhIsInShouldWork() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where ngaySinh in DEFAULT_NGAY_SINH or UPDATED_NGAY_SINH
        defaultNhanVienShouldBeFound("ngaySinh.in=" + DEFAULT_NGAY_SINH + "," + UPDATED_NGAY_SINH);

        // Get all the nhanVienList where ngaySinh equals to UPDATED_NGAY_SINH
        defaultNhanVienShouldNotBeFound("ngaySinh.in=" + UPDATED_NGAY_SINH);
    }

    @Test
    @Transactional
    public void getAllNhanViensByNgaySinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where ngaySinh is not null
        defaultNhanVienShouldBeFound("ngaySinh.specified=true");

        // Get all the nhanVienList where ngaySinh is null
        defaultNhanVienShouldNotBeFound("ngaySinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllNhanViensByNgaySinhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where ngaySinh is greater than or equal to DEFAULT_NGAY_SINH
        defaultNhanVienShouldBeFound("ngaySinh.greaterThanOrEqual=" + DEFAULT_NGAY_SINH);

        // Get all the nhanVienList where ngaySinh is greater than or equal to UPDATED_NGAY_SINH
        defaultNhanVienShouldNotBeFound("ngaySinh.greaterThanOrEqual=" + UPDATED_NGAY_SINH);
    }

    @Test
    @Transactional
    public void getAllNhanViensByNgaySinhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where ngaySinh is less than or equal to DEFAULT_NGAY_SINH
        defaultNhanVienShouldBeFound("ngaySinh.lessThanOrEqual=" + DEFAULT_NGAY_SINH);

        // Get all the nhanVienList where ngaySinh is less than or equal to SMALLER_NGAY_SINH
        defaultNhanVienShouldNotBeFound("ngaySinh.lessThanOrEqual=" + SMALLER_NGAY_SINH);
    }

    @Test
    @Transactional
    public void getAllNhanViensByNgaySinhIsLessThanSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where ngaySinh is less than DEFAULT_NGAY_SINH
        defaultNhanVienShouldNotBeFound("ngaySinh.lessThan=" + DEFAULT_NGAY_SINH);

        // Get all the nhanVienList where ngaySinh is less than UPDATED_NGAY_SINH
        defaultNhanVienShouldBeFound("ngaySinh.lessThan=" + UPDATED_NGAY_SINH);
    }

    @Test
    @Transactional
    public void getAllNhanViensByNgaySinhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        // Get all the nhanVienList where ngaySinh is greater than DEFAULT_NGAY_SINH
        defaultNhanVienShouldNotBeFound("ngaySinh.greaterThan=" + DEFAULT_NGAY_SINH);

        // Get all the nhanVienList where ngaySinh is greater than SMALLER_NGAY_SINH
        defaultNhanVienShouldBeFound("ngaySinh.greaterThan=" + SMALLER_NGAY_SINH);
    }


    @Test
    @Transactional
    public void getAllNhanViensByChucDanhIsEqualToSomething() throws Exception {
        // Get already existing entity
        ChucDanh chucDanh = nhanVien.getChucDanh();
        nhanVienRepository.saveAndFlush(nhanVien);
        Long chucDanhId = chucDanh.getId();

        // Get all the nhanVienList where chucDanh equals to chucDanhId
        defaultNhanVienShouldBeFound("chucDanhId.equals=" + chucDanhId);

        // Get all the nhanVienList where chucDanh equals to chucDanhId + 1
        defaultNhanVienShouldNotBeFound("chucDanhId.equals=" + (chucDanhId + 1));
    }


    @Test
    @Transactional
    public void getAllNhanViensByUserExtraIsEqualToSomething() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);
        UserExtra userExtra = UserExtraResourceIT.createEntity(em);
        em.persist(userExtra);
        em.flush();
        nhanVien.setUserExtra(userExtra);
        nhanVienRepository.saveAndFlush(nhanVien);
        Long userExtraId = userExtra.getId();

        // Get all the nhanVienList where userExtra equals to userExtraId
        defaultNhanVienShouldBeFound("userExtraId.equals=" + userExtraId);

        // Get all the nhanVienList where userExtra equals to userExtraId + 1
        defaultNhanVienShouldNotBeFound("userExtraId.equals=" + (userExtraId + 1));
    }


    @Test
    @Transactional
    public void getAllNhanViensByChucVuIsEqualToSomething() throws Exception {
        // Get already existing entity
        ChucVu chucVu = nhanVien.getChucVu();
        nhanVienRepository.saveAndFlush(nhanVien);
        Long chucVuId = chucVu.getId();

        // Get all the nhanVienList where chucVu equals to chucVuId
        defaultNhanVienShouldBeFound("chucVuId.equals=" + chucVuId);

        // Get all the nhanVienList where chucVu equals to chucVuId + 1
        defaultNhanVienShouldNotBeFound("chucVuId.equals=" + (chucVuId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultNhanVienShouldBeFound(String filter) throws Exception {
        restNhanVienMockMvc.perform(get("/api/nhan-viens?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nhanVien.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].chungChiHanhNghe").value(hasItem(DEFAULT_CHUNG_CHI_HANH_NGHE)))
            .andExpect(jsonPath("$.[*].cmnd").value(hasItem(DEFAULT_CMND)))
            .andExpect(jsonPath("$.[*].gioiTinh").value(hasItem(DEFAULT_GIOI_TINH)))
            .andExpect(jsonPath("$.[*].ngaySinh").value(hasItem(DEFAULT_NGAY_SINH.toString())));

        // Check, that the count call also returns 1
        restNhanVienMockMvc.perform(get("/api/nhan-viens/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultNhanVienShouldNotBeFound(String filter) throws Exception {
        restNhanVienMockMvc.perform(get("/api/nhan-viens?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restNhanVienMockMvc.perform(get("/api/nhan-viens/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingNhanVien() throws Exception {
        // Get the nhanVien
        restNhanVienMockMvc.perform(get("/api/nhan-viens/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNhanVien() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        int databaseSizeBeforeUpdate = nhanVienRepository.findAll().size();

        // Update the nhanVien
        NhanVien updatedNhanVien = nhanVienRepository.findById(nhanVien.getId()).get();
        // Disconnect from session so that the updates on updatedNhanVien are not directly saved in db
        em.detach(updatedNhanVien);
        updatedNhanVien
            .ten(UPDATED_TEN)
            .chungChiHanhNghe(UPDATED_CHUNG_CHI_HANH_NGHE)
            .cmnd(UPDATED_CMND)
            .gioiTinh(UPDATED_GIOI_TINH)
            .ngaySinh(UPDATED_NGAY_SINH);
        NhanVienDTO nhanVienDTO = nhanVienMapper.toDto(updatedNhanVien);

        restNhanVienMockMvc.perform(put("/api/nhan-viens").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhanVienDTO)))
            .andExpect(status().isOk());

        // Validate the NhanVien in the database
        List<NhanVien> nhanVienList = nhanVienRepository.findAll();
        assertThat(nhanVienList).hasSize(databaseSizeBeforeUpdate);
        NhanVien testNhanVien = nhanVienList.get(nhanVienList.size() - 1);
        assertThat(testNhanVien.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testNhanVien.getChungChiHanhNghe()).isEqualTo(UPDATED_CHUNG_CHI_HANH_NGHE);
        assertThat(testNhanVien.getCmnd()).isEqualTo(UPDATED_CMND);
        assertThat(testNhanVien.getGioiTinh()).isEqualTo(UPDATED_GIOI_TINH);
        assertThat(testNhanVien.getNgaySinh()).isEqualTo(UPDATED_NGAY_SINH);
    }

    @Test
    @Transactional
    public void updateNonExistingNhanVien() throws Exception {
        int databaseSizeBeforeUpdate = nhanVienRepository.findAll().size();

        // Create the NhanVien
        NhanVienDTO nhanVienDTO = nhanVienMapper.toDto(nhanVien);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNhanVienMockMvc.perform(put("/api/nhan-viens").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhanVienDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NhanVien in the database
        List<NhanVien> nhanVienList = nhanVienRepository.findAll();
        assertThat(nhanVienList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNhanVien() throws Exception {
        // Initialize the database
        nhanVienRepository.saveAndFlush(nhanVien);

        int databaseSizeBeforeDelete = nhanVienRepository.findAll().size();

        // Delete the nhanVien
        restNhanVienMockMvc.perform(delete("/api/nhan-viens/{id}", nhanVien.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<NhanVien> nhanVienList = nhanVienRepository.findAll();
        assertThat(nhanVienList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
