package vn.vnpt.web.rest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.DonVi;
import vn.vnpt.domain.DotThayDoiMaDichVu;
import vn.vnpt.repository.DotThayDoiMaDichVuRepository;
import vn.vnpt.service.DotThayDoiMaDichVuQueryService;
import vn.vnpt.service.DotThayDoiMaDichVuService;
import vn.vnpt.service.dto.DotThayDoiMaDichVuDTO;
import vn.vnpt.service.mapper.DotThayDoiMaDichVuMapper;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DotThayDoiMaDichVuResource} REST controller.
 */
@SpringBootTest(classes = {KhamchuabenhApp.class, TestSecurityConfiguration.class})

@AutoConfigureMockMvc
@WithMockUser
public class DotThayDoiMaDichVuResourceIT {

    private static final Integer DEFAULT_DOT_TUONG_AP_DUNG = 1;
    private static final Integer UPDATED_DOT_TUONG_AP_DUNG = 2;
    private static final Integer SMALLER_DOT_TUONG_AP_DUNG = 1 - 1;

    private static final String DEFAULT_GHI_CHU = "AAAAAAAAAA";
    private static final String UPDATED_GHI_CHU = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_NGAY_AP_DUNG = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NGAY_AP_DUNG = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_NGAY_AP_DUNG = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    @Autowired
    private DotThayDoiMaDichVuRepository dotThayDoiMaDichVuRepository;

    @Autowired
    private DotThayDoiMaDichVuMapper dotThayDoiMaDichVuMapper;

    @Autowired
    private DotThayDoiMaDichVuService dotThayDoiMaDichVuService;

    @Autowired
    private DotThayDoiMaDichVuQueryService dotThayDoiMaDichVuQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDotThayDoiMaDichVuMockMvc;

    private DotThayDoiMaDichVu dotThayDoiMaDichVu;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DotThayDoiMaDichVu createEntity(EntityManager em) {
        DotThayDoiMaDichVu dotThayDoiMaDichVu = new DotThayDoiMaDichVu()
            .dotTuongApDung(DEFAULT_DOT_TUONG_AP_DUNG)
            .ghiChu(DEFAULT_GHI_CHU)
            .ngayApDung(DEFAULT_NGAY_AP_DUNG)
            .ten(DEFAULT_TEN);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        dotThayDoiMaDichVu.setDonVi(donVi);
        return dotThayDoiMaDichVu;
    }

    /**
     * Create an updated entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DotThayDoiMaDichVu createUpdatedEntity(EntityManager em) {
        DotThayDoiMaDichVu dotThayDoiMaDichVu = new DotThayDoiMaDichVu()
            .dotTuongApDung(UPDATED_DOT_TUONG_AP_DUNG)
            .ghiChu(UPDATED_GHI_CHU)
            .ngayApDung(UPDATED_NGAY_AP_DUNG)
            .ten(UPDATED_TEN);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createUpdatedEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        dotThayDoiMaDichVu.setDonVi(donVi);
        return dotThayDoiMaDichVu;
    }

    @BeforeEach
    public void initTest() {
        dotThayDoiMaDichVu = createEntity(em);
    }

    @Test
    @Transactional
    public void createDotThayDoiMaDichVu() throws Exception {
        int databaseSizeBeforeCreate = dotThayDoiMaDichVuRepository.findAll().size();

        // Create the DotThayDoiMaDichVu
        DotThayDoiMaDichVuDTO dotThayDoiMaDichVuDTO = dotThayDoiMaDichVuMapper.toDto(dotThayDoiMaDichVu);
        restDotThayDoiMaDichVuMockMvc.perform(post("/api/dot-thay-doi-ma-dich-vus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dotThayDoiMaDichVuDTO)))
            .andExpect(status().isCreated());

        // Validate the DotThayDoiMaDichVu in the database
        List<DotThayDoiMaDichVu> dotThayDoiMaDichVuList = dotThayDoiMaDichVuRepository.findAll();
        assertThat(dotThayDoiMaDichVuList).hasSize(databaseSizeBeforeCreate + 1);
        DotThayDoiMaDichVu testDotThayDoiMaDichVu = dotThayDoiMaDichVuList.get(dotThayDoiMaDichVuList.size() - 1);
        assertThat(testDotThayDoiMaDichVu.getDotTuongApDung()).isEqualTo(DEFAULT_DOT_TUONG_AP_DUNG);
        assertThat(testDotThayDoiMaDichVu.getGhiChu()).isEqualTo(DEFAULT_GHI_CHU);
        assertThat(testDotThayDoiMaDichVu.getNgayApDung()).isEqualTo(DEFAULT_NGAY_AP_DUNG);
        assertThat(testDotThayDoiMaDichVu.getTen()).isEqualTo(DEFAULT_TEN);
    }

    @Test
    @Transactional
    public void createDotThayDoiMaDichVuWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dotThayDoiMaDichVuRepository.findAll().size();

        // Create the DotThayDoiMaDichVu with an existing ID
        dotThayDoiMaDichVu.setId(1L);
        DotThayDoiMaDichVuDTO dotThayDoiMaDichVuDTO = dotThayDoiMaDichVuMapper.toDto(dotThayDoiMaDichVu);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDotThayDoiMaDichVuMockMvc.perform(post("/api/dot-thay-doi-ma-dich-vus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dotThayDoiMaDichVuDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DotThayDoiMaDichVu in the database
        List<DotThayDoiMaDichVu> dotThayDoiMaDichVuList = dotThayDoiMaDichVuRepository.findAll();
        assertThat(dotThayDoiMaDichVuList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNgayApDungIsRequired() throws Exception {
        int databaseSizeBeforeTest = dotThayDoiMaDichVuRepository.findAll().size();
        // set the field null
        dotThayDoiMaDichVu.setNgayApDung(null);

        // Create the DotThayDoiMaDichVu, which fails.
        DotThayDoiMaDichVuDTO dotThayDoiMaDichVuDTO = dotThayDoiMaDichVuMapper.toDto(dotThayDoiMaDichVu);

        restDotThayDoiMaDichVuMockMvc.perform(post("/api/dot-thay-doi-ma-dich-vus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dotThayDoiMaDichVuDTO)))
            .andExpect(status().isBadRequest());

        List<DotThayDoiMaDichVu> dotThayDoiMaDichVuList = dotThayDoiMaDichVuRepository.findAll();
        assertThat(dotThayDoiMaDichVuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = dotThayDoiMaDichVuRepository.findAll().size();
        // set the field null
        dotThayDoiMaDichVu.setTen(null);

        // Create the DotThayDoiMaDichVu, which fails.
        DotThayDoiMaDichVuDTO dotThayDoiMaDichVuDTO = dotThayDoiMaDichVuMapper.toDto(dotThayDoiMaDichVu);

        restDotThayDoiMaDichVuMockMvc.perform(post("/api/dot-thay-doi-ma-dich-vus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dotThayDoiMaDichVuDTO)))
            .andExpect(status().isBadRequest());

        List<DotThayDoiMaDichVu> dotThayDoiMaDichVuList = dotThayDoiMaDichVuRepository.findAll();
        assertThat(dotThayDoiMaDichVuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVus() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList
        restDotThayDoiMaDichVuMockMvc.perform(get("/api/dot-thay-doi-ma-dich-vus?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dotThayDoiMaDichVu.getId().intValue())))
            .andExpect(jsonPath("$.[*].dotTuongApDung").value(hasItem(DEFAULT_DOT_TUONG_AP_DUNG)))
            .andExpect(jsonPath("$.[*].ghiChu").value(hasItem(DEFAULT_GHI_CHU)))
            .andExpect(jsonPath("$.[*].ngayApDung").value(hasItem(DEFAULT_NGAY_AP_DUNG.toString())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));
    }

    @Test
    @Transactional
    public void getDotThayDoiMaDichVu() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get the dotThayDoiMaDichVu
        restDotThayDoiMaDichVuMockMvc.perform(get("/api/dot-thay-doi-ma-dich-vus/{id}", dotThayDoiMaDichVu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(dotThayDoiMaDichVu.getId().intValue()))
            .andExpect(jsonPath("$.dotTuongApDung").value(DEFAULT_DOT_TUONG_AP_DUNG))
            .andExpect(jsonPath("$.ghiChu").value(DEFAULT_GHI_CHU))
            .andExpect(jsonPath("$.ngayApDung").value(DEFAULT_NGAY_AP_DUNG.toString()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN));
    }


    @Test
    @Transactional
    public void getDotThayDoiMaDichVusByIdFiltering() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        Long id = dotThayDoiMaDichVu.getId();

        defaultDotThayDoiMaDichVuShouldBeFound("id.equals=" + id);
        defaultDotThayDoiMaDichVuShouldNotBeFound("id.notEquals=" + id);

        defaultDotThayDoiMaDichVuShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultDotThayDoiMaDichVuShouldNotBeFound("id.greaterThan=" + id);

        defaultDotThayDoiMaDichVuShouldBeFound("id.lessThanOrEqual=" + id);
        defaultDotThayDoiMaDichVuShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByDotTuongApDungIsEqualToSomething() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where dotTuongApDung equals to DEFAULT_DOT_TUONG_AP_DUNG
        defaultDotThayDoiMaDichVuShouldBeFound("dotTuongApDung.equals=" + DEFAULT_DOT_TUONG_AP_DUNG);

        // Get all the dotThayDoiMaDichVuList where dotTuongApDung equals to UPDATED_DOT_TUONG_AP_DUNG
        defaultDotThayDoiMaDichVuShouldNotBeFound("dotTuongApDung.equals=" + UPDATED_DOT_TUONG_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByDotTuongApDungIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where dotTuongApDung not equals to DEFAULT_DOT_TUONG_AP_DUNG
        defaultDotThayDoiMaDichVuShouldNotBeFound("dotTuongApDung.notEquals=" + DEFAULT_DOT_TUONG_AP_DUNG);

        // Get all the dotThayDoiMaDichVuList where dotTuongApDung not equals to UPDATED_DOT_TUONG_AP_DUNG
        defaultDotThayDoiMaDichVuShouldBeFound("dotTuongApDung.notEquals=" + UPDATED_DOT_TUONG_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByDotTuongApDungIsInShouldWork() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where dotTuongApDung in DEFAULT_DOT_TUONG_AP_DUNG or UPDATED_DOT_TUONG_AP_DUNG
        defaultDotThayDoiMaDichVuShouldBeFound("dotTuongApDung.in=" + DEFAULT_DOT_TUONG_AP_DUNG + "," + UPDATED_DOT_TUONG_AP_DUNG);

        // Get all the dotThayDoiMaDichVuList where dotTuongApDung equals to UPDATED_DOT_TUONG_AP_DUNG
        defaultDotThayDoiMaDichVuShouldNotBeFound("dotTuongApDung.in=" + UPDATED_DOT_TUONG_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByDotTuongApDungIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where dotTuongApDung is not null
        defaultDotThayDoiMaDichVuShouldBeFound("dotTuongApDung.specified=true");

        // Get all the dotThayDoiMaDichVuList where dotTuongApDung is null
        defaultDotThayDoiMaDichVuShouldNotBeFound("dotTuongApDung.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByDotTuongApDungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where dotTuongApDung is greater than or equal to DEFAULT_DOT_TUONG_AP_DUNG
        defaultDotThayDoiMaDichVuShouldBeFound("dotTuongApDung.greaterThanOrEqual=" + DEFAULT_DOT_TUONG_AP_DUNG);

        // Get all the dotThayDoiMaDichVuList where dotTuongApDung is greater than or equal to UPDATED_DOT_TUONG_AP_DUNG
        defaultDotThayDoiMaDichVuShouldNotBeFound("dotTuongApDung.greaterThanOrEqual=" + UPDATED_DOT_TUONG_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByDotTuongApDungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where dotTuongApDung is less than or equal to DEFAULT_DOT_TUONG_AP_DUNG
        defaultDotThayDoiMaDichVuShouldBeFound("dotTuongApDung.lessThanOrEqual=" + DEFAULT_DOT_TUONG_AP_DUNG);

        // Get all the dotThayDoiMaDichVuList where dotTuongApDung is less than or equal to SMALLER_DOT_TUONG_AP_DUNG
        defaultDotThayDoiMaDichVuShouldNotBeFound("dotTuongApDung.lessThanOrEqual=" + SMALLER_DOT_TUONG_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByDotTuongApDungIsLessThanSomething() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where dotTuongApDung is less than DEFAULT_DOT_TUONG_AP_DUNG
        defaultDotThayDoiMaDichVuShouldNotBeFound("dotTuongApDung.lessThan=" + DEFAULT_DOT_TUONG_AP_DUNG);

        // Get all the dotThayDoiMaDichVuList where dotTuongApDung is less than UPDATED_DOT_TUONG_AP_DUNG
        defaultDotThayDoiMaDichVuShouldBeFound("dotTuongApDung.lessThan=" + UPDATED_DOT_TUONG_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByDotTuongApDungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where dotTuongApDung is greater than DEFAULT_DOT_TUONG_AP_DUNG
        defaultDotThayDoiMaDichVuShouldNotBeFound("dotTuongApDung.greaterThan=" + DEFAULT_DOT_TUONG_AP_DUNG);

        // Get all the dotThayDoiMaDichVuList where dotTuongApDung is greater than SMALLER_DOT_TUONG_AP_DUNG
        defaultDotThayDoiMaDichVuShouldBeFound("dotTuongApDung.greaterThan=" + SMALLER_DOT_TUONG_AP_DUNG);
    }


    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByGhiChuIsEqualToSomething() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where ghiChu equals to DEFAULT_GHI_CHU
        defaultDotThayDoiMaDichVuShouldBeFound("ghiChu.equals=" + DEFAULT_GHI_CHU);

        // Get all the dotThayDoiMaDichVuList where ghiChu equals to UPDATED_GHI_CHU
        defaultDotThayDoiMaDichVuShouldNotBeFound("ghiChu.equals=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByGhiChuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where ghiChu not equals to DEFAULT_GHI_CHU
        defaultDotThayDoiMaDichVuShouldNotBeFound("ghiChu.notEquals=" + DEFAULT_GHI_CHU);

        // Get all the dotThayDoiMaDichVuList where ghiChu not equals to UPDATED_GHI_CHU
        defaultDotThayDoiMaDichVuShouldBeFound("ghiChu.notEquals=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByGhiChuIsInShouldWork() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where ghiChu in DEFAULT_GHI_CHU or UPDATED_GHI_CHU
        defaultDotThayDoiMaDichVuShouldBeFound("ghiChu.in=" + DEFAULT_GHI_CHU + "," + UPDATED_GHI_CHU);

        // Get all the dotThayDoiMaDichVuList where ghiChu equals to UPDATED_GHI_CHU
        defaultDotThayDoiMaDichVuShouldNotBeFound("ghiChu.in=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByGhiChuIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where ghiChu is not null
        defaultDotThayDoiMaDichVuShouldBeFound("ghiChu.specified=true");

        // Get all the dotThayDoiMaDichVuList where ghiChu is null
        defaultDotThayDoiMaDichVuShouldNotBeFound("ghiChu.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByGhiChuContainsSomething() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where ghiChu contains DEFAULT_GHI_CHU
        defaultDotThayDoiMaDichVuShouldBeFound("ghiChu.contains=" + DEFAULT_GHI_CHU);

        // Get all the dotThayDoiMaDichVuList where ghiChu contains UPDATED_GHI_CHU
        defaultDotThayDoiMaDichVuShouldNotBeFound("ghiChu.contains=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByGhiChuNotContainsSomething() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where ghiChu does not contain DEFAULT_GHI_CHU
        defaultDotThayDoiMaDichVuShouldNotBeFound("ghiChu.doesNotContain=" + DEFAULT_GHI_CHU);

        // Get all the dotThayDoiMaDichVuList where ghiChu does not contain UPDATED_GHI_CHU
        defaultDotThayDoiMaDichVuShouldBeFound("ghiChu.doesNotContain=" + UPDATED_GHI_CHU);
    }


    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByNgayApDungIsEqualToSomething() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where ngayApDung equals to DEFAULT_NGAY_AP_DUNG
        defaultDotThayDoiMaDichVuShouldBeFound("ngayApDung.equals=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the dotThayDoiMaDichVuList where ngayApDung equals to UPDATED_NGAY_AP_DUNG
        defaultDotThayDoiMaDichVuShouldNotBeFound("ngayApDung.equals=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByNgayApDungIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where ngayApDung not equals to DEFAULT_NGAY_AP_DUNG
        defaultDotThayDoiMaDichVuShouldNotBeFound("ngayApDung.notEquals=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the dotThayDoiMaDichVuList where ngayApDung not equals to UPDATED_NGAY_AP_DUNG
        defaultDotThayDoiMaDichVuShouldBeFound("ngayApDung.notEquals=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByNgayApDungIsInShouldWork() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where ngayApDung in DEFAULT_NGAY_AP_DUNG or UPDATED_NGAY_AP_DUNG
        defaultDotThayDoiMaDichVuShouldBeFound("ngayApDung.in=" + DEFAULT_NGAY_AP_DUNG + "," + UPDATED_NGAY_AP_DUNG);

        // Get all the dotThayDoiMaDichVuList where ngayApDung equals to UPDATED_NGAY_AP_DUNG
        defaultDotThayDoiMaDichVuShouldNotBeFound("ngayApDung.in=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByNgayApDungIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where ngayApDung is not null
        defaultDotThayDoiMaDichVuShouldBeFound("ngayApDung.specified=true");

        // Get all the dotThayDoiMaDichVuList where ngayApDung is null
        defaultDotThayDoiMaDichVuShouldNotBeFound("ngayApDung.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByNgayApDungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where ngayApDung is greater than or equal to DEFAULT_NGAY_AP_DUNG
        defaultDotThayDoiMaDichVuShouldBeFound("ngayApDung.greaterThanOrEqual=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the dotThayDoiMaDichVuList where ngayApDung is greater than or equal to UPDATED_NGAY_AP_DUNG
        defaultDotThayDoiMaDichVuShouldNotBeFound("ngayApDung.greaterThanOrEqual=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByNgayApDungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where ngayApDung is less than or equal to DEFAULT_NGAY_AP_DUNG
        defaultDotThayDoiMaDichVuShouldBeFound("ngayApDung.lessThanOrEqual=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the dotThayDoiMaDichVuList where ngayApDung is less than or equal to SMALLER_NGAY_AP_DUNG
        defaultDotThayDoiMaDichVuShouldNotBeFound("ngayApDung.lessThanOrEqual=" + SMALLER_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByNgayApDungIsLessThanSomething() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where ngayApDung is less than DEFAULT_NGAY_AP_DUNG
        defaultDotThayDoiMaDichVuShouldNotBeFound("ngayApDung.lessThan=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the dotThayDoiMaDichVuList where ngayApDung is less than UPDATED_NGAY_AP_DUNG
        defaultDotThayDoiMaDichVuShouldBeFound("ngayApDung.lessThan=" + UPDATED_NGAY_AP_DUNG);
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByNgayApDungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where ngayApDung is greater than DEFAULT_NGAY_AP_DUNG
        defaultDotThayDoiMaDichVuShouldNotBeFound("ngayApDung.greaterThan=" + DEFAULT_NGAY_AP_DUNG);

        // Get all the dotThayDoiMaDichVuList where ngayApDung is greater than SMALLER_NGAY_AP_DUNG
        defaultDotThayDoiMaDichVuShouldBeFound("ngayApDung.greaterThan=" + SMALLER_NGAY_AP_DUNG);
    }


    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where ten equals to DEFAULT_TEN
        defaultDotThayDoiMaDichVuShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the dotThayDoiMaDichVuList where ten equals to UPDATED_TEN
        defaultDotThayDoiMaDichVuShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where ten not equals to DEFAULT_TEN
        defaultDotThayDoiMaDichVuShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the dotThayDoiMaDichVuList where ten not equals to UPDATED_TEN
        defaultDotThayDoiMaDichVuShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByTenIsInShouldWork() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultDotThayDoiMaDichVuShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the dotThayDoiMaDichVuList where ten equals to UPDATED_TEN
        defaultDotThayDoiMaDichVuShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where ten is not null
        defaultDotThayDoiMaDichVuShouldBeFound("ten.specified=true");

        // Get all the dotThayDoiMaDichVuList where ten is null
        defaultDotThayDoiMaDichVuShouldNotBeFound("ten.specified=false");
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByTenContainsSomething() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where ten contains DEFAULT_TEN
        defaultDotThayDoiMaDichVuShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the dotThayDoiMaDichVuList where ten contains UPDATED_TEN
        defaultDotThayDoiMaDichVuShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByTenNotContainsSomething() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        // Get all the dotThayDoiMaDichVuList where ten does not contain DEFAULT_TEN
        defaultDotThayDoiMaDichVuShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the dotThayDoiMaDichVuList where ten does not contain UPDATED_TEN
        defaultDotThayDoiMaDichVuShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllDotThayDoiMaDichVusByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        DonVi donVi = dotThayDoiMaDichVu.getDonVi();
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);
        Long donViId = donVi.getId();

        // Get all the dotThayDoiMaDichVuList where donVi equals to donViId
        defaultDotThayDoiMaDichVuShouldBeFound("donViId.equals=" + donViId);

        // Get all the dotThayDoiMaDichVuList where donVi equals to donViId + 1
        defaultDotThayDoiMaDichVuShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDotThayDoiMaDichVuShouldBeFound(String filter) throws Exception {
        restDotThayDoiMaDichVuMockMvc.perform(get("/api/dot-thay-doi-ma-dich-vus?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dotThayDoiMaDichVu.getId().intValue())))
            .andExpect(jsonPath("$.[*].dotTuongApDung").value(hasItem(DEFAULT_DOT_TUONG_AP_DUNG)))
            .andExpect(jsonPath("$.[*].ghiChu").value(hasItem(DEFAULT_GHI_CHU)))
            .andExpect(jsonPath("$.[*].ngayApDung").value(hasItem(DEFAULT_NGAY_AP_DUNG.toString())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));

        // Check, that the count call also returns 1
        restDotThayDoiMaDichVuMockMvc.perform(get("/api/dot-thay-doi-ma-dich-vus/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDotThayDoiMaDichVuShouldNotBeFound(String filter) throws Exception {
        restDotThayDoiMaDichVuMockMvc.perform(get("/api/dot-thay-doi-ma-dich-vus?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDotThayDoiMaDichVuMockMvc.perform(get("/api/dot-thay-doi-ma-dich-vus/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingDotThayDoiMaDichVu() throws Exception {
        // Get the dotThayDoiMaDichVu
        restDotThayDoiMaDichVuMockMvc.perform(get("/api/dot-thay-doi-ma-dich-vus/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDotThayDoiMaDichVu() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        int databaseSizeBeforeUpdate = dotThayDoiMaDichVuRepository.findAll().size();

        // Update the dotThayDoiMaDichVu
        DotThayDoiMaDichVu updatedDotThayDoiMaDichVu = dotThayDoiMaDichVuRepository.findById(dotThayDoiMaDichVu.getId()).get();
        // Disconnect from session so that the updates on updatedDotThayDoiMaDichVu are not directly saved in db
        em.detach(updatedDotThayDoiMaDichVu);
        updatedDotThayDoiMaDichVu
            .dotTuongApDung(UPDATED_DOT_TUONG_AP_DUNG)
            .ghiChu(UPDATED_GHI_CHU)
            .ngayApDung(UPDATED_NGAY_AP_DUNG)
            .ten(UPDATED_TEN);
        DotThayDoiMaDichVuDTO dotThayDoiMaDichVuDTO = dotThayDoiMaDichVuMapper.toDto(updatedDotThayDoiMaDichVu);

        restDotThayDoiMaDichVuMockMvc.perform(put("/api/dot-thay-doi-ma-dich-vus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dotThayDoiMaDichVuDTO)))
            .andExpect(status().isOk());

        // Validate the DotThayDoiMaDichVu in the database
        List<DotThayDoiMaDichVu> dotThayDoiMaDichVuList = dotThayDoiMaDichVuRepository.findAll();
        assertThat(dotThayDoiMaDichVuList).hasSize(databaseSizeBeforeUpdate);
        DotThayDoiMaDichVu testDotThayDoiMaDichVu = dotThayDoiMaDichVuList.get(dotThayDoiMaDichVuList.size() - 1);
        assertThat(testDotThayDoiMaDichVu.getDotTuongApDung()).isEqualTo(UPDATED_DOT_TUONG_AP_DUNG);
        assertThat(testDotThayDoiMaDichVu.getGhiChu()).isEqualTo(UPDATED_GHI_CHU);
        assertThat(testDotThayDoiMaDichVu.getNgayApDung()).isEqualTo(UPDATED_NGAY_AP_DUNG);
        assertThat(testDotThayDoiMaDichVu.getTen()).isEqualTo(UPDATED_TEN);
    }

    @Test
    @Transactional
    public void updateNonExistingDotThayDoiMaDichVu() throws Exception {
        int databaseSizeBeforeUpdate = dotThayDoiMaDichVuRepository.findAll().size();

        // Create the DotThayDoiMaDichVu
        DotThayDoiMaDichVuDTO dotThayDoiMaDichVuDTO = dotThayDoiMaDichVuMapper.toDto(dotThayDoiMaDichVu);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDotThayDoiMaDichVuMockMvc.perform(put("/api/dot-thay-doi-ma-dich-vus").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dotThayDoiMaDichVuDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DotThayDoiMaDichVu in the database
        List<DotThayDoiMaDichVu> dotThayDoiMaDichVuList = dotThayDoiMaDichVuRepository.findAll();
        assertThat(dotThayDoiMaDichVuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDotThayDoiMaDichVu() throws Exception {
        // Initialize the database
        dotThayDoiMaDichVuRepository.saveAndFlush(dotThayDoiMaDichVu);

        int databaseSizeBeforeDelete = dotThayDoiMaDichVuRepository.findAll().size();

        // Delete the dotThayDoiMaDichVu
        restDotThayDoiMaDichVuMockMvc.perform(delete("/api/dot-thay-doi-ma-dich-vus/{id}", dotThayDoiMaDichVu.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DotThayDoiMaDichVu> dotThayDoiMaDichVuList = dotThayDoiMaDichVuRepository.findAll();
        assertThat(dotThayDoiMaDichVuList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
