package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.NhomXetNghiem;
import vn.vnpt.domain.DonVi;
import vn.vnpt.repository.NhomXetNghiemRepository;
import vn.vnpt.service.NhomXetNghiemService;
import vn.vnpt.service.dto.NhomXetNghiemDTO;
import vn.vnpt.service.mapper.NhomXetNghiemMapper;
import vn.vnpt.service.dto.NhomXetNghiemCriteria;
import vn.vnpt.service.NhomXetNghiemQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link NhomXetNghiemResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class NhomXetNghiemResourceIT {

    private static final Boolean DEFAULT_ENABLE = false;
    private static final Boolean UPDATED_ENABLE = true;

    private static final Integer DEFAULT_LEVEL = 1;
    private static final Integer UPDATED_LEVEL = 2;
    private static final Integer SMALLER_LEVEL = 1 - 1;

    private static final BigDecimal DEFAULT_PARENT_ID = new BigDecimal(1);
    private static final BigDecimal UPDATED_PARENT_ID = new BigDecimal(2);
    private static final BigDecimal SMALLER_PARENT_ID = new BigDecimal(1 - 1);

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    @Autowired
    private NhomXetNghiemRepository nhomXetNghiemRepository;

    @Autowired
    private NhomXetNghiemMapper nhomXetNghiemMapper;

    @Autowired
    private NhomXetNghiemService nhomXetNghiemService;

    @Autowired
    private NhomXetNghiemQueryService nhomXetNghiemQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restNhomXetNghiemMockMvc;

    private NhomXetNghiem nhomXetNghiem;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NhomXetNghiem createEntity(EntityManager em) {
        NhomXetNghiem nhomXetNghiem = new NhomXetNghiem()
            .enable(DEFAULT_ENABLE)
            .level(DEFAULT_LEVEL)
            .parentId(DEFAULT_PARENT_ID)
            .ten(DEFAULT_TEN);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        nhomXetNghiem.setDonVi(donVi);
        return nhomXetNghiem;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NhomXetNghiem createUpdatedEntity(EntityManager em) {
        NhomXetNghiem nhomXetNghiem = new NhomXetNghiem()
            .enable(UPDATED_ENABLE)
            .level(UPDATED_LEVEL)
            .parentId(UPDATED_PARENT_ID)
            .ten(UPDATED_TEN);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createUpdatedEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        nhomXetNghiem.setDonVi(donVi);
        return nhomXetNghiem;
    }

    @BeforeEach
    public void initTest() {
        nhomXetNghiem = createEntity(em);
    }

    @Test
    @Transactional
    public void createNhomXetNghiem() throws Exception {
        int databaseSizeBeforeCreate = nhomXetNghiemRepository.findAll().size();

        // Create the NhomXetNghiem
        NhomXetNghiemDTO nhomXetNghiemDTO = nhomXetNghiemMapper.toDto(nhomXetNghiem);
        restNhomXetNghiemMockMvc.perform(post("/api/nhom-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomXetNghiemDTO)))
            .andExpect(status().isCreated());

        // Validate the NhomXetNghiem in the database
        List<NhomXetNghiem> nhomXetNghiemList = nhomXetNghiemRepository.findAll();
        assertThat(nhomXetNghiemList).hasSize(databaseSizeBeforeCreate + 1);
        NhomXetNghiem testNhomXetNghiem = nhomXetNghiemList.get(nhomXetNghiemList.size() - 1);
        assertThat(testNhomXetNghiem.isEnable()).isEqualTo(DEFAULT_ENABLE);
        assertThat(testNhomXetNghiem.getLevel()).isEqualTo(DEFAULT_LEVEL);
        assertThat(testNhomXetNghiem.getParentId()).isEqualTo(DEFAULT_PARENT_ID);
        assertThat(testNhomXetNghiem.getTen()).isEqualTo(DEFAULT_TEN);
    }

    @Test
    @Transactional
    public void createNhomXetNghiemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = nhomXetNghiemRepository.findAll().size();

        // Create the NhomXetNghiem with an existing ID
        nhomXetNghiem.setId(1L);
        NhomXetNghiemDTO nhomXetNghiemDTO = nhomXetNghiemMapper.toDto(nhomXetNghiem);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNhomXetNghiemMockMvc.perform(post("/api/nhom-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NhomXetNghiem in the database
        List<NhomXetNghiem> nhomXetNghiemList = nhomXetNghiemRepository.findAll();
        assertThat(nhomXetNghiemList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkEnableIsRequired() throws Exception {
        int databaseSizeBeforeTest = nhomXetNghiemRepository.findAll().size();
        // set the field null
        nhomXetNghiem.setEnable(null);

        // Create the NhomXetNghiem, which fails.
        NhomXetNghiemDTO nhomXetNghiemDTO = nhomXetNghiemMapper.toDto(nhomXetNghiem);

        restNhomXetNghiemMockMvc.perform(post("/api/nhom-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<NhomXetNghiem> nhomXetNghiemList = nhomXetNghiemRepository.findAll();
        assertThat(nhomXetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLevelIsRequired() throws Exception {
        int databaseSizeBeforeTest = nhomXetNghiemRepository.findAll().size();
        // set the field null
        nhomXetNghiem.setLevel(null);

        // Create the NhomXetNghiem, which fails.
        NhomXetNghiemDTO nhomXetNghiemDTO = nhomXetNghiemMapper.toDto(nhomXetNghiem);

        restNhomXetNghiemMockMvc.perform(post("/api/nhom-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<NhomXetNghiem> nhomXetNghiemList = nhomXetNghiemRepository.findAll();
        assertThat(nhomXetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkParentIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = nhomXetNghiemRepository.findAll().size();
        // set the field null
        nhomXetNghiem.setParentId(null);

        // Create the NhomXetNghiem, which fails.
        NhomXetNghiemDTO nhomXetNghiemDTO = nhomXetNghiemMapper.toDto(nhomXetNghiem);

        restNhomXetNghiemMockMvc.perform(post("/api/nhom-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        List<NhomXetNghiem> nhomXetNghiemList = nhomXetNghiemRepository.findAll();
        assertThat(nhomXetNghiemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiems() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList
        restNhomXetNghiemMockMvc.perform(get("/api/nhom-xet-nghiems?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nhomXetNghiem.getId().intValue())))
            .andExpect(jsonPath("$.[*].enable").value(hasItem(DEFAULT_ENABLE.booleanValue())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL)))
            .andExpect(jsonPath("$.[*].parentId").value(hasItem(DEFAULT_PARENT_ID.intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));
    }
    
    @Test
    @Transactional
    public void getNhomXetNghiem() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get the nhomXetNghiem
        restNhomXetNghiemMockMvc.perform(get("/api/nhom-xet-nghiems/{id}", nhomXetNghiem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(nhomXetNghiem.getId().intValue()))
            .andExpect(jsonPath("$.enable").value(DEFAULT_ENABLE.booleanValue()))
            .andExpect(jsonPath("$.level").value(DEFAULT_LEVEL))
            .andExpect(jsonPath("$.parentId").value(DEFAULT_PARENT_ID.intValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN));
    }


    @Test
    @Transactional
    public void getNhomXetNghiemsByIdFiltering() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        Long id = nhomXetNghiem.getId();

        defaultNhomXetNghiemShouldBeFound("id.equals=" + id);
        defaultNhomXetNghiemShouldNotBeFound("id.notEquals=" + id);

        defaultNhomXetNghiemShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultNhomXetNghiemShouldNotBeFound("id.greaterThan=" + id);

        defaultNhomXetNghiemShouldBeFound("id.lessThanOrEqual=" + id);
        defaultNhomXetNghiemShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllNhomXetNghiemsByEnableIsEqualToSomething() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where enable equals to DEFAULT_ENABLE
        defaultNhomXetNghiemShouldBeFound("enable.equals=" + DEFAULT_ENABLE);

        // Get all the nhomXetNghiemList where enable equals to UPDATED_ENABLE
        defaultNhomXetNghiemShouldNotBeFound("enable.equals=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByEnableIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where enable not equals to DEFAULT_ENABLE
        defaultNhomXetNghiemShouldNotBeFound("enable.notEquals=" + DEFAULT_ENABLE);

        // Get all the nhomXetNghiemList where enable not equals to UPDATED_ENABLE
        defaultNhomXetNghiemShouldBeFound("enable.notEquals=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByEnableIsInShouldWork() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where enable in DEFAULT_ENABLE or UPDATED_ENABLE
        defaultNhomXetNghiemShouldBeFound("enable.in=" + DEFAULT_ENABLE + "," + UPDATED_ENABLE);

        // Get all the nhomXetNghiemList where enable equals to UPDATED_ENABLE
        defaultNhomXetNghiemShouldNotBeFound("enable.in=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByEnableIsNullOrNotNull() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where enable is not null
        defaultNhomXetNghiemShouldBeFound("enable.specified=true");

        // Get all the nhomXetNghiemList where enable is null
        defaultNhomXetNghiemShouldNotBeFound("enable.specified=false");
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByLevelIsEqualToSomething() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where level equals to DEFAULT_LEVEL
        defaultNhomXetNghiemShouldBeFound("level.equals=" + DEFAULT_LEVEL);

        // Get all the nhomXetNghiemList where level equals to UPDATED_LEVEL
        defaultNhomXetNghiemShouldNotBeFound("level.equals=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByLevelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where level not equals to DEFAULT_LEVEL
        defaultNhomXetNghiemShouldNotBeFound("level.notEquals=" + DEFAULT_LEVEL);

        // Get all the nhomXetNghiemList where level not equals to UPDATED_LEVEL
        defaultNhomXetNghiemShouldBeFound("level.notEquals=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByLevelIsInShouldWork() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where level in DEFAULT_LEVEL or UPDATED_LEVEL
        defaultNhomXetNghiemShouldBeFound("level.in=" + DEFAULT_LEVEL + "," + UPDATED_LEVEL);

        // Get all the nhomXetNghiemList where level equals to UPDATED_LEVEL
        defaultNhomXetNghiemShouldNotBeFound("level.in=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByLevelIsNullOrNotNull() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where level is not null
        defaultNhomXetNghiemShouldBeFound("level.specified=true");

        // Get all the nhomXetNghiemList where level is null
        defaultNhomXetNghiemShouldNotBeFound("level.specified=false");
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByLevelIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where level is greater than or equal to DEFAULT_LEVEL
        defaultNhomXetNghiemShouldBeFound("level.greaterThanOrEqual=" + DEFAULT_LEVEL);

        // Get all the nhomXetNghiemList where level is greater than or equal to UPDATED_LEVEL
        defaultNhomXetNghiemShouldNotBeFound("level.greaterThanOrEqual=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByLevelIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where level is less than or equal to DEFAULT_LEVEL
        defaultNhomXetNghiemShouldBeFound("level.lessThanOrEqual=" + DEFAULT_LEVEL);

        // Get all the nhomXetNghiemList where level is less than or equal to SMALLER_LEVEL
        defaultNhomXetNghiemShouldNotBeFound("level.lessThanOrEqual=" + SMALLER_LEVEL);
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByLevelIsLessThanSomething() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where level is less than DEFAULT_LEVEL
        defaultNhomXetNghiemShouldNotBeFound("level.lessThan=" + DEFAULT_LEVEL);

        // Get all the nhomXetNghiemList where level is less than UPDATED_LEVEL
        defaultNhomXetNghiemShouldBeFound("level.lessThan=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByLevelIsGreaterThanSomething() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where level is greater than DEFAULT_LEVEL
        defaultNhomXetNghiemShouldNotBeFound("level.greaterThan=" + DEFAULT_LEVEL);

        // Get all the nhomXetNghiemList where level is greater than SMALLER_LEVEL
        defaultNhomXetNghiemShouldBeFound("level.greaterThan=" + SMALLER_LEVEL);
    }


    @Test
    @Transactional
    public void getAllNhomXetNghiemsByParentIdIsEqualToSomething() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where parentId equals to DEFAULT_PARENT_ID
        defaultNhomXetNghiemShouldBeFound("parentId.equals=" + DEFAULT_PARENT_ID);

        // Get all the nhomXetNghiemList where parentId equals to UPDATED_PARENT_ID
        defaultNhomXetNghiemShouldNotBeFound("parentId.equals=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByParentIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where parentId not equals to DEFAULT_PARENT_ID
        defaultNhomXetNghiemShouldNotBeFound("parentId.notEquals=" + DEFAULT_PARENT_ID);

        // Get all the nhomXetNghiemList where parentId not equals to UPDATED_PARENT_ID
        defaultNhomXetNghiemShouldBeFound("parentId.notEquals=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByParentIdIsInShouldWork() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where parentId in DEFAULT_PARENT_ID or UPDATED_PARENT_ID
        defaultNhomXetNghiemShouldBeFound("parentId.in=" + DEFAULT_PARENT_ID + "," + UPDATED_PARENT_ID);

        // Get all the nhomXetNghiemList where parentId equals to UPDATED_PARENT_ID
        defaultNhomXetNghiemShouldNotBeFound("parentId.in=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByParentIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where parentId is not null
        defaultNhomXetNghiemShouldBeFound("parentId.specified=true");

        // Get all the nhomXetNghiemList where parentId is null
        defaultNhomXetNghiemShouldNotBeFound("parentId.specified=false");
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByParentIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where parentId is greater than or equal to DEFAULT_PARENT_ID
        defaultNhomXetNghiemShouldBeFound("parentId.greaterThanOrEqual=" + DEFAULT_PARENT_ID);

        // Get all the nhomXetNghiemList where parentId is greater than or equal to UPDATED_PARENT_ID
        defaultNhomXetNghiemShouldNotBeFound("parentId.greaterThanOrEqual=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByParentIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where parentId is less than or equal to DEFAULT_PARENT_ID
        defaultNhomXetNghiemShouldBeFound("parentId.lessThanOrEqual=" + DEFAULT_PARENT_ID);

        // Get all the nhomXetNghiemList where parentId is less than or equal to SMALLER_PARENT_ID
        defaultNhomXetNghiemShouldNotBeFound("parentId.lessThanOrEqual=" + SMALLER_PARENT_ID);
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByParentIdIsLessThanSomething() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where parentId is less than DEFAULT_PARENT_ID
        defaultNhomXetNghiemShouldNotBeFound("parentId.lessThan=" + DEFAULT_PARENT_ID);

        // Get all the nhomXetNghiemList where parentId is less than UPDATED_PARENT_ID
        defaultNhomXetNghiemShouldBeFound("parentId.lessThan=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByParentIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where parentId is greater than DEFAULT_PARENT_ID
        defaultNhomXetNghiemShouldNotBeFound("parentId.greaterThan=" + DEFAULT_PARENT_ID);

        // Get all the nhomXetNghiemList where parentId is greater than SMALLER_PARENT_ID
        defaultNhomXetNghiemShouldBeFound("parentId.greaterThan=" + SMALLER_PARENT_ID);
    }


    @Test
    @Transactional
    public void getAllNhomXetNghiemsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where ten equals to DEFAULT_TEN
        defaultNhomXetNghiemShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the nhomXetNghiemList where ten equals to UPDATED_TEN
        defaultNhomXetNghiemShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where ten not equals to DEFAULT_TEN
        defaultNhomXetNghiemShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the nhomXetNghiemList where ten not equals to UPDATED_TEN
        defaultNhomXetNghiemShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultNhomXetNghiemShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the nhomXetNghiemList where ten equals to UPDATED_TEN
        defaultNhomXetNghiemShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where ten is not null
        defaultNhomXetNghiemShouldBeFound("ten.specified=true");

        // Get all the nhomXetNghiemList where ten is null
        defaultNhomXetNghiemShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllNhomXetNghiemsByTenContainsSomething() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where ten contains DEFAULT_TEN
        defaultNhomXetNghiemShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the nhomXetNghiemList where ten contains UPDATED_TEN
        defaultNhomXetNghiemShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhomXetNghiemsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        // Get all the nhomXetNghiemList where ten does not contain DEFAULT_TEN
        defaultNhomXetNghiemShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the nhomXetNghiemList where ten does not contain UPDATED_TEN
        defaultNhomXetNghiemShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllNhomXetNghiemsByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        DonVi donVi = nhomXetNghiem.getDonVi();
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);
        Long donViId = donVi.getId();

        // Get all the nhomXetNghiemList where donVi equals to donViId
        defaultNhomXetNghiemShouldBeFound("donViId.equals=" + donViId);

        // Get all the nhomXetNghiemList where donVi equals to donViId + 1
        defaultNhomXetNghiemShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultNhomXetNghiemShouldBeFound(String filter) throws Exception {
        restNhomXetNghiemMockMvc.perform(get("/api/nhom-xet-nghiems?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nhomXetNghiem.getId().intValue())))
            .andExpect(jsonPath("$.[*].enable").value(hasItem(DEFAULT_ENABLE.booleanValue())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL)))
            .andExpect(jsonPath("$.[*].parentId").value(hasItem(DEFAULT_PARENT_ID.intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));

        // Check, that the count call also returns 1
        restNhomXetNghiemMockMvc.perform(get("/api/nhom-xet-nghiems/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultNhomXetNghiemShouldNotBeFound(String filter) throws Exception {
        restNhomXetNghiemMockMvc.perform(get("/api/nhom-xet-nghiems?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restNhomXetNghiemMockMvc.perform(get("/api/nhom-xet-nghiems/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingNhomXetNghiem() throws Exception {
        // Get the nhomXetNghiem
        restNhomXetNghiemMockMvc.perform(get("/api/nhom-xet-nghiems/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNhomXetNghiem() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        int databaseSizeBeforeUpdate = nhomXetNghiemRepository.findAll().size();

        // Update the nhomXetNghiem
        NhomXetNghiem updatedNhomXetNghiem = nhomXetNghiemRepository.findById(nhomXetNghiem.getId()).get();
        // Disconnect from session so that the updates on updatedNhomXetNghiem are not directly saved in db
        em.detach(updatedNhomXetNghiem);
        updatedNhomXetNghiem
            .enable(UPDATED_ENABLE)
            .level(UPDATED_LEVEL)
            .parentId(UPDATED_PARENT_ID)
            .ten(UPDATED_TEN);
        NhomXetNghiemDTO nhomXetNghiemDTO = nhomXetNghiemMapper.toDto(updatedNhomXetNghiem);

        restNhomXetNghiemMockMvc.perform(put("/api/nhom-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomXetNghiemDTO)))
            .andExpect(status().isOk());

        // Validate the NhomXetNghiem in the database
        List<NhomXetNghiem> nhomXetNghiemList = nhomXetNghiemRepository.findAll();
        assertThat(nhomXetNghiemList).hasSize(databaseSizeBeforeUpdate);
        NhomXetNghiem testNhomXetNghiem = nhomXetNghiemList.get(nhomXetNghiemList.size() - 1);
        assertThat(testNhomXetNghiem.isEnable()).isEqualTo(UPDATED_ENABLE);
        assertThat(testNhomXetNghiem.getLevel()).isEqualTo(UPDATED_LEVEL);
        assertThat(testNhomXetNghiem.getParentId()).isEqualTo(UPDATED_PARENT_ID);
        assertThat(testNhomXetNghiem.getTen()).isEqualTo(UPDATED_TEN);
    }

    @Test
    @Transactional
    public void updateNonExistingNhomXetNghiem() throws Exception {
        int databaseSizeBeforeUpdate = nhomXetNghiemRepository.findAll().size();

        // Create the NhomXetNghiem
        NhomXetNghiemDTO nhomXetNghiemDTO = nhomXetNghiemMapper.toDto(nhomXetNghiem);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNhomXetNghiemMockMvc.perform(put("/api/nhom-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NhomXetNghiem in the database
        List<NhomXetNghiem> nhomXetNghiemList = nhomXetNghiemRepository.findAll();
        assertThat(nhomXetNghiemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNhomXetNghiem() throws Exception {
        // Initialize the database
        nhomXetNghiemRepository.saveAndFlush(nhomXetNghiem);

        int databaseSizeBeforeDelete = nhomXetNghiemRepository.findAll().size();

        // Delete the nhomXetNghiem
        restNhomXetNghiemMockMvc.perform(delete("/api/nhom-xet-nghiems/{id}", nhomXetNghiem.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<NhomXetNghiem> nhomXetNghiemList = nhomXetNghiemRepository.findAll();
        assertThat(nhomXetNghiemList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
