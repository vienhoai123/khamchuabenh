package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.TPhanNhomXetNghiem;
import vn.vnpt.domain.NhomXetNghiem;
import vn.vnpt.domain.XetNghiem;
import vn.vnpt.repository.TPhanNhomXetNghiemRepository;
import vn.vnpt.service.TPhanNhomXetNghiemService;
import vn.vnpt.service.dto.TPhanNhomXetNghiemDTO;
import vn.vnpt.service.mapper.TPhanNhomXetNghiemMapper;
import vn.vnpt.service.dto.TPhanNhomXetNghiemCriteria;
import vn.vnpt.service.TPhanNhomXetNghiemQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TPhanNhomXetNghiemResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class TPhanNhomXetNghiemResourceIT {

    @Autowired
    private TPhanNhomXetNghiemRepository tPhanNhomXetNghiemRepository;

    @Autowired
    private TPhanNhomXetNghiemMapper tPhanNhomXetNghiemMapper;

    @Autowired
    private TPhanNhomXetNghiemService tPhanNhomXetNghiemService;

    @Autowired
    private TPhanNhomXetNghiemQueryService tPhanNhomXetNghiemQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTPhanNhomXetNghiemMockMvc;

    private TPhanNhomXetNghiem tPhanNhomXetNghiem;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TPhanNhomXetNghiem createEntity(EntityManager em) {
        TPhanNhomXetNghiem tPhanNhomXetNghiem = new TPhanNhomXetNghiem();
        // Add required entity
        NhomXetNghiem nhomXetNghiem;
        if (TestUtil.findAll(em, NhomXetNghiem.class).isEmpty()) {
            nhomXetNghiem = NhomXetNghiemResourceIT.createEntity(em);
            em.persist(nhomXetNghiem);
            em.flush();
        } else {
            nhomXetNghiem = TestUtil.findAll(em, NhomXetNghiem.class).get(0);
        }
        tPhanNhomXetNghiem.setNhomXN(nhomXetNghiem);
        // Add required entity
        XetNghiem xetNghiem;
        if (TestUtil.findAll(em, XetNghiem.class).isEmpty()) {
            xetNghiem = XetNghiemResourceIT.createEntity(em);
            em.persist(xetNghiem);
            em.flush();
        } else {
            xetNghiem = TestUtil.findAll(em, XetNghiem.class).get(0);
        }
        tPhanNhomXetNghiem.setXN(xetNghiem);
        return tPhanNhomXetNghiem;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TPhanNhomXetNghiem createUpdatedEntity(EntityManager em) {
        TPhanNhomXetNghiem tPhanNhomXetNghiem = new TPhanNhomXetNghiem();
        // Add required entity
        NhomXetNghiem nhomXetNghiem;
        if (TestUtil.findAll(em, NhomXetNghiem.class).isEmpty()) {
            nhomXetNghiem = NhomXetNghiemResourceIT.createUpdatedEntity(em);
            em.persist(nhomXetNghiem);
            em.flush();
        } else {
            nhomXetNghiem = TestUtil.findAll(em, NhomXetNghiem.class).get(0);
        }
        tPhanNhomXetNghiem.setNhomXN(nhomXetNghiem);
        // Add required entity
        XetNghiem xetNghiem;
        if (TestUtil.findAll(em, XetNghiem.class).isEmpty()) {
            xetNghiem = XetNghiemResourceIT.createUpdatedEntity(em);
            em.persist(xetNghiem);
            em.flush();
        } else {
            xetNghiem = TestUtil.findAll(em, XetNghiem.class).get(0);
        }
        tPhanNhomXetNghiem.setXN(xetNghiem);
        return tPhanNhomXetNghiem;
    }

    @BeforeEach
    public void initTest() {
        tPhanNhomXetNghiem = createEntity(em);
    }

    @Test
    @Transactional
    public void createTPhanNhomXetNghiem() throws Exception {
        int databaseSizeBeforeCreate = tPhanNhomXetNghiemRepository.findAll().size();

        // Create the TPhanNhomXetNghiem
        TPhanNhomXetNghiemDTO tPhanNhomXetNghiemDTO = tPhanNhomXetNghiemMapper.toDto(tPhanNhomXetNghiem);
        restTPhanNhomXetNghiemMockMvc.perform(post("/api/t-phan-nhom-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tPhanNhomXetNghiemDTO)))
            .andExpect(status().isCreated());

        // Validate the TPhanNhomXetNghiem in the database
        List<TPhanNhomXetNghiem> tPhanNhomXetNghiemList = tPhanNhomXetNghiemRepository.findAll();
        assertThat(tPhanNhomXetNghiemList).hasSize(databaseSizeBeforeCreate + 1);
        TPhanNhomXetNghiem testTPhanNhomXetNghiem = tPhanNhomXetNghiemList.get(tPhanNhomXetNghiemList.size() - 1);
    }

    @Test
    @Transactional
    public void createTPhanNhomXetNghiemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tPhanNhomXetNghiemRepository.findAll().size();

        // Create the TPhanNhomXetNghiem with an existing ID
        tPhanNhomXetNghiem.setId(1L);
        TPhanNhomXetNghiemDTO tPhanNhomXetNghiemDTO = tPhanNhomXetNghiemMapper.toDto(tPhanNhomXetNghiem);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTPhanNhomXetNghiemMockMvc.perform(post("/api/t-phan-nhom-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tPhanNhomXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TPhanNhomXetNghiem in the database
        List<TPhanNhomXetNghiem> tPhanNhomXetNghiemList = tPhanNhomXetNghiemRepository.findAll();
        assertThat(tPhanNhomXetNghiemList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTPhanNhomXetNghiems() throws Exception {
        // Initialize the database
        tPhanNhomXetNghiemRepository.saveAndFlush(tPhanNhomXetNghiem);

        // Get all the tPhanNhomXetNghiemList
        restTPhanNhomXetNghiemMockMvc.perform(get("/api/t-phan-nhom-xet-nghiems?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tPhanNhomXetNghiem.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getTPhanNhomXetNghiem() throws Exception {
        // Initialize the database
        tPhanNhomXetNghiemRepository.saveAndFlush(tPhanNhomXetNghiem);

        // Get the tPhanNhomXetNghiem
        restTPhanNhomXetNghiemMockMvc.perform(get("/api/t-phan-nhom-xet-nghiems/{id}", tPhanNhomXetNghiem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(tPhanNhomXetNghiem.getId().intValue()));
    }


    @Test
    @Transactional
    public void getTPhanNhomXetNghiemsByIdFiltering() throws Exception {
        // Initialize the database
        tPhanNhomXetNghiemRepository.saveAndFlush(tPhanNhomXetNghiem);

        Long id = tPhanNhomXetNghiem.getId();

        defaultTPhanNhomXetNghiemShouldBeFound("id.equals=" + id);
        defaultTPhanNhomXetNghiemShouldNotBeFound("id.notEquals=" + id);

        defaultTPhanNhomXetNghiemShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTPhanNhomXetNghiemShouldNotBeFound("id.greaterThan=" + id);

        defaultTPhanNhomXetNghiemShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTPhanNhomXetNghiemShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllTPhanNhomXetNghiemsByNhomXNIsEqualToSomething() throws Exception {
        // Get already existing entity
        NhomXetNghiem nhomXN = tPhanNhomXetNghiem.getNhomXN();
        tPhanNhomXetNghiemRepository.saveAndFlush(tPhanNhomXetNghiem);
        Long nhomXNId = nhomXN.getId();

        // Get all the tPhanNhomXetNghiemList where nhomXN equals to nhomXNId
        defaultTPhanNhomXetNghiemShouldBeFound("nhomXNId.equals=" + nhomXNId);

        // Get all the tPhanNhomXetNghiemList where nhomXN equals to nhomXNId + 1
        defaultTPhanNhomXetNghiemShouldNotBeFound("nhomXNId.equals=" + (nhomXNId + 1));
    }


    @Test
    @Transactional
    public void getAllTPhanNhomXetNghiemsByXNIsEqualToSomething() throws Exception {
        // Get already existing entity
        XetNghiem xN = tPhanNhomXetNghiem.getXN();
        tPhanNhomXetNghiemRepository.saveAndFlush(tPhanNhomXetNghiem);
        Long xNId = xN.getId();

        // Get all the tPhanNhomXetNghiemList where xN equals to xNId
        defaultTPhanNhomXetNghiemShouldBeFound("xNId.equals=" + xNId);

        // Get all the tPhanNhomXetNghiemList where xN equals to xNId + 1
        defaultTPhanNhomXetNghiemShouldNotBeFound("xNId.equals=" + (xNId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTPhanNhomXetNghiemShouldBeFound(String filter) throws Exception {
        restTPhanNhomXetNghiemMockMvc.perform(get("/api/t-phan-nhom-xet-nghiems?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tPhanNhomXetNghiem.getId().intValue())));

        // Check, that the count call also returns 1
        restTPhanNhomXetNghiemMockMvc.perform(get("/api/t-phan-nhom-xet-nghiems/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTPhanNhomXetNghiemShouldNotBeFound(String filter) throws Exception {
        restTPhanNhomXetNghiemMockMvc.perform(get("/api/t-phan-nhom-xet-nghiems?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTPhanNhomXetNghiemMockMvc.perform(get("/api/t-phan-nhom-xet-nghiems/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTPhanNhomXetNghiem() throws Exception {
        // Get the tPhanNhomXetNghiem
        restTPhanNhomXetNghiemMockMvc.perform(get("/api/t-phan-nhom-xet-nghiems/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTPhanNhomXetNghiem() throws Exception {
        // Initialize the database
        tPhanNhomXetNghiemRepository.saveAndFlush(tPhanNhomXetNghiem);

        int databaseSizeBeforeUpdate = tPhanNhomXetNghiemRepository.findAll().size();

        // Update the tPhanNhomXetNghiem
        TPhanNhomXetNghiem updatedTPhanNhomXetNghiem = tPhanNhomXetNghiemRepository.findById(tPhanNhomXetNghiem.getId()).get();
        // Disconnect from session so that the updates on updatedTPhanNhomXetNghiem are not directly saved in db
        em.detach(updatedTPhanNhomXetNghiem);
        TPhanNhomXetNghiemDTO tPhanNhomXetNghiemDTO = tPhanNhomXetNghiemMapper.toDto(updatedTPhanNhomXetNghiem);

        restTPhanNhomXetNghiemMockMvc.perform(put("/api/t-phan-nhom-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tPhanNhomXetNghiemDTO)))
            .andExpect(status().isOk());

        // Validate the TPhanNhomXetNghiem in the database
        List<TPhanNhomXetNghiem> tPhanNhomXetNghiemList = tPhanNhomXetNghiemRepository.findAll();
        assertThat(tPhanNhomXetNghiemList).hasSize(databaseSizeBeforeUpdate);
        TPhanNhomXetNghiem testTPhanNhomXetNghiem = tPhanNhomXetNghiemList.get(tPhanNhomXetNghiemList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingTPhanNhomXetNghiem() throws Exception {
        int databaseSizeBeforeUpdate = tPhanNhomXetNghiemRepository.findAll().size();

        // Create the TPhanNhomXetNghiem
        TPhanNhomXetNghiemDTO tPhanNhomXetNghiemDTO = tPhanNhomXetNghiemMapper.toDto(tPhanNhomXetNghiem);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTPhanNhomXetNghiemMockMvc.perform(put("/api/t-phan-nhom-xet-nghiems").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tPhanNhomXetNghiemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TPhanNhomXetNghiem in the database
        List<TPhanNhomXetNghiem> tPhanNhomXetNghiemList = tPhanNhomXetNghiemRepository.findAll();
        assertThat(tPhanNhomXetNghiemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTPhanNhomXetNghiem() throws Exception {
        // Initialize the database
        tPhanNhomXetNghiemRepository.saveAndFlush(tPhanNhomXetNghiem);

        int databaseSizeBeforeDelete = tPhanNhomXetNghiemRepository.findAll().size();

        // Delete the tPhanNhomXetNghiem
        restTPhanNhomXetNghiemMockMvc.perform(delete("/api/t-phan-nhom-xet-nghiems/{id}", tPhanNhomXetNghiem.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TPhanNhomXetNghiem> tPhanNhomXetNghiemList = tPhanNhomXetNghiemRepository.findAll();
        assertThat(tPhanNhomXetNghiemList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
