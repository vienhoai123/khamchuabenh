package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.NhomBenhLy;
import vn.vnpt.domain.LoaiBenhLy;
import vn.vnpt.repository.NhomBenhLyRepository;
import vn.vnpt.service.NhomBenhLyService;
import vn.vnpt.service.dto.NhomBenhLyDTO;
import vn.vnpt.service.mapper.NhomBenhLyMapper;
import vn.vnpt.service.dto.NhomBenhLyCriteria;
import vn.vnpt.service.NhomBenhLyQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link NhomBenhLyResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class NhomBenhLyResourceIT {

    private static final String DEFAULT_MO_TA = "AAAAAAAAAA";
    private static final String UPDATED_MO_TA = "BBBBBBBBBB";

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    @Autowired
    private NhomBenhLyRepository nhomBenhLyRepository;

    @Autowired
    private NhomBenhLyMapper nhomBenhLyMapper;

    @Autowired
    private NhomBenhLyService nhomBenhLyService;

    @Autowired
    private NhomBenhLyQueryService nhomBenhLyQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restNhomBenhLyMockMvc;

    private NhomBenhLy nhomBenhLy;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NhomBenhLy createEntity(EntityManager em) {
        NhomBenhLy nhomBenhLy = new NhomBenhLy()
            .moTa(DEFAULT_MO_TA)
            .ten(DEFAULT_TEN);
        // Add required entity
        LoaiBenhLy loaiBenhLy;
        if (TestUtil.findAll(em, LoaiBenhLy.class).isEmpty()) {
            loaiBenhLy = LoaiBenhLyResourceIT.createEntity(em);
            em.persist(loaiBenhLy);
            em.flush();
        } else {
            loaiBenhLy = TestUtil.findAll(em, LoaiBenhLy.class).get(0);
        }
        nhomBenhLy.setLoaiBenhLy(loaiBenhLy);
        return nhomBenhLy;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NhomBenhLy createUpdatedEntity(EntityManager em) {
        NhomBenhLy nhomBenhLy = new NhomBenhLy()
            .moTa(UPDATED_MO_TA)
            .ten(UPDATED_TEN);
        // Add required entity
        LoaiBenhLy loaiBenhLy;
        if (TestUtil.findAll(em, LoaiBenhLy.class).isEmpty()) {
            loaiBenhLy = LoaiBenhLyResourceIT.createUpdatedEntity(em);
            em.persist(loaiBenhLy);
            em.flush();
        } else {
            loaiBenhLy = TestUtil.findAll(em, LoaiBenhLy.class).get(0);
        }
        nhomBenhLy.setLoaiBenhLy(loaiBenhLy);
        return nhomBenhLy;
    }

    @BeforeEach
    public void initTest() {
        nhomBenhLy = createEntity(em);
    }

    @Test
    @Transactional
    public void createNhomBenhLy() throws Exception {
        int databaseSizeBeforeCreate = nhomBenhLyRepository.findAll().size();

        // Create the NhomBenhLy
        NhomBenhLyDTO nhomBenhLyDTO = nhomBenhLyMapper.toDto(nhomBenhLy);
        restNhomBenhLyMockMvc.perform(post("/api/nhom-benh-lies").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomBenhLyDTO)))
            .andExpect(status().isCreated());

        // Validate the NhomBenhLy in the database
        List<NhomBenhLy> nhomBenhLyList = nhomBenhLyRepository.findAll();
        assertThat(nhomBenhLyList).hasSize(databaseSizeBeforeCreate + 1);
        NhomBenhLy testNhomBenhLy = nhomBenhLyList.get(nhomBenhLyList.size() - 1);
        assertThat(testNhomBenhLy.getMoTa()).isEqualTo(DEFAULT_MO_TA);
        assertThat(testNhomBenhLy.getTen()).isEqualTo(DEFAULT_TEN);
    }

    @Test
    @Transactional
    public void createNhomBenhLyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = nhomBenhLyRepository.findAll().size();

        // Create the NhomBenhLy with an existing ID
        nhomBenhLy.setId(1L);
        NhomBenhLyDTO nhomBenhLyDTO = nhomBenhLyMapper.toDto(nhomBenhLy);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNhomBenhLyMockMvc.perform(post("/api/nhom-benh-lies").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomBenhLyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NhomBenhLy in the database
        List<NhomBenhLy> nhomBenhLyList = nhomBenhLyRepository.findAll();
        assertThat(nhomBenhLyList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = nhomBenhLyRepository.findAll().size();
        // set the field null
        nhomBenhLy.setTen(null);

        // Create the NhomBenhLy, which fails.
        NhomBenhLyDTO nhomBenhLyDTO = nhomBenhLyMapper.toDto(nhomBenhLy);

        restNhomBenhLyMockMvc.perform(post("/api/nhom-benh-lies").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomBenhLyDTO)))
            .andExpect(status().isBadRequest());

        List<NhomBenhLy> nhomBenhLyList = nhomBenhLyRepository.findAll();
        assertThat(nhomBenhLyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNhomBenhLies() throws Exception {
        // Initialize the database
        nhomBenhLyRepository.saveAndFlush(nhomBenhLy);

        // Get all the nhomBenhLyList
        restNhomBenhLyMockMvc.perform(get("/api/nhom-benh-lies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nhomBenhLy.getId().intValue())))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));
    }
    
    @Test
    @Transactional
    public void getNhomBenhLy() throws Exception {
        // Initialize the database
        nhomBenhLyRepository.saveAndFlush(nhomBenhLy);

        // Get the nhomBenhLy
        restNhomBenhLyMockMvc.perform(get("/api/nhom-benh-lies/{id}", nhomBenhLy.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(nhomBenhLy.getId().intValue()))
            .andExpect(jsonPath("$.moTa").value(DEFAULT_MO_TA))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN));
    }


    @Test
    @Transactional
    public void getNhomBenhLiesByIdFiltering() throws Exception {
        // Initialize the database
        nhomBenhLyRepository.saveAndFlush(nhomBenhLy);

        Long id = nhomBenhLy.getId();

        defaultNhomBenhLyShouldBeFound("id.equals=" + id);
        defaultNhomBenhLyShouldNotBeFound("id.notEquals=" + id);

        defaultNhomBenhLyShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultNhomBenhLyShouldNotBeFound("id.greaterThan=" + id);

        defaultNhomBenhLyShouldBeFound("id.lessThanOrEqual=" + id);
        defaultNhomBenhLyShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllNhomBenhLiesByMoTaIsEqualToSomething() throws Exception {
        // Initialize the database
        nhomBenhLyRepository.saveAndFlush(nhomBenhLy);

        // Get all the nhomBenhLyList where moTa equals to DEFAULT_MO_TA
        defaultNhomBenhLyShouldBeFound("moTa.equals=" + DEFAULT_MO_TA);

        // Get all the nhomBenhLyList where moTa equals to UPDATED_MO_TA
        defaultNhomBenhLyShouldNotBeFound("moTa.equals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllNhomBenhLiesByMoTaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nhomBenhLyRepository.saveAndFlush(nhomBenhLy);

        // Get all the nhomBenhLyList where moTa not equals to DEFAULT_MO_TA
        defaultNhomBenhLyShouldNotBeFound("moTa.notEquals=" + DEFAULT_MO_TA);

        // Get all the nhomBenhLyList where moTa not equals to UPDATED_MO_TA
        defaultNhomBenhLyShouldBeFound("moTa.notEquals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllNhomBenhLiesByMoTaIsInShouldWork() throws Exception {
        // Initialize the database
        nhomBenhLyRepository.saveAndFlush(nhomBenhLy);

        // Get all the nhomBenhLyList where moTa in DEFAULT_MO_TA or UPDATED_MO_TA
        defaultNhomBenhLyShouldBeFound("moTa.in=" + DEFAULT_MO_TA + "," + UPDATED_MO_TA);

        // Get all the nhomBenhLyList where moTa equals to UPDATED_MO_TA
        defaultNhomBenhLyShouldNotBeFound("moTa.in=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllNhomBenhLiesByMoTaIsNullOrNotNull() throws Exception {
        // Initialize the database
        nhomBenhLyRepository.saveAndFlush(nhomBenhLy);

        // Get all the nhomBenhLyList where moTa is not null
        defaultNhomBenhLyShouldBeFound("moTa.specified=true");

        // Get all the nhomBenhLyList where moTa is null
        defaultNhomBenhLyShouldNotBeFound("moTa.specified=false");
    }
                @Test
    @Transactional
    public void getAllNhomBenhLiesByMoTaContainsSomething() throws Exception {
        // Initialize the database
        nhomBenhLyRepository.saveAndFlush(nhomBenhLy);

        // Get all the nhomBenhLyList where moTa contains DEFAULT_MO_TA
        defaultNhomBenhLyShouldBeFound("moTa.contains=" + DEFAULT_MO_TA);

        // Get all the nhomBenhLyList where moTa contains UPDATED_MO_TA
        defaultNhomBenhLyShouldNotBeFound("moTa.contains=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllNhomBenhLiesByMoTaNotContainsSomething() throws Exception {
        // Initialize the database
        nhomBenhLyRepository.saveAndFlush(nhomBenhLy);

        // Get all the nhomBenhLyList where moTa does not contain DEFAULT_MO_TA
        defaultNhomBenhLyShouldNotBeFound("moTa.doesNotContain=" + DEFAULT_MO_TA);

        // Get all the nhomBenhLyList where moTa does not contain UPDATED_MO_TA
        defaultNhomBenhLyShouldBeFound("moTa.doesNotContain=" + UPDATED_MO_TA);
    }


    @Test
    @Transactional
    public void getAllNhomBenhLiesByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        nhomBenhLyRepository.saveAndFlush(nhomBenhLy);

        // Get all the nhomBenhLyList where ten equals to DEFAULT_TEN
        defaultNhomBenhLyShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the nhomBenhLyList where ten equals to UPDATED_TEN
        defaultNhomBenhLyShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhomBenhLiesByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nhomBenhLyRepository.saveAndFlush(nhomBenhLy);

        // Get all the nhomBenhLyList where ten not equals to DEFAULT_TEN
        defaultNhomBenhLyShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the nhomBenhLyList where ten not equals to UPDATED_TEN
        defaultNhomBenhLyShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhomBenhLiesByTenIsInShouldWork() throws Exception {
        // Initialize the database
        nhomBenhLyRepository.saveAndFlush(nhomBenhLy);

        // Get all the nhomBenhLyList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultNhomBenhLyShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the nhomBenhLyList where ten equals to UPDATED_TEN
        defaultNhomBenhLyShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhomBenhLiesByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        nhomBenhLyRepository.saveAndFlush(nhomBenhLy);

        // Get all the nhomBenhLyList where ten is not null
        defaultNhomBenhLyShouldBeFound("ten.specified=true");

        // Get all the nhomBenhLyList where ten is null
        defaultNhomBenhLyShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllNhomBenhLiesByTenContainsSomething() throws Exception {
        // Initialize the database
        nhomBenhLyRepository.saveAndFlush(nhomBenhLy);

        // Get all the nhomBenhLyList where ten contains DEFAULT_TEN
        defaultNhomBenhLyShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the nhomBenhLyList where ten contains UPDATED_TEN
        defaultNhomBenhLyShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhomBenhLiesByTenNotContainsSomething() throws Exception {
        // Initialize the database
        nhomBenhLyRepository.saveAndFlush(nhomBenhLy);

        // Get all the nhomBenhLyList where ten does not contain DEFAULT_TEN
        defaultNhomBenhLyShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the nhomBenhLyList where ten does not contain UPDATED_TEN
        defaultNhomBenhLyShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllNhomBenhLiesByLoaiBenhLyIsEqualToSomething() throws Exception {
        // Get already existing entity
        LoaiBenhLy loaiBenhLy = nhomBenhLy.getLoaiBenhLy();
        nhomBenhLyRepository.saveAndFlush(nhomBenhLy);
        Long loaiBenhLyId = loaiBenhLy.getId();

        // Get all the nhomBenhLyList where loaiBenhLy equals to loaiBenhLyId
        defaultNhomBenhLyShouldBeFound("loaiBenhLyId.equals=" + loaiBenhLyId);

        // Get all the nhomBenhLyList where loaiBenhLy equals to loaiBenhLyId + 1
        defaultNhomBenhLyShouldNotBeFound("loaiBenhLyId.equals=" + (loaiBenhLyId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultNhomBenhLyShouldBeFound(String filter) throws Exception {
        restNhomBenhLyMockMvc.perform(get("/api/nhom-benh-lies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nhomBenhLy.getId().intValue())))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));

        // Check, that the count call also returns 1
        restNhomBenhLyMockMvc.perform(get("/api/nhom-benh-lies/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultNhomBenhLyShouldNotBeFound(String filter) throws Exception {
        restNhomBenhLyMockMvc.perform(get("/api/nhom-benh-lies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restNhomBenhLyMockMvc.perform(get("/api/nhom-benh-lies/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingNhomBenhLy() throws Exception {
        // Get the nhomBenhLy
        restNhomBenhLyMockMvc.perform(get("/api/nhom-benh-lies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNhomBenhLy() throws Exception {
        // Initialize the database
        nhomBenhLyRepository.saveAndFlush(nhomBenhLy);

        int databaseSizeBeforeUpdate = nhomBenhLyRepository.findAll().size();

        // Update the nhomBenhLy
        NhomBenhLy updatedNhomBenhLy = nhomBenhLyRepository.findById(nhomBenhLy.getId()).get();
        // Disconnect from session so that the updates on updatedNhomBenhLy are not directly saved in db
        em.detach(updatedNhomBenhLy);
        updatedNhomBenhLy
            .moTa(UPDATED_MO_TA)
            .ten(UPDATED_TEN);
        NhomBenhLyDTO nhomBenhLyDTO = nhomBenhLyMapper.toDto(updatedNhomBenhLy);

        restNhomBenhLyMockMvc.perform(put("/api/nhom-benh-lies").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomBenhLyDTO)))
            .andExpect(status().isOk());

        // Validate the NhomBenhLy in the database
        List<NhomBenhLy> nhomBenhLyList = nhomBenhLyRepository.findAll();
        assertThat(nhomBenhLyList).hasSize(databaseSizeBeforeUpdate);
        NhomBenhLy testNhomBenhLy = nhomBenhLyList.get(nhomBenhLyList.size() - 1);
        assertThat(testNhomBenhLy.getMoTa()).isEqualTo(UPDATED_MO_TA);
        assertThat(testNhomBenhLy.getTen()).isEqualTo(UPDATED_TEN);
    }

    @Test
    @Transactional
    public void updateNonExistingNhomBenhLy() throws Exception {
        int databaseSizeBeforeUpdate = nhomBenhLyRepository.findAll().size();

        // Create the NhomBenhLy
        NhomBenhLyDTO nhomBenhLyDTO = nhomBenhLyMapper.toDto(nhomBenhLy);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNhomBenhLyMockMvc.perform(put("/api/nhom-benh-lies").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomBenhLyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NhomBenhLy in the database
        List<NhomBenhLy> nhomBenhLyList = nhomBenhLyRepository.findAll();
        assertThat(nhomBenhLyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNhomBenhLy() throws Exception {
        // Initialize the database
        nhomBenhLyRepository.saveAndFlush(nhomBenhLy);

        int databaseSizeBeforeDelete = nhomBenhLyRepository.findAll().size();

        // Delete the nhomBenhLy
        restNhomBenhLyMockMvc.perform(delete("/api/nhom-benh-lies/{id}", nhomBenhLy.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<NhomBenhLy> nhomBenhLyList = nhomBenhLyRepository.findAll();
        assertThat(nhomBenhLyList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
