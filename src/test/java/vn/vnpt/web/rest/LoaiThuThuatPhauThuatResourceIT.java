package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.LoaiThuThuatPhauThuat;
import vn.vnpt.domain.DonVi;
import vn.vnpt.repository.LoaiThuThuatPhauThuatRepository;
import vn.vnpt.service.LoaiThuThuatPhauThuatService;
import vn.vnpt.service.dto.LoaiThuThuatPhauThuatDTO;
import vn.vnpt.service.mapper.LoaiThuThuatPhauThuatMapper;
import vn.vnpt.service.dto.LoaiThuThuatPhauThuatCriteria;
import vn.vnpt.service.LoaiThuThuatPhauThuatQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LoaiThuThuatPhauThuatResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class LoaiThuThuatPhauThuatResourceIT {

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String DEFAULT_MO_TA = "AAAAAAAAAA";
    private static final String UPDATED_MO_TA = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ENABLE = false;
    private static final Boolean UPDATED_ENABLE = true;

    private static final Integer DEFAULT_UU_TIEN = 1;
    private static final Integer UPDATED_UU_TIEN = 2;
    private static final Integer SMALLER_UU_TIEN = 1 - 1;

    private static final String DEFAULT_MA_PHAN_LOAI = "AAAAAAAAAA";
    private static final String UPDATED_MA_PHAN_LOAI = "BBBBBBBBBB";

    @Autowired
    private LoaiThuThuatPhauThuatRepository loaiThuThuatPhauThuatRepository;

    @Autowired
    private LoaiThuThuatPhauThuatMapper loaiThuThuatPhauThuatMapper;

    @Autowired
    private LoaiThuThuatPhauThuatService loaiThuThuatPhauThuatService;

    @Autowired
    private LoaiThuThuatPhauThuatQueryService loaiThuThuatPhauThuatQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLoaiThuThuatPhauThuatMockMvc;

    private LoaiThuThuatPhauThuat loaiThuThuatPhauThuat;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LoaiThuThuatPhauThuat createEntity(EntityManager em) {
        LoaiThuThuatPhauThuat loaiThuThuatPhauThuat = new LoaiThuThuatPhauThuat()
            .ten(DEFAULT_TEN)
            .moTa(DEFAULT_MO_TA)
            .enable(DEFAULT_ENABLE)
            .uuTien(DEFAULT_UU_TIEN)
            .maPhanLoai(DEFAULT_MA_PHAN_LOAI);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        loaiThuThuatPhauThuat.setDonVi(donVi);
        return loaiThuThuatPhauThuat;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LoaiThuThuatPhauThuat createUpdatedEntity(EntityManager em) {
        LoaiThuThuatPhauThuat loaiThuThuatPhauThuat = new LoaiThuThuatPhauThuat()
            .ten(UPDATED_TEN)
            .moTa(UPDATED_MO_TA)
            .enable(UPDATED_ENABLE)
            .uuTien(UPDATED_UU_TIEN)
            .maPhanLoai(UPDATED_MA_PHAN_LOAI);
        // Add required entity
        DonVi donVi;
        if (TestUtil.findAll(em, DonVi.class).isEmpty()) {
            donVi = DonViResourceIT.createUpdatedEntity(em);
            em.persist(donVi);
            em.flush();
        } else {
            donVi = TestUtil.findAll(em, DonVi.class).get(0);
        }
        loaiThuThuatPhauThuat.setDonVi(donVi);
        return loaiThuThuatPhauThuat;
    }

    @BeforeEach
    public void initTest() {
        loaiThuThuatPhauThuat = createEntity(em);
    }

    @Test
    @Transactional
    public void createLoaiThuThuatPhauThuat() throws Exception {
        int databaseSizeBeforeCreate = loaiThuThuatPhauThuatRepository.findAll().size();

        // Create the LoaiThuThuatPhauThuat
        LoaiThuThuatPhauThuatDTO loaiThuThuatPhauThuatDTO = loaiThuThuatPhauThuatMapper.toDto(loaiThuThuatPhauThuat);
        restLoaiThuThuatPhauThuatMockMvc.perform(post("/api/loai-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loaiThuThuatPhauThuatDTO)))
            .andExpect(status().isCreated());

        // Validate the LoaiThuThuatPhauThuat in the database
        List<LoaiThuThuatPhauThuat> loaiThuThuatPhauThuatList = loaiThuThuatPhauThuatRepository.findAll();
        assertThat(loaiThuThuatPhauThuatList).hasSize(databaseSizeBeforeCreate + 1);
        LoaiThuThuatPhauThuat testLoaiThuThuatPhauThuat = loaiThuThuatPhauThuatList.get(loaiThuThuatPhauThuatList.size() - 1);
        assertThat(testLoaiThuThuatPhauThuat.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testLoaiThuThuatPhauThuat.getMoTa()).isEqualTo(DEFAULT_MO_TA);
        assertThat(testLoaiThuThuatPhauThuat.isEnable()).isEqualTo(DEFAULT_ENABLE);
        assertThat(testLoaiThuThuatPhauThuat.getUuTien()).isEqualTo(DEFAULT_UU_TIEN);
        assertThat(testLoaiThuThuatPhauThuat.getMaPhanLoai()).isEqualTo(DEFAULT_MA_PHAN_LOAI);
    }

    @Test
    @Transactional
    public void createLoaiThuThuatPhauThuatWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = loaiThuThuatPhauThuatRepository.findAll().size();

        // Create the LoaiThuThuatPhauThuat with an existing ID
        loaiThuThuatPhauThuat.setId(1L);
        LoaiThuThuatPhauThuatDTO loaiThuThuatPhauThuatDTO = loaiThuThuatPhauThuatMapper.toDto(loaiThuThuatPhauThuat);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLoaiThuThuatPhauThuatMockMvc.perform(post("/api/loai-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loaiThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LoaiThuThuatPhauThuat in the database
        List<LoaiThuThuatPhauThuat> loaiThuThuatPhauThuatList = loaiThuThuatPhauThuatRepository.findAll();
        assertThat(loaiThuThuatPhauThuatList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkUuTienIsRequired() throws Exception {
        int databaseSizeBeforeTest = loaiThuThuatPhauThuatRepository.findAll().size();
        // set the field null
        loaiThuThuatPhauThuat.setUuTien(null);

        // Create the LoaiThuThuatPhauThuat, which fails.
        LoaiThuThuatPhauThuatDTO loaiThuThuatPhauThuatDTO = loaiThuThuatPhauThuatMapper.toDto(loaiThuThuatPhauThuat);

        restLoaiThuThuatPhauThuatMockMvc.perform(post("/api/loai-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loaiThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        List<LoaiThuThuatPhauThuat> loaiThuThuatPhauThuatList = loaiThuThuatPhauThuatRepository.findAll();
        assertThat(loaiThuThuatPhauThuatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuats() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList
        restLoaiThuThuatPhauThuatMockMvc.perform(get("/api/loai-thu-thuat-phau-thuats?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(loaiThuThuatPhauThuat.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].enable").value(hasItem(DEFAULT_ENABLE.booleanValue())))
            .andExpect(jsonPath("$.[*].uuTien").value(hasItem(DEFAULT_UU_TIEN)))
            .andExpect(jsonPath("$.[*].maPhanLoai").value(hasItem(DEFAULT_MA_PHAN_LOAI)));
    }
    
    @Test
    @Transactional
    public void getLoaiThuThuatPhauThuat() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get the loaiThuThuatPhauThuat
        restLoaiThuThuatPhauThuatMockMvc.perform(get("/api/loai-thu-thuat-phau-thuats/{id}", loaiThuThuatPhauThuat.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(loaiThuThuatPhauThuat.getId().intValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.moTa").value(DEFAULT_MO_TA))
            .andExpect(jsonPath("$.enable").value(DEFAULT_ENABLE.booleanValue()))
            .andExpect(jsonPath("$.uuTien").value(DEFAULT_UU_TIEN))
            .andExpect(jsonPath("$.maPhanLoai").value(DEFAULT_MA_PHAN_LOAI));
    }


    @Test
    @Transactional
    public void getLoaiThuThuatPhauThuatsByIdFiltering() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        Long id = loaiThuThuatPhauThuat.getId();

        defaultLoaiThuThuatPhauThuatShouldBeFound("id.equals=" + id);
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("id.notEquals=" + id);

        defaultLoaiThuThuatPhauThuatShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("id.greaterThan=" + id);

        defaultLoaiThuThuatPhauThuatShouldBeFound("id.lessThanOrEqual=" + id);
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where ten equals to DEFAULT_TEN
        defaultLoaiThuThuatPhauThuatShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the loaiThuThuatPhauThuatList where ten equals to UPDATED_TEN
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where ten not equals to DEFAULT_TEN
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the loaiThuThuatPhauThuatList where ten not equals to UPDATED_TEN
        defaultLoaiThuThuatPhauThuatShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultLoaiThuThuatPhauThuatShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the loaiThuThuatPhauThuatList where ten equals to UPDATED_TEN
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where ten is not null
        defaultLoaiThuThuatPhauThuatShouldBeFound("ten.specified=true");

        // Get all the loaiThuThuatPhauThuatList where ten is null
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByTenContainsSomething() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where ten contains DEFAULT_TEN
        defaultLoaiThuThuatPhauThuatShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the loaiThuThuatPhauThuatList where ten contains UPDATED_TEN
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where ten does not contain DEFAULT_TEN
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the loaiThuThuatPhauThuatList where ten does not contain UPDATED_TEN
        defaultLoaiThuThuatPhauThuatShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByMoTaIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where moTa equals to DEFAULT_MO_TA
        defaultLoaiThuThuatPhauThuatShouldBeFound("moTa.equals=" + DEFAULT_MO_TA);

        // Get all the loaiThuThuatPhauThuatList where moTa equals to UPDATED_MO_TA
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("moTa.equals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByMoTaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where moTa not equals to DEFAULT_MO_TA
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("moTa.notEquals=" + DEFAULT_MO_TA);

        // Get all the loaiThuThuatPhauThuatList where moTa not equals to UPDATED_MO_TA
        defaultLoaiThuThuatPhauThuatShouldBeFound("moTa.notEquals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByMoTaIsInShouldWork() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where moTa in DEFAULT_MO_TA or UPDATED_MO_TA
        defaultLoaiThuThuatPhauThuatShouldBeFound("moTa.in=" + DEFAULT_MO_TA + "," + UPDATED_MO_TA);

        // Get all the loaiThuThuatPhauThuatList where moTa equals to UPDATED_MO_TA
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("moTa.in=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByMoTaIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where moTa is not null
        defaultLoaiThuThuatPhauThuatShouldBeFound("moTa.specified=true");

        // Get all the loaiThuThuatPhauThuatList where moTa is null
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("moTa.specified=false");
    }
                @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByMoTaContainsSomething() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where moTa contains DEFAULT_MO_TA
        defaultLoaiThuThuatPhauThuatShouldBeFound("moTa.contains=" + DEFAULT_MO_TA);

        // Get all the loaiThuThuatPhauThuatList where moTa contains UPDATED_MO_TA
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("moTa.contains=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByMoTaNotContainsSomething() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where moTa does not contain DEFAULT_MO_TA
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("moTa.doesNotContain=" + DEFAULT_MO_TA);

        // Get all the loaiThuThuatPhauThuatList where moTa does not contain UPDATED_MO_TA
        defaultLoaiThuThuatPhauThuatShouldBeFound("moTa.doesNotContain=" + UPDATED_MO_TA);
    }


    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByEnableIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where enable equals to DEFAULT_ENABLE
        defaultLoaiThuThuatPhauThuatShouldBeFound("enable.equals=" + DEFAULT_ENABLE);

        // Get all the loaiThuThuatPhauThuatList where enable equals to UPDATED_ENABLE
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("enable.equals=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByEnableIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where enable not equals to DEFAULT_ENABLE
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("enable.notEquals=" + DEFAULT_ENABLE);

        // Get all the loaiThuThuatPhauThuatList where enable not equals to UPDATED_ENABLE
        defaultLoaiThuThuatPhauThuatShouldBeFound("enable.notEquals=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByEnableIsInShouldWork() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where enable in DEFAULT_ENABLE or UPDATED_ENABLE
        defaultLoaiThuThuatPhauThuatShouldBeFound("enable.in=" + DEFAULT_ENABLE + "," + UPDATED_ENABLE);

        // Get all the loaiThuThuatPhauThuatList where enable equals to UPDATED_ENABLE
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("enable.in=" + UPDATED_ENABLE);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByEnableIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where enable is not null
        defaultLoaiThuThuatPhauThuatShouldBeFound("enable.specified=true");

        // Get all the loaiThuThuatPhauThuatList where enable is null
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("enable.specified=false");
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByUuTienIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where uuTien equals to DEFAULT_UU_TIEN
        defaultLoaiThuThuatPhauThuatShouldBeFound("uuTien.equals=" + DEFAULT_UU_TIEN);

        // Get all the loaiThuThuatPhauThuatList where uuTien equals to UPDATED_UU_TIEN
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("uuTien.equals=" + UPDATED_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByUuTienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where uuTien not equals to DEFAULT_UU_TIEN
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("uuTien.notEquals=" + DEFAULT_UU_TIEN);

        // Get all the loaiThuThuatPhauThuatList where uuTien not equals to UPDATED_UU_TIEN
        defaultLoaiThuThuatPhauThuatShouldBeFound("uuTien.notEquals=" + UPDATED_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByUuTienIsInShouldWork() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where uuTien in DEFAULT_UU_TIEN or UPDATED_UU_TIEN
        defaultLoaiThuThuatPhauThuatShouldBeFound("uuTien.in=" + DEFAULT_UU_TIEN + "," + UPDATED_UU_TIEN);

        // Get all the loaiThuThuatPhauThuatList where uuTien equals to UPDATED_UU_TIEN
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("uuTien.in=" + UPDATED_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByUuTienIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where uuTien is not null
        defaultLoaiThuThuatPhauThuatShouldBeFound("uuTien.specified=true");

        // Get all the loaiThuThuatPhauThuatList where uuTien is null
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("uuTien.specified=false");
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByUuTienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where uuTien is greater than or equal to DEFAULT_UU_TIEN
        defaultLoaiThuThuatPhauThuatShouldBeFound("uuTien.greaterThanOrEqual=" + DEFAULT_UU_TIEN);

        // Get all the loaiThuThuatPhauThuatList where uuTien is greater than or equal to UPDATED_UU_TIEN
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("uuTien.greaterThanOrEqual=" + UPDATED_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByUuTienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where uuTien is less than or equal to DEFAULT_UU_TIEN
        defaultLoaiThuThuatPhauThuatShouldBeFound("uuTien.lessThanOrEqual=" + DEFAULT_UU_TIEN);

        // Get all the loaiThuThuatPhauThuatList where uuTien is less than or equal to SMALLER_UU_TIEN
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("uuTien.lessThanOrEqual=" + SMALLER_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByUuTienIsLessThanSomething() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where uuTien is less than DEFAULT_UU_TIEN
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("uuTien.lessThan=" + DEFAULT_UU_TIEN);

        // Get all the loaiThuThuatPhauThuatList where uuTien is less than UPDATED_UU_TIEN
        defaultLoaiThuThuatPhauThuatShouldBeFound("uuTien.lessThan=" + UPDATED_UU_TIEN);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByUuTienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where uuTien is greater than DEFAULT_UU_TIEN
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("uuTien.greaterThan=" + DEFAULT_UU_TIEN);

        // Get all the loaiThuThuatPhauThuatList where uuTien is greater than SMALLER_UU_TIEN
        defaultLoaiThuThuatPhauThuatShouldBeFound("uuTien.greaterThan=" + SMALLER_UU_TIEN);
    }


    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByMaPhanLoaiIsEqualToSomething() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where maPhanLoai equals to DEFAULT_MA_PHAN_LOAI
        defaultLoaiThuThuatPhauThuatShouldBeFound("maPhanLoai.equals=" + DEFAULT_MA_PHAN_LOAI);

        // Get all the loaiThuThuatPhauThuatList where maPhanLoai equals to UPDATED_MA_PHAN_LOAI
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("maPhanLoai.equals=" + UPDATED_MA_PHAN_LOAI);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByMaPhanLoaiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where maPhanLoai not equals to DEFAULT_MA_PHAN_LOAI
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("maPhanLoai.notEquals=" + DEFAULT_MA_PHAN_LOAI);

        // Get all the loaiThuThuatPhauThuatList where maPhanLoai not equals to UPDATED_MA_PHAN_LOAI
        defaultLoaiThuThuatPhauThuatShouldBeFound("maPhanLoai.notEquals=" + UPDATED_MA_PHAN_LOAI);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByMaPhanLoaiIsInShouldWork() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where maPhanLoai in DEFAULT_MA_PHAN_LOAI or UPDATED_MA_PHAN_LOAI
        defaultLoaiThuThuatPhauThuatShouldBeFound("maPhanLoai.in=" + DEFAULT_MA_PHAN_LOAI + "," + UPDATED_MA_PHAN_LOAI);

        // Get all the loaiThuThuatPhauThuatList where maPhanLoai equals to UPDATED_MA_PHAN_LOAI
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("maPhanLoai.in=" + UPDATED_MA_PHAN_LOAI);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByMaPhanLoaiIsNullOrNotNull() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where maPhanLoai is not null
        defaultLoaiThuThuatPhauThuatShouldBeFound("maPhanLoai.specified=true");

        // Get all the loaiThuThuatPhauThuatList where maPhanLoai is null
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("maPhanLoai.specified=false");
    }
                @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByMaPhanLoaiContainsSomething() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where maPhanLoai contains DEFAULT_MA_PHAN_LOAI
        defaultLoaiThuThuatPhauThuatShouldBeFound("maPhanLoai.contains=" + DEFAULT_MA_PHAN_LOAI);

        // Get all the loaiThuThuatPhauThuatList where maPhanLoai contains UPDATED_MA_PHAN_LOAI
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("maPhanLoai.contains=" + UPDATED_MA_PHAN_LOAI);
    }

    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByMaPhanLoaiNotContainsSomething() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        // Get all the loaiThuThuatPhauThuatList where maPhanLoai does not contain DEFAULT_MA_PHAN_LOAI
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("maPhanLoai.doesNotContain=" + DEFAULT_MA_PHAN_LOAI);

        // Get all the loaiThuThuatPhauThuatList where maPhanLoai does not contain UPDATED_MA_PHAN_LOAI
        defaultLoaiThuThuatPhauThuatShouldBeFound("maPhanLoai.doesNotContain=" + UPDATED_MA_PHAN_LOAI);
    }


    @Test
    @Transactional
    public void getAllLoaiThuThuatPhauThuatsByDonViIsEqualToSomething() throws Exception {
        // Get already existing entity
        DonVi donVi = loaiThuThuatPhauThuat.getDonVi();
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);
        Long donViId = donVi.getId();

        // Get all the loaiThuThuatPhauThuatList where donVi equals to donViId
        defaultLoaiThuThuatPhauThuatShouldBeFound("donViId.equals=" + donViId);

        // Get all the loaiThuThuatPhauThuatList where donVi equals to donViId + 1
        defaultLoaiThuThuatPhauThuatShouldNotBeFound("donViId.equals=" + (donViId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLoaiThuThuatPhauThuatShouldBeFound(String filter) throws Exception {
        restLoaiThuThuatPhauThuatMockMvc.perform(get("/api/loai-thu-thuat-phau-thuats?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(loaiThuThuatPhauThuat.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].enable").value(hasItem(DEFAULT_ENABLE.booleanValue())))
            .andExpect(jsonPath("$.[*].uuTien").value(hasItem(DEFAULT_UU_TIEN)))
            .andExpect(jsonPath("$.[*].maPhanLoai").value(hasItem(DEFAULT_MA_PHAN_LOAI)));

        // Check, that the count call also returns 1
        restLoaiThuThuatPhauThuatMockMvc.perform(get("/api/loai-thu-thuat-phau-thuats/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLoaiThuThuatPhauThuatShouldNotBeFound(String filter) throws Exception {
        restLoaiThuThuatPhauThuatMockMvc.perform(get("/api/loai-thu-thuat-phau-thuats?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLoaiThuThuatPhauThuatMockMvc.perform(get("/api/loai-thu-thuat-phau-thuats/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingLoaiThuThuatPhauThuat() throws Exception {
        // Get the loaiThuThuatPhauThuat
        restLoaiThuThuatPhauThuatMockMvc.perform(get("/api/loai-thu-thuat-phau-thuats/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLoaiThuThuatPhauThuat() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        int databaseSizeBeforeUpdate = loaiThuThuatPhauThuatRepository.findAll().size();

        // Update the loaiThuThuatPhauThuat
        LoaiThuThuatPhauThuat updatedLoaiThuThuatPhauThuat = loaiThuThuatPhauThuatRepository.findById(loaiThuThuatPhauThuat.getId()).get();
        // Disconnect from session so that the updates on updatedLoaiThuThuatPhauThuat are not directly saved in db
        em.detach(updatedLoaiThuThuatPhauThuat);
        updatedLoaiThuThuatPhauThuat
            .ten(UPDATED_TEN)
            .moTa(UPDATED_MO_TA)
            .enable(UPDATED_ENABLE)
            .uuTien(UPDATED_UU_TIEN)
            .maPhanLoai(UPDATED_MA_PHAN_LOAI);
        LoaiThuThuatPhauThuatDTO loaiThuThuatPhauThuatDTO = loaiThuThuatPhauThuatMapper.toDto(updatedLoaiThuThuatPhauThuat);

        restLoaiThuThuatPhauThuatMockMvc.perform(put("/api/loai-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loaiThuThuatPhauThuatDTO)))
            .andExpect(status().isOk());

        // Validate the LoaiThuThuatPhauThuat in the database
        List<LoaiThuThuatPhauThuat> loaiThuThuatPhauThuatList = loaiThuThuatPhauThuatRepository.findAll();
        assertThat(loaiThuThuatPhauThuatList).hasSize(databaseSizeBeforeUpdate);
        LoaiThuThuatPhauThuat testLoaiThuThuatPhauThuat = loaiThuThuatPhauThuatList.get(loaiThuThuatPhauThuatList.size() - 1);
        assertThat(testLoaiThuThuatPhauThuat.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testLoaiThuThuatPhauThuat.getMoTa()).isEqualTo(UPDATED_MO_TA);
        assertThat(testLoaiThuThuatPhauThuat.isEnable()).isEqualTo(UPDATED_ENABLE);
        assertThat(testLoaiThuThuatPhauThuat.getUuTien()).isEqualTo(UPDATED_UU_TIEN);
        assertThat(testLoaiThuThuatPhauThuat.getMaPhanLoai()).isEqualTo(UPDATED_MA_PHAN_LOAI);
    }

    @Test
    @Transactional
    public void updateNonExistingLoaiThuThuatPhauThuat() throws Exception {
        int databaseSizeBeforeUpdate = loaiThuThuatPhauThuatRepository.findAll().size();

        // Create the LoaiThuThuatPhauThuat
        LoaiThuThuatPhauThuatDTO loaiThuThuatPhauThuatDTO = loaiThuThuatPhauThuatMapper.toDto(loaiThuThuatPhauThuat);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLoaiThuThuatPhauThuatMockMvc.perform(put("/api/loai-thu-thuat-phau-thuats").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loaiThuThuatPhauThuatDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LoaiThuThuatPhauThuat in the database
        List<LoaiThuThuatPhauThuat> loaiThuThuatPhauThuatList = loaiThuThuatPhauThuatRepository.findAll();
        assertThat(loaiThuThuatPhauThuatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLoaiThuThuatPhauThuat() throws Exception {
        // Initialize the database
        loaiThuThuatPhauThuatRepository.saveAndFlush(loaiThuThuatPhauThuat);

        int databaseSizeBeforeDelete = loaiThuThuatPhauThuatRepository.findAll().size();

        // Delete the loaiThuThuatPhauThuat
        restLoaiThuThuatPhauThuatMockMvc.perform(delete("/api/loai-thu-thuat-phau-thuats/{id}", loaiThuThuatPhauThuat.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LoaiThuThuatPhauThuat> loaiThuThuatPhauThuatList = loaiThuThuatPhauThuatRepository.findAll();
        assertThat(loaiThuThuatPhauThuatList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
