package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.TPhanNhomTTPT;
import vn.vnpt.domain.NhomThuThuatPhauThuat;
import vn.vnpt.domain.ThuThuatPhauThuat;
import vn.vnpt.repository.TPhanNhomTTPTRepository;
import vn.vnpt.service.TPhanNhomTTPTService;
import vn.vnpt.service.dto.TPhanNhomTTPTDTO;
import vn.vnpt.service.mapper.TPhanNhomTTPTMapper;
import vn.vnpt.service.dto.TPhanNhomTTPTCriteria;
import vn.vnpt.service.TPhanNhomTTPTQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TPhanNhomTTPTResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class TPhanNhomTTPTResourceIT {

    @Autowired
    private TPhanNhomTTPTRepository tPhanNhomTTPTRepository;

    @Autowired
    private TPhanNhomTTPTMapper tPhanNhomTTPTMapper;

    @Autowired
    private TPhanNhomTTPTService tPhanNhomTTPTService;

    @Autowired
    private TPhanNhomTTPTQueryService tPhanNhomTTPTQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTPhanNhomTTPTMockMvc;

    private TPhanNhomTTPT tPhanNhomTTPT;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TPhanNhomTTPT createEntity(EntityManager em) {
        TPhanNhomTTPT tPhanNhomTTPT = new TPhanNhomTTPT();
        // Add required entity
        NhomThuThuatPhauThuat nhomThuThuatPhauThuat;
        if (TestUtil.findAll(em, NhomThuThuatPhauThuat.class).isEmpty()) {
            nhomThuThuatPhauThuat = NhomThuThuatPhauThuatResourceIT.createEntity(em);
            em.persist(nhomThuThuatPhauThuat);
            em.flush();
        } else {
            nhomThuThuatPhauThuat = TestUtil.findAll(em, NhomThuThuatPhauThuat.class).get(0);
        }
        tPhanNhomTTPT.setNhomttpt(nhomThuThuatPhauThuat);
        // Add required entity
        ThuThuatPhauThuat thuThuatPhauThuat;
        if (TestUtil.findAll(em, ThuThuatPhauThuat.class).isEmpty()) {
            thuThuatPhauThuat = ThuThuatPhauThuatResourceIT.createEntity(em);
            em.persist(thuThuatPhauThuat);
            em.flush();
        } else {
            thuThuatPhauThuat = TestUtil.findAll(em, ThuThuatPhauThuat.class).get(0);
        }
        tPhanNhomTTPT.setTtpt(thuThuatPhauThuat);
        return tPhanNhomTTPT;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TPhanNhomTTPT createUpdatedEntity(EntityManager em) {
        TPhanNhomTTPT tPhanNhomTTPT = new TPhanNhomTTPT();
        // Add required entity
        NhomThuThuatPhauThuat nhomThuThuatPhauThuat;
        if (TestUtil.findAll(em, NhomThuThuatPhauThuat.class).isEmpty()) {
            nhomThuThuatPhauThuat = NhomThuThuatPhauThuatResourceIT.createUpdatedEntity(em);
            em.persist(nhomThuThuatPhauThuat);
            em.flush();
        } else {
            nhomThuThuatPhauThuat = TestUtil.findAll(em, NhomThuThuatPhauThuat.class).get(0);
        }
        tPhanNhomTTPT.setNhomttpt(nhomThuThuatPhauThuat);
        // Add required entity
        ThuThuatPhauThuat thuThuatPhauThuat;
        if (TestUtil.findAll(em, ThuThuatPhauThuat.class).isEmpty()) {
            thuThuatPhauThuat = ThuThuatPhauThuatResourceIT.createUpdatedEntity(em);
            em.persist(thuThuatPhauThuat);
            em.flush();
        } else {
            thuThuatPhauThuat = TestUtil.findAll(em, ThuThuatPhauThuat.class).get(0);
        }
        tPhanNhomTTPT.setTtpt(thuThuatPhauThuat);
        return tPhanNhomTTPT;
    }

    @BeforeEach
    public void initTest() {
        tPhanNhomTTPT = createEntity(em);
    }

    @Test
    @Transactional
    public void createTPhanNhomTTPT() throws Exception {
        int databaseSizeBeforeCreate = tPhanNhomTTPTRepository.findAll().size();

        // Create the TPhanNhomTTPT
        TPhanNhomTTPTDTO tPhanNhomTTPTDTO = tPhanNhomTTPTMapper.toDto(tPhanNhomTTPT);
        restTPhanNhomTTPTMockMvc.perform(post("/api/t-phan-nhom-ttpts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tPhanNhomTTPTDTO)))
            .andExpect(status().isCreated());

        // Validate the TPhanNhomTTPT in the database
        List<TPhanNhomTTPT> tPhanNhomTTPTList = tPhanNhomTTPTRepository.findAll();
        assertThat(tPhanNhomTTPTList).hasSize(databaseSizeBeforeCreate + 1);
        TPhanNhomTTPT testTPhanNhomTTPT = tPhanNhomTTPTList.get(tPhanNhomTTPTList.size() - 1);
    }

    @Test
    @Transactional
    public void createTPhanNhomTTPTWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tPhanNhomTTPTRepository.findAll().size();

        // Create the TPhanNhomTTPT with an existing ID
        tPhanNhomTTPT.setId(1L);
        TPhanNhomTTPTDTO tPhanNhomTTPTDTO = tPhanNhomTTPTMapper.toDto(tPhanNhomTTPT);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTPhanNhomTTPTMockMvc.perform(post("/api/t-phan-nhom-ttpts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tPhanNhomTTPTDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TPhanNhomTTPT in the database
        List<TPhanNhomTTPT> tPhanNhomTTPTList = tPhanNhomTTPTRepository.findAll();
        assertThat(tPhanNhomTTPTList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTPhanNhomTTPTS() throws Exception {
        // Initialize the database
        tPhanNhomTTPTRepository.saveAndFlush(tPhanNhomTTPT);

        // Get all the tPhanNhomTTPTList
        restTPhanNhomTTPTMockMvc.perform(get("/api/t-phan-nhom-ttpts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tPhanNhomTTPT.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getTPhanNhomTTPT() throws Exception {
        // Initialize the database
        tPhanNhomTTPTRepository.saveAndFlush(tPhanNhomTTPT);

        // Get the tPhanNhomTTPT
        restTPhanNhomTTPTMockMvc.perform(get("/api/t-phan-nhom-ttpts/{id}", tPhanNhomTTPT.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(tPhanNhomTTPT.getId().intValue()));
    }


    @Test
    @Transactional
    public void getTPhanNhomTTPTSByIdFiltering() throws Exception {
        // Initialize the database
        tPhanNhomTTPTRepository.saveAndFlush(tPhanNhomTTPT);

        Long id = tPhanNhomTTPT.getId();

        defaultTPhanNhomTTPTShouldBeFound("id.equals=" + id);
        defaultTPhanNhomTTPTShouldNotBeFound("id.notEquals=" + id);

        defaultTPhanNhomTTPTShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTPhanNhomTTPTShouldNotBeFound("id.greaterThan=" + id);

        defaultTPhanNhomTTPTShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTPhanNhomTTPTShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllTPhanNhomTTPTSByNhomttptIsEqualToSomething() throws Exception {
        // Get already existing entity
        NhomThuThuatPhauThuat nhomttpt = tPhanNhomTTPT.getNhomttpt();
        tPhanNhomTTPTRepository.saveAndFlush(tPhanNhomTTPT);
        Long nhomttptId = nhomttpt.getId();

        // Get all the tPhanNhomTTPTList where nhomttpt equals to nhomttptId
        defaultTPhanNhomTTPTShouldBeFound("nhomttptId.equals=" + nhomttptId);

        // Get all the tPhanNhomTTPTList where nhomttpt equals to nhomttptId + 1
        defaultTPhanNhomTTPTShouldNotBeFound("nhomttptId.equals=" + (nhomttptId + 1));
    }


    @Test
    @Transactional
    public void getAllTPhanNhomTTPTSByTtptIsEqualToSomething() throws Exception {
        // Get already existing entity
        ThuThuatPhauThuat ttpt = tPhanNhomTTPT.getTtpt();
        tPhanNhomTTPTRepository.saveAndFlush(tPhanNhomTTPT);
        Long ttptId = ttpt.getId();

        // Get all the tPhanNhomTTPTList where ttpt equals to ttptId
        defaultTPhanNhomTTPTShouldBeFound("ttptId.equals=" + ttptId);

        // Get all the tPhanNhomTTPTList where ttpt equals to ttptId + 1
        defaultTPhanNhomTTPTShouldNotBeFound("ttptId.equals=" + (ttptId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTPhanNhomTTPTShouldBeFound(String filter) throws Exception {
        restTPhanNhomTTPTMockMvc.perform(get("/api/t-phan-nhom-ttpts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tPhanNhomTTPT.getId().intValue())));

        // Check, that the count call also returns 1
        restTPhanNhomTTPTMockMvc.perform(get("/api/t-phan-nhom-ttpts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTPhanNhomTTPTShouldNotBeFound(String filter) throws Exception {
        restTPhanNhomTTPTMockMvc.perform(get("/api/t-phan-nhom-ttpts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTPhanNhomTTPTMockMvc.perform(get("/api/t-phan-nhom-ttpts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTPhanNhomTTPT() throws Exception {
        // Get the tPhanNhomTTPT
        restTPhanNhomTTPTMockMvc.perform(get("/api/t-phan-nhom-ttpts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTPhanNhomTTPT() throws Exception {
        // Initialize the database
        tPhanNhomTTPTRepository.saveAndFlush(tPhanNhomTTPT);

        int databaseSizeBeforeUpdate = tPhanNhomTTPTRepository.findAll().size();

        // Update the tPhanNhomTTPT
        TPhanNhomTTPT updatedTPhanNhomTTPT = tPhanNhomTTPTRepository.findById(tPhanNhomTTPT.getId()).get();
        // Disconnect from session so that the updates on updatedTPhanNhomTTPT are not directly saved in db
        em.detach(updatedTPhanNhomTTPT);
        TPhanNhomTTPTDTO tPhanNhomTTPTDTO = tPhanNhomTTPTMapper.toDto(updatedTPhanNhomTTPT);

        restTPhanNhomTTPTMockMvc.perform(put("/api/t-phan-nhom-ttpts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tPhanNhomTTPTDTO)))
            .andExpect(status().isOk());

        // Validate the TPhanNhomTTPT in the database
        List<TPhanNhomTTPT> tPhanNhomTTPTList = tPhanNhomTTPTRepository.findAll();
        assertThat(tPhanNhomTTPTList).hasSize(databaseSizeBeforeUpdate);
        TPhanNhomTTPT testTPhanNhomTTPT = tPhanNhomTTPTList.get(tPhanNhomTTPTList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingTPhanNhomTTPT() throws Exception {
        int databaseSizeBeforeUpdate = tPhanNhomTTPTRepository.findAll().size();

        // Create the TPhanNhomTTPT
        TPhanNhomTTPTDTO tPhanNhomTTPTDTO = tPhanNhomTTPTMapper.toDto(tPhanNhomTTPT);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTPhanNhomTTPTMockMvc.perform(put("/api/t-phan-nhom-ttpts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tPhanNhomTTPTDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TPhanNhomTTPT in the database
        List<TPhanNhomTTPT> tPhanNhomTTPTList = tPhanNhomTTPTRepository.findAll();
        assertThat(tPhanNhomTTPTList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTPhanNhomTTPT() throws Exception {
        // Initialize the database
        tPhanNhomTTPTRepository.saveAndFlush(tPhanNhomTTPT);

        int databaseSizeBeforeDelete = tPhanNhomTTPTRepository.findAll().size();

        // Delete the tPhanNhomTTPT
        restTPhanNhomTTPTMockMvc.perform(delete("/api/t-phan-nhom-ttpts/{id}", tPhanNhomTTPT.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TPhanNhomTTPT> tPhanNhomTTPTList = tPhanNhomTTPTRepository.findAll();
        assertThat(tPhanNhomTTPTList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
