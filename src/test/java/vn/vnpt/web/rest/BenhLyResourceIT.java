package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.BenhLy;
import vn.vnpt.domain.NhomBenhLy;
import vn.vnpt.repository.BenhLyRepository;
import vn.vnpt.service.BenhLyService;
import vn.vnpt.service.dto.BenhLyDTO;
import vn.vnpt.service.mapper.BenhLyMapper;
import vn.vnpt.service.dto.BenhLyCriteria;
import vn.vnpt.service.BenhLyQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BenhLyResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class BenhLyResourceIT {

    private static final Boolean DEFAULT_ENABLED = false;
    private static final Boolean UPDATED_ENABLED = true;

    private static final String DEFAULT_ICD = "AAAAAAAAAA";
    private static final String UPDATED_ICD = "BBBBBBBBBB";

    private static final String DEFAULT_MO_TA = "AAAAAAAAAA";
    private static final String UPDATED_MO_TA = "BBBBBBBBBB";

    private static final String DEFAULT_MO_TA_TIENG_ANH = "AAAAAAAAAA";
    private static final String UPDATED_MO_TA_TIENG_ANH = "BBBBBBBBBB";

    private static final String DEFAULT_TEN_KHONG_DAU = "AAAAAAAAAA";
    private static final String UPDATED_TEN_KHONG_DAU = "BBBBBBBBBB";

    private static final String DEFAULT_VIET_TAT = "AAAAAAAAAA";
    private static final String UPDATED_VIET_TAT = "BBBBBBBBBB";

    private static final String DEFAULT_VNCODE = "AAAAAAAAAA";
    private static final String UPDATED_VNCODE = "BBBBBBBBBB";

    @Autowired
    private BenhLyRepository benhLyRepository;

    @Autowired
    private BenhLyMapper benhLyMapper;

    @Autowired
    private BenhLyService benhLyService;

    @Autowired
    private BenhLyQueryService benhLyQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBenhLyMockMvc;

    private BenhLy benhLy;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BenhLy createEntity(EntityManager em) {
        BenhLy benhLy = new BenhLy()
            .enabled(DEFAULT_ENABLED)
            .icd(DEFAULT_ICD)
            .moTa(DEFAULT_MO_TA)
            .moTaTiengAnh(DEFAULT_MO_TA_TIENG_ANH)
            .tenKhongDau(DEFAULT_TEN_KHONG_DAU)
            .vietTat(DEFAULT_VIET_TAT)
            .vncode(DEFAULT_VNCODE);
        // Add required entity
        NhomBenhLy nhomBenhLy;
        if (TestUtil.findAll(em, NhomBenhLy.class).isEmpty()) {
            nhomBenhLy = NhomBenhLyResourceIT.createEntity(em);
            em.persist(nhomBenhLy);
            em.flush();
        } else {
            nhomBenhLy = TestUtil.findAll(em, NhomBenhLy.class).get(0);
        }
        benhLy.setNhomBenhLy(nhomBenhLy);
        return benhLy;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BenhLy createUpdatedEntity(EntityManager em) {
        BenhLy benhLy = new BenhLy()
            .enabled(UPDATED_ENABLED)
            .icd(UPDATED_ICD)
            .moTa(UPDATED_MO_TA)
            .moTaTiengAnh(UPDATED_MO_TA_TIENG_ANH)
            .tenKhongDau(UPDATED_TEN_KHONG_DAU)
            .vietTat(UPDATED_VIET_TAT)
            .vncode(UPDATED_VNCODE);
        // Add required entity
        NhomBenhLy nhomBenhLy;
        if (TestUtil.findAll(em, NhomBenhLy.class).isEmpty()) {
            nhomBenhLy = NhomBenhLyResourceIT.createUpdatedEntity(em);
            em.persist(nhomBenhLy);
            em.flush();
        } else {
            nhomBenhLy = TestUtil.findAll(em, NhomBenhLy.class).get(0);
        }
        benhLy.setNhomBenhLy(nhomBenhLy);
        return benhLy;
    }

    @BeforeEach
    public void initTest() {
        benhLy = createEntity(em);
    }

    @Test
    @Transactional
    public void createBenhLy() throws Exception {
        int databaseSizeBeforeCreate = benhLyRepository.findAll().size();

        // Create the BenhLy
        BenhLyDTO benhLyDTO = benhLyMapper.toDto(benhLy);
        restBenhLyMockMvc.perform(post("/api/benh-lies").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhLyDTO)))
            .andExpect(status().isCreated());

        // Validate the BenhLy in the database
        List<BenhLy> benhLyList = benhLyRepository.findAll();
        assertThat(benhLyList).hasSize(databaseSizeBeforeCreate + 1);
        BenhLy testBenhLy = benhLyList.get(benhLyList.size() - 1);
        assertThat(testBenhLy.isEnabled()).isEqualTo(DEFAULT_ENABLED);
        assertThat(testBenhLy.getIcd()).isEqualTo(DEFAULT_ICD);
        assertThat(testBenhLy.getMoTa()).isEqualTo(DEFAULT_MO_TA);
        assertThat(testBenhLy.getMoTaTiengAnh()).isEqualTo(DEFAULT_MO_TA_TIENG_ANH);
        assertThat(testBenhLy.getTenKhongDau()).isEqualTo(DEFAULT_TEN_KHONG_DAU);
        assertThat(testBenhLy.getVietTat()).isEqualTo(DEFAULT_VIET_TAT);
        assertThat(testBenhLy.getVncode()).isEqualTo(DEFAULT_VNCODE);
    }

    @Test
    @Transactional
    public void createBenhLyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = benhLyRepository.findAll().size();

        // Create the BenhLy with an existing ID
        benhLy.setId(1L);
        BenhLyDTO benhLyDTO = benhLyMapper.toDto(benhLy);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBenhLyMockMvc.perform(post("/api/benh-lies").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhLyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BenhLy in the database
        List<BenhLy> benhLyList = benhLyRepository.findAll();
        assertThat(benhLyList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkEnabledIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhLyRepository.findAll().size();
        // set the field null
        benhLy.setEnabled(null);

        // Create the BenhLy, which fails.
        BenhLyDTO benhLyDTO = benhLyMapper.toDto(benhLy);

        restBenhLyMockMvc.perform(post("/api/benh-lies").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhLyDTO)))
            .andExpect(status().isBadRequest());

        List<BenhLy> benhLyList = benhLyRepository.findAll();
        assertThat(benhLyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIcdIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhLyRepository.findAll().size();
        // set the field null
        benhLy.setIcd(null);

        // Create the BenhLy, which fails.
        BenhLyDTO benhLyDTO = benhLyMapper.toDto(benhLy);

        restBenhLyMockMvc.perform(post("/api/benh-lies").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhLyDTO)))
            .andExpect(status().isBadRequest());

        List<BenhLy> benhLyList = benhLyRepository.findAll();
        assertThat(benhLyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMoTaIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhLyRepository.findAll().size();
        // set the field null
        benhLy.setMoTa(null);

        // Create the BenhLy, which fails.
        BenhLyDTO benhLyDTO = benhLyMapper.toDto(benhLy);

        restBenhLyMockMvc.perform(post("/api/benh-lies").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhLyDTO)))
            .andExpect(status().isBadRequest());

        List<BenhLy> benhLyList = benhLyRepository.findAll();
        assertThat(benhLyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkVncodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = benhLyRepository.findAll().size();
        // set the field null
        benhLy.setVncode(null);

        // Create the BenhLy, which fails.
        BenhLyDTO benhLyDTO = benhLyMapper.toDto(benhLy);

        restBenhLyMockMvc.perform(post("/api/benh-lies").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhLyDTO)))
            .andExpect(status().isBadRequest());

        List<BenhLy> benhLyList = benhLyRepository.findAll();
        assertThat(benhLyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBenhLies() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList
        restBenhLyMockMvc.perform(get("/api/benh-lies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(benhLy.getId().intValue())))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].icd").value(hasItem(DEFAULT_ICD)))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].moTaTiengAnh").value(hasItem(DEFAULT_MO_TA_TIENG_ANH)))
            .andExpect(jsonPath("$.[*].tenKhongDau").value(hasItem(DEFAULT_TEN_KHONG_DAU)))
            .andExpect(jsonPath("$.[*].vietTat").value(hasItem(DEFAULT_VIET_TAT)))
            .andExpect(jsonPath("$.[*].vncode").value(hasItem(DEFAULT_VNCODE)));
    }
    
    @Test
    @Transactional
    public void getBenhLy() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get the benhLy
        restBenhLyMockMvc.perform(get("/api/benh-lies/{id}", benhLy.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(benhLy.getId().intValue()))
            .andExpect(jsonPath("$.enabled").value(DEFAULT_ENABLED.booleanValue()))
            .andExpect(jsonPath("$.icd").value(DEFAULT_ICD))
            .andExpect(jsonPath("$.moTa").value(DEFAULT_MO_TA))
            .andExpect(jsonPath("$.moTaTiengAnh").value(DEFAULT_MO_TA_TIENG_ANH))
            .andExpect(jsonPath("$.tenKhongDau").value(DEFAULT_TEN_KHONG_DAU))
            .andExpect(jsonPath("$.vietTat").value(DEFAULT_VIET_TAT))
            .andExpect(jsonPath("$.vncode").value(DEFAULT_VNCODE));
    }


    @Test
    @Transactional
    public void getBenhLiesByIdFiltering() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        Long id = benhLy.getId();

        defaultBenhLyShouldBeFound("id.equals=" + id);
        defaultBenhLyShouldNotBeFound("id.notEquals=" + id);

        defaultBenhLyShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultBenhLyShouldNotBeFound("id.greaterThan=" + id);

        defaultBenhLyShouldBeFound("id.lessThanOrEqual=" + id);
        defaultBenhLyShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllBenhLiesByEnabledIsEqualToSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where enabled equals to DEFAULT_ENABLED
        defaultBenhLyShouldBeFound("enabled.equals=" + DEFAULT_ENABLED);

        // Get all the benhLyList where enabled equals to UPDATED_ENABLED
        defaultBenhLyShouldNotBeFound("enabled.equals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByEnabledIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where enabled not equals to DEFAULT_ENABLED
        defaultBenhLyShouldNotBeFound("enabled.notEquals=" + DEFAULT_ENABLED);

        // Get all the benhLyList where enabled not equals to UPDATED_ENABLED
        defaultBenhLyShouldBeFound("enabled.notEquals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByEnabledIsInShouldWork() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where enabled in DEFAULT_ENABLED or UPDATED_ENABLED
        defaultBenhLyShouldBeFound("enabled.in=" + DEFAULT_ENABLED + "," + UPDATED_ENABLED);

        // Get all the benhLyList where enabled equals to UPDATED_ENABLED
        defaultBenhLyShouldNotBeFound("enabled.in=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByEnabledIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where enabled is not null
        defaultBenhLyShouldBeFound("enabled.specified=true");

        // Get all the benhLyList where enabled is null
        defaultBenhLyShouldNotBeFound("enabled.specified=false");
    }

    @Test
    @Transactional
    public void getAllBenhLiesByIcdIsEqualToSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where icd equals to DEFAULT_ICD
        defaultBenhLyShouldBeFound("icd.equals=" + DEFAULT_ICD);

        // Get all the benhLyList where icd equals to UPDATED_ICD
        defaultBenhLyShouldNotBeFound("icd.equals=" + UPDATED_ICD);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByIcdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where icd not equals to DEFAULT_ICD
        defaultBenhLyShouldNotBeFound("icd.notEquals=" + DEFAULT_ICD);

        // Get all the benhLyList where icd not equals to UPDATED_ICD
        defaultBenhLyShouldBeFound("icd.notEquals=" + UPDATED_ICD);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByIcdIsInShouldWork() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where icd in DEFAULT_ICD or UPDATED_ICD
        defaultBenhLyShouldBeFound("icd.in=" + DEFAULT_ICD + "," + UPDATED_ICD);

        // Get all the benhLyList where icd equals to UPDATED_ICD
        defaultBenhLyShouldNotBeFound("icd.in=" + UPDATED_ICD);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByIcdIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where icd is not null
        defaultBenhLyShouldBeFound("icd.specified=true");

        // Get all the benhLyList where icd is null
        defaultBenhLyShouldNotBeFound("icd.specified=false");
    }
                @Test
    @Transactional
    public void getAllBenhLiesByIcdContainsSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where icd contains DEFAULT_ICD
        defaultBenhLyShouldBeFound("icd.contains=" + DEFAULT_ICD);

        // Get all the benhLyList where icd contains UPDATED_ICD
        defaultBenhLyShouldNotBeFound("icd.contains=" + UPDATED_ICD);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByIcdNotContainsSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where icd does not contain DEFAULT_ICD
        defaultBenhLyShouldNotBeFound("icd.doesNotContain=" + DEFAULT_ICD);

        // Get all the benhLyList where icd does not contain UPDATED_ICD
        defaultBenhLyShouldBeFound("icd.doesNotContain=" + UPDATED_ICD);
    }


    @Test
    @Transactional
    public void getAllBenhLiesByMoTaIsEqualToSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where moTa equals to DEFAULT_MO_TA
        defaultBenhLyShouldBeFound("moTa.equals=" + DEFAULT_MO_TA);

        // Get all the benhLyList where moTa equals to UPDATED_MO_TA
        defaultBenhLyShouldNotBeFound("moTa.equals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByMoTaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where moTa not equals to DEFAULT_MO_TA
        defaultBenhLyShouldNotBeFound("moTa.notEquals=" + DEFAULT_MO_TA);

        // Get all the benhLyList where moTa not equals to UPDATED_MO_TA
        defaultBenhLyShouldBeFound("moTa.notEquals=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByMoTaIsInShouldWork() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where moTa in DEFAULT_MO_TA or UPDATED_MO_TA
        defaultBenhLyShouldBeFound("moTa.in=" + DEFAULT_MO_TA + "," + UPDATED_MO_TA);

        // Get all the benhLyList where moTa equals to UPDATED_MO_TA
        defaultBenhLyShouldNotBeFound("moTa.in=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByMoTaIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where moTa is not null
        defaultBenhLyShouldBeFound("moTa.specified=true");

        // Get all the benhLyList where moTa is null
        defaultBenhLyShouldNotBeFound("moTa.specified=false");
    }
                @Test
    @Transactional
    public void getAllBenhLiesByMoTaContainsSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where moTa contains DEFAULT_MO_TA
        defaultBenhLyShouldBeFound("moTa.contains=" + DEFAULT_MO_TA);

        // Get all the benhLyList where moTa contains UPDATED_MO_TA
        defaultBenhLyShouldNotBeFound("moTa.contains=" + UPDATED_MO_TA);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByMoTaNotContainsSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where moTa does not contain DEFAULT_MO_TA
        defaultBenhLyShouldNotBeFound("moTa.doesNotContain=" + DEFAULT_MO_TA);

        // Get all the benhLyList where moTa does not contain UPDATED_MO_TA
        defaultBenhLyShouldBeFound("moTa.doesNotContain=" + UPDATED_MO_TA);
    }


    @Test
    @Transactional
    public void getAllBenhLiesByMoTaTiengAnhIsEqualToSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where moTaTiengAnh equals to DEFAULT_MO_TA_TIENG_ANH
        defaultBenhLyShouldBeFound("moTaTiengAnh.equals=" + DEFAULT_MO_TA_TIENG_ANH);

        // Get all the benhLyList where moTaTiengAnh equals to UPDATED_MO_TA_TIENG_ANH
        defaultBenhLyShouldNotBeFound("moTaTiengAnh.equals=" + UPDATED_MO_TA_TIENG_ANH);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByMoTaTiengAnhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where moTaTiengAnh not equals to DEFAULT_MO_TA_TIENG_ANH
        defaultBenhLyShouldNotBeFound("moTaTiengAnh.notEquals=" + DEFAULT_MO_TA_TIENG_ANH);

        // Get all the benhLyList where moTaTiengAnh not equals to UPDATED_MO_TA_TIENG_ANH
        defaultBenhLyShouldBeFound("moTaTiengAnh.notEquals=" + UPDATED_MO_TA_TIENG_ANH);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByMoTaTiengAnhIsInShouldWork() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where moTaTiengAnh in DEFAULT_MO_TA_TIENG_ANH or UPDATED_MO_TA_TIENG_ANH
        defaultBenhLyShouldBeFound("moTaTiengAnh.in=" + DEFAULT_MO_TA_TIENG_ANH + "," + UPDATED_MO_TA_TIENG_ANH);

        // Get all the benhLyList where moTaTiengAnh equals to UPDATED_MO_TA_TIENG_ANH
        defaultBenhLyShouldNotBeFound("moTaTiengAnh.in=" + UPDATED_MO_TA_TIENG_ANH);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByMoTaTiengAnhIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where moTaTiengAnh is not null
        defaultBenhLyShouldBeFound("moTaTiengAnh.specified=true");

        // Get all the benhLyList where moTaTiengAnh is null
        defaultBenhLyShouldNotBeFound("moTaTiengAnh.specified=false");
    }
                @Test
    @Transactional
    public void getAllBenhLiesByMoTaTiengAnhContainsSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where moTaTiengAnh contains DEFAULT_MO_TA_TIENG_ANH
        defaultBenhLyShouldBeFound("moTaTiengAnh.contains=" + DEFAULT_MO_TA_TIENG_ANH);

        // Get all the benhLyList where moTaTiengAnh contains UPDATED_MO_TA_TIENG_ANH
        defaultBenhLyShouldNotBeFound("moTaTiengAnh.contains=" + UPDATED_MO_TA_TIENG_ANH);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByMoTaTiengAnhNotContainsSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where moTaTiengAnh does not contain DEFAULT_MO_TA_TIENG_ANH
        defaultBenhLyShouldNotBeFound("moTaTiengAnh.doesNotContain=" + DEFAULT_MO_TA_TIENG_ANH);

        // Get all the benhLyList where moTaTiengAnh does not contain UPDATED_MO_TA_TIENG_ANH
        defaultBenhLyShouldBeFound("moTaTiengAnh.doesNotContain=" + UPDATED_MO_TA_TIENG_ANH);
    }


    @Test
    @Transactional
    public void getAllBenhLiesByTenKhongDauIsEqualToSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where tenKhongDau equals to DEFAULT_TEN_KHONG_DAU
        defaultBenhLyShouldBeFound("tenKhongDau.equals=" + DEFAULT_TEN_KHONG_DAU);

        // Get all the benhLyList where tenKhongDau equals to UPDATED_TEN_KHONG_DAU
        defaultBenhLyShouldNotBeFound("tenKhongDau.equals=" + UPDATED_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByTenKhongDauIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where tenKhongDau not equals to DEFAULT_TEN_KHONG_DAU
        defaultBenhLyShouldNotBeFound("tenKhongDau.notEquals=" + DEFAULT_TEN_KHONG_DAU);

        // Get all the benhLyList where tenKhongDau not equals to UPDATED_TEN_KHONG_DAU
        defaultBenhLyShouldBeFound("tenKhongDau.notEquals=" + UPDATED_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByTenKhongDauIsInShouldWork() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where tenKhongDau in DEFAULT_TEN_KHONG_DAU or UPDATED_TEN_KHONG_DAU
        defaultBenhLyShouldBeFound("tenKhongDau.in=" + DEFAULT_TEN_KHONG_DAU + "," + UPDATED_TEN_KHONG_DAU);

        // Get all the benhLyList where tenKhongDau equals to UPDATED_TEN_KHONG_DAU
        defaultBenhLyShouldNotBeFound("tenKhongDau.in=" + UPDATED_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByTenKhongDauIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where tenKhongDau is not null
        defaultBenhLyShouldBeFound("tenKhongDau.specified=true");

        // Get all the benhLyList where tenKhongDau is null
        defaultBenhLyShouldNotBeFound("tenKhongDau.specified=false");
    }
                @Test
    @Transactional
    public void getAllBenhLiesByTenKhongDauContainsSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where tenKhongDau contains DEFAULT_TEN_KHONG_DAU
        defaultBenhLyShouldBeFound("tenKhongDau.contains=" + DEFAULT_TEN_KHONG_DAU);

        // Get all the benhLyList where tenKhongDau contains UPDATED_TEN_KHONG_DAU
        defaultBenhLyShouldNotBeFound("tenKhongDau.contains=" + UPDATED_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByTenKhongDauNotContainsSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where tenKhongDau does not contain DEFAULT_TEN_KHONG_DAU
        defaultBenhLyShouldNotBeFound("tenKhongDau.doesNotContain=" + DEFAULT_TEN_KHONG_DAU);

        // Get all the benhLyList where tenKhongDau does not contain UPDATED_TEN_KHONG_DAU
        defaultBenhLyShouldBeFound("tenKhongDau.doesNotContain=" + UPDATED_TEN_KHONG_DAU);
    }


    @Test
    @Transactional
    public void getAllBenhLiesByVietTatIsEqualToSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where vietTat equals to DEFAULT_VIET_TAT
        defaultBenhLyShouldBeFound("vietTat.equals=" + DEFAULT_VIET_TAT);

        // Get all the benhLyList where vietTat equals to UPDATED_VIET_TAT
        defaultBenhLyShouldNotBeFound("vietTat.equals=" + UPDATED_VIET_TAT);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByVietTatIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where vietTat not equals to DEFAULT_VIET_TAT
        defaultBenhLyShouldNotBeFound("vietTat.notEquals=" + DEFAULT_VIET_TAT);

        // Get all the benhLyList where vietTat not equals to UPDATED_VIET_TAT
        defaultBenhLyShouldBeFound("vietTat.notEquals=" + UPDATED_VIET_TAT);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByVietTatIsInShouldWork() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where vietTat in DEFAULT_VIET_TAT or UPDATED_VIET_TAT
        defaultBenhLyShouldBeFound("vietTat.in=" + DEFAULT_VIET_TAT + "," + UPDATED_VIET_TAT);

        // Get all the benhLyList where vietTat equals to UPDATED_VIET_TAT
        defaultBenhLyShouldNotBeFound("vietTat.in=" + UPDATED_VIET_TAT);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByVietTatIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where vietTat is not null
        defaultBenhLyShouldBeFound("vietTat.specified=true");

        // Get all the benhLyList where vietTat is null
        defaultBenhLyShouldNotBeFound("vietTat.specified=false");
    }
                @Test
    @Transactional
    public void getAllBenhLiesByVietTatContainsSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where vietTat contains DEFAULT_VIET_TAT
        defaultBenhLyShouldBeFound("vietTat.contains=" + DEFAULT_VIET_TAT);

        // Get all the benhLyList where vietTat contains UPDATED_VIET_TAT
        defaultBenhLyShouldNotBeFound("vietTat.contains=" + UPDATED_VIET_TAT);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByVietTatNotContainsSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where vietTat does not contain DEFAULT_VIET_TAT
        defaultBenhLyShouldNotBeFound("vietTat.doesNotContain=" + DEFAULT_VIET_TAT);

        // Get all the benhLyList where vietTat does not contain UPDATED_VIET_TAT
        defaultBenhLyShouldBeFound("vietTat.doesNotContain=" + UPDATED_VIET_TAT);
    }


    @Test
    @Transactional
    public void getAllBenhLiesByVncodeIsEqualToSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where vncode equals to DEFAULT_VNCODE
        defaultBenhLyShouldBeFound("vncode.equals=" + DEFAULT_VNCODE);

        // Get all the benhLyList where vncode equals to UPDATED_VNCODE
        defaultBenhLyShouldNotBeFound("vncode.equals=" + UPDATED_VNCODE);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByVncodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where vncode not equals to DEFAULT_VNCODE
        defaultBenhLyShouldNotBeFound("vncode.notEquals=" + DEFAULT_VNCODE);

        // Get all the benhLyList where vncode not equals to UPDATED_VNCODE
        defaultBenhLyShouldBeFound("vncode.notEquals=" + UPDATED_VNCODE);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByVncodeIsInShouldWork() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where vncode in DEFAULT_VNCODE or UPDATED_VNCODE
        defaultBenhLyShouldBeFound("vncode.in=" + DEFAULT_VNCODE + "," + UPDATED_VNCODE);

        // Get all the benhLyList where vncode equals to UPDATED_VNCODE
        defaultBenhLyShouldNotBeFound("vncode.in=" + UPDATED_VNCODE);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByVncodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where vncode is not null
        defaultBenhLyShouldBeFound("vncode.specified=true");

        // Get all the benhLyList where vncode is null
        defaultBenhLyShouldNotBeFound("vncode.specified=false");
    }
                @Test
    @Transactional
    public void getAllBenhLiesByVncodeContainsSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where vncode contains DEFAULT_VNCODE
        defaultBenhLyShouldBeFound("vncode.contains=" + DEFAULT_VNCODE);

        // Get all the benhLyList where vncode contains UPDATED_VNCODE
        defaultBenhLyShouldNotBeFound("vncode.contains=" + UPDATED_VNCODE);
    }

    @Test
    @Transactional
    public void getAllBenhLiesByVncodeNotContainsSomething() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        // Get all the benhLyList where vncode does not contain DEFAULT_VNCODE
        defaultBenhLyShouldNotBeFound("vncode.doesNotContain=" + DEFAULT_VNCODE);

        // Get all the benhLyList where vncode does not contain UPDATED_VNCODE
        defaultBenhLyShouldBeFound("vncode.doesNotContain=" + UPDATED_VNCODE);
    }


    @Test
    @Transactional
    public void getAllBenhLiesByNhomBenhLyIsEqualToSomething() throws Exception {
        // Get already existing entity
        NhomBenhLy nhomBenhLy = benhLy.getNhomBenhLy();
        benhLyRepository.saveAndFlush(benhLy);
        Long nhomBenhLyId = nhomBenhLy.getId();

        // Get all the benhLyList where nhomBenhLy equals to nhomBenhLyId
        defaultBenhLyShouldBeFound("nhomBenhLyId.equals=" + nhomBenhLyId);

        // Get all the benhLyList where nhomBenhLy equals to nhomBenhLyId + 1
        defaultBenhLyShouldNotBeFound("nhomBenhLyId.equals=" + (nhomBenhLyId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultBenhLyShouldBeFound(String filter) throws Exception {
        restBenhLyMockMvc.perform(get("/api/benh-lies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(benhLy.getId().intValue())))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())))
            .andExpect(jsonPath("$.[*].icd").value(hasItem(DEFAULT_ICD)))
            .andExpect(jsonPath("$.[*].moTa").value(hasItem(DEFAULT_MO_TA)))
            .andExpect(jsonPath("$.[*].moTaTiengAnh").value(hasItem(DEFAULT_MO_TA_TIENG_ANH)))
            .andExpect(jsonPath("$.[*].tenKhongDau").value(hasItem(DEFAULT_TEN_KHONG_DAU)))
            .andExpect(jsonPath("$.[*].vietTat").value(hasItem(DEFAULT_VIET_TAT)))
            .andExpect(jsonPath("$.[*].vncode").value(hasItem(DEFAULT_VNCODE)));

        // Check, that the count call also returns 1
        restBenhLyMockMvc.perform(get("/api/benh-lies/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultBenhLyShouldNotBeFound(String filter) throws Exception {
        restBenhLyMockMvc.perform(get("/api/benh-lies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restBenhLyMockMvc.perform(get("/api/benh-lies/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingBenhLy() throws Exception {
        // Get the benhLy
        restBenhLyMockMvc.perform(get("/api/benh-lies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBenhLy() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        int databaseSizeBeforeUpdate = benhLyRepository.findAll().size();

        // Update the benhLy
        BenhLy updatedBenhLy = benhLyRepository.findById(benhLy.getId()).get();
        // Disconnect from session so that the updates on updatedBenhLy are not directly saved in db
        em.detach(updatedBenhLy);
        updatedBenhLy
            .enabled(UPDATED_ENABLED)
            .icd(UPDATED_ICD)
            .moTa(UPDATED_MO_TA)
            .moTaTiengAnh(UPDATED_MO_TA_TIENG_ANH)
            .tenKhongDau(UPDATED_TEN_KHONG_DAU)
            .vietTat(UPDATED_VIET_TAT)
            .vncode(UPDATED_VNCODE);
        BenhLyDTO benhLyDTO = benhLyMapper.toDto(updatedBenhLy);

        restBenhLyMockMvc.perform(put("/api/benh-lies").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhLyDTO)))
            .andExpect(status().isOk());

        // Validate the BenhLy in the database
        List<BenhLy> benhLyList = benhLyRepository.findAll();
        assertThat(benhLyList).hasSize(databaseSizeBeforeUpdate);
        BenhLy testBenhLy = benhLyList.get(benhLyList.size() - 1);
        assertThat(testBenhLy.isEnabled()).isEqualTo(UPDATED_ENABLED);
        assertThat(testBenhLy.getIcd()).isEqualTo(UPDATED_ICD);
        assertThat(testBenhLy.getMoTa()).isEqualTo(UPDATED_MO_TA);
        assertThat(testBenhLy.getMoTaTiengAnh()).isEqualTo(UPDATED_MO_TA_TIENG_ANH);
        assertThat(testBenhLy.getTenKhongDau()).isEqualTo(UPDATED_TEN_KHONG_DAU);
        assertThat(testBenhLy.getVietTat()).isEqualTo(UPDATED_VIET_TAT);
        assertThat(testBenhLy.getVncode()).isEqualTo(UPDATED_VNCODE);
    }

    @Test
    @Transactional
    public void updateNonExistingBenhLy() throws Exception {
        int databaseSizeBeforeUpdate = benhLyRepository.findAll().size();

        // Create the BenhLy
        BenhLyDTO benhLyDTO = benhLyMapper.toDto(benhLy);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBenhLyMockMvc.perform(put("/api/benh-lies").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(benhLyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BenhLy in the database
        List<BenhLy> benhLyList = benhLyRepository.findAll();
        assertThat(benhLyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBenhLy() throws Exception {
        // Initialize the database
        benhLyRepository.saveAndFlush(benhLy);

        int databaseSizeBeforeDelete = benhLyRepository.findAll().size();

        // Delete the benhLy
        restBenhLyMockMvc.perform(delete("/api/benh-lies/{id}", benhLy.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BenhLy> benhLyList = benhLyRepository.findAll();
        assertThat(benhLyList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
