package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.QuanHuyen;
import vn.vnpt.domain.TinhThanhPho;
import vn.vnpt.repository.QuanHuyenRepository;
import vn.vnpt.service.QuanHuyenService;
import vn.vnpt.service.dto.QuanHuyenDTO;
import vn.vnpt.service.mapper.QuanHuyenMapper;
import vn.vnpt.service.dto.QuanHuyenCriteria;
import vn.vnpt.service.QuanHuyenQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link QuanHuyenResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class QuanHuyenResourceIT {

    private static final String DEFAULT_CAP = "AAAAAAAAAA";
    private static final String UPDATED_CAP = "BBBBBBBBBB";

    private static final String DEFAULT_GUESS_PHRASE = "AAAAAAAAAA";
    private static final String UPDATED_GUESS_PHRASE = "BBBBBBBBBB";

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String DEFAULT_TEN_KHONG_DAU = "AAAAAAAAAA";
    private static final String UPDATED_TEN_KHONG_DAU = "BBBBBBBBBB";

    @Autowired
    private QuanHuyenRepository quanHuyenRepository;

    @Autowired
    private QuanHuyenMapper quanHuyenMapper;

    @Autowired
    private QuanHuyenService quanHuyenService;

    @Autowired
    private QuanHuyenQueryService quanHuyenQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restQuanHuyenMockMvc;

    private QuanHuyen quanHuyen;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static QuanHuyen createEntity(EntityManager em) {
        QuanHuyen quanHuyen = new QuanHuyen()
            .cap(DEFAULT_CAP)
            .guessPhrase(DEFAULT_GUESS_PHRASE)
            .ten(DEFAULT_TEN)
            .tenKhongDau(DEFAULT_TEN_KHONG_DAU);
        // Add required entity
        TinhThanhPho tinhThanhPho;
        if (TestUtil.findAll(em, TinhThanhPho.class).isEmpty()) {
            tinhThanhPho = TinhThanhPhoResourceIT.createEntity(em);
            em.persist(tinhThanhPho);
            em.flush();
        } else {
            tinhThanhPho = TestUtil.findAll(em, TinhThanhPho.class).get(0);
        }
        quanHuyen.setTinhThanhPho(tinhThanhPho);
        return quanHuyen;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static QuanHuyen createUpdatedEntity(EntityManager em) {
        QuanHuyen quanHuyen = new QuanHuyen()
            .cap(UPDATED_CAP)
            .guessPhrase(UPDATED_GUESS_PHRASE)
            .ten(UPDATED_TEN)
            .tenKhongDau(UPDATED_TEN_KHONG_DAU);
        // Add required entity
        TinhThanhPho tinhThanhPho;
        if (TestUtil.findAll(em, TinhThanhPho.class).isEmpty()) {
            tinhThanhPho = TinhThanhPhoResourceIT.createUpdatedEntity(em);
            em.persist(tinhThanhPho);
            em.flush();
        } else {
            tinhThanhPho = TestUtil.findAll(em, TinhThanhPho.class).get(0);
        }
        quanHuyen.setTinhThanhPho(tinhThanhPho);
        return quanHuyen;
    }

    @BeforeEach
    public void initTest() {
        quanHuyen = createEntity(em);
    }

    @Test
    @Transactional
    public void createQuanHuyen() throws Exception {
        int databaseSizeBeforeCreate = quanHuyenRepository.findAll().size();

        // Create the QuanHuyen
        QuanHuyenDTO quanHuyenDTO = quanHuyenMapper.toDto(quanHuyen);
        restQuanHuyenMockMvc.perform(post("/api/quan-huyens").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(quanHuyenDTO)))
            .andExpect(status().isCreated());

        // Validate the QuanHuyen in the database
        List<QuanHuyen> quanHuyenList = quanHuyenRepository.findAll();
        assertThat(quanHuyenList).hasSize(databaseSizeBeforeCreate + 1);
        QuanHuyen testQuanHuyen = quanHuyenList.get(quanHuyenList.size() - 1);
        assertThat(testQuanHuyen.getCap()).isEqualTo(DEFAULT_CAP);
        assertThat(testQuanHuyen.getGuessPhrase()).isEqualTo(DEFAULT_GUESS_PHRASE);
        assertThat(testQuanHuyen.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testQuanHuyen.getTenKhongDau()).isEqualTo(DEFAULT_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void createQuanHuyenWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = quanHuyenRepository.findAll().size();

        // Create the QuanHuyen with an existing ID
        quanHuyen.setId(1L);
        QuanHuyenDTO quanHuyenDTO = quanHuyenMapper.toDto(quanHuyen);

        // An entity with an existing ID cannot be created, so this API call must fail
        restQuanHuyenMockMvc.perform(post("/api/quan-huyens").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(quanHuyenDTO)))
            .andExpect(status().isBadRequest());

        // Validate the QuanHuyen in the database
        List<QuanHuyen> quanHuyenList = quanHuyenRepository.findAll();
        assertThat(quanHuyenList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCapIsRequired() throws Exception {
        int databaseSizeBeforeTest = quanHuyenRepository.findAll().size();
        // set the field null
        quanHuyen.setCap(null);

        // Create the QuanHuyen, which fails.
        QuanHuyenDTO quanHuyenDTO = quanHuyenMapper.toDto(quanHuyen);

        restQuanHuyenMockMvc.perform(post("/api/quan-huyens").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(quanHuyenDTO)))
            .andExpect(status().isBadRequest());

        List<QuanHuyen> quanHuyenList = quanHuyenRepository.findAll();
        assertThat(quanHuyenList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = quanHuyenRepository.findAll().size();
        // set the field null
        quanHuyen.setTen(null);

        // Create the QuanHuyen, which fails.
        QuanHuyenDTO quanHuyenDTO = quanHuyenMapper.toDto(quanHuyen);

        restQuanHuyenMockMvc.perform(post("/api/quan-huyens").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(quanHuyenDTO)))
            .andExpect(status().isBadRequest());

        List<QuanHuyen> quanHuyenList = quanHuyenRepository.findAll();
        assertThat(quanHuyenList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTenKhongDauIsRequired() throws Exception {
        int databaseSizeBeforeTest = quanHuyenRepository.findAll().size();
        // set the field null
        quanHuyen.setTenKhongDau(null);

        // Create the QuanHuyen, which fails.
        QuanHuyenDTO quanHuyenDTO = quanHuyenMapper.toDto(quanHuyen);

        restQuanHuyenMockMvc.perform(post("/api/quan-huyens").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(quanHuyenDTO)))
            .andExpect(status().isBadRequest());

        List<QuanHuyen> quanHuyenList = quanHuyenRepository.findAll();
        assertThat(quanHuyenList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllQuanHuyens() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList
        restQuanHuyenMockMvc.perform(get("/api/quan-huyens?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(quanHuyen.getId().intValue())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].guessPhrase").value(hasItem(DEFAULT_GUESS_PHRASE)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].tenKhongDau").value(hasItem(DEFAULT_TEN_KHONG_DAU)));
    }
    
    @Test
    @Transactional
    public void getQuanHuyen() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get the quanHuyen
        restQuanHuyenMockMvc.perform(get("/api/quan-huyens/{id}", quanHuyen.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(quanHuyen.getId().intValue()))
            .andExpect(jsonPath("$.cap").value(DEFAULT_CAP))
            .andExpect(jsonPath("$.guessPhrase").value(DEFAULT_GUESS_PHRASE))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.tenKhongDau").value(DEFAULT_TEN_KHONG_DAU));
    }


    @Test
    @Transactional
    public void getQuanHuyensByIdFiltering() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        Long id = quanHuyen.getId();

        defaultQuanHuyenShouldBeFound("id.equals=" + id);
        defaultQuanHuyenShouldNotBeFound("id.notEquals=" + id);

        defaultQuanHuyenShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultQuanHuyenShouldNotBeFound("id.greaterThan=" + id);

        defaultQuanHuyenShouldBeFound("id.lessThanOrEqual=" + id);
        defaultQuanHuyenShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllQuanHuyensByCapIsEqualToSomething() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where cap equals to DEFAULT_CAP
        defaultQuanHuyenShouldBeFound("cap.equals=" + DEFAULT_CAP);

        // Get all the quanHuyenList where cap equals to UPDATED_CAP
        defaultQuanHuyenShouldNotBeFound("cap.equals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllQuanHuyensByCapIsNotEqualToSomething() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where cap not equals to DEFAULT_CAP
        defaultQuanHuyenShouldNotBeFound("cap.notEquals=" + DEFAULT_CAP);

        // Get all the quanHuyenList where cap not equals to UPDATED_CAP
        defaultQuanHuyenShouldBeFound("cap.notEquals=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllQuanHuyensByCapIsInShouldWork() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where cap in DEFAULT_CAP or UPDATED_CAP
        defaultQuanHuyenShouldBeFound("cap.in=" + DEFAULT_CAP + "," + UPDATED_CAP);

        // Get all the quanHuyenList where cap equals to UPDATED_CAP
        defaultQuanHuyenShouldNotBeFound("cap.in=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllQuanHuyensByCapIsNullOrNotNull() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where cap is not null
        defaultQuanHuyenShouldBeFound("cap.specified=true");

        // Get all the quanHuyenList where cap is null
        defaultQuanHuyenShouldNotBeFound("cap.specified=false");
    }
                @Test
    @Transactional
    public void getAllQuanHuyensByCapContainsSomething() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where cap contains DEFAULT_CAP
        defaultQuanHuyenShouldBeFound("cap.contains=" + DEFAULT_CAP);

        // Get all the quanHuyenList where cap contains UPDATED_CAP
        defaultQuanHuyenShouldNotBeFound("cap.contains=" + UPDATED_CAP);
    }

    @Test
    @Transactional
    public void getAllQuanHuyensByCapNotContainsSomething() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where cap does not contain DEFAULT_CAP
        defaultQuanHuyenShouldNotBeFound("cap.doesNotContain=" + DEFAULT_CAP);

        // Get all the quanHuyenList where cap does not contain UPDATED_CAP
        defaultQuanHuyenShouldBeFound("cap.doesNotContain=" + UPDATED_CAP);
    }


    @Test
    @Transactional
    public void getAllQuanHuyensByGuessPhraseIsEqualToSomething() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where guessPhrase equals to DEFAULT_GUESS_PHRASE
        defaultQuanHuyenShouldBeFound("guessPhrase.equals=" + DEFAULT_GUESS_PHRASE);

        // Get all the quanHuyenList where guessPhrase equals to UPDATED_GUESS_PHRASE
        defaultQuanHuyenShouldNotBeFound("guessPhrase.equals=" + UPDATED_GUESS_PHRASE);
    }

    @Test
    @Transactional
    public void getAllQuanHuyensByGuessPhraseIsNotEqualToSomething() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where guessPhrase not equals to DEFAULT_GUESS_PHRASE
        defaultQuanHuyenShouldNotBeFound("guessPhrase.notEquals=" + DEFAULT_GUESS_PHRASE);

        // Get all the quanHuyenList where guessPhrase not equals to UPDATED_GUESS_PHRASE
        defaultQuanHuyenShouldBeFound("guessPhrase.notEquals=" + UPDATED_GUESS_PHRASE);
    }

    @Test
    @Transactional
    public void getAllQuanHuyensByGuessPhraseIsInShouldWork() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where guessPhrase in DEFAULT_GUESS_PHRASE or UPDATED_GUESS_PHRASE
        defaultQuanHuyenShouldBeFound("guessPhrase.in=" + DEFAULT_GUESS_PHRASE + "," + UPDATED_GUESS_PHRASE);

        // Get all the quanHuyenList where guessPhrase equals to UPDATED_GUESS_PHRASE
        defaultQuanHuyenShouldNotBeFound("guessPhrase.in=" + UPDATED_GUESS_PHRASE);
    }

    @Test
    @Transactional
    public void getAllQuanHuyensByGuessPhraseIsNullOrNotNull() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where guessPhrase is not null
        defaultQuanHuyenShouldBeFound("guessPhrase.specified=true");

        // Get all the quanHuyenList where guessPhrase is null
        defaultQuanHuyenShouldNotBeFound("guessPhrase.specified=false");
    }
                @Test
    @Transactional
    public void getAllQuanHuyensByGuessPhraseContainsSomething() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where guessPhrase contains DEFAULT_GUESS_PHRASE
        defaultQuanHuyenShouldBeFound("guessPhrase.contains=" + DEFAULT_GUESS_PHRASE);

        // Get all the quanHuyenList where guessPhrase contains UPDATED_GUESS_PHRASE
        defaultQuanHuyenShouldNotBeFound("guessPhrase.contains=" + UPDATED_GUESS_PHRASE);
    }

    @Test
    @Transactional
    public void getAllQuanHuyensByGuessPhraseNotContainsSomething() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where guessPhrase does not contain DEFAULT_GUESS_PHRASE
        defaultQuanHuyenShouldNotBeFound("guessPhrase.doesNotContain=" + DEFAULT_GUESS_PHRASE);

        // Get all the quanHuyenList where guessPhrase does not contain UPDATED_GUESS_PHRASE
        defaultQuanHuyenShouldBeFound("guessPhrase.doesNotContain=" + UPDATED_GUESS_PHRASE);
    }


    @Test
    @Transactional
    public void getAllQuanHuyensByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where ten equals to DEFAULT_TEN
        defaultQuanHuyenShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the quanHuyenList where ten equals to UPDATED_TEN
        defaultQuanHuyenShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllQuanHuyensByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where ten not equals to DEFAULT_TEN
        defaultQuanHuyenShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the quanHuyenList where ten not equals to UPDATED_TEN
        defaultQuanHuyenShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllQuanHuyensByTenIsInShouldWork() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultQuanHuyenShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the quanHuyenList where ten equals to UPDATED_TEN
        defaultQuanHuyenShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllQuanHuyensByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where ten is not null
        defaultQuanHuyenShouldBeFound("ten.specified=true");

        // Get all the quanHuyenList where ten is null
        defaultQuanHuyenShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllQuanHuyensByTenContainsSomething() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where ten contains DEFAULT_TEN
        defaultQuanHuyenShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the quanHuyenList where ten contains UPDATED_TEN
        defaultQuanHuyenShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllQuanHuyensByTenNotContainsSomething() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where ten does not contain DEFAULT_TEN
        defaultQuanHuyenShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the quanHuyenList where ten does not contain UPDATED_TEN
        defaultQuanHuyenShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }


    @Test
    @Transactional
    public void getAllQuanHuyensByTenKhongDauIsEqualToSomething() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where tenKhongDau equals to DEFAULT_TEN_KHONG_DAU
        defaultQuanHuyenShouldBeFound("tenKhongDau.equals=" + DEFAULT_TEN_KHONG_DAU);

        // Get all the quanHuyenList where tenKhongDau equals to UPDATED_TEN_KHONG_DAU
        defaultQuanHuyenShouldNotBeFound("tenKhongDau.equals=" + UPDATED_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void getAllQuanHuyensByTenKhongDauIsNotEqualToSomething() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where tenKhongDau not equals to DEFAULT_TEN_KHONG_DAU
        defaultQuanHuyenShouldNotBeFound("tenKhongDau.notEquals=" + DEFAULT_TEN_KHONG_DAU);

        // Get all the quanHuyenList where tenKhongDau not equals to UPDATED_TEN_KHONG_DAU
        defaultQuanHuyenShouldBeFound("tenKhongDau.notEquals=" + UPDATED_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void getAllQuanHuyensByTenKhongDauIsInShouldWork() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where tenKhongDau in DEFAULT_TEN_KHONG_DAU or UPDATED_TEN_KHONG_DAU
        defaultQuanHuyenShouldBeFound("tenKhongDau.in=" + DEFAULT_TEN_KHONG_DAU + "," + UPDATED_TEN_KHONG_DAU);

        // Get all the quanHuyenList where tenKhongDau equals to UPDATED_TEN_KHONG_DAU
        defaultQuanHuyenShouldNotBeFound("tenKhongDau.in=" + UPDATED_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void getAllQuanHuyensByTenKhongDauIsNullOrNotNull() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where tenKhongDau is not null
        defaultQuanHuyenShouldBeFound("tenKhongDau.specified=true");

        // Get all the quanHuyenList where tenKhongDau is null
        defaultQuanHuyenShouldNotBeFound("tenKhongDau.specified=false");
    }
                @Test
    @Transactional
    public void getAllQuanHuyensByTenKhongDauContainsSomething() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where tenKhongDau contains DEFAULT_TEN_KHONG_DAU
        defaultQuanHuyenShouldBeFound("tenKhongDau.contains=" + DEFAULT_TEN_KHONG_DAU);

        // Get all the quanHuyenList where tenKhongDau contains UPDATED_TEN_KHONG_DAU
        defaultQuanHuyenShouldNotBeFound("tenKhongDau.contains=" + UPDATED_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void getAllQuanHuyensByTenKhongDauNotContainsSomething() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        // Get all the quanHuyenList where tenKhongDau does not contain DEFAULT_TEN_KHONG_DAU
        defaultQuanHuyenShouldNotBeFound("tenKhongDau.doesNotContain=" + DEFAULT_TEN_KHONG_DAU);

        // Get all the quanHuyenList where tenKhongDau does not contain UPDATED_TEN_KHONG_DAU
        defaultQuanHuyenShouldBeFound("tenKhongDau.doesNotContain=" + UPDATED_TEN_KHONG_DAU);
    }


    @Test
    @Transactional
    public void getAllQuanHuyensByTinhThanhPhoIsEqualToSomething() throws Exception {
        // Get already existing entity
        TinhThanhPho tinhThanhPho = quanHuyen.getTinhThanhPho();
        quanHuyenRepository.saveAndFlush(quanHuyen);
        Long tinhThanhPhoId = tinhThanhPho.getId();

        // Get all the quanHuyenList where tinhThanhPho equals to tinhThanhPhoId
        defaultQuanHuyenShouldBeFound("tinhThanhPhoId.equals=" + tinhThanhPhoId);

        // Get all the quanHuyenList where tinhThanhPho equals to tinhThanhPhoId + 1
        defaultQuanHuyenShouldNotBeFound("tinhThanhPhoId.equals=" + (tinhThanhPhoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultQuanHuyenShouldBeFound(String filter) throws Exception {
        restQuanHuyenMockMvc.perform(get("/api/quan-huyens?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(quanHuyen.getId().intValue())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP)))
            .andExpect(jsonPath("$.[*].guessPhrase").value(hasItem(DEFAULT_GUESS_PHRASE)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].tenKhongDau").value(hasItem(DEFAULT_TEN_KHONG_DAU)));

        // Check, that the count call also returns 1
        restQuanHuyenMockMvc.perform(get("/api/quan-huyens/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultQuanHuyenShouldNotBeFound(String filter) throws Exception {
        restQuanHuyenMockMvc.perform(get("/api/quan-huyens?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restQuanHuyenMockMvc.perform(get("/api/quan-huyens/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingQuanHuyen() throws Exception {
        // Get the quanHuyen
        restQuanHuyenMockMvc.perform(get("/api/quan-huyens/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateQuanHuyen() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        int databaseSizeBeforeUpdate = quanHuyenRepository.findAll().size();

        // Update the quanHuyen
        QuanHuyen updatedQuanHuyen = quanHuyenRepository.findById(quanHuyen.getId()).get();
        // Disconnect from session so that the updates on updatedQuanHuyen are not directly saved in db
        em.detach(updatedQuanHuyen);
        updatedQuanHuyen
            .cap(UPDATED_CAP)
            .guessPhrase(UPDATED_GUESS_PHRASE)
            .ten(UPDATED_TEN)
            .tenKhongDau(UPDATED_TEN_KHONG_DAU);
        QuanHuyenDTO quanHuyenDTO = quanHuyenMapper.toDto(updatedQuanHuyen);

        restQuanHuyenMockMvc.perform(put("/api/quan-huyens").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(quanHuyenDTO)))
            .andExpect(status().isOk());

        // Validate the QuanHuyen in the database
        List<QuanHuyen> quanHuyenList = quanHuyenRepository.findAll();
        assertThat(quanHuyenList).hasSize(databaseSizeBeforeUpdate);
        QuanHuyen testQuanHuyen = quanHuyenList.get(quanHuyenList.size() - 1);
        assertThat(testQuanHuyen.getCap()).isEqualTo(UPDATED_CAP);
        assertThat(testQuanHuyen.getGuessPhrase()).isEqualTo(UPDATED_GUESS_PHRASE);
        assertThat(testQuanHuyen.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testQuanHuyen.getTenKhongDau()).isEqualTo(UPDATED_TEN_KHONG_DAU);
    }

    @Test
    @Transactional
    public void updateNonExistingQuanHuyen() throws Exception {
        int databaseSizeBeforeUpdate = quanHuyenRepository.findAll().size();

        // Create the QuanHuyen
        QuanHuyenDTO quanHuyenDTO = quanHuyenMapper.toDto(quanHuyen);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restQuanHuyenMockMvc.perform(put("/api/quan-huyens").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(quanHuyenDTO)))
            .andExpect(status().isBadRequest());

        // Validate the QuanHuyen in the database
        List<QuanHuyen> quanHuyenList = quanHuyenRepository.findAll();
        assertThat(quanHuyenList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteQuanHuyen() throws Exception {
        // Initialize the database
        quanHuyenRepository.saveAndFlush(quanHuyen);

        int databaseSizeBeforeDelete = quanHuyenRepository.findAll().size();

        // Delete the quanHuyen
        restQuanHuyenMockMvc.perform(delete("/api/quan-huyens/{id}", quanHuyen.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<QuanHuyen> quanHuyenList = quanHuyenRepository.findAll();
        assertThat(quanHuyenList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
