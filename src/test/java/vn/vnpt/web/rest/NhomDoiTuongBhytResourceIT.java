package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.NhomDoiTuongBhyt;
import vn.vnpt.repository.NhomDoiTuongBhytRepository;
import vn.vnpt.service.NhomDoiTuongBhytService;
import vn.vnpt.service.dto.NhomDoiTuongBhytDTO;
import vn.vnpt.service.mapper.NhomDoiTuongBhytMapper;
import vn.vnpt.service.dto.NhomDoiTuongBhytCriteria;
import vn.vnpt.service.NhomDoiTuongBhytQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link NhomDoiTuongBhytResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class NhomDoiTuongBhytResourceIT {

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    @Autowired
    private NhomDoiTuongBhytRepository nhomDoiTuongBhytRepository;

    @Autowired
    private NhomDoiTuongBhytMapper nhomDoiTuongBhytMapper;

    @Autowired
    private NhomDoiTuongBhytService nhomDoiTuongBhytService;

    @Autowired
    private NhomDoiTuongBhytQueryService nhomDoiTuongBhytQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restNhomDoiTuongBhytMockMvc;

    private NhomDoiTuongBhyt nhomDoiTuongBhyt;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NhomDoiTuongBhyt createEntity(EntityManager em) {
        NhomDoiTuongBhyt nhomDoiTuongBhyt = new NhomDoiTuongBhyt()
            .ten(DEFAULT_TEN);
        return nhomDoiTuongBhyt;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NhomDoiTuongBhyt createUpdatedEntity(EntityManager em) {
        NhomDoiTuongBhyt nhomDoiTuongBhyt = new NhomDoiTuongBhyt()
            .ten(UPDATED_TEN);
        return nhomDoiTuongBhyt;
    }

    @BeforeEach
    public void initTest() {
        nhomDoiTuongBhyt = createEntity(em);
    }

    @Test
    @Transactional
    public void createNhomDoiTuongBhyt() throws Exception {
        int databaseSizeBeforeCreate = nhomDoiTuongBhytRepository.findAll().size();

        // Create the NhomDoiTuongBhyt
        NhomDoiTuongBhytDTO nhomDoiTuongBhytDTO = nhomDoiTuongBhytMapper.toDto(nhomDoiTuongBhyt);
        restNhomDoiTuongBhytMockMvc.perform(post("/api/nhom-doi-tuong-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomDoiTuongBhytDTO)))
            .andExpect(status().isCreated());

        // Validate the NhomDoiTuongBhyt in the database
        List<NhomDoiTuongBhyt> nhomDoiTuongBhytList = nhomDoiTuongBhytRepository.findAll();
        assertThat(nhomDoiTuongBhytList).hasSize(databaseSizeBeforeCreate + 1);
        NhomDoiTuongBhyt testNhomDoiTuongBhyt = nhomDoiTuongBhytList.get(nhomDoiTuongBhytList.size() - 1);
        assertThat(testNhomDoiTuongBhyt.getTen()).isEqualTo(DEFAULT_TEN);
    }

    @Test
    @Transactional
    public void createNhomDoiTuongBhytWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = nhomDoiTuongBhytRepository.findAll().size();

        // Create the NhomDoiTuongBhyt with an existing ID
        nhomDoiTuongBhyt.setId(1L);
        NhomDoiTuongBhytDTO nhomDoiTuongBhytDTO = nhomDoiTuongBhytMapper.toDto(nhomDoiTuongBhyt);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNhomDoiTuongBhytMockMvc.perform(post("/api/nhom-doi-tuong-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomDoiTuongBhytDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NhomDoiTuongBhyt in the database
        List<NhomDoiTuongBhyt> nhomDoiTuongBhytList = nhomDoiTuongBhytRepository.findAll();
        assertThat(nhomDoiTuongBhytList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = nhomDoiTuongBhytRepository.findAll().size();
        // set the field null
        nhomDoiTuongBhyt.setTen(null);

        // Create the NhomDoiTuongBhyt, which fails.
        NhomDoiTuongBhytDTO nhomDoiTuongBhytDTO = nhomDoiTuongBhytMapper.toDto(nhomDoiTuongBhyt);

        restNhomDoiTuongBhytMockMvc.perform(post("/api/nhom-doi-tuong-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomDoiTuongBhytDTO)))
            .andExpect(status().isBadRequest());

        List<NhomDoiTuongBhyt> nhomDoiTuongBhytList = nhomDoiTuongBhytRepository.findAll();
        assertThat(nhomDoiTuongBhytList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNhomDoiTuongBhyts() throws Exception {
        // Initialize the database
        nhomDoiTuongBhytRepository.saveAndFlush(nhomDoiTuongBhyt);

        // Get all the nhomDoiTuongBhytList
        restNhomDoiTuongBhytMockMvc.perform(get("/api/nhom-doi-tuong-bhyts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nhomDoiTuongBhyt.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));
    }
    
    @Test
    @Transactional
    public void getNhomDoiTuongBhyt() throws Exception {
        // Initialize the database
        nhomDoiTuongBhytRepository.saveAndFlush(nhomDoiTuongBhyt);

        // Get the nhomDoiTuongBhyt
        restNhomDoiTuongBhytMockMvc.perform(get("/api/nhom-doi-tuong-bhyts/{id}", nhomDoiTuongBhyt.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(nhomDoiTuongBhyt.getId().intValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN));
    }


    @Test
    @Transactional
    public void getNhomDoiTuongBhytsByIdFiltering() throws Exception {
        // Initialize the database
        nhomDoiTuongBhytRepository.saveAndFlush(nhomDoiTuongBhyt);

        Long id = nhomDoiTuongBhyt.getId();

        defaultNhomDoiTuongBhytShouldBeFound("id.equals=" + id);
        defaultNhomDoiTuongBhytShouldNotBeFound("id.notEquals=" + id);

        defaultNhomDoiTuongBhytShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultNhomDoiTuongBhytShouldNotBeFound("id.greaterThan=" + id);

        defaultNhomDoiTuongBhytShouldBeFound("id.lessThanOrEqual=" + id);
        defaultNhomDoiTuongBhytShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllNhomDoiTuongBhytsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        nhomDoiTuongBhytRepository.saveAndFlush(nhomDoiTuongBhyt);

        // Get all the nhomDoiTuongBhytList where ten equals to DEFAULT_TEN
        defaultNhomDoiTuongBhytShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the nhomDoiTuongBhytList where ten equals to UPDATED_TEN
        defaultNhomDoiTuongBhytShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhomDoiTuongBhytsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nhomDoiTuongBhytRepository.saveAndFlush(nhomDoiTuongBhyt);

        // Get all the nhomDoiTuongBhytList where ten not equals to DEFAULT_TEN
        defaultNhomDoiTuongBhytShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the nhomDoiTuongBhytList where ten not equals to UPDATED_TEN
        defaultNhomDoiTuongBhytShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhomDoiTuongBhytsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        nhomDoiTuongBhytRepository.saveAndFlush(nhomDoiTuongBhyt);

        // Get all the nhomDoiTuongBhytList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultNhomDoiTuongBhytShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the nhomDoiTuongBhytList where ten equals to UPDATED_TEN
        defaultNhomDoiTuongBhytShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhomDoiTuongBhytsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        nhomDoiTuongBhytRepository.saveAndFlush(nhomDoiTuongBhyt);

        // Get all the nhomDoiTuongBhytList where ten is not null
        defaultNhomDoiTuongBhytShouldBeFound("ten.specified=true");

        // Get all the nhomDoiTuongBhytList where ten is null
        defaultNhomDoiTuongBhytShouldNotBeFound("ten.specified=false");
    }
                @Test
    @Transactional
    public void getAllNhomDoiTuongBhytsByTenContainsSomething() throws Exception {
        // Initialize the database
        nhomDoiTuongBhytRepository.saveAndFlush(nhomDoiTuongBhyt);

        // Get all the nhomDoiTuongBhytList where ten contains DEFAULT_TEN
        defaultNhomDoiTuongBhytShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the nhomDoiTuongBhytList where ten contains UPDATED_TEN
        defaultNhomDoiTuongBhytShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    public void getAllNhomDoiTuongBhytsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        nhomDoiTuongBhytRepository.saveAndFlush(nhomDoiTuongBhyt);

        // Get all the nhomDoiTuongBhytList where ten does not contain DEFAULT_TEN
        defaultNhomDoiTuongBhytShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the nhomDoiTuongBhytList where ten does not contain UPDATED_TEN
        defaultNhomDoiTuongBhytShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultNhomDoiTuongBhytShouldBeFound(String filter) throws Exception {
        restNhomDoiTuongBhytMockMvc.perform(get("/api/nhom-doi-tuong-bhyts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nhomDoiTuongBhyt.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));

        // Check, that the count call also returns 1
        restNhomDoiTuongBhytMockMvc.perform(get("/api/nhom-doi-tuong-bhyts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultNhomDoiTuongBhytShouldNotBeFound(String filter) throws Exception {
        restNhomDoiTuongBhytMockMvc.perform(get("/api/nhom-doi-tuong-bhyts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restNhomDoiTuongBhytMockMvc.perform(get("/api/nhom-doi-tuong-bhyts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingNhomDoiTuongBhyt() throws Exception {
        // Get the nhomDoiTuongBhyt
        restNhomDoiTuongBhytMockMvc.perform(get("/api/nhom-doi-tuong-bhyts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNhomDoiTuongBhyt() throws Exception {
        // Initialize the database
        nhomDoiTuongBhytRepository.saveAndFlush(nhomDoiTuongBhyt);

        int databaseSizeBeforeUpdate = nhomDoiTuongBhytRepository.findAll().size();

        // Update the nhomDoiTuongBhyt
        NhomDoiTuongBhyt updatedNhomDoiTuongBhyt = nhomDoiTuongBhytRepository.findById(nhomDoiTuongBhyt.getId()).get();
        // Disconnect from session so that the updates on updatedNhomDoiTuongBhyt are not directly saved in db
        em.detach(updatedNhomDoiTuongBhyt);
        updatedNhomDoiTuongBhyt
            .ten(UPDATED_TEN);
        NhomDoiTuongBhytDTO nhomDoiTuongBhytDTO = nhomDoiTuongBhytMapper.toDto(updatedNhomDoiTuongBhyt);

        restNhomDoiTuongBhytMockMvc.perform(put("/api/nhom-doi-tuong-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomDoiTuongBhytDTO)))
            .andExpect(status().isOk());

        // Validate the NhomDoiTuongBhyt in the database
        List<NhomDoiTuongBhyt> nhomDoiTuongBhytList = nhomDoiTuongBhytRepository.findAll();
        assertThat(nhomDoiTuongBhytList).hasSize(databaseSizeBeforeUpdate);
        NhomDoiTuongBhyt testNhomDoiTuongBhyt = nhomDoiTuongBhytList.get(nhomDoiTuongBhytList.size() - 1);
        assertThat(testNhomDoiTuongBhyt.getTen()).isEqualTo(UPDATED_TEN);
    }

    @Test
    @Transactional
    public void updateNonExistingNhomDoiTuongBhyt() throws Exception {
        int databaseSizeBeforeUpdate = nhomDoiTuongBhytRepository.findAll().size();

        // Create the NhomDoiTuongBhyt
        NhomDoiTuongBhytDTO nhomDoiTuongBhytDTO = nhomDoiTuongBhytMapper.toDto(nhomDoiTuongBhyt);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNhomDoiTuongBhytMockMvc.perform(put("/api/nhom-doi-tuong-bhyts").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(nhomDoiTuongBhytDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NhomDoiTuongBhyt in the database
        List<NhomDoiTuongBhyt> nhomDoiTuongBhytList = nhomDoiTuongBhytRepository.findAll();
        assertThat(nhomDoiTuongBhytList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNhomDoiTuongBhyt() throws Exception {
        // Initialize the database
        nhomDoiTuongBhytRepository.saveAndFlush(nhomDoiTuongBhyt);

        int databaseSizeBeforeDelete = nhomDoiTuongBhytRepository.findAll().size();

        // Delete the nhomDoiTuongBhyt
        restNhomDoiTuongBhytMockMvc.perform(delete("/api/nhom-doi-tuong-bhyts/{id}", nhomDoiTuongBhyt.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<NhomDoiTuongBhyt> nhomDoiTuongBhytList = nhomDoiTuongBhytRepository.findAll();
        assertThat(nhomDoiTuongBhytList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
