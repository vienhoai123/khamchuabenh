package vn.vnpt.web.rest;

import vn.vnpt.KhamchuabenhApp;
import vn.vnpt.config.TestSecurityConfiguration;
import vn.vnpt.domain.DichVuKham;
import vn.vnpt.domain.DotThayDoiMaDichVu;
import vn.vnpt.repository.DichVuKhamRepository;
import vn.vnpt.service.DichVuKhamService;
import vn.vnpt.service.dto.DichVuKhamDTO;
import vn.vnpt.service.mapper.DichVuKhamMapper;
import vn.vnpt.service.dto.DichVuKhamCriteria;
import vn.vnpt.service.DichVuKhamQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DichVuKhamResource} REST controller.
 */
@SpringBootTest(classes = { KhamchuabenhApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class DichVuKhamResourceIT {

    public static final Integer DEFAULT_DELETED = 1;
    public static final Integer UPDATED_DELETED = 2;
    public static final Integer SMALLER_DELETED = 1 - 1;

    public static final Integer DEFAULT_ENABLED = 1;
    public static final Integer UPDATED_ENABLED = 2;
    public static final Integer SMALLER_ENABLED = 1 - 1;

    public static final Integer DEFAULT_GIOI_HAN_CHI_DINH = 1;
    public static final Integer UPDATED_GIOI_HAN_CHI_DINH = 2;
    public static final Integer SMALLER_GIOI_HAN_CHI_DINH = 1 - 1;

    public static final Integer DEFAULT_PHAN_THEO_GIO_TINH = 1;
    public static final Integer UPDATED_PHAN_THEO_GIO_TINH = 2;
    public static final Integer SMALLER_PHAN_THEO_GIO_TINH = 1 - 1;

    public static final String DEFAULT_TEN_HIEN_THI = "AAAAAAAAAA";
    public static final String UPDATED_TEN_HIEN_THI = "BBBBBBBBBB";

    public static final String DEFAULT_MA_DUNG_CHUNG = "AAAAAAAAAA";
    public static final String UPDATED_MA_DUNG_CHUNG = "BBBBBBBBBB";

    public static final String DEFAULT_MA_NOI_BO = "AAAAAAAAAA";
    public static final String UPDATED_MA_NOI_BO = "BBBBBBBBBB";

    public static final Integer DEFAULT_PHAM_VI_CHI_DINH = 1;
    public static final Integer UPDATED_PHAM_VI_CHI_DINH = 2;
    public static final Integer SMALLER_PHAM_VI_CHI_DINH = 1 - 1;

    public static final String DEFAULT_DON_VI_TINH = "AAAAAAAAAA";
    public static final String UPDATED_DON_VI_TINH = "BBBBBBBBBB";

    public static final BigDecimal DEFAULT_DON_GIA_BENH_VIEN = new BigDecimal(1);
    public static final BigDecimal UPDATED_DON_GIA_BENH_VIEN = new BigDecimal(2);
    public static final BigDecimal SMALLER_DON_GIA_BENH_VIEN = new BigDecimal(1 - 1);

    @Autowired
    private DichVuKhamRepository dichVuKhamRepository;

    @Autowired
    private DichVuKhamMapper dichVuKhamMapper;

    @Autowired
    private DichVuKhamService dichVuKhamService;

    @Autowired
    private DichVuKhamQueryService dichVuKhamQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDichVuKhamMockMvc;

    private DichVuKham dichVuKham;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DichVuKham createEntity(EntityManager em) {
        DichVuKham dichVuKham = new DichVuKham()
            .deleted(DEFAULT_DELETED)
            .enabled(DEFAULT_ENABLED)
            .gioiHanChiDinh(DEFAULT_GIOI_HAN_CHI_DINH)
            .phanTheoGioTinh(DEFAULT_PHAN_THEO_GIO_TINH)
            .tenHienThi(DEFAULT_TEN_HIEN_THI)
            .maDungChung(DEFAULT_MA_DUNG_CHUNG)
            .maNoiBo(DEFAULT_MA_NOI_BO)
            .phamViChiDinh(DEFAULT_PHAM_VI_CHI_DINH)
            .donViTinh(DEFAULT_DON_VI_TINH)
            .donGiaBenhVien(DEFAULT_DON_GIA_BENH_VIEN);
        // Add required entity
        DotThayDoiMaDichVu newDotThayDoiMaDichVu = DotThayDoiMaDichVuResourceIT.createEntity(em);
        DotThayDoiMaDichVu dotThayDoiMaDichVu = TestUtil.findAll(em, DotThayDoiMaDichVu.class).stream()
            .filter(x -> x.getDotTuongApDung().equals(newDotThayDoiMaDichVu.getDotTuongApDung()))
            .findAny().orElse(null);
        if (dotThayDoiMaDichVu == null) {
            dotThayDoiMaDichVu = newDotThayDoiMaDichVu;
            em.persist(dotThayDoiMaDichVu);
            em.flush();
        }
        dichVuKham.setDotThayDoiMaDichVu(dotThayDoiMaDichVu);
        return dichVuKham;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DichVuKham createUpdatedEntity(EntityManager em) {
        DichVuKham dichVuKham = new DichVuKham()
            .deleted(UPDATED_DELETED)
            .enabled(UPDATED_ENABLED)
            .gioiHanChiDinh(UPDATED_GIOI_HAN_CHI_DINH)
            .phanTheoGioTinh(UPDATED_PHAN_THEO_GIO_TINH)
            .tenHienThi(UPDATED_TEN_HIEN_THI)
            .maDungChung(UPDATED_MA_DUNG_CHUNG)
            .maNoiBo(UPDATED_MA_NOI_BO)
            .phamViChiDinh(UPDATED_PHAM_VI_CHI_DINH)
            .donViTinh(UPDATED_DON_VI_TINH)
            .donGiaBenhVien(UPDATED_DON_GIA_BENH_VIEN);
        // Add required entity
        DotThayDoiMaDichVu newDotThayDoiMaDichVu = DotThayDoiMaDichVuResourceIT.createUpdatedEntity(em);
        DotThayDoiMaDichVu dotThayDoiMaDichVu = TestUtil.findAll(em, DotThayDoiMaDichVu.class).stream()
            .filter(x -> x.getDotTuongApDung().equals(newDotThayDoiMaDichVu.getDotTuongApDung()))
            .findAny().orElse(null);
        if (dotThayDoiMaDichVu == null) {
            dotThayDoiMaDichVu = newDotThayDoiMaDichVu;
            em.persist(dotThayDoiMaDichVu);
            em.flush();
        }
        dichVuKham.setDotThayDoiMaDichVu(dotThayDoiMaDichVu);
        return dichVuKham;
    }

    @BeforeEach
    public void initTest() {
        dichVuKham = createEntity(em);
    }

    @Test
    @Transactional
    public void createDichVuKham() throws Exception {
        int databaseSizeBeforeCreate = dichVuKhamRepository.findAll().size();

        // Create the DichVuKham
        DichVuKhamDTO dichVuKhamDTO = dichVuKhamMapper.toDto(dichVuKham);
        restDichVuKhamMockMvc.perform(post("/api/dich-vu-khams").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dichVuKhamDTO)))
            .andExpect(status().isCreated());

        // Validate the DichVuKham in the database
        List<DichVuKham> dichVuKhamList = dichVuKhamRepository.findAll();
        assertThat(dichVuKhamList).hasSize(databaseSizeBeforeCreate + 1);
        DichVuKham testDichVuKham = dichVuKhamList.get(dichVuKhamList.size() - 1);
        assertThat(testDichVuKham.getDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testDichVuKham.getEnabled()).isEqualTo(DEFAULT_ENABLED);
        assertThat(testDichVuKham.getGioiHanChiDinh()).isEqualTo(DEFAULT_GIOI_HAN_CHI_DINH);
        assertThat(testDichVuKham.getPhanTheoGioTinh()).isEqualTo(DEFAULT_PHAN_THEO_GIO_TINH);
        assertThat(testDichVuKham.getTenHienThi()).isEqualTo(DEFAULT_TEN_HIEN_THI);
        assertThat(testDichVuKham.getMaDungChung()).isEqualTo(DEFAULT_MA_DUNG_CHUNG);
        assertThat(testDichVuKham.getMaNoiBo()).isEqualTo(DEFAULT_MA_NOI_BO);
        assertThat(testDichVuKham.getPhamViChiDinh()).isEqualTo(DEFAULT_PHAM_VI_CHI_DINH);
        assertThat(testDichVuKham.getDonViTinh()).isEqualTo(DEFAULT_DON_VI_TINH);
        assertThat(testDichVuKham.getDonGiaBenhVien()).isEqualTo(DEFAULT_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void createDichVuKhamWithExistingId() throws Exception {
        dichVuKhamRepository.save(dichVuKham);
        int databaseSizeBeforeCreate = dichVuKhamRepository.findAll().size();

        // Create the DichVuKham with an existing ID
        dichVuKham.setId(dichVuKham.getId());
        DichVuKhamDTO dichVuKhamDTO = dichVuKhamMapper.toDto(dichVuKham);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDichVuKhamMockMvc.perform(post("/api/dich-vu-khams").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dichVuKhamDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DichVuKham in the database
        List<DichVuKham> dichVuKhamList = dichVuKhamRepository.findAll();
        assertThat(dichVuKhamList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDonGiaBenhVienIsRequired() throws Exception {
        int databaseSizeBeforeTest = dichVuKhamRepository.findAll().size();
        // set the field null
        dichVuKham.setDonGiaBenhVien(null);

        // Create the DichVuKham, which fails.
        DichVuKhamDTO dichVuKhamDTO = dichVuKhamMapper.toDto(dichVuKham);

        restDichVuKhamMockMvc.perform(post("/api/dich-vu-khams").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dichVuKhamDTO)))
            .andExpect(status().isBadRequest());

        List<DichVuKham> dichVuKhamList = dichVuKhamRepository.findAll();
        assertThat(dichVuKhamList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDichVuKhams() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList
        restDichVuKhamMockMvc.perform(get("/api/dich-vu-khams"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dichVuKham.getId().intValue())))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED)))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED)))
            .andExpect(jsonPath("$.[*].gioiHanChiDinh").value(hasItem(DEFAULT_GIOI_HAN_CHI_DINH)))
            .andExpect(jsonPath("$.[*].phanTheoGioTinh").value(hasItem(DEFAULT_PHAN_THEO_GIO_TINH)))
            .andExpect(jsonPath("$.[*].tenHienThi").value(hasItem(DEFAULT_TEN_HIEN_THI)))
            .andExpect(jsonPath("$.[*].maDungChung").value(hasItem(DEFAULT_MA_DUNG_CHUNG)))
            .andExpect(jsonPath("$.[*].maNoiBo").value(hasItem(DEFAULT_MA_NOI_BO)))
            .andExpect(jsonPath("$.[*].phamViChiDinh").value(hasItem(DEFAULT_PHAM_VI_CHI_DINH)))
            .andExpect(jsonPath("$.[*].donViTinh").value(hasItem(DEFAULT_DON_VI_TINH)))
            .andExpect(jsonPath("$.[*].donGiaBenhVien").value(hasItem(DEFAULT_DON_GIA_BENH_VIEN.intValue())));
    }

    @Test
    @Transactional
    public void getDichVuKham() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get the dichVuKham
        restDichVuKhamMockMvc.perform(get("/api/dich-vu-khams/{id}", dichVuKham.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(dichVuKham.getId().intValue()))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED))
            .andExpect(jsonPath("$.enabled").value(DEFAULT_ENABLED))
            .andExpect(jsonPath("$.gioiHanChiDinh").value(DEFAULT_GIOI_HAN_CHI_DINH))
            .andExpect(jsonPath("$.phanTheoGioTinh").value(DEFAULT_PHAN_THEO_GIO_TINH))
            .andExpect(jsonPath("$.tenHienThi").value(DEFAULT_TEN_HIEN_THI))
            .andExpect(jsonPath("$.maDungChung").value(DEFAULT_MA_DUNG_CHUNG))
            .andExpect(jsonPath("$.maNoiBo").value(DEFAULT_MA_NOI_BO))
            .andExpect(jsonPath("$.phamViChiDinh").value(DEFAULT_PHAM_VI_CHI_DINH))
            .andExpect(jsonPath("$.donViTinh").value(DEFAULT_DON_VI_TINH))
            .andExpect(jsonPath("$.donGiaBenhVien").value(DEFAULT_DON_GIA_BENH_VIEN.intValue()));
    }

    @Test
    @Transactional
    public void getDichVuKhamsByIdFiltering() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        Long id = dichVuKham.getId();

        defaultDichVuKhamShouldBeFound("id.equals=" + id);
        defaultDichVuKhamShouldNotBeFound("id.notEquals=" + id);

        defaultDichVuKhamShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultDichVuKhamShouldNotBeFound("id.greaterThan=" + id);

        defaultDichVuKhamShouldBeFound("id.lessThanOrEqual=" + id);
        defaultDichVuKhamShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where deleted equals to DEFAULT_DELETED
        defaultDichVuKhamShouldBeFound("deleted.equals=" + DEFAULT_DELETED);

        // Get all the dichVuKhamList where deleted equals to UPDATED_DELETED
        defaultDichVuKhamShouldNotBeFound("deleted.equals=" + UPDATED_DELETED);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where deleted not equals to DEFAULT_DELETED
        defaultDichVuKhamShouldNotBeFound("deleted.notEquals=" + DEFAULT_DELETED);

        // Get all the dichVuKhamList where deleted not equals to UPDATED_DELETED
        defaultDichVuKhamShouldBeFound("deleted.notEquals=" + UPDATED_DELETED);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where deleted in DEFAULT_DELETED or UPDATED_DELETED
        defaultDichVuKhamShouldBeFound("deleted.in=" + DEFAULT_DELETED + "," + UPDATED_DELETED);

        // Get all the dichVuKhamList where deleted equals to UPDATED_DELETED
        defaultDichVuKhamShouldNotBeFound("deleted.in=" + UPDATED_DELETED);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where deleted is not null
        defaultDichVuKhamShouldBeFound("deleted.specified=true");

        // Get all the dichVuKhamList where deleted is null
        defaultDichVuKhamShouldNotBeFound("deleted.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDeletedIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where deleted is greater than or equal to DEFAULT_DELETED
        defaultDichVuKhamShouldBeFound("deleted.greaterThanOrEqual=" + DEFAULT_DELETED);

        // Get all the dichVuKhamList where deleted is greater than or equal to UPDATED_DELETED
        defaultDichVuKhamShouldNotBeFound("deleted.greaterThanOrEqual=" + UPDATED_DELETED);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDeletedIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where deleted is less than or equal to DEFAULT_DELETED
        defaultDichVuKhamShouldBeFound("deleted.lessThanOrEqual=" + DEFAULT_DELETED);

        // Get all the dichVuKhamList where deleted is less than or equal to SMALLER_DELETED
        defaultDichVuKhamShouldNotBeFound("deleted.lessThanOrEqual=" + SMALLER_DELETED);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDeletedIsLessThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where deleted is less than DEFAULT_DELETED
        defaultDichVuKhamShouldNotBeFound("deleted.lessThan=" + DEFAULT_DELETED);

        // Get all the dichVuKhamList where deleted is less than UPDATED_DELETED
        defaultDichVuKhamShouldBeFound("deleted.lessThan=" + UPDATED_DELETED);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDeletedIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where deleted is greater than DEFAULT_DELETED
        defaultDichVuKhamShouldNotBeFound("deleted.greaterThan=" + DEFAULT_DELETED);

        // Get all the dichVuKhamList where deleted is greater than SMALLER_DELETED
        defaultDichVuKhamShouldBeFound("deleted.greaterThan=" + SMALLER_DELETED);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByEnabledIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where enabled equals to DEFAULT_ENABLED
        defaultDichVuKhamShouldBeFound("enabled.equals=" + DEFAULT_ENABLED);

        // Get all the dichVuKhamList where enabled equals to UPDATED_ENABLED
        defaultDichVuKhamShouldNotBeFound("enabled.equals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByEnabledIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where enabled not equals to DEFAULT_ENABLED
        defaultDichVuKhamShouldNotBeFound("enabled.notEquals=" + DEFAULT_ENABLED);

        // Get all the dichVuKhamList where enabled not equals to UPDATED_ENABLED
        defaultDichVuKhamShouldBeFound("enabled.notEquals=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByEnabledIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where enabled in DEFAULT_ENABLED or UPDATED_ENABLED
        defaultDichVuKhamShouldBeFound("enabled.in=" + DEFAULT_ENABLED + "," + UPDATED_ENABLED);

        // Get all the dichVuKhamList where enabled equals to UPDATED_ENABLED
        defaultDichVuKhamShouldNotBeFound("enabled.in=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByEnabledIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where enabled is not null
        defaultDichVuKhamShouldBeFound("enabled.specified=true");

        // Get all the dichVuKhamList where enabled is null
        defaultDichVuKhamShouldNotBeFound("enabled.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByEnabledIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where enabled is greater than or equal to DEFAULT_ENABLED
        defaultDichVuKhamShouldBeFound("enabled.greaterThanOrEqual=" + DEFAULT_ENABLED);

        // Get all the dichVuKhamList where enabled is greater than or equal to UPDATED_ENABLED
        defaultDichVuKhamShouldNotBeFound("enabled.greaterThanOrEqual=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByEnabledIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where enabled is less than or equal to DEFAULT_ENABLED
        defaultDichVuKhamShouldBeFound("enabled.lessThanOrEqual=" + DEFAULT_ENABLED);

        // Get all the dichVuKhamList where enabled is less than or equal to SMALLER_ENABLED
        defaultDichVuKhamShouldNotBeFound("enabled.lessThanOrEqual=" + SMALLER_ENABLED);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByEnabledIsLessThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where enabled is less than DEFAULT_ENABLED
        defaultDichVuKhamShouldNotBeFound("enabled.lessThan=" + DEFAULT_ENABLED);

        // Get all the dichVuKhamList where enabled is less than UPDATED_ENABLED
        defaultDichVuKhamShouldBeFound("enabled.lessThan=" + UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByEnabledIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where enabled is greater than DEFAULT_ENABLED
        defaultDichVuKhamShouldNotBeFound("enabled.greaterThan=" + DEFAULT_ENABLED);

        // Get all the dichVuKhamList where enabled is greater than SMALLER_ENABLED
        defaultDichVuKhamShouldBeFound("enabled.greaterThan=" + SMALLER_ENABLED);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByGioiHanChiDinhIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where gioiHanChiDinh equals to DEFAULT_GIOI_HAN_CHI_DINH
        defaultDichVuKhamShouldBeFound("gioiHanChiDinh.equals=" + DEFAULT_GIOI_HAN_CHI_DINH);

        // Get all the dichVuKhamList where gioiHanChiDinh equals to UPDATED_GIOI_HAN_CHI_DINH
        defaultDichVuKhamShouldNotBeFound("gioiHanChiDinh.equals=" + UPDATED_GIOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByGioiHanChiDinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where gioiHanChiDinh not equals to DEFAULT_GIOI_HAN_CHI_DINH
        defaultDichVuKhamShouldNotBeFound("gioiHanChiDinh.notEquals=" + DEFAULT_GIOI_HAN_CHI_DINH);

        // Get all the dichVuKhamList where gioiHanChiDinh not equals to UPDATED_GIOI_HAN_CHI_DINH
        defaultDichVuKhamShouldBeFound("gioiHanChiDinh.notEquals=" + UPDATED_GIOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByGioiHanChiDinhIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where gioiHanChiDinh in DEFAULT_GIOI_HAN_CHI_DINH or UPDATED_GIOI_HAN_CHI_DINH
        defaultDichVuKhamShouldBeFound("gioiHanChiDinh.in=" + DEFAULT_GIOI_HAN_CHI_DINH + "," + UPDATED_GIOI_HAN_CHI_DINH);

        // Get all the dichVuKhamList where gioiHanChiDinh equals to UPDATED_GIOI_HAN_CHI_DINH
        defaultDichVuKhamShouldNotBeFound("gioiHanChiDinh.in=" + UPDATED_GIOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByGioiHanChiDinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where gioiHanChiDinh is not null
        defaultDichVuKhamShouldBeFound("gioiHanChiDinh.specified=true");

        // Get all the dichVuKhamList where gioiHanChiDinh is null
        defaultDichVuKhamShouldNotBeFound("gioiHanChiDinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByGioiHanChiDinhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where gioiHanChiDinh is greater than or equal to DEFAULT_GIOI_HAN_CHI_DINH
        defaultDichVuKhamShouldBeFound("gioiHanChiDinh.greaterThanOrEqual=" + DEFAULT_GIOI_HAN_CHI_DINH);

        // Get all the dichVuKhamList where gioiHanChiDinh is greater than or equal to UPDATED_GIOI_HAN_CHI_DINH
        defaultDichVuKhamShouldNotBeFound("gioiHanChiDinh.greaterThanOrEqual=" + UPDATED_GIOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByGioiHanChiDinhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where gioiHanChiDinh is less than or equal to DEFAULT_GIOI_HAN_CHI_DINH
        defaultDichVuKhamShouldBeFound("gioiHanChiDinh.lessThanOrEqual=" + DEFAULT_GIOI_HAN_CHI_DINH);

        // Get all the dichVuKhamList where gioiHanChiDinh is less than or equal to SMALLER_GIOI_HAN_CHI_DINH
        defaultDichVuKhamShouldNotBeFound("gioiHanChiDinh.lessThanOrEqual=" + SMALLER_GIOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByGioiHanChiDinhIsLessThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where gioiHanChiDinh is less than DEFAULT_GIOI_HAN_CHI_DINH
        defaultDichVuKhamShouldNotBeFound("gioiHanChiDinh.lessThan=" + DEFAULT_GIOI_HAN_CHI_DINH);

        // Get all the dichVuKhamList where gioiHanChiDinh is less than UPDATED_GIOI_HAN_CHI_DINH
        defaultDichVuKhamShouldBeFound("gioiHanChiDinh.lessThan=" + UPDATED_GIOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByGioiHanChiDinhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where gioiHanChiDinh is greater than DEFAULT_GIOI_HAN_CHI_DINH
        defaultDichVuKhamShouldNotBeFound("gioiHanChiDinh.greaterThan=" + DEFAULT_GIOI_HAN_CHI_DINH);

        // Get all the dichVuKhamList where gioiHanChiDinh is greater than SMALLER_GIOI_HAN_CHI_DINH
        defaultDichVuKhamShouldBeFound("gioiHanChiDinh.greaterThan=" + SMALLER_GIOI_HAN_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByPhanTheoGioTinhIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where phanTheoGioTinh equals to DEFAULT_PHAN_THEO_GIO_TINH
        defaultDichVuKhamShouldBeFound("phanTheoGioTinh.equals=" + DEFAULT_PHAN_THEO_GIO_TINH);

        // Get all the dichVuKhamList where phanTheoGioTinh equals to UPDATED_PHAN_THEO_GIO_TINH
        defaultDichVuKhamShouldNotBeFound("phanTheoGioTinh.equals=" + UPDATED_PHAN_THEO_GIO_TINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByPhanTheoGioTinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where phanTheoGioTinh not equals to DEFAULT_PHAN_THEO_GIO_TINH
        defaultDichVuKhamShouldNotBeFound("phanTheoGioTinh.notEquals=" + DEFAULT_PHAN_THEO_GIO_TINH);

        // Get all the dichVuKhamList where phanTheoGioTinh not equals to UPDATED_PHAN_THEO_GIO_TINH
        defaultDichVuKhamShouldBeFound("phanTheoGioTinh.notEquals=" + UPDATED_PHAN_THEO_GIO_TINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByPhanTheoGioTinhIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where phanTheoGioTinh in DEFAULT_PHAN_THEO_GIO_TINH or UPDATED_PHAN_THEO_GIO_TINH
        defaultDichVuKhamShouldBeFound("phanTheoGioTinh.in=" + DEFAULT_PHAN_THEO_GIO_TINH + "," + UPDATED_PHAN_THEO_GIO_TINH);

        // Get all the dichVuKhamList where phanTheoGioTinh equals to UPDATED_PHAN_THEO_GIO_TINH
        defaultDichVuKhamShouldNotBeFound("phanTheoGioTinh.in=" + UPDATED_PHAN_THEO_GIO_TINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByPhanTheoGioTinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where phanTheoGioTinh is not null
        defaultDichVuKhamShouldBeFound("phanTheoGioTinh.specified=true");

        // Get all the dichVuKhamList where phanTheoGioTinh is null
        defaultDichVuKhamShouldNotBeFound("phanTheoGioTinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByPhanTheoGioTinhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where phanTheoGioTinh is greater than or equal to DEFAULT_PHAN_THEO_GIO_TINH
        defaultDichVuKhamShouldBeFound("phanTheoGioTinh.greaterThanOrEqual=" + DEFAULT_PHAN_THEO_GIO_TINH);

        // Get all the dichVuKhamList where phanTheoGioTinh is greater than or equal to UPDATED_PHAN_THEO_GIO_TINH
        defaultDichVuKhamShouldNotBeFound("phanTheoGioTinh.greaterThanOrEqual=" + UPDATED_PHAN_THEO_GIO_TINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByPhanTheoGioTinhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where phanTheoGioTinh is less than or equal to DEFAULT_PHAN_THEO_GIO_TINH
        defaultDichVuKhamShouldBeFound("phanTheoGioTinh.lessThanOrEqual=" + DEFAULT_PHAN_THEO_GIO_TINH);

        // Get all the dichVuKhamList where phanTheoGioTinh is less than or equal to SMALLER_PHAN_THEO_GIO_TINH
        defaultDichVuKhamShouldNotBeFound("phanTheoGioTinh.lessThanOrEqual=" + SMALLER_PHAN_THEO_GIO_TINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByPhanTheoGioTinhIsLessThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where phanTheoGioTinh is less than DEFAULT_PHAN_THEO_GIO_TINH
        defaultDichVuKhamShouldNotBeFound("phanTheoGioTinh.lessThan=" + DEFAULT_PHAN_THEO_GIO_TINH);

        // Get all the dichVuKhamList where phanTheoGioTinh is less than UPDATED_PHAN_THEO_GIO_TINH
        defaultDichVuKhamShouldBeFound("phanTheoGioTinh.lessThan=" + UPDATED_PHAN_THEO_GIO_TINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByPhanTheoGioTinhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where phanTheoGioTinh is greater than DEFAULT_PHAN_THEO_GIO_TINH
        defaultDichVuKhamShouldNotBeFound("phanTheoGioTinh.greaterThan=" + DEFAULT_PHAN_THEO_GIO_TINH);

        // Get all the dichVuKhamList where phanTheoGioTinh is greater than SMALLER_PHAN_THEO_GIO_TINH
        defaultDichVuKhamShouldBeFound("phanTheoGioTinh.greaterThan=" + SMALLER_PHAN_THEO_GIO_TINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByTenHienThiIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where tenHienThi equals to DEFAULT_TEN_HIEN_THI
        defaultDichVuKhamShouldBeFound("tenHienThi.equals=" + DEFAULT_TEN_HIEN_THI);

        // Get all the dichVuKhamList where tenHienThi equals to UPDATED_TEN_HIEN_THI
        defaultDichVuKhamShouldNotBeFound("tenHienThi.equals=" + UPDATED_TEN_HIEN_THI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByTenHienThiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where tenHienThi not equals to DEFAULT_TEN_HIEN_THI
        defaultDichVuKhamShouldNotBeFound("tenHienThi.notEquals=" + DEFAULT_TEN_HIEN_THI);

        // Get all the dichVuKhamList where tenHienThi not equals to UPDATED_TEN_HIEN_THI
        defaultDichVuKhamShouldBeFound("tenHienThi.notEquals=" + UPDATED_TEN_HIEN_THI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByTenHienThiIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where tenHienThi in DEFAULT_TEN_HIEN_THI or UPDATED_TEN_HIEN_THI
        defaultDichVuKhamShouldBeFound("tenHienThi.in=" + DEFAULT_TEN_HIEN_THI + "," + UPDATED_TEN_HIEN_THI);

        // Get all the dichVuKhamList where tenHienThi equals to UPDATED_TEN_HIEN_THI
        defaultDichVuKhamShouldNotBeFound("tenHienThi.in=" + UPDATED_TEN_HIEN_THI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByTenHienThiIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where tenHienThi is not null
        defaultDichVuKhamShouldBeFound("tenHienThi.specified=true");

        // Get all the dichVuKhamList where tenHienThi is null
        defaultDichVuKhamShouldNotBeFound("tenHienThi.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByTenHienThiContainsSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where tenHienThi contains DEFAULT_TEN_HIEN_THI
        defaultDichVuKhamShouldBeFound("tenHienThi.contains=" + DEFAULT_TEN_HIEN_THI);

        // Get all the dichVuKhamList where tenHienThi contains UPDATED_TEN_HIEN_THI
        defaultDichVuKhamShouldNotBeFound("tenHienThi.contains=" + UPDATED_TEN_HIEN_THI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByTenHienThiNotContainsSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where tenHienThi does not contain DEFAULT_TEN_HIEN_THI
        defaultDichVuKhamShouldNotBeFound("tenHienThi.doesNotContain=" + DEFAULT_TEN_HIEN_THI);

        // Get all the dichVuKhamList where tenHienThi does not contain UPDATED_TEN_HIEN_THI
        defaultDichVuKhamShouldBeFound("tenHienThi.doesNotContain=" + UPDATED_TEN_HIEN_THI);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByMaDungChungIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where maDungChung equals to DEFAULT_MA_DUNG_CHUNG
        defaultDichVuKhamShouldBeFound("maDungChung.equals=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the dichVuKhamList where maDungChung equals to UPDATED_MA_DUNG_CHUNG
        defaultDichVuKhamShouldNotBeFound("maDungChung.equals=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByMaDungChungIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where maDungChung not equals to DEFAULT_MA_DUNG_CHUNG
        defaultDichVuKhamShouldNotBeFound("maDungChung.notEquals=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the dichVuKhamList where maDungChung not equals to UPDATED_MA_DUNG_CHUNG
        defaultDichVuKhamShouldBeFound("maDungChung.notEquals=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByMaDungChungIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where maDungChung in DEFAULT_MA_DUNG_CHUNG or UPDATED_MA_DUNG_CHUNG
        defaultDichVuKhamShouldBeFound("maDungChung.in=" + DEFAULT_MA_DUNG_CHUNG + "," + UPDATED_MA_DUNG_CHUNG);

        // Get all the dichVuKhamList where maDungChung equals to UPDATED_MA_DUNG_CHUNG
        defaultDichVuKhamShouldNotBeFound("maDungChung.in=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByMaDungChungIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where maDungChung is not null
        defaultDichVuKhamShouldBeFound("maDungChung.specified=true");

        // Get all the dichVuKhamList where maDungChung is null
        defaultDichVuKhamShouldNotBeFound("maDungChung.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByMaDungChungContainsSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where maDungChung contains DEFAULT_MA_DUNG_CHUNG
        defaultDichVuKhamShouldBeFound("maDungChung.contains=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the dichVuKhamList where maDungChung contains UPDATED_MA_DUNG_CHUNG
        defaultDichVuKhamShouldNotBeFound("maDungChung.contains=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByMaDungChungNotContainsSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where maDungChung does not contain DEFAULT_MA_DUNG_CHUNG
        defaultDichVuKhamShouldNotBeFound("maDungChung.doesNotContain=" + DEFAULT_MA_DUNG_CHUNG);

        // Get all the dichVuKhamList where maDungChung does not contain UPDATED_MA_DUNG_CHUNG
        defaultDichVuKhamShouldBeFound("maDungChung.doesNotContain=" + UPDATED_MA_DUNG_CHUNG);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByMaNoiBoIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where maNoiBo equals to DEFAULT_MA_NOI_BO
        defaultDichVuKhamShouldBeFound("maNoiBo.equals=" + DEFAULT_MA_NOI_BO);

        // Get all the dichVuKhamList where maNoiBo equals to UPDATED_MA_NOI_BO
        defaultDichVuKhamShouldNotBeFound("maNoiBo.equals=" + UPDATED_MA_NOI_BO);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByMaNoiBoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where maNoiBo not equals to DEFAULT_MA_NOI_BO
        defaultDichVuKhamShouldNotBeFound("maNoiBo.notEquals=" + DEFAULT_MA_NOI_BO);

        // Get all the dichVuKhamList where maNoiBo not equals to UPDATED_MA_NOI_BO
        defaultDichVuKhamShouldBeFound("maNoiBo.notEquals=" + UPDATED_MA_NOI_BO);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByMaNoiBoIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where maNoiBo in DEFAULT_MA_NOI_BO or UPDATED_MA_NOI_BO
        defaultDichVuKhamShouldBeFound("maNoiBo.in=" + DEFAULT_MA_NOI_BO + "," + UPDATED_MA_NOI_BO);

        // Get all the dichVuKhamList where maNoiBo equals to UPDATED_MA_NOI_BO
        defaultDichVuKhamShouldNotBeFound("maNoiBo.in=" + UPDATED_MA_NOI_BO);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByMaNoiBoIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where maNoiBo is not null
        defaultDichVuKhamShouldBeFound("maNoiBo.specified=true");

        // Get all the dichVuKhamList where maNoiBo is null
        defaultDichVuKhamShouldNotBeFound("maNoiBo.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByMaNoiBoContainsSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where maNoiBo contains DEFAULT_MA_NOI_BO
        defaultDichVuKhamShouldBeFound("maNoiBo.contains=" + DEFAULT_MA_NOI_BO);

        // Get all the dichVuKhamList where maNoiBo contains UPDATED_MA_NOI_BO
        defaultDichVuKhamShouldNotBeFound("maNoiBo.contains=" + UPDATED_MA_NOI_BO);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByMaNoiBoNotContainsSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where maNoiBo does not contain DEFAULT_MA_NOI_BO
        defaultDichVuKhamShouldNotBeFound("maNoiBo.doesNotContain=" + DEFAULT_MA_NOI_BO);

        // Get all the dichVuKhamList where maNoiBo does not contain UPDATED_MA_NOI_BO
        defaultDichVuKhamShouldBeFound("maNoiBo.doesNotContain=" + UPDATED_MA_NOI_BO);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByPhamViChiDinhIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where phamViChiDinh equals to DEFAULT_PHAM_VI_CHI_DINH
        defaultDichVuKhamShouldBeFound("phamViChiDinh.equals=" + DEFAULT_PHAM_VI_CHI_DINH);

        // Get all the dichVuKhamList where phamViChiDinh equals to UPDATED_PHAM_VI_CHI_DINH
        defaultDichVuKhamShouldNotBeFound("phamViChiDinh.equals=" + UPDATED_PHAM_VI_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByPhamViChiDinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where phamViChiDinh not equals to DEFAULT_PHAM_VI_CHI_DINH
        defaultDichVuKhamShouldNotBeFound("phamViChiDinh.notEquals=" + DEFAULT_PHAM_VI_CHI_DINH);

        // Get all the dichVuKhamList where phamViChiDinh not equals to UPDATED_PHAM_VI_CHI_DINH
        defaultDichVuKhamShouldBeFound("phamViChiDinh.notEquals=" + UPDATED_PHAM_VI_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByPhamViChiDinhIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where phamViChiDinh in DEFAULT_PHAM_VI_CHI_DINH or UPDATED_PHAM_VI_CHI_DINH
        defaultDichVuKhamShouldBeFound("phamViChiDinh.in=" + DEFAULT_PHAM_VI_CHI_DINH + "," + UPDATED_PHAM_VI_CHI_DINH);

        // Get all the dichVuKhamList where phamViChiDinh equals to UPDATED_PHAM_VI_CHI_DINH
        defaultDichVuKhamShouldNotBeFound("phamViChiDinh.in=" + UPDATED_PHAM_VI_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByPhamViChiDinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where phamViChiDinh is not null
        defaultDichVuKhamShouldBeFound("phamViChiDinh.specified=true");

        // Get all the dichVuKhamList where phamViChiDinh is null
        defaultDichVuKhamShouldNotBeFound("phamViChiDinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByPhamViChiDinhIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where phamViChiDinh is greater than or equal to DEFAULT_PHAM_VI_CHI_DINH
        defaultDichVuKhamShouldBeFound("phamViChiDinh.greaterThanOrEqual=" + DEFAULT_PHAM_VI_CHI_DINH);

        // Get all the dichVuKhamList where phamViChiDinh is greater than or equal to UPDATED_PHAM_VI_CHI_DINH
        defaultDichVuKhamShouldNotBeFound("phamViChiDinh.greaterThanOrEqual=" + UPDATED_PHAM_VI_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByPhamViChiDinhIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where phamViChiDinh is less than or equal to DEFAULT_PHAM_VI_CHI_DINH
        defaultDichVuKhamShouldBeFound("phamViChiDinh.lessThanOrEqual=" + DEFAULT_PHAM_VI_CHI_DINH);

        // Get all the dichVuKhamList where phamViChiDinh is less than or equal to SMALLER_PHAM_VI_CHI_DINH
        defaultDichVuKhamShouldNotBeFound("phamViChiDinh.lessThanOrEqual=" + SMALLER_PHAM_VI_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByPhamViChiDinhIsLessThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where phamViChiDinh is less than DEFAULT_PHAM_VI_CHI_DINH
        defaultDichVuKhamShouldNotBeFound("phamViChiDinh.lessThan=" + DEFAULT_PHAM_VI_CHI_DINH);

        // Get all the dichVuKhamList where phamViChiDinh is less than UPDATED_PHAM_VI_CHI_DINH
        defaultDichVuKhamShouldBeFound("phamViChiDinh.lessThan=" + UPDATED_PHAM_VI_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByPhamViChiDinhIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where phamViChiDinh is greater than DEFAULT_PHAM_VI_CHI_DINH
        defaultDichVuKhamShouldNotBeFound("phamViChiDinh.greaterThan=" + DEFAULT_PHAM_VI_CHI_DINH);

        // Get all the dichVuKhamList where phamViChiDinh is greater than SMALLER_PHAM_VI_CHI_DINH
        defaultDichVuKhamShouldBeFound("phamViChiDinh.greaterThan=" + SMALLER_PHAM_VI_CHI_DINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDonViTinhIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where donViTinh equals to DEFAULT_DON_VI_TINH
        defaultDichVuKhamShouldBeFound("donViTinh.equals=" + DEFAULT_DON_VI_TINH);

        // Get all the dichVuKhamList where donViTinh equals to UPDATED_DON_VI_TINH
        defaultDichVuKhamShouldNotBeFound("donViTinh.equals=" + UPDATED_DON_VI_TINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDonViTinhIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where donViTinh not equals to DEFAULT_DON_VI_TINH
        defaultDichVuKhamShouldNotBeFound("donViTinh.notEquals=" + DEFAULT_DON_VI_TINH);

        // Get all the dichVuKhamList where donViTinh not equals to UPDATED_DON_VI_TINH
        defaultDichVuKhamShouldBeFound("donViTinh.notEquals=" + UPDATED_DON_VI_TINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDonViTinhIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where donViTinh in DEFAULT_DON_VI_TINH or UPDATED_DON_VI_TINH
        defaultDichVuKhamShouldBeFound("donViTinh.in=" + DEFAULT_DON_VI_TINH + "," + UPDATED_DON_VI_TINH);

        // Get all the dichVuKhamList where donViTinh equals to UPDATED_DON_VI_TINH
        defaultDichVuKhamShouldNotBeFound("donViTinh.in=" + UPDATED_DON_VI_TINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDonViTinhIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where donViTinh is not null
        defaultDichVuKhamShouldBeFound("donViTinh.specified=true");

        // Get all the dichVuKhamList where donViTinh is null
        defaultDichVuKhamShouldNotBeFound("donViTinh.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDonViTinhContainsSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where donViTinh contains DEFAULT_DON_VI_TINH
        defaultDichVuKhamShouldBeFound("donViTinh.contains=" + DEFAULT_DON_VI_TINH);

        // Get all the dichVuKhamList where donViTinh contains UPDATED_DON_VI_TINH
        defaultDichVuKhamShouldNotBeFound("donViTinh.contains=" + UPDATED_DON_VI_TINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDonViTinhNotContainsSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where donViTinh does not contain DEFAULT_DON_VI_TINH
        defaultDichVuKhamShouldNotBeFound("donViTinh.doesNotContain=" + DEFAULT_DON_VI_TINH);

        // Get all the dichVuKhamList where donViTinh does not contain UPDATED_DON_VI_TINH
        defaultDichVuKhamShouldBeFound("donViTinh.doesNotContain=" + UPDATED_DON_VI_TINH);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDonGiaBenhVienIsEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where donGiaBenhVien equals to DEFAULT_DON_GIA_BENH_VIEN
        defaultDichVuKhamShouldBeFound("donGiaBenhVien.equals=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the dichVuKhamList where donGiaBenhVien equals to UPDATED_DON_GIA_BENH_VIEN
        defaultDichVuKhamShouldNotBeFound("donGiaBenhVien.equals=" + UPDATED_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDonGiaBenhVienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where donGiaBenhVien not equals to DEFAULT_DON_GIA_BENH_VIEN
        defaultDichVuKhamShouldNotBeFound("donGiaBenhVien.notEquals=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the dichVuKhamList where donGiaBenhVien not equals to UPDATED_DON_GIA_BENH_VIEN
        defaultDichVuKhamShouldBeFound("donGiaBenhVien.notEquals=" + UPDATED_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDonGiaBenhVienIsInShouldWork() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where donGiaBenhVien in DEFAULT_DON_GIA_BENH_VIEN or UPDATED_DON_GIA_BENH_VIEN
        defaultDichVuKhamShouldBeFound("donGiaBenhVien.in=" + DEFAULT_DON_GIA_BENH_VIEN + "," + UPDATED_DON_GIA_BENH_VIEN);

        // Get all the dichVuKhamList where donGiaBenhVien equals to UPDATED_DON_GIA_BENH_VIEN
        defaultDichVuKhamShouldNotBeFound("donGiaBenhVien.in=" + UPDATED_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDonGiaBenhVienIsNullOrNotNull() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where donGiaBenhVien is not null
        defaultDichVuKhamShouldBeFound("donGiaBenhVien.specified=true");

        // Get all the dichVuKhamList where donGiaBenhVien is null
        defaultDichVuKhamShouldNotBeFound("donGiaBenhVien.specified=false");
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDonGiaBenhVienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where donGiaBenhVien is greater than or equal to DEFAULT_DON_GIA_BENH_VIEN
        defaultDichVuKhamShouldBeFound("donGiaBenhVien.greaterThanOrEqual=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the dichVuKhamList where donGiaBenhVien is greater than or equal to UPDATED_DON_GIA_BENH_VIEN
        defaultDichVuKhamShouldNotBeFound("donGiaBenhVien.greaterThanOrEqual=" + UPDATED_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDonGiaBenhVienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where donGiaBenhVien is less than or equal to DEFAULT_DON_GIA_BENH_VIEN
        defaultDichVuKhamShouldBeFound("donGiaBenhVien.lessThanOrEqual=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the dichVuKhamList where donGiaBenhVien is less than or equal to SMALLER_DON_GIA_BENH_VIEN
        defaultDichVuKhamShouldNotBeFound("donGiaBenhVien.lessThanOrEqual=" + SMALLER_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDonGiaBenhVienIsLessThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where donGiaBenhVien is less than DEFAULT_DON_GIA_BENH_VIEN
        defaultDichVuKhamShouldNotBeFound("donGiaBenhVien.lessThan=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the dichVuKhamList where donGiaBenhVien is less than UPDATED_DON_GIA_BENH_VIEN
        defaultDichVuKhamShouldBeFound("donGiaBenhVien.lessThan=" + UPDATED_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDonGiaBenhVienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        // Get all the dichVuKhamList where donGiaBenhVien is greater than DEFAULT_DON_GIA_BENH_VIEN
        defaultDichVuKhamShouldNotBeFound("donGiaBenhVien.greaterThan=" + DEFAULT_DON_GIA_BENH_VIEN);

        // Get all the dichVuKhamList where donGiaBenhVien is greater than SMALLER_DON_GIA_BENH_VIEN
        defaultDichVuKhamShouldBeFound("donGiaBenhVien.greaterThan=" + SMALLER_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void getAllDichVuKhamsByDotThayDoiMaDichVuIdIsEqualToSomething() throws Exception {
        // Get already existing entity
        DotThayDoiMaDichVu dotThayDoiMaDichVu = dichVuKham.getDotThayDoiMaDichVu();
        dichVuKhamRepository.saveAndFlush(dichVuKham);
        Long dotThayDoiMaDichVuId = dotThayDoiMaDichVu.getId();

        // Get all the dichVuKhamList where dotThayDoiMaDichVuId equals to dotThayDoiMaDichVuId
        defaultDichVuKhamShouldBeFound("dotThayDoiMaDichVuId.equals=" + dotThayDoiMaDichVuId);

        // Get all the dichVuKhamList where dotThayDoiMaDichVuId equals to dotThayDoiMaDichVuId + 1
        defaultDichVuKhamShouldNotBeFound("dotThayDoiMaDichVuId.equals=" + (dotThayDoiMaDichVuId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDichVuKhamShouldBeFound(String filter) throws Exception {
        restDichVuKhamMockMvc.perform(get("/api/dich-vu-khams?" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dichVuKham.getId().intValue())))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED)))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED)))
            .andExpect(jsonPath("$.[*].gioiHanChiDinh").value(hasItem(DEFAULT_GIOI_HAN_CHI_DINH)))
            .andExpect(jsonPath("$.[*].phanTheoGioTinh").value(hasItem(DEFAULT_PHAN_THEO_GIO_TINH)))
            .andExpect(jsonPath("$.[*].tenHienThi").value(hasItem(DEFAULT_TEN_HIEN_THI)))
            .andExpect(jsonPath("$.[*].maDungChung").value(hasItem(DEFAULT_MA_DUNG_CHUNG)))
            .andExpect(jsonPath("$.[*].maNoiBo").value(hasItem(DEFAULT_MA_NOI_BO)))
            .andExpect(jsonPath("$.[*].phamViChiDinh").value(hasItem(DEFAULT_PHAM_VI_CHI_DINH)))
            .andExpect(jsonPath("$.[*].donViTinh").value(hasItem(DEFAULT_DON_VI_TINH)))
            .andExpect(jsonPath("$.[*].donGiaBenhVien").value(hasItem(DEFAULT_DON_GIA_BENH_VIEN.intValue())));

        // Check, that the count call also returns 1
        restDichVuKhamMockMvc.perform(get("/api/dich-vu-khams/count?" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDichVuKhamShouldNotBeFound(String filter) throws Exception {
        restDichVuKhamMockMvc.perform(get("/api/dich-vu-khams?" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDichVuKhamMockMvc.perform(get("/api/dich-vu-khams/count?" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingDichVuKham() throws Exception {
        // Get the dichVuKham
        restDichVuKhamMockMvc.perform(get("/api/dich-vu-khams/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDichVuKham() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        int databaseSizeBeforeUpdate = dichVuKhamRepository.findAll().size();

        // Update the dichVuKham
        DichVuKham updatedDichVuKham = dichVuKhamRepository.findById(dichVuKham.getId()).get();
        // Disconnect from session so that the updates on updatedDichVuKham are not directly saved in db
        em.detach(updatedDichVuKham);
        updatedDichVuKham
            .deleted(UPDATED_DELETED)
            .enabled(UPDATED_ENABLED)
            .gioiHanChiDinh(UPDATED_GIOI_HAN_CHI_DINH)
            .phanTheoGioTinh(UPDATED_PHAN_THEO_GIO_TINH)
            .tenHienThi(UPDATED_TEN_HIEN_THI)
            .maDungChung(UPDATED_MA_DUNG_CHUNG)
            .maNoiBo(UPDATED_MA_NOI_BO)
            .phamViChiDinh(UPDATED_PHAM_VI_CHI_DINH)
            .donViTinh(UPDATED_DON_VI_TINH)
            .donGiaBenhVien(UPDATED_DON_GIA_BENH_VIEN);
        DichVuKhamDTO dichVuKhamDTO = dichVuKhamMapper.toDto(updatedDichVuKham);

        restDichVuKhamMockMvc.perform(put("/api/dich-vu-khams").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dichVuKhamDTO)))
            .andExpect(status().isOk());

        // Validate the DichVuKham in the database
        List<DichVuKham> dichVuKhamList = dichVuKhamRepository.findAll();
        assertThat(dichVuKhamList).hasSize(databaseSizeBeforeUpdate);
        DichVuKham testDichVuKham = dichVuKhamList.get(dichVuKhamList.size() - 1);
        assertThat(testDichVuKham.getDeleted()).isEqualTo(UPDATED_DELETED);
        assertThat(testDichVuKham.getEnabled()).isEqualTo(UPDATED_ENABLED);
        assertThat(testDichVuKham.getGioiHanChiDinh()).isEqualTo(UPDATED_GIOI_HAN_CHI_DINH);
        assertThat(testDichVuKham.getPhanTheoGioTinh()).isEqualTo(UPDATED_PHAN_THEO_GIO_TINH);
        assertThat(testDichVuKham.getTenHienThi()).isEqualTo(UPDATED_TEN_HIEN_THI);
        assertThat(testDichVuKham.getMaDungChung()).isEqualTo(UPDATED_MA_DUNG_CHUNG);
        assertThat(testDichVuKham.getMaNoiBo()).isEqualTo(UPDATED_MA_NOI_BO);
        assertThat(testDichVuKham.getPhamViChiDinh()).isEqualTo(UPDATED_PHAM_VI_CHI_DINH);
        assertThat(testDichVuKham.getDonViTinh()).isEqualTo(UPDATED_DON_VI_TINH);
        assertThat(testDichVuKham.getDonGiaBenhVien()).isEqualTo(UPDATED_DON_GIA_BENH_VIEN);
    }

    @Test
    @Transactional
    public void updateNonExistingDichVuKham() throws Exception {
        int databaseSizeBeforeUpdate = dichVuKhamRepository.findAll().size();

        // Create the DichVuKham
        DichVuKhamDTO dichVuKhamDTO = dichVuKhamMapper.toDto(dichVuKham);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDichVuKhamMockMvc.perform(put("/api/dich-vu-khams").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dichVuKhamDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DichVuKham in the database
        List<DichVuKham> dichVuKhamList = dichVuKhamRepository.findAll();
        assertThat(dichVuKhamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDichVuKham() throws Exception {
        // Initialize the database
        dichVuKhamRepository.saveAndFlush(dichVuKham);

        int databaseSizeBeforeDelete = dichVuKhamRepository.findAll().size();

        // Delete the dichVuKham
        restDichVuKhamMockMvc.perform(delete("/api/dich-vu-khams/{id}", dichVuKham.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DichVuKham> dichVuKhamList = dichVuKhamRepository.findAll();
        assertThat(dichVuKhamList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
